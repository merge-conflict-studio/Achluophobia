﻿using UnityEngine;

namespace Achluophobia
{
    [CreateAssetMenu(fileName = "Zombie", menuName = "Game/ScriptableObject/ZombieClass")]
    public class ZombieClass : ScriptableObject
    {
        [SerializeField, Tooltip("The walking speed of the zombie")] 
        private int speed;

        [SerializeField, Tooltip("The minimum damage the zombie can deal to a player")]
        private int minDamage;

        [SerializeField, Tooltip("The maximum damage the zombie can deal to a player")] 
        private int maxDamage;

        [SerializeField, Tooltip("The range in which the zombie can attack a player")] 
        private int range;

        [SerializeField, Tooltip("The frequency in seconds at which the zombie attacks")] 
        private float attackSpeed;

        [SerializeField, Tooltip("The type of the zombie")]
        private ZombieType zombieType;

        public int Speed
        {
            get { return speed; }
        }

        public int MinDamage
        {
            get { return minDamage; }
        }

        public int MaxDamage
        {
            get { return maxDamage; }
        }

        public int Range
        {
            get { return range; }
        }

        public float AttackSpeed
        {
            get { return attackSpeed; }
        }

        public ZombieType ZombieType
        {
            get { return zombieType; }
        }
    }
}
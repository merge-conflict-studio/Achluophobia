﻿using System;
using System.Collections;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public class CanvasGroupFader : Script
    {
        private bool isFadingOut;

        public event Action OnFadingFinished;

        public void FadeOut(CanvasGroup canvasGroup)
        {
            if (!isFadingOut) StartCoroutine(FadeOutCoroutine(canvasGroup));
        }

        private IEnumerator FadeOutCoroutine(CanvasGroup canvasGroup)
        {
            canvasGroup.interactable = false;
            isFadingOut = true;

            while (canvasGroup.alpha > 0)
            {
                canvasGroup.alpha -= Time.deltaTime / 4f;
                yield return null;
            }

            isFadingOut = false;
            if (OnFadingFinished != null) OnFadingFinished();
        }
    }
}


﻿using UnityEngine;
using System.Collections.Generic;

namespace Achluophobia
{
    /// <summary>
    /// Make the camera follow one or multiple target with damping. This class is inspired from the Unity Tank
    /// Tutorial. Jonathan Saindon
    /// SOURCE : https://unity3d.com/fr/learn/tutorials/projects/tanks-tutorial/camera-control?playlist=20081
    /// </summary>
    [AddComponentMenu("Game/Actuator/FollowingCamera")]
    public class FollowingCamera : GameScript
    {
        [SerializeField, Tooltip("Time for the camera to refocus")]
        private float dampingTime = 0.2f;

        private const float CameraYPosition = 20.96f;
        
        private List<GameObject> targets;
        private Vector3 moveVelocity;
        private int numTargets;

        private void Awake()
        {
            targets = new List<GameObject>();
            SetStartPosition();
        }

        private void LateUpdate()
        {
            if (targets.Count > 0) Move();
        }

        public void AddTarget(GameObject target)
        {
            targets.Add(target);
        }

        public void RemoveTarget(GameObject target)
        {
            targets.Remove(target);
        }

        public void RemoveAllTargets()
        {
            if (targets != null)
            {
                targets.Clear();
            }
        }

        /// <summary>
        /// Find the average position between the targets in the array.
        /// </summary>
        private Vector3 FindAveragePosition()
        {
            Vector3 averagePos = new Vector3(0, 0, 0);
            numTargets = 0;

            if (targets.Count == 0) averagePos = transform.position;

            foreach (GameObject gameObject in targets)
            {
                if (!gameObject.activeSelf)
                    continue;

                averagePos += gameObject.transform.position;
                numTargets++;
            }

            if (numTargets > 0)
                averagePos /= numTargets;

            return new Vector3(averagePos.x, CameraYPosition , averagePos.z);
        }
        
        private void Move()
        {
            Vector3 desiredPosition = FindAveragePosition();

            transform.position = Vector3.SmoothDamp(transform.position, desiredPosition, ref moveVelocity, dampingTime);
        }
        
        private void SetStartPosition()
        {
            transform.position = FindAveragePosition();
        }
    }
}
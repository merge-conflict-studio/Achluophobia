﻿using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Actuator/ParticleSpawner")]
    public class ParticleSpawner : GameScript
    {
        [SerializeField, Tooltip("Flesh impact prefab")] 
        private GameObject fleshImpact;
        
        [SerializeField, Tooltip("Metal impact prefab")] 
        private GameObject metalImpact;
        
        [SerializeField, Tooltip("Wood impact prefab")] 
        private GameObject woodImpact;

        public void Spawn(Vector3 worldposition, Quaternion rotation, ObjectMaterial material)
        {
            GameObject particle = null;
            switch (material)
            {
                case ObjectMaterial.Flesh:
                    particle = Instantiate(fleshImpact, worldposition, rotation);
                    break;
                case ObjectMaterial.Wood:
                    particle = Instantiate(woodImpact, worldposition, rotation);
                    break;
                case ObjectMaterial.Metal:
                    particle = Instantiate(metalImpact, worldposition, rotation);
                    break;
            }
            Destroy(particle, 1);
        }
    }
}
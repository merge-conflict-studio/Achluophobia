﻿using UnityEngine;
using Harmony;

namespace Achluophobia
{
    public delegate void EntityDestroyedEventHandler();

    [AddComponentMenu("Game/Actuator/EntityDestroyer")]
    public class EntityDestroyer : GameScript
    {
        private GameObject rootGameObject;

        public event EntityDestroyedEventHandler OnDestroyed;

        private void InjectEntityDestroyer([RootScope] GameObject rootGameObject)
        {
            this.rootGameObject = rootGameObject;
        }

        private void Awake()
        {
            InjectDependencies("InjectEntityDestroyer");
        }

        [CalledOutsideOfCode]
        public void Destroy()
        {
            rootGameObject.Destroy();

            if (OnDestroyed != null) OnDestroyed();
        }
    }
}
﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public delegate void RaycastHitEventHandler(RaycastHit hit);

    [AddComponentMenu("Game/Actuator/RayCastShooter")]
    public class RaycastShooter : GameScript
    {
        [SerializeField, Tooltip("The layers to collide with.")]
        private LayerMask layerMask;
                
        private RaycastHit hit;
       
        public event RaycastHitEventHandler OnRayCastHit;
                
        public void Shoot(float maxDistance)
        {
            bool hasHit = Physics.Raycast(
                transform.position, 
                transform.TransformDirection(Vector3.forward),
                out hit, 
                maxDistance, 
                layerMask);

            if (!hasHit) return;
            if (OnRayCastHit != null) OnRayCastHit(hit);
        }
    }
}
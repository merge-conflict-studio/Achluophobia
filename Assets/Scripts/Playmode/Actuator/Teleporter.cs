﻿using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Actuator/Teleporter")]
    public class Teleporter
    {
        public static void Teleport(GameObject playerToTeleport, Vector3 positionToTeleport)
        {
            playerToTeleport.transform.position = positionToTeleport;
        }
    }
}
﻿using UnityEngine;
using Harmony;

namespace Achluophobia
{
    [AddComponentMenu("Game/Actuator/EntityRotater")]
    public class EntityRotater : Script
    {
        /// <summary>
        /// This deadzone must be compared using sqrMagnitude to save computing.
        /// </summary>
        [Range(0.03f, 0.5625f), Tooltip("SqrMagniture deazone of the thumbstick")]
        [SerializeField] private float thumbstickDeadzone = 0.2f;

        private Vector3 targetDirection;

        public void LookTowards(Vector2 direction, float rotationSpeed)
        {
            //This verification makes the player continue the rotation towards
            //the direction even if the thumbstick is released.
            if (direction.sqrMagnitude >= thumbstickDeadzone)
            {
                targetDirection = new Vector3(direction.x, 0, direction.y);
            }

            float step = rotationSpeed * Time.deltaTime;
            Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, step, 0);
            transform.rotation = Quaternion.LookRotation(newDirection);
        }
    }
}
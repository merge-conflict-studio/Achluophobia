﻿using UnityEngine;
using Harmony;

namespace Achluophobia
{
    [AddComponentMenu("Game/Actuator/FlameShooter")]
    public class FlameShooter : GameScript
    {
        private int damagePerParticle;
        
        private ParticleSystem flameThrower;

        private void InjectFlameShooter([EntityScope] ParticleSystem flameThrower)
        {
            this.flameThrower = flameThrower;
        }

        private void Awake()
        {
            InjectDependencies("InjectFlameShooter");
        }

        public void SetDamagePerParticle(int damage)
        {
            damagePerParticle = damage;
        }

        public void Off()
        {
            flameThrower.Stop();
        }

        public void On()
        {
            flameThrower.Play();
        }
        
        private void OnParticleCollision(GameObject other)
        {
            HitSensor hitSensor = other.GetComponentInChildren<HitSensor>();
            if (hitSensor != null)
            {
                hitSensor.Hit(transform.root.gameObject, damagePerParticle, false);
            }
        }
    }
}

﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Actuator/LaserSight")]
    public class LaserSight : GameScript, IAddon
    {
        [SerializeField, Tooltip("The color of the laser.")]
        private Color laserColor = Color.red;

        [SerializeField, Tooltip("The layers to collide with.")]
        private LayerMask layerMask;

        private bool isOn;
        private bool isEquipped;
        private float maxDistance;
        private RaycastHit hit;
        private Vector3 laserStart;
        private Vector3 laserEnd;
        private LineRenderer lineRenderer;
        private PlayerController playerController;
        private RaycastShootingWeapon raycastShootingWeapon;

        private void InjectLaserSight([GameObjectScope] LineRenderer lineRenderer,
                                      [RootScope] PlayerController playerController)

        {
            this.lineRenderer = lineRenderer;
            maxDistance = GetComponentInParent<RaycastShootingWeapon>().GetShootingWeaponData().FiringRange;
            this.playerController = playerController;
        }

        private void Awake()
        {
            InjectDependencies("InjectLaserSight");
            raycastShootingWeapon = GetComponentInParent<RaycastShootingWeapon>();
            laserStart = raycastShootingWeapon.transform.position;
            lineRenderer.startColor = Color.clear;
            lineRenderer.endColor = laserColor;
            lineRenderer.SetPositions(new[] { laserStart, laserStart });
        }
        
        private void OnEnable()
        {
            playerController.OnAim += TurnOn;
            playerController.OnStopAiming += TurnOff;
        }
        
        private void OnDisable()
        {
            playerController.OnAim -= TurnOn;
            playerController.OnStopAiming -= TurnOff;
        }

        private void Update()
        {
            if (!isEquipped || !isOn) return;

            laserStart = raycastShootingWeapon.transform.position;

            if (Physics.Raycast(laserStart, raycastShootingWeapon.transform.TransformDirection(Vector3.forward), out hit, maxDistance, layerMask))
            {
                laserEnd = hit.point;
            }

            else
            {
                laserEnd = laserStart + raycastShootingWeapon.transform.TransformDirection(Vector3.forward) * maxDistance;
            }

            lineRenderer.SetPositions(new[] { laserStart, laserEnd });
        }

        public void Equip()
        {
            isEquipped = true;
        }

        public void Unequip()
        {
            isEquipped = false;
            lineRenderer.SetPositions(new[] { new Vector3(0, 0, 0), new Vector3(0, 0, 0) });
        }

        private void TurnOn()
        {
            isOn = true;
            lineRenderer.enabled = true;
            raycastShootingWeapon = GetComponentInParent<RaycastShootingWeapon>();
        }

        private void TurnOff()
        {
            isOn = false;
            lineRenderer.enabled = false;
            raycastShootingWeapon = null;
        }
    }
}


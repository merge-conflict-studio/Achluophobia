﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public class ExplosionSphere : Script
    {
        [SerializeField, Tooltip("The layers affected by the explosion")]
        private LayerMask explosionMask;

        [SerializeField, Tooltip("The explosion radius un meters")]
        private float radius;

        public Collider[] GetExplosionTargets()
        {
            Collider[] explosionTargets = Physics.OverlapSphere(
                transform.position,
                radius,
                explosionMask);

            return explosionTargets;
        }
    }
}


﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Actuator/WitchProjectileThrower")]
    public class WitchProjectileThrower : GameScript
    {
        [SerializeField, Tooltip("The speed at which the witch spits her projectile")]
        private int spitForce;

        private Rigidbody parentRigidBody;

        private void InjectWitchProjectileThrower([RootScope] Rigidbody parentRigidbody)
        {
            this.parentRigidBody = parentRigidbody;
        }

        private void Awake()
        {
            InjectDependencies("InjectWitchProjectileThrower");
            parentRigidBody.AddRelativeForce(Vector3.forward * spitForce, ForceMode.Impulse);
        }
    }
}

﻿using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Actuator/GameObjectSpawner")]
    public class GameObjectSpawner : GameScript
    {
        [SerializeField, Tooltip("The prefab to spawn")] 
        private GameObject gameObjectPrefab;

        public GameObject Spawn(Transform spawnPoint)
        {
            return Instantiate(gameObjectPrefab, spawnPoint.position, spawnPoint.rotation);
        }
    }
}
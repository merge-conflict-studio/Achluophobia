﻿using UnityEngine;
using Harmony;

namespace Achluophobia
{
    [AddComponentMenu("Game/Actuator/PlayerSpawner")]
    public class PlayerSpawner : GameScript
    {
        public static bool IsTestMode = false;

        [SerializeField, Tooltip("Assault class prefab")]
        private GameObject assaultPrefab;

        [SerializeField, Tooltip("Medic class prefab")]
        private GameObject medicPrefab;

        [SerializeField, Tooltip("Scout class prefab")]
        private GameObject scoutPrefab;

        [SerializeField, Tooltip("Mastodonte class prefab")]
        private GameObject mastodontePrefab;

        [SerializeField, Tooltip("Incinerator class prefab")]
        private GameObject incineratorPrefab;


        private ImportantGameValues importantGameValues;
        private ClassesInfo classesInfo;


        private void InjectPlayerSpawner([ApplicationScope] ClassesInfo classesInfo,
                                         [ApplicationScope] ImportantGameValues importantGameValues)
        {
            this.classesInfo = classesInfo;
            this.importantGameValues = importantGameValues;
        }

        private void Awake()
        {
            if (!IsTestMode) InjectDependencies("InjectPlayerSpawner");
        }

        public GameObject Spawn(PlayerClass playerClass, Vector3 position)
        {
            GameObject player = null;

            switch (playerClass)
            {
                case PlayerClass.Assault:
                    player = Instantiate(assaultPrefab, position, assaultPrefab.transform.rotation);
                    break;
                case PlayerClass.Medic:
                    player = Instantiate(medicPrefab, position, medicPrefab.transform.rotation);
                    break;
                case PlayerClass.Scout:
                    player = Instantiate(scoutPrefab, position, scoutPrefab.transform.rotation);
                    break;
                case PlayerClass.Mastodonte:
                    player = Instantiate(mastodontePrefab, position, mastodontePrefab.transform.rotation);
                    break;
                case PlayerClass.Incinerator:
                    player = Instantiate(incineratorPrefab, position, incineratorPrefab.transform.rotation);
                    break;
            }

            if (player != null)
            {
                PlayerController playerController = player.GetComponent<PlayerController>();
                if (playerController)
                {
                    Configure(player, classesInfo.GetInfo(playerClass).Color);
                    importantGameValues.Players[(int)playerController.PlayerNumber] = player;
                }
            }
            return player;
        }
         
        private void Configure(GameObject player, Color playerColor)
        {
            PlayerController playerController = player.GetComponent<PlayerController>();
            playerController.Initialize(playerColor);
        }
    }
}
﻿using System;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Actuator/GrenadeThrower")]
    public class GrenadeThrower : GameScript
    {
        [SerializeField, Tooltip("The force at which we throw our grenade")]
        private int throwForce;

        private GameObjectSpawner gameObjectSpawner;
        private PlayerController playerController;

        private void InjectGrenadeThrower(
            [GameObjectScope] GameObjectSpawner gameObjectSpawner,
            [RootScope] PlayerController playerController)
        {
            this.gameObjectSpawner = gameObjectSpawner;
            this.playerController = playerController;
        }

        private void Awake()
        {
            InjectDependencies("InjectGrenadeThrower");
        }

        private void OnEnable()
        {
            playerController.OnTryingToSpawnSpecialItem += ThrowGrenade;
        }

        private void OnDisable()
        {
            playerController.OnTryingToSpawnSpecialItem -= ThrowGrenade;
        }

        private void ThrowGrenade()
        {
            GameObject grenadeGO = gameObjectSpawner.Spawn(transform);
            Grenade grenade = grenadeGO.GetComponent<Grenade>();
            grenade.StartTimedExplosion(GetRoot());
            grenadeGO.GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * throwForce, ForceMode.Impulse);
        }


    }
}
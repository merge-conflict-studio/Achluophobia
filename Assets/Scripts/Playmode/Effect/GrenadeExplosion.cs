﻿using UnityEngine;

namespace Achluophobia
{
    [CreateAssetMenu(fileName = "Create new grenade explosion effect", menuName = "Game/Effect/GrenadeExplosion")]
    public class GrenadeExplosion : InstantEffect
    {
        [SerializeField, Tooltip("The ammount of damage dealt by the explosion")]
        private int maxDamages;


        public override void ApplyOn(GameScript effectExecutor, GameObject rootGameObject)
        {
            HitSensor hitSensor = rootGameObject.GetComponentInChildren<HitSensor>();
            ApplyDamages(hitSensor);
        }

        private void ApplyDamages(HitSensor hitSensor)
        {
            hitSensor.Hit(null, maxDamages, true);
        }
    }
}

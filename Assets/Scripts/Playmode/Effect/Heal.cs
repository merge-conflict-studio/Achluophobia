﻿using System.Collections;
using UnityEngine;

namespace Achluophobia
{
    [CreateAssetMenu(fileName = "New Heal Effect", menuName = "Game/Effect/Heal")]
    public class Heal : InstantEffect
    {
        [SerializeField, Tooltip("The duration of the effect in seconds")] 
        private int durationInSeconds;

        [SerializeField, Tooltip("The ammount of health given per seconds")] 
        private int healPointsPerSecond;

        public override void ApplyOn(GameScript effectExecutor, GameObject rootGameObject)
        {
            Health health = rootGameObject.GetComponentInChildren<Health>();
            effectExecutor.StartCoroutine(ApplyOnRoutine(health));
        }

        private IEnumerator ApplyOnRoutine(Health health)
        {
            float healEndTime = Time.time + durationInSeconds;
            while (Time.time < healEndTime)
            {
                health.Heal(healPointsPerSecond, false);
                yield return new WaitForSeconds(1);
            }
        }
    }
}
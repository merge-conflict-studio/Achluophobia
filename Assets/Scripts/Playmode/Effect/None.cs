﻿using UnityEngine;

namespace Achluophobia
{
    [CreateAssetMenu(fileName = "New No Effect", menuName = "Game/Effect/None")]
    public class None : InstantEffect
    {
        public override void ApplyOn(GameScript effectExecutor, GameObject rootGameObject)
        {
            //Is empty on purpose (since we want it to do nothing)
        }
    }
}
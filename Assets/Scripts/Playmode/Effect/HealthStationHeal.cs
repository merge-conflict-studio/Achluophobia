﻿using System.Collections;
using UnityEngine;

namespace Achluophobia
{
    [CreateAssetMenu(fileName = "New Health Station Heal Effect", menuName = "Game/Effect/HealthStationHeal")]
    public class HealthStationHeal : ContinuousEffect
    {
        [SerializeField, Tooltip("The number of health points given per seconds")]
        private int healPointsPerSeconds;

        private bool canHeal;

        public override void ApplyOn(GameScript effectExecutor, GameObject rootGameObject)
        {
            Health health = rootGameObject.GetComponentInChildren<Health>();
            canHeal = true;
            effectExecutor.StartCoroutine(ApplyOnRoutine(health));
        }

        public override void StopEffect(GameScript effectExecutor)
        {
            canHeal = false;
        }

        private IEnumerator ApplyOnRoutine(Health health)
        {
            while (canHeal)
            {
                health.Heal(healPointsPerSeconds, true);
                yield return new WaitForSeconds(1);
            }
        }
    }
}
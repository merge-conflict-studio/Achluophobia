﻿using UnityEngine;

namespace Achluophobia
{
    [CreateAssetMenu(fileName = "New Instant Effect", menuName = "Game/Effect/InstantEffect")]
    public abstract class InstantEffect : ScriptableObject
    {
        public abstract void ApplyOn(GameScript effectExecutor, GameObject rootGameObject);
    }
}


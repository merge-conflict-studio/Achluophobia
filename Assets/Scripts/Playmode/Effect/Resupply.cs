﻿using UnityEngine;

namespace Achluophobia
{
    [CreateAssetMenu(fileName = "Ressuply Effect", menuName = "Game/Effect/Ressuply")]
    public class Resupply : InstantEffect
    {
        [SerializeField, Tooltip("The ammount of ammos we want to give with the effect")] 
        private int ammoQuantity;

        public override void ApplyOn(GameScript effectExecutor, GameObject rootGameObject)
        {
            Weapon primaryWeapon = rootGameObject.GetComponent<PlayerController>().GetPrimaryWeapon();

            if (primaryWeapon.WeaponType != WeaponType.Shooting) return;
            Ammo ammo = primaryWeapon.GetComponent<Ammo>();
            ammo.PickUpAmmo(ammoQuantity);
        }
    }
}
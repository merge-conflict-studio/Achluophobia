﻿using UnityEngine;

namespace Achluophobia
{
    public abstract class ContinuousEffect : ScriptableObject
    {
        public abstract void ApplyOn(GameScript effectExecutor, GameObject rootGameObject);
        public abstract void StopEffect(GameScript effectExecutor);
    }
}

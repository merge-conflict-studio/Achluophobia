﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public class TimingUIController : GameScript
    {
        private const float ZAxisOffset = -0.05f;
        private const string ZoneName = "Zone ";

        [SerializeField, Tooltip("The \"released\" sprite of the A button for the tutorial")]
        private Sprite releasedButtonSprites;

        [SerializeField, Tooltip("The \"pressed\" sprite of the A button for the tutorial")]
        private Sprite pressedButtonSprites;

        [SerializeField, Tooltip("The sprite which indicates success")]
        private Sprite successSprite;

        [SerializeField, Tooltip("The sprite which indicates failure")]
        private Sprite failureSprite;

        private TimingLogicController logicController;
        private SpriteRenderer[] spriteRenderers;
        private SpriteRenderer successIndicator;
        private Transform progressBarTransform;
        private int currentButtonIndex;
        private float progressBarMaxScale;

        private void InjectTimingUIController([GameObjectScope] TimingLogicController logicController,
                                              [ChildScope][Named("ProgressBar")] GameObject progressBar,
                                              [ChildScope][Named("SuccessIndicator")] SpriteRenderer successIndicator)
        {
            this.logicController = logicController;
            this.successIndicator = successIndicator;
            progressBarTransform = progressBar.transform;
        }

        private void Awake()
        {
            InjectDependencies("InjectTimingUIController");
            currentButtonIndex = 0;
            spriteRenderers = null;

            Vector3 progressBarScale = progressBarTransform.localScale;
            progressBarMaxScale = progressBarScale.x;
            progressBarTransform.localScale = new Vector3(0, progressBarScale.y, progressBarScale.z);
        }

        private void OnEnable()
        {
            logicController.OnMinigameEnd += OnMinigameEnd;
            logicController.OnPress += OnPress;
            logicController.OnProgressionPercentageChanged += UpdateProgressionPercentage;
            logicController.OnZonesGenerated += GenerateZonesSprites;
        }

        private void OnMinigameEnd(bool isCompleted)
        {
            logicController.OnMinigameEnd -= OnMinigameEnd;
            logicController.OnPress -= OnPress;
            logicController.OnProgressionPercentageChanged -= UpdateProgressionPercentage;
            logicController.OnZonesGenerated -= GenerateZonesSprites;

            successIndicator.sprite = isCompleted ? successSprite : failureSprite;
        }

        private void OnPress(bool isRightTiming)
        {
            if (!isRightTiming) return;
            spriteRenderers[currentButtonIndex].sprite = pressedButtonSprites;
            spriteRenderers[currentButtonIndex++].color = Color.grey;
        }

        private void UpdateProgressionPercentage(float completionPercentage)
        {
            progressBarTransform.localScale = new Vector3(completionPercentage * progressBarMaxScale, 1, 1);
        }

        private void GenerateZonesSprites(float[] zonesToPress)
        {
            spriteRenderers = new SpriteRenderer[zonesToPress.Length];
            for (int i = 0; i < zonesToPress.Length; i++)
            {
                GameObject zoneToAdd = new GameObject();
                SpriteRenderer rendererToAdd = zoneToAdd.AddComponent<SpriteRenderer>();
                float zoneScale = progressBarMaxScale * TimingLogicController.HalfZonePercentage * 2;

                zoneToAdd.transform.parent = progressBarTransform.parent;
                zoneToAdd.transform.localPosition = new Vector3(progressBarMaxScale * zonesToPress[i], 0, ZAxisOffset);
                zoneToAdd.transform.localRotation = Quaternion.Euler(0, 0, 0);
                zoneToAdd.transform.localScale = new Vector3(zoneScale, 1, zoneScale);

                rendererToAdd.sprite = releasedButtonSprites;
                spriteRenderers[i] = rendererToAdd;
                zoneToAdd.name = ZoneName + i;
            }
        }
    }
}

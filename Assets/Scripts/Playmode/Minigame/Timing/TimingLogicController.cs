﻿using System.Collections;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public delegate void TimingPressEventHandler(bool isRightTiming);
    public delegate void ZonesGenerationEventHandler(float[] zonesToPress);

    public class TimingLogicController : GameScript, MinigameLogicController
    {
        public const float HalfZonePercentage = 0.03f;

        private const int NumberOfZonesToPress = 7;
        private const int SecondsBeforeDestroy = 3;
        private const float SecondsBeforeStart = 0.1f;
        private const float SpacingBetweenZonesInPercentage = 0.06f;
        private const float MaxPercentage = 1.0f;
        private const float MinPercentage = 0.0f;
        private const float ProgressionPercentagePerSecond = 0.2f;


        private PlayerNumber playerNumber;
        private PlayerDeathEventChannel playerDeathEventChannel;
        private GamePadSensor playerGamePadSensor;

        private float[] zonesToPressInPercentage;
        private float progressionPercentage;
        private int currentZoneToPressIndex;
        private bool isCompleted;

        private float ProgressionPercentage
        {
            get { return progressionPercentage; }
            set { SetValueIfValueIsBeyondThreshold(value); }
        }

        public event MinigameEndEventHandler OnMinigameEnd;
        public event MinigameEndEventHandler OnMinigameDestroy;
        public event MinigameProgressionEventHandler OnProgressionPercentageChanged;
        public event TimingPressEventHandler OnPress;
        public event ZonesGenerationEventHandler OnZonesGenerated;

        private void InjectTimingLogicController([ApplicationScope] GamePadsController gamePadsController,
                                                 [EventChannelScope] PlayerDeathEventChannel playerDeathEventChannel)
        {
            playerGamePadSensor = gamePadsController.GetSensor(playerNumber);
            this.playerDeathEventChannel = playerDeathEventChannel;
        }

        private void Awake()
        {
            zonesToPressInPercentage = new float[NumberOfZonesToPress];
            for (int i = 0; i < zonesToPressInPercentage.Length; i++)
            {
                float ceiling = MaxPercentage / NumberOfZonesToPress * (1 + i) - SpacingBetweenZonesInPercentage;
                int lastIndex = i == 0 ? 0 : i - 1;
                float floor = Mathf.Max(MinPercentage + SpacingBetweenZonesInPercentage, zonesToPressInPercentage[lastIndex] + SpacingBetweenZonesInPercentage);
                zonesToPressInPercentage[i] = Random.Range(floor, ceiling);
            }

            ProgressionPercentage = 0;
            currentZoneToPressIndex = 0;
            isCompleted = false;
        }

        private void OnDestroy()
        {
            UnsubscribeFromEvents();
        }

        public void LaunchMinigame(PlayerNumber playerNumber)
        {
            this.playerNumber = playerNumber;
            InjectDependencies("InjectTimingLogicController");

            StartCoroutine(StartMinigameAfterSeconds());
        }

        private void UnsubscribeFromEvents()
        {
            if (playerGamePadSensor != null)
            {
                playerGamePadSensor.OnAPressed -= OnAButtonPressed;
                playerGamePadSensor = null;
            }

            if (playerDeathEventChannel != null)
            {
                playerDeathEventChannel.OnEventPublished -= OnPlayerDeath;
                playerDeathEventChannel = null;
            }
        }

        private void OnAButtonPressed()
        {
            float currentZonePercentage = zonesToPressInPercentage[currentZoneToPressIndex++];
            bool successfulPress = ProgressionPercentage <= currentZonePercentage + HalfZonePercentage &&
                                   ProgressionPercentage >= currentZonePercentage - HalfZonePercentage;
            if (successfulPress && currentZoneToPressIndex == NumberOfZonesToPress) isCompleted = true;
            if (OnPress != null) OnPress(successfulPress);
            if (!successfulPress) EndMinigame(false);
        }

        private IEnumerator StartMinigameAfterSeconds()
        {
            yield return new WaitForSeconds(SecondsBeforeStart);

            playerDeathEventChannel.OnEventPublished += OnPlayerDeath;
            playerGamePadSensor.OnAPressed += OnAButtonPressed;

            if (OnZonesGenerated != null) OnZonesGenerated(zonesToPressInPercentage);

            StartCoroutine(UpdateProgression());
        }

        private IEnumerator DestroyAfterSeconds(float timeInSeconds, bool isCompleted)
        {
            yield return new WaitForSeconds(timeInSeconds);
            if (OnMinigameDestroy != null) OnMinigameDestroy(isCompleted);
            OnMinigameDestroy = null;
            Destroy(gameObject);
        }

        private IEnumerator UpdateProgression()
        {
            while (true)
            {
                ProgressionPercentage += ProgressionPercentagePerSecond * Time.deltaTime;
                yield return null;
            }
        }

        private void EndMinigame(bool isCompleted)
        {
            UnsubscribeFromEvents();

            if (OnMinigameEnd != null) OnMinigameEnd(isCompleted);

            OnMinigameEnd = null;
            OnPress = null;
            OnProgressionPercentageChanged = null;
            OnZonesGenerated = null;

            StopAllCoroutines();
            StartCoroutine(DestroyAfterSeconds(SecondsBeforeDestroy, isCompleted));
        }

        private void OnPlayerDeath(PlayerDeathEvent playerDeathEvent)
        {
            if (playerDeathEvent.DeadPlayer.GetComponent<PlayerController>().PlayerNumber == playerNumber)
            {
                EndMinigame(false);
            }
        }

        private void SetValueIfValueIsBeyondThreshold(float value)
        {
            progressionPercentage = Mathf.Clamp(value, MinPercentage, MaxPercentage);
            if (OnProgressionPercentageChanged != null) OnProgressionPercentageChanged(progressionPercentage);
            if (progressionPercentage > zonesToPressInPercentage[Mathf.Min(currentZoneToPressIndex, zonesToPressInPercentage.Length - 1)] + HalfZonePercentage &&
                !isCompleted)
            {
                EndMinigame(false);
            }
            else if (progressionPercentage >= MaxPercentage) EndMinigame(true);
        }
    }
}

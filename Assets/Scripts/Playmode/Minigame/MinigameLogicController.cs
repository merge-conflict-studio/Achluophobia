﻿namespace Achluophobia
{
    public delegate void CountdownEventHandler();
    public delegate void ButtonGenerationEventHandler(XBoxButton button);
    public delegate void MinigameProgressionEventHandler(float completionPercentage);
    public delegate void MinigameEndEventHandler(bool isCompleted);
    public delegate void WaitingForButtonPressEventHandler(bool waitingForButtonPress);
    public delegate void ButtonPressEventHandler(XBoxButton xBoxButtonPressed, bool isRightButton);
    public delegate void ButtonSequenceGenerationEventHandler(XBoxButton[] sequence);

    public interface MinigameLogicController
    {
        event MinigameEndEventHandler OnMinigameEnd;
        event MinigameEndEventHandler OnMinigameDestroy;
        void LaunchMinigame(PlayerNumber playerNumber);
    }
}



﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public class PressAndReleaseUIController : GameScript
    {
        [SerializeField, Tooltip("The sprites for the tutorial")]
        private Sprite[] buttonSprites;

        [SerializeField, Tooltip("The sprite which indicates success")]
        private Sprite successSprite;

        [SerializeField, Tooltip("The sprite which indicates failure")]
        private Sprite failureSprite;

        private GameObject progressBar;
        private GameObject tutorialButton;
        private SpriteRenderer successIndicator;
        private PressAndReleaseLogicController minigameLogicController;
        private float progressBarMaxScale;

        private void InjectPressAndReleaseUIController([ChildScope][Named("ProgressBar")] GameObject progressBar,
                                                       [ChildScope][Named("TutorialButton")] GameObject tutorialButton,
                                                       [ChildScope][Named("SuccessIndicator")] SpriteRenderer successIndicator,
                                                       [GameObjectScope] PressAndReleaseLogicController minigameLogicController)
        {
            this.progressBar = progressBar;
            this.tutorialButton = tutorialButton;
            this.successIndicator = successIndicator;
            this.minigameLogicController = minigameLogicController;
        }

        private void Awake()
        {
            InjectDependencies("InjectPressAndReleaseUIController");
            progressBarMaxScale = progressBar.transform.localScale.x;
            progressBar.transform.localScale = new Vector3(0, 1, 1);
        }

        private void OnEnable()
        {
            minigameLogicController.OnWaitingForButtonPressChanged += SwitchInstructionButtonSprite;
            minigameLogicController.OnCompletionPercentageChanged += UpdateProgressBar;
            minigameLogicController.OnMinigameEnd += OnMinigameEnd;
        }

        private void SwitchInstructionButtonSprite(bool isWaitingForButtonPress)
        {
            if (isWaitingForButtonPress)
            {
                tutorialButton.GetComponent<SpriteRenderer>().sprite = buttonSprites[0];
                tutorialButton.GetComponent<SpriteRenderer>().color = Color.white;
            }
            else
            {
                tutorialButton.GetComponent<SpriteRenderer>().sprite = buttonSprites[1];
                tutorialButton.GetComponent<SpriteRenderer>().color = Color.grey;
            }
        }

        private void UpdateProgressBar(float completionPercentage)
        {
            progressBar.transform.localScale = new Vector3(completionPercentage * progressBarMaxScale, 1, 1);
        }

        private void OnMinigameEnd(bool isCompleted)
        {
            minigameLogicController.OnWaitingForButtonPressChanged -= SwitchInstructionButtonSprite;
            minigameLogicController.OnCompletionPercentageChanged -= UpdateProgressBar;
            minigameLogicController.OnMinigameEnd -= OnMinigameEnd;
            
            successIndicator.sprite = isCompleted ? successSprite : failureSprite;
            Destroy(this);
        }
    }

}
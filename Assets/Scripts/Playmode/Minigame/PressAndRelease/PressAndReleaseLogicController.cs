﻿using System.Collections;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public class PressAndReleaseLogicController : GameScript, MinigameLogicController
    {
        private const float ButtonPressedTimeInSeconds = 1.0f;
        private const float ButtonReleasedTimeInSeconds = 0.5f;
        private const float CompletionPercentagePerSecond = 0.3f;
        private const float PercentageLossPerSecond = CompletionPercentagePerSecond / 4.0f;
        private const int TimeToCompleteMinigame = 15;
        private const int SecondsBeforeDestroy = 3;

        private GamePadSensor playerGamePadSensor;
        private PlayerNumber playerNumber;
        private PlayerDeathEventChannel playerDeathEventChannel;

        private bool waitingForButtonPress;
        private bool isButtonPressed;
        private float completionPercentage;
        private WaitForSeconds timeToPressButton;
        private WaitForSeconds timeToReleaseButton;

        private float CompletionPercentage
        {
            get { return completionPercentage; }
            set
            {
                completionPercentage = Mathf.Clamp(value, 0.0f, 1.0f);
            }
        }
        private bool WaitingForButtonPress
        {
            set
            {
                waitingForButtonPress = value;
                if (OnWaitingForButtonPressChanged != null) OnWaitingForButtonPressChanged(waitingForButtonPress);
            }
        }

        public event MinigameEndEventHandler OnMinigameEnd;
        public event MinigameEndEventHandler OnMinigameDestroy;
        public event MinigameProgressionEventHandler OnCompletionPercentageChanged;
        public event WaitingForButtonPressEventHandler OnWaitingForButtonPressChanged;

        private void InjectPressAndReleaseLogicController([ApplicationScope] GamePadsController gamePadsController,
                                                          [EventChannelScope] PlayerDeathEventChannel playerDeathEventChannel)
        {
            playerGamePadSensor = gamePadsController.GetSensor(playerNumber);
            this.playerDeathEventChannel = playerDeathEventChannel;
        }

        private void Awake()
        {
            waitingForButtonPress = true;
            isButtonPressed = false;
            completionPercentage = 0.0f;
            timeToPressButton = new WaitForSeconds(ButtonPressedTimeInSeconds);
            timeToReleaseButton = new WaitForSeconds(ButtonReleasedTimeInSeconds);
        }

        private void OnDestroy()
        {
            UnsubscribeFromEvents();
        }

        public void LaunchMinigame(PlayerNumber playerNumber)
        {
            this.playerNumber = playerNumber;
            InjectDependencies("InjectPressAndReleaseLogicController");

            playerDeathEventChannel.OnEventPublished += OnPlayerDeath;
            playerGamePadSensor.OnAPressed += OnAButtonPressed;
            playerGamePadSensor.OnAReleased += OnAButtonReleased;

            StartCoroutine(EndMiniGameAfterDelay(TimeToCompleteMinigame));
            StartCoroutine(InvertWaitingForButtonPressAfterDelay());
            StartCoroutine(CheckButtonState());
        }

        private void UnsubscribeFromEvents()
        {
            if (playerGamePadSensor != null)
            {
                playerGamePadSensor.OnAPressed -= OnAButtonPressed;
                playerGamePadSensor.OnAReleased -= OnAButtonReleased;
                playerGamePadSensor = null;
            }

            if (playerDeathEventChannel != null)
            {
                playerDeathEventChannel.OnEventPublished -= OnPlayerDeath;
                playerDeathEventChannel = null;
            }
        }

        private void OnAButtonPressed()
        {
            isButtonPressed = true;
        }

        private void OnAButtonReleased()
        {
            isButtonPressed = false;
        }

        private IEnumerator InvertWaitingForButtonPressAfterDelay()
        {
            while (true)
            {
                WaitingForButtonPress = true;
                yield return timeToPressButton;

                WaitingForButtonPress = false;
                yield return timeToReleaseButton;
            }
        }

        private IEnumerator EndMiniGameAfterDelay(float timeInSeconds)
        {
            yield return new WaitForSeconds(timeInSeconds);
            EndMinigame(false);
        }

        private IEnumerator CheckButtonState()
        {
            while (true)
            {
                if (waitingForButtonPress && isButtonPressed)
                {
                    CompletionPercentage += CompletionPercentagePerSecond * Time.deltaTime;
                    if (completionPercentage >= 1.0f) EndMinigame(true);
                }
                else if (!waitingForButtonPress && isButtonPressed)
                {
                    CompletionPercentage -= CompletionPercentagePerSecond * Time.deltaTime;
                }
                CompletionPercentage -= PercentageLossPerSecond * Time.deltaTime;
                if (OnCompletionPercentageChanged != null) OnCompletionPercentageChanged(CompletionPercentage);
                yield return null;
            }
        }

        private void EndMinigame(bool isCompleted)
        {
            UnsubscribeFromEvents();

            if (OnMinigameEnd != null) OnMinigameEnd(isCompleted);

            OnMinigameEnd = null;
            OnCompletionPercentageChanged = null;
            OnWaitingForButtonPressChanged = null;

            StopAllCoroutines();
            StartCoroutine(DestroyAfterSeconds(SecondsBeforeDestroy, isCompleted));
        }

        private IEnumerator DestroyAfterSeconds(float seconds, bool isCompleted)
        {
            yield return new WaitForSeconds(seconds);
            if (OnMinigameDestroy != null) OnMinigameDestroy(isCompleted);
            OnMinigameDestroy = null;
            Destroy(gameObject);
        }

        private void OnPlayerDeath(PlayerDeathEvent playerDeathEvent)
        {
            if (playerDeathEvent.DeadPlayer.GetComponent<PlayerController>().PlayerNumber == playerNumber)
            {
                EndMinigame(false);
            }
        }
    }
}



﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public class SequenceUIController : GameScript
    {
        [SerializeField, Tooltip("The \"released\" sprites of each xBoxButton for the tutorial")]
        private Sprite[] releasedButtonSprites;
        
        [SerializeField, Tooltip("The \"pressed\" sprites of each xBoxButton for the tutorial")]
        private Sprite[] pressedButtonSprites;

        [SerializeField, Tooltip("The sprite which indicates success")]
        private Sprite successSprite;

        [SerializeField, Tooltip("The sprite which indicates failure")]
        private Sprite failureSprite;

        private SequenceLogicController minigameLogicController;
        private SpriteRenderer[] spriteRenderers;
        private SpriteRenderer successIndicator;
        private int currentSequenceIndex;

        private void InjectSequenceUIController([GameObjectScope] SequenceLogicController minigameLogicController,
                                                [ChildScope][Named("Buttons")] GameObject buttons,
                                                [ChildScope][Named("SuccessIndicator")] SpriteRenderer successIndicator)
        {
            this.minigameLogicController = minigameLogicController;
            this.successIndicator = successIndicator;
            spriteRenderers = buttons.GetComponentsInChildren<SpriteRenderer>();
        }

        private void Awake()
        {
            InjectDependencies("InjectSequenceUIController");
            currentSequenceIndex = 0;
        }

        private void OnEnable()
        {
            minigameLogicController.OnMinigameEnd += OnMinigameEnd;
            minigameLogicController.OnButtonPress += OnButtonPress;
            minigameLogicController.OnSequenceGenerated += OnSequenceGenerated;
        }

        private void OnButtonPress(XBoxButton xBoxButton, bool isRightButton)
        {
            spriteRenderers[currentSequenceIndex].color = Color.gray;
            spriteRenderers[currentSequenceIndex++].sprite = pressedButtonSprites[(int) xBoxButton];
        }

        private void OnMinigameEnd(bool isCompleted)
        {
            minigameLogicController.OnMinigameEnd -= OnMinigameEnd;
            minigameLogicController.OnButtonPress -= OnButtonPress;
            minigameLogicController.OnSequenceGenerated -= OnSequenceGenerated;

            successIndicator.sprite = isCompleted ? successSprite : failureSprite;
        }

        private void OnSequenceGenerated(XBoxButton[] sequence)
        {
            for (int i = 0; i < sequence.Length; i++)
            {
                spriteRenderers[i].sprite = releasedButtonSprites[(int) sequence[i]];
            }
        }
    }
}

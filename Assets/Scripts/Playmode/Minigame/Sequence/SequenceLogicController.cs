﻿using System;
using System.Collections;
using Harmony;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Achluophobia
{
    public class SequenceLogicController : GameScript, MinigameLogicController
    {
        private const int TimeToCompleteMinigameInSeconds = 9;
        private const int SecondsBeforeDestroy = 3;
        private const float SecondsBeforeStart = 0.1f;

        private PlayerNumber playerNumber;
        private PlayerDeathEventChannel playerDeathEventChannel;
        private GamePadSensor playerGamePadSensor;

        private XBoxButton[] xboxButtonSequence;
        private int currentSequenceIndex;

        public event MinigameEndEventHandler OnMinigameEnd;
        public event MinigameEndEventHandler OnMinigameDestroy;
        public event ButtonPressEventHandler OnButtonPress;
        public event ButtonSequenceGenerationEventHandler OnSequenceGenerated;

        private void InjectSequenceLogicController([ApplicationScope] GamePadsController gamePadsController,
                                                   [EventChannelScope] PlayerDeathEventChannel playerDeathEventChannel)
        {
            playerGamePadSensor = gamePadsController.GetSensor(playerNumber);
            this.playerDeathEventChannel = playerDeathEventChannel;
        }

        private void Awake()
        {
            int xboxButtonsNumber = Enum.GetValues(typeof(XBoxButton)).Length;
            xboxButtonSequence = new[]
            {
                (XBoxButton)Random.Range(0, xboxButtonsNumber),
                (XBoxButton)Random.Range(0, xboxButtonsNumber),
                (XBoxButton)Random.Range(0, xboxButtonsNumber),
                (XBoxButton)Random.Range(0, xboxButtonsNumber),
                (XBoxButton)Random.Range(0, xboxButtonsNumber),
                (XBoxButton)Random.Range(0, xboxButtonsNumber),
                (XBoxButton)Random.Range(0, xboxButtonsNumber),
                (XBoxButton)Random.Range(0, xboxButtonsNumber),
                (XBoxButton)Random.Range(0, xboxButtonsNumber),
                (XBoxButton)Random.Range(0, xboxButtonsNumber)
            };
            currentSequenceIndex = 0;
        }

        private void Start()
        {
            if (OnSequenceGenerated != null) OnSequenceGenerated(xboxButtonSequence);
        }

        private void OnDestroy()
        {
            UnsubscribeFromEvents();
        }

        public void LaunchMinigame(PlayerNumber playerNumber)
        {
            this.playerNumber = playerNumber;
            InjectDependencies("InjectSequenceLogicController");

            StartCoroutine(StartMinigameAfterSeconds());
        }

        private void UnsubscribeFromEvents()
        {
            if (playerGamePadSensor != null)
            {
                playerGamePadSensor.OnAPressed -= OnAButtonPressed;
                playerGamePadSensor.OnBPressed -= OnBButtonPressed;
                playerGamePadSensor.OnXPressed -= OnXButtonPressed;
                playerGamePadSensor.OnYPressed -= OnYButtonPressed;
                playerGamePadSensor = null;
            }

            if (playerDeathEventChannel != null)
            {
                playerDeathEventChannel.OnEventPublished -= OnPlayerDeath;
                playerDeathEventChannel = null;
            }
        }

        private void OnAButtonPressed()
        {
            ProcessButtonPress(XBoxButton.A);
        }

        private void OnBButtonPressed()
        {
            ProcessButtonPress(XBoxButton.B);
        }

        private void OnXButtonPressed()
        {
            ProcessButtonPress(XBoxButton.X);
        }

        private void OnYButtonPressed()
        {
            ProcessButtonPress(XBoxButton.Y);
        }

        private void ProcessButtonPress(XBoxButton button)
        {
            if (xboxButtonSequence[currentSequenceIndex++] == button)
            {
                if (OnButtonPress != null) OnButtonPress(button, true);
                if (currentSequenceIndex == xboxButtonSequence.Length) EndMinigame(true);
            }
            else
            {
                if (OnButtonPress != null) OnButtonPress(button, false);
                EndMinigame(false);
            }
        }

        private void EndMinigame(bool isCompleted)
        {
            UnsubscribeFromEvents();

            if (OnMinigameEnd != null) OnMinigameEnd(isCompleted);

            OnMinigameEnd = null;
            OnButtonPress = null;
            OnSequenceGenerated = null;

            StopAllCoroutines();
            StartCoroutine(DestroyAfterSeconds(SecondsBeforeDestroy, isCompleted));
        }

        private IEnumerator DestroyAfterSeconds(float seconds, bool isCompleted)
        {
            yield return new WaitForSeconds(seconds);
            if (OnMinigameDestroy != null) OnMinigameDestroy(isCompleted);
            OnMinigameDestroy = null;
            Destroy(gameObject);
        }

        private IEnumerator StartMinigameAfterSeconds()
        {
            yield return new WaitForSeconds(SecondsBeforeStart);

            playerDeathEventChannel.OnEventPublished += OnPlayerDeath;
            playerGamePadSensor.OnAPressed += OnAButtonPressed;
            playerGamePadSensor.OnBPressed += OnBButtonPressed;
            playerGamePadSensor.OnXPressed += OnXButtonPressed;
            playerGamePadSensor.OnYPressed += OnYButtonPressed;

            StartCoroutine(EndMinigameAfterDelay(TimeToCompleteMinigameInSeconds));
        }

        private IEnumerator EndMinigameAfterDelay(float timeInSeconds)
        {
            yield return new WaitForSeconds(timeInSeconds);
            EndMinigame(false);
        }

        private void OnPlayerDeath(PlayerDeathEvent playerDeathEvent)
        {
            if (playerDeathEvent.DeadPlayer.GetComponent<PlayerController>().PlayerNumber == playerNumber)
            {
                EndMinigame(false);
            }
        }
    }
}


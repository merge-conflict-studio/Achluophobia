﻿using System;
using System.Collections;
using Harmony;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Achluophobia
{ 
    public class LastSecondPressLogicController : GameScript, MinigameLogicController
    {
        public const int NumberOfTicks = 4;
        private const float TimeWindowToPressButtonInSeconds = 0.5f;
        private const int SecondsBeforeDestroy = 3;

        private PlayerNumber playerNumber;
        private PlayerDeathEventChannel playerDeathEventChannel;
        private GamePadSensor playerGamePadSensor;

        private XBoxButton buttonToPress;
        private bool isWaitingForButtonPress;
        private WaitForSeconds waitOneSecond;

        public event MinigameEndEventHandler OnMinigameEnd;
        public event MinigameEndEventHandler OnMinigameDestroy;
        public event CountdownEventHandler OnCountdownTick;
        public event ButtonGenerationEventHandler OnButtonGenerated;

        private void InjectLastSecondPressLogicController([ApplicationScope] GamePadsController gamePadsController,
                                                          [EventChannelScope] PlayerDeathEventChannel playerDeathEventChannel)
        {
            playerGamePadSensor = gamePadsController.GetSensor(playerNumber);
            this.playerDeathEventChannel = playerDeathEventChannel;
        }

        private void Awake()
        {
            int xboxButtonsNumber = Enum.GetValues(typeof(XBoxButton)).Length;
            buttonToPress = (XBoxButton)Random.Range(0, xboxButtonsNumber);
            isWaitingForButtonPress = false;
            waitOneSecond = new WaitForSeconds(1);
        }

        private void Start()
        {
            if (OnButtonGenerated != null) OnButtonGenerated(buttonToPress);
        }

        private void OnDestroy()
        {
            UnsubscribeFromEvents();
        }

        public void LaunchMinigame(PlayerNumber playerNumber)
        {
            this.playerNumber = playerNumber;
            InjectDependencies("InjectLastSecondPressLogicController");

            playerDeathEventChannel.OnEventPublished += OnPlayerDeath;
            playerGamePadSensor.OnAPressed += OnAButtonPressed;
            playerGamePadSensor.OnBPressed += OnBButtonPressed;
            playerGamePadSensor.OnXPressed += OnXButtonPressed;
            playerGamePadSensor.OnYPressed += OnYButtonPressed;

            StartCoroutine(StartCountdown());
        }

        private void UnsubscribeFromEvents()
        {
            if (playerGamePadSensor != null)
            {
                playerGamePadSensor.OnAPressed -= OnAButtonPressed;
                playerGamePadSensor.OnBPressed -= OnBButtonPressed;
                playerGamePadSensor.OnXPressed -= OnXButtonPressed;
                playerGamePadSensor.OnYPressed -= OnYButtonPressed;
                playerGamePadSensor = null;
            }

            if (playerDeathEventChannel != null)
            {
                playerDeathEventChannel.OnEventPublished -= OnPlayerDeath;
                playerDeathEventChannel = null;
            }
        }

        private void OnAButtonPressed()
        {
            if (isWaitingForButtonPress) EndMinigame(buttonToPress == XBoxButton.A);
        }

        private void OnBButtonPressed()
        {
            if (isWaitingForButtonPress) EndMinigame(buttonToPress == XBoxButton.B);
        }

        private void OnXButtonPressed()
        {
            if (isWaitingForButtonPress) EndMinigame(buttonToPress == XBoxButton.X);
        }

        private void OnYButtonPressed()
        {
            if (isWaitingForButtonPress) EndMinigame(buttonToPress == XBoxButton.Y);
        }

        private IEnumerator StartCountdown()
        {
            for (int i = 0; i < NumberOfTicks; i++)
            {
                yield return waitOneSecond;
                if (OnCountdownTick != null) OnCountdownTick();
            }
            isWaitingForButtonPress = true;
            yield return new WaitForSeconds(TimeWindowToPressButtonInSeconds);
            isWaitingForButtonPress = false;
            EndMinigame(false);
        }

        private void EndMinigame(bool isCompleted)
        {
            UnsubscribeFromEvents();

            if (OnMinigameEnd != null) OnMinigameEnd(isCompleted);

            OnMinigameEnd = null;
            OnButtonGenerated = null;
            OnCountdownTick = null;

            StopAllCoroutines();
            StartCoroutine(DestroyAfterSeconds(SecondsBeforeDestroy, isCompleted));
        }

        private IEnumerator DestroyAfterSeconds(float seconds, bool isCompleted)
        {
            yield return new WaitForSeconds(seconds);
            if (OnMinigameDestroy != null) OnMinigameDestroy(isCompleted);
            OnMinigameDestroy = null;
            Destroy(gameObject);
        }

        private void OnPlayerDeath(PlayerDeathEvent playerDeathEvent)
        {
            if (playerDeathEvent.DeadPlayer.GetComponent<PlayerController>().PlayerNumber == playerNumber)
            {
                EndMinigame(false);
            }
        }
    }
}


﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public class LastSecondPressUIController : GameScript
    {
        [SerializeField, Tooltip("The \"released\" sprites of each xBoxButton for the instructions")]
        private Sprite[] releasedButtonSprites;
        
        [SerializeField, Tooltip("The \"pressed\" sprites of each xBoxButton for the instructions")]
        private Sprite[] pressedButtonSprites;

        [SerializeField, Tooltip("The sprite which indicates success")]
        private Sprite successSprite;

        [SerializeField, Tooltip("The sprite which indicates failure")]
        private Sprite failureSprite;

        private LastSecondPressLogicController minigameLogicController;
        private SpriteRenderer[] spriteRenderers;
        private int currentCountdownIndex;
        private XBoxButton buttonToPress;

        private void InjectLastSecondPressUIController([GameObjectScope] LastSecondPressLogicController minigameLogicController,
                                                       [ChildScope][Named("UIElements")] GameObject UIElements)
        {
            this.minigameLogicController = minigameLogicController;
            spriteRenderers = UIElements.GetComponentsInChildren<SpriteRenderer>();
        }

        private void Awake()
        {
            InjectDependencies("InjectLastSecondPressUIController");
            currentCountdownIndex = 0;
        }

        private void OnEnable()
        {
            minigameLogicController.OnMinigameEnd += OnMinigameEnd;
            minigameLogicController.OnCountdownTick += OnCountdownTick;
            minigameLogicController.OnButtonGenerated += OnButtonGenerated;
        }

        private void OnMinigameEnd(bool isCompleted)
        {
            minigameLogicController.OnMinigameEnd -= OnMinigameEnd;
            minigameLogicController.OnCountdownTick -= OnCountdownTick;
            minigameLogicController.OnButtonGenerated -= OnButtonGenerated;

            spriteRenderers[currentCountdownIndex].sprite = isCompleted ? successSprite : failureSprite;
        }

        private void OnCountdownTick()
        {
            if (currentCountdownIndex < LastSecondPressLogicController.NumberOfTicks - 1)
            {
                spriteRenderers[currentCountdownIndex++].color = Color.yellow;
            }
            else
            {
                for (int i = 0; i < currentCountdownIndex; i++)
                {
                    spriteRenderers[i].color = Color.green;
                }
                spriteRenderers[currentCountdownIndex].sprite = releasedButtonSprites[(int)buttonToPress];
            }
        }

        private void OnButtonGenerated(XBoxButton button)
        {
            buttonToPress = button;
        }
    }
}
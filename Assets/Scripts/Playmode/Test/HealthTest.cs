﻿using System;
using System.Reflection;
using NUnit.Framework;
using UnityEngine;

namespace Achluophobia
{
    public class HealthTest
    {
        [Test]
        public void TestHealthConstruction()
        {
            Health health = CreateTestEnvironnement(100, 50);

            Assert.AreEqual(health.GetInitialHealth(), health.HealthPoints);
            Assert.AreEqual(50, health.GetInitialHealth());
            Assert.AreEqual(100, health.GetMaxHealth());
        }

        [Test]
        public void TestHealthConstructionWithInitialHealthOverMaxHealth()
        {
            Health health = CreateTestEnvironnement(100, 150);

            Assert.AreEqual(health.GetMaxHealth(), health.GetHealth());
        }

        [Test]
        public void TestHealthHealWithValidValue()
        {
            Health health = CreateTestEnvironnement(100, 50);

            health.Heal(25, false);

            Assert.AreEqual(health.GetInitialHealth() + 25, health.GetHealth());
        }

        [Test]
        public void TestHealthHealWithNegativeValue()
        {
            Health health = CreateTestEnvironnement(100, 50);

            Assert.Catch<ArgumentException>(() => health.Heal(-50, false));
        }

        [Test]
        public void TestHealthHealOverMaxHealth()
        {
            Health health = CreateTestEnvironnement(100, 50);

            health.Heal(100, false);

            Assert.AreEqual(health.GetMaxHealth(), health.GetHealth());
        }

        [Test]
        public void TestHealthHitWithValidValue()
        {
            Health health = CreateTestEnvironnement(100, 50);

            health.Hit(null, 25, false);

            Assert.AreEqual(health.GetInitialHealth() - 25, health.GetHealth());
        }

        [Test]
        public void TestHealthHitWithNegativeValue()
        {
            Health health = CreateTestEnvironnement(100, 50);

            Assert.Catch<ArgumentException>(() => health.Hit(null, -25, false));
        }

        [Test]
        public void TestHealthHitUnderMinimalHealth()
        {
            Health health = CreateTestEnvironnement(100, 50);

            health.Hit(null, 100, false);

            Assert.AreEqual(health.GetHealth(), 0);
        }

        [Test]
        public void TestLastAttacker()
        {
            Health health = CreateTestEnvironnement(100, 50);
            GameObject emptyAttacker = new GameObject();

            health.Hit(emptyAttacker, 25, false);

            Assert.AreEqual(emptyAttacker, health.LastAttacker);
        }

        private Health CreateTestEnvironnement(int maxHealth, int initialHealth)
        {
            GameObject healthObject = new GameObject();
            Health health = healthObject.AddComponent<Health>();

            FieldInfo fieldInfo = health.GetType().GetField("health", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            HealthData data = ScriptableObject.CreateInstance<HealthData>();
            data.InitialHealth = initialHealth;
            data.MaxHealth = maxHealth;
            fieldInfo.SetValue(health, data);

            health.Awake();

            return health;
        }
    }
}

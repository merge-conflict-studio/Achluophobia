﻿using System;
using System.Reflection;
using NUnit.Framework;
using UnityEngine;

namespace Achluophobia
{
    public class QuestItemTest
    {
        [Test]
        public void TestQuestItemConstruction()
        {
            QuestItemGenerator questItemGenerator = CreateTestEnvironnement(GameGlobalVariables.Level + 1, 0);

            Assert.AreEqual(questItemGenerator.GetComponent<QuestItemsData>().LevelName,
                (GameGlobalVariables.Level + 1));
            Assert.AreEqual(questItemGenerator.GetComponent<QuestItemsData>().LevelName,
                GameGlobalVariables.Level + questItemGenerator.GetLevelModifier());
        }

        [Test]
        public void TestQuestItemsAllCollected()
        {
            QuestItemGenerator questItemGenerator = CreateTestEnvironnement(GameGlobalVariables.Level + 1, 3);

            Assert.IsTrue(questItemGenerator.GetComponent<QuestItemsData>().CheckAllItemsCollected());
        }

        [Test]
        public void TestQuestItemsAdd()
        {
            QuestItemGenerator questItemGenerator = CreateTestEnvironnement(GameGlobalVariables.Level + 1, 1);

            questItemGenerator.tag = "Level1";

            questItemGenerator.GetComponent<QuestItemsData>().AddOneCollectedQuestItemsForTest();

            Assert.IsFalse(questItemGenerator.GetComponent<QuestItemsData>().CheckAllItemsCollected());

            questItemGenerator.GetComponent<QuestItemsData>().AddOneCollectedQuestItemsForTest();

            Assert.IsTrue(questItemGenerator.GetComponent<QuestItemsData>().CheckAllItemsCollected());
        }

        [Test]
        public void TestQuestItemsCollectingWithWrongTag()
        {
            QuestItemGenerator questItemGenerator = CreateTestEnvironnement(GameGlobalVariables.Level + 2, 2);

            questItemGenerator.tag = "Level1";

            questItemGenerator.GetComponent<QuestItemsData>().AddOneCollectedQuestItemsForTest();

            Assert.IsFalse(questItemGenerator.GetComponent<QuestItemsData>().CheckAllItemsCollected());
        }

        [Test]
        public void TestQuestItemsCollectingWithTooMuchItems()
        {
            QuestItemGenerator questItemGenerator = CreateTestEnvironnement(GameGlobalVariables.Level + 1, 2);

            questItemGenerator.tag = "Level1";

            questItemGenerator.GetComponent<QuestItemsData>().AddOneCollectedQuestItemsForTest();
            questItemGenerator.GetComponent<QuestItemsData>().AddOneCollectedQuestItemsForTest();

            Assert.Catch<ArgumentOutOfRangeException>(() =>
                questItemGenerator.GetComponent<QuestItemsData>().AddOneCollectedQuestItemsForTest());
        }

        [Test]
        public void TestQuestItemsCollectingWithInvalidTag()
        {
            QuestItemGenerator questItemGenerator = CreateTestEnvironnement(GameGlobalVariables.Level + 1, 2);

            Assert.Catch<ArgumentNullException>(() =>
                questItemGenerator.GetComponent<QuestItemsData>().AddOneCollectedQuestItemsForTest());
        }

        private QuestItemGenerator CreateTestEnvironnement(string levelModifier, int itemsCollected)
        {
            GameObject questItemObject = new GameObject();
            QuestItemGenerator questItemGenerator = questItemObject.AddComponent<QuestItemGenerator>();

            FieldInfo fieldInfoQuestItemsArray = questItemGenerator.GetType().GetField("questItemsArray",
                BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            GameObject[] questItemsArray = fieldInfoQuestItemsArray.GetValue(questItemGenerator) as GameObject[];

            questItemsArray = new GameObject[3];

            QuestItemsData data = questItemObject.AddComponent<QuestItemsData>();
            data.LevelName = levelModifier;
            data.ItemsCollected = itemsCollected;
            data.ItemsToCollect = questItemsArray.Length;

            questItemGenerator.GenerateTest(int.Parse(levelModifier.Substring(levelModifier.Length - 1)));

            return questItemGenerator;
        }
    }
}
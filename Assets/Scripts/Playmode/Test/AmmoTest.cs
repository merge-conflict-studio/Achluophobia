﻿using System;
using UnityEngine;
using NUnit.Framework;
using System.Reflection;

namespace Achluophobia
{
    public class AmmoTest
    {
        [Test]
        public void AmmoGoesDownAfterGunShoots()
        {
            Ammo ammo = CreateTestEnvironment(30, 60);
            ammo.Deplete(0);
            Assert.AreEqual(ammo.GetAmmosLeftInWeapon(), 29);
        }

        [Test]
        public void AmmoDoesNotGoUnderMagazineLimit()
        {
            Ammo ammo = CreateTestEnvironment(30, 1);
            for (int i = 0; i < 40; i++)
            {
                ammo.Deplete(0);
            }
            Assert.AreEqual(ammo.GetAmmosLeftInWeapon(), 0);
        }

        [Test]
        public void AmmoGoesUpOnReload()
        {
            Ammo ammo = CreateTestEnvironment(30, 60);
            ammo.Deplete(0);
            ammo.ReloadMock(1);
            Assert.AreEqual(ammo.GetAmmosLeftInWeapon(), 30);
        }

        [Test]
        public void AmmoOnPlayerGoesDownOnReload()
        {
            Ammo ammo = CreateTestEnvironment(30, 60);
            ammo.Deplete(0);
            ammo.ReloadMock(1);
            Assert.AreEqual(ammo.GetAmmosLeftOnPlayer(), 59);
        }

        [Test]
        public void AmmoDoesNotReloadOnNegativeReloadTime()
        {
            Ammo ammo = CreateTestEnvironment(30, 60);
            ammo.Deplete(0);
            Assert.Catch<ArgumentException>(() => ammo.ReloadMock(-1));
        }

        [Test]
        public void CannotPickUpNegativeAmmo()
        {
            Ammo ammo = CreateTestEnvironment(30, 60);
            Assert.Catch<ArgumentException>(() => ammo.PickUpAmmo(-50));
        }

        [Test]
        public void BurnerUsesAmmo()
        {
            Ammo ammo = CreateTestEnvironment(0, 60);
            ammo.DepleteForBurner(20);
            Assert.AreEqual(ammo.GetAmmosLeftInWeapon(), 40);
        }

        [Test]
        public void BurnerDoesNotUseMoreAmmoThanItShould()
        {
            Ammo ammo = CreateTestEnvironment(0, 60);
            ammo.DepleteForBurner(80);
            Assert.AreEqual(ammo.GetAmmosLeftInWeapon(), 0);
        }

        [Test]
        public void CanPickUpAmmoForBurner()
        {
            Ammo ammo = CreateTestEnvironment(0, 80);
            ammo.DepleteForBurner(20);
            ammo.PickUpAmmo(20);
            Assert.AreEqual(ammo.GetAmmosLeftInWeapon(), 80);
        }

        [Test]
        public void CanPickUpAmmo()
        {
            Ammo ammo = CreateTestEnvironment(1, 60); //Doit setter le ammo a 1 pour ne pas avoir un effet de bruleur
            ammo.Deplete(0);
            ammo.PickUpAmmo(60);
            Assert.AreEqual(ammo.GetAmmosLeftOnPlayer(), 60);
        }

        [Test]
        public void CannotPickUpMoreAmmoThanAmmoLimit()
        {
            Ammo ammo = CreateTestEnvironment(1, 60); //Doit setter le ammo a 1 pour ne pas avoir un effet de bruleur
            ammo.Deplete(0);
            ammo.PickUpAmmo(80);
            Assert.AreEqual(ammo.GetAmmosLeftOnPlayer(), 60);
        }

        private Ammo CreateTestEnvironment(int initialAmmo, int maximumAmmo)
        {
            Ammo.IsTestMode = true;
            GameObject gameObject = new GameObject();
            Ammo ammo = gameObject.AddComponent<Ammo>();

            AmmoData ammoData = ScriptableObject.CreateInstance<AmmoData>();
            ammoData.MagasineSize = initialAmmo;
            ammoData.MaxAmmoOnPlayer = maximumAmmo;

            FieldInfo fieldInfo = ammo.GetType().GetField("ammo", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            fieldInfo.SetValue(ammo, ammoData);

            ammo.Awake();
            return ammo;
        }
    }
}
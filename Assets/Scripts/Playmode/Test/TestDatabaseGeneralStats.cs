﻿using NUnit.Framework;
using UnityEngine;
using Harmony;

//Tout mis en commentaire parce que lorsque mes tests marchaient, sa mettait des erreurs dans la base de donnée du jeu donc j'ai dût tout mettre 
// en commentaire pour faire marcher la base de données du jeu.
//- Mathieu Carrier
namespace Achluophobia
{
    public class TestDatabaseGeneralStats
    {
        //private GameObject applicationController;
        //private GeneralStatsRepository generalStatsRepository;
        //private SqLiteConnectionFactory sqLiteConnection;

        //[SetUp]
        //public void Setup()
        //{
        //    applicationController = new GameObject { tag = R.S.Tag.ApplicationDependencies };

        //    applicationController.AddComponent<ApplicationConfiguration>();
        //    applicationController.AddComponent<SqLiteParameterFactory>();
        //    sqLiteConnection = applicationController.AddComponent<SqLiteConnectionFactory>();
        //    sqLiteConnection.BeginTransaction();


        //    GameObject gameObject = new GameObject();
        //    generalStatsRepository = gameObject.AddComponent<GeneralStatsRepository>();
        //    generalStatsRepository.Awake();
        //}

        //[Test]
        //public void TestGeneralStatsRepositoryInsert()
        //{
        //    Assert.IsNull(generalStatsRepository.GetStatsFromId(1));

        //    GeneralStats generalStats1 = new GeneralStats
        //    {
        //        Id = 1,
        //        BaseZombieKilled = 1
        //    };
        //    generalStatsRepository.AddStats(generalStats1);
        //    Assert.AreEqual(generalStats1.BaseZombieKilled, generalStatsRepository.GetStatsFromId(1).BaseZombieKilled);
        //}

        //[Test]
        //public void TestGeneralStatsRepositorySelectExistingTable()
        //{
        //    GeneralStats generalStats1 = new GeneralStats
        //    {
        //        Id = 1,
        //        BaseZombieKilled = 1
        //    };
        //    GeneralStats generalStats2 = new GeneralStats
        //    {
        //        Id = 2,
        //        BaseZombieKilled = 2
        //    };

        //    generalStatsRepository.AddStats(generalStats1);
        //    generalStatsRepository.AddStats(generalStats2);

        //    Assert.AreEqual(generalStats1.BaseZombieKilled, generalStatsRepository.GetStatsFromId(1).BaseZombieKilled);
        //    Assert.AreEqual(generalStats2.BaseZombieKilled, generalStatsRepository.GetStatsFromId(2).BaseZombieKilled);
        //}

        //[Test]
        //public void TestGeneralStatsRepositorySelectNonExistingTable()
        //{
        //    Assert.IsNull(generalStatsRepository.GetStatsFromId(1));
        //}

        //[Test]
        //public void TestGeneralStatsRepositoryUpdateStats()
        //{
        //    GeneralStats generalStats1 = new GeneralStats
        //    {
        //        Id = 1,
        //        BaseZombieKilled = 1
        //    };
        //    GeneralStats generalStats2 = new GeneralStats
        //    {
        //        Id = 1,
        //        BaseZombieKilled = 2
        //    };

        //    generalStatsRepository.AddStats(generalStats1);

        //    Assert.AreEqual(generalStats1.BaseZombieKilled, generalStatsRepository.GetStatsFromId(1).BaseZombieKilled);

        //    generalStatsRepository.ChangePlayerGeneralStats(generalStats2);

        //    Assert.AreEqual(generalStats2.BaseZombieKilled, generalStatsRepository.GetStatsFromId(1).BaseZombieKilled);
        //}

        //[TearDown]
        //public void TearDown()
        //{
        //    sqLiteConnection.RollbackTransaction();
        //    Object.Destroy(applicationController);
        //}
    }
}

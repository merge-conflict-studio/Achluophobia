﻿using System;
using System.Reflection;
using UnityEngine;
using NUnit.Framework;

namespace Achluophobia
{
    public class PlayerSpawnerTest
    {
        private GameObject playerPrefab;

        [OneTimeSetUp]
        public void SetUp()
        {
            PlayerSpawner.IsTestMode = true;
        }

        [OneTimeTearDown]
        public void TearDown()
        {
            PlayerSpawner.IsTestMode = false;
        }

        [Test]
        public void TestAssaultSpawnedIsNotPrefab()
        {
            PlayerSpawner spawner = CreateTestEnvironement(
                PlayerClass.Assault,
                new Vector3(0, 0, 0),
                Quaternion.identity);

            GameObject player = spawner.Spawn(PlayerClass.Assault, new Vector3(0, 0, 0));
            Assert.AreNotSame(player, playerPrefab);
        }

        [Test]
        public void TestMedicSpawnedIsNotPrefab()
        {
            PlayerSpawner spawner = CreateTestEnvironement(
                PlayerClass.Medic,
                new Vector3(0, 0, 0),
                Quaternion.identity);

            GameObject player = spawner.Spawn(PlayerClass.Medic, new Vector3(0, 0, 0));
            Assert.AreNotSame(player, playerPrefab);
        }

        [Test]
        public void TestScoutSpawnedIsNotPrefab()
        {
            PlayerSpawner spawner = CreateTestEnvironement(
                PlayerClass.Scout,
                new Vector3(0, 0, 0),
                Quaternion.identity);

            GameObject player = spawner.Spawn(PlayerClass.Scout, new Vector3(0, 0, 0));
            Assert.AreNotSame(player, playerPrefab);
        }

        [Test]
        public void TestMastodontetSpawnedIsNotPrefab()
        {
            PlayerSpawner spawner = CreateTestEnvironement(
                PlayerClass.Mastodonte,
                new Vector3(0, 0, 0),
                Quaternion.identity);

            GameObject player = spawner.Spawn(PlayerClass.Mastodonte, new Vector3(0, 0, 0));
            Assert.AreNotSame(player, playerPrefab);
        }

        [Test]
        public void TestIncineratorSpawnedIsNotPrefab()
        {
            PlayerSpawner spawner = CreateTestEnvironement(
                PlayerClass.Incinerator,
                new Vector3(0, 0, 0),
                Quaternion.identity);

            GameObject player = spawner.Spawn(PlayerClass.Incinerator, new Vector3(0, 0, 0));
            Assert.AreNotSame(player, playerPrefab);
        }

        [Test]
        public void TestPlayerSpawnedAtGoodPosition()
        {
            Vector3 position = new Vector3(15, 13, 5);
            PlayerSpawner spawner = CreateTestEnvironement(
                PlayerClass.Assault,
                new Vector3(0, 0, 0),
                Quaternion.identity);

            GameObject player = spawner.Spawn(PlayerClass.Assault, position);

            Assert.AreEqual(position, player.transform.position);
        }

        [Test]
        public void TestPlayerSpawnedAtGoodRotation()
        {
            Quaternion rotation = Quaternion.Euler(5, 10, 7);
            PlayerSpawner spawner = CreateTestEnvironement(
                PlayerClass.Assault,
                new Vector3(0, 0, 0),
                rotation);

            GameObject player = spawner.Spawn(PlayerClass.Assault, new Vector3(0, 0, 0));

            Assert.AreEqual(rotation, player.transform.rotation);
        }

        private PlayerSpawner CreateTestEnvironement(PlayerClass playerClass, Vector3 prefabPosition, Quaternion prefabRotation)
        {
            GameObject gameObject = new GameObject();
            PlayerSpawner spawner = gameObject.AddComponent<PlayerSpawner>();
            playerPrefab = new GameObject();
            playerPrefab.transform.position = prefabPosition;
            playerPrefab.transform.rotation = prefabRotation;

            FieldInfo fieldInfo = spawner.GetType().GetField(
                GetPrefabName(playerClass),
                BindingFlags.Public |
                BindingFlags.NonPublic |
                BindingFlags.Instance);

            if (fieldInfo == null) throw new NullReferenceException("The fieldinfo in PlayerSpawner is unaccessible");

            fieldInfo.SetValue(spawner, playerPrefab);

            return spawner;
        }

        private static string GetPrefabName(PlayerClass playerClass)
        {
            string name = string.Empty;

            switch (playerClass)
            {
                case PlayerClass.Assault:
                    name = "assaultPrefab";
                    break;
                case PlayerClass.Medic:
                    name = "medicPrefab";
                    break;
                case PlayerClass.Scout:
                    name = "scoutPrefab";
                    break;
                case PlayerClass.Mastodonte:
                    name = "mastodontePrefab";
                    break;
                case PlayerClass.Incinerator:
                    name = "incineratorPrefab";
                    break;
            }
            return name;
        }
    }
}


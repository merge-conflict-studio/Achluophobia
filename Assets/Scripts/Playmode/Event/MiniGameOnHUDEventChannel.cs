﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/MiniGameOnHUDEventChannel")]
    public class MiniGameOnHUDEventChannel : EventChannel<MiniGameOnHUDEvent>
    {
    }
}

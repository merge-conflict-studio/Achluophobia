﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public class AchievementPopupEvent : IEvent
    {
        public GameAchievements GameAchievement;
        public ClassAchievements ClassAchievement;

        public AchievementPopupEvent(GameAchievements gameAchievements)
        {
            GameAchievement = gameAchievements;
        }

        public AchievementPopupEvent(ClassAchievements classAchievements)
        {
            ClassAchievement = classAchievements;
        }
    }
}

﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/GameEndEventPublisher")]
    public class GameEndEventPublisher : GameScript
    {
        private GameEndEventChannel eventChannel;
        private GameController gameController;

        private void InjectGameEndEventPublisher([EventChannelScope] GameEndEventChannel eventChannel,
                                                 [EntityScope] GameController gameController)
        {
            this.eventChannel = eventChannel;
            this.gameController = gameController;
        }

        private void Awake()
        {
            InjectDependencies("InjectGameEndEventPublisher");
        }

        private void OnEnable()
        {
            gameController.OnGameOver += OnGameEnd;
        }

        private void OnDisable()
        {
            gameController.OnGameOver -= OnGameEnd;
        }

        private void OnGameEnd(EndingType endingType, PlayerClass[] players)
        {
            eventChannel.Publish(new GameEndEvent(endingType, players));
        }
    }
}

﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/PlayerRespawnEventPublisher")]
    public class PlayerRespawnEventPublisher : GameScript
    {
        private VersusController versusController1;
        private VersusController versusController2;
        private PlayerRespawnEventChannel eventChannel;
        private RespawnerSensor respawnerSensor;

        private void InjectPlayerRespawnEventPublisher([Named(R.S.GameObject.VersusController1)][SceneScope] VersusController versusController1,
                                                       [Named(R.S.GameObject.VersusController2)][SceneScope] VersusController versusController2,
                                                       [EventChannelScope] PlayerRespawnEventChannel eventChannel,
                                                       [EntityScope] RespawnerSensor respawnerSensor)
        {
            this.versusController1 = versusController1;
            this.versusController2 = versusController2;
            this.eventChannel = eventChannel;
            this.respawnerSensor = respawnerSensor;
        }

        private void Awake()
        {
            InjectDependencies("InjectPlayerRespawnEventPublisher");
        }

        private void OnEnable()
        {
            respawnerSensor.OnRespawnPlayer += OnRespawn;
            versusController1.OnPlayerRespawn += OnRespawn;
            versusController2.OnPlayerRespawn += OnRespawn;
        }

        private void OnDisable()
        {
            respawnerSensor.OnRespawnPlayer -= OnRespawn;
            versusController1.OnPlayerRespawn -= OnRespawn;
            versusController2.OnPlayerRespawn -= OnRespawn;
        }

        private void OnRespawn(GameObject playerToRespawn, Vector3 positionToSpawn, bool isReadyToRespawn)
        {
            eventChannel.Publish(new PlayerRespawnEvent(playerToRespawn,positionToSpawn, isReadyToRespawn));
        }
    }
}
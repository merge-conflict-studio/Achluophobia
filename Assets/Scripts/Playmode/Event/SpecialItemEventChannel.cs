﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/SpecialItemEventChannel")]
    public class SpecialItemEventChannel : EventChannel<SpecialItemEvent>
    {
    }
}

﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/ItemsOnHUDEventChannel")]
    public class ItemsOnHUDEventChannel : EventChannel<ItemsOnHUDEvent>
    {
    }
}
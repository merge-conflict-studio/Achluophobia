﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public class PlayerHealthEvent : IEvent
    {
        public PlayerNumber PlayerNumber { get; private set; }

        public Health PlayerHealth { get; private set; }

        public float OldHealthPoints { get; private set; }

        public float NewHealthPoints { get; private set; }

        public PlayerHealthEvent(PlayerNumber playerNumber, Health playerHealth, float oldHealthPoints,
            float newHealthPoints)
        {
            PlayerNumber = playerNumber;
            PlayerHealth = playerHealth;
            OldHealthPoints = oldHealthPoints;
            NewHealthPoints = newHealthPoints;
        }
    }
}
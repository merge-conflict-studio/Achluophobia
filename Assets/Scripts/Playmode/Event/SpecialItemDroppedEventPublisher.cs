﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/SpecialItemDroppedEventPublisher")]
    public class SpecialItemDroppedEventPublisher : GameScript
    {
        private SpecialItemEventChannel eventChannel;
        private PlayerController playerController;

        private void InjectSpecialItemDroppedEventPublisher([EventChannelScope] SpecialItemEventChannel eventChannel,
                                                            [EntityScope] PlayerController playerController)
        {
            this.eventChannel = eventChannel;
            this.playerController = playerController;
        }

        private void Awake()
        {
            InjectDependencies("InjectSpecialItemDroppedEventPublisher");
        }

        private void OnEnable()
        {
            playerController.OnTryingToSpawnSpecialItem += OnSpecialItemSpawn;
        }

        private void OnDisable()
        {
            playerController.OnTryingToSpawnSpecialItem -= OnSpecialItemSpawn;
        }

        private void OnSpecialItemSpawn()
        {
            eventChannel.Publish(new SpecialItemEvent(playerController.GetPlayerClass(), 1));
        }
    }
}

﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/SpecialItemOnHUDEventChannel")]
    public class SpecialItemOnHUDEventChannel : EventChannel<SpecialItemOnHUDEvent>
    {
    }

}

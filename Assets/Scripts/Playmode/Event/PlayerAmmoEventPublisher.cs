﻿using Harmony;
using UnityEngine;
using XInputDotNetPure;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/PlayerAmmoEventPublisher")]
    public class PlayerAmmoEventPublisher : GameScript
    {
        private Ammo ammo;
        private PlayerAmmoEventChannel eventChannel;
        private PlayerNumber playerNumber;

        private void InjectPlayerAmmoEventPublisher([GameObjectScope] Ammo ammo,
                                                    [EventChannelScope] PlayerAmmoEventChannel eventChannel,
                                                    [EntityScope] PlayerController playerController)
        {
            this.ammo = ammo;
            this.eventChannel = eventChannel;
            playerNumber = playerController.PlayerNumber;
        }

        private void Awake()
        {
            InjectDependencies("InjectPlayerAmmoEventPublisher");
        }

        private void OnEnable()
        {
            ammo.OnAmmoChanged += OnAmmoChanged;
        }

        private void OnDisable()
        {
            ammo.OnAmmoChanged -= OnAmmoChanged;
        }

        private void OnAmmoChanged(int ammoInWeapon, int ammoOnPlayer, bool isAmmoUnlimited)
        {
            eventChannel.Publish(new PlayerAmmoEvent(playerNumber, ammoInWeapon, ammoOnPlayer, isAmmoUnlimited));
        }
    }
}
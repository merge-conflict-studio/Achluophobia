﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/MiniGameEndEventPublisher")]
    public class MiniGameEndEventPublisher : GameScript
    {
        private MinigameSensor minigameSensor;
        private MiniGameEndEventChannel eventChannel;

        private void InjectMiniGameEndEventPublisher([EntityScope] MinigameSensor minigameSensor,
                                                     [EventChannelScope] MiniGameEndEventChannel eventChannel)
        {
            this.minigameSensor = minigameSensor;
            this.eventChannel = eventChannel;
        }

        private void Awake()
        {
            InjectDependencies("InjectMiniGameEndEventPublisher");
        }

        private void OnEnable()
        {
            minigameSensor.OnMinigameEnd += OnMiniGameEnd;
        }

        private void OnDisable()
        {
            minigameSensor.OnMinigameEnd -= OnMiniGameEnd;
        }

        private void OnMiniGameEnd(bool isSuccessful)
        {
            eventChannel.Publish(new MiniGameEndEvent(isSuccessful));
        }
    }
}
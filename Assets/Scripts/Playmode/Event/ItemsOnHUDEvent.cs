﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public class ItemsOnHUDEvent : IEvent
    {
        public int ItemsLeft { get; private set; }

        public GameObject ItemToCollect { get; private set; }

        public ItemsOnHUDEvent(int itemsLeft, GameObject itemToCollect)
        {
            ItemsLeft = itemsLeft;
            ItemToCollect = itemToCollect;
        }
    }
}
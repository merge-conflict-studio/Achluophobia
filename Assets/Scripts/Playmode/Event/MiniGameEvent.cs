﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public class MiniGameEvent : IEvent
    {
        public GameObject MissionProp { get; private set; }

        public MiniGameEvent(GameObject missionProp)
        {
            MissionProp = missionProp;
        }
    }
}

﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/ZombieDeathEventChannel")]
    public class ZombieDeathEventChannel : EventChannel<ZombieDeathEvent>
    {
    }
}

﻿using Harmony;

namespace Achluophobia
{
    public class DeathEvent : IEvent
    {
        public R.E.Prefab DeadPrefab { get; private set; }

        public DeathEvent(R.E.Prefab deadPrefab)
        {
            DeadPrefab = deadPrefab;
        }
    }
}
﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public class MiniGameOnHUDEvent : IEvent
    {
        public int MiniGamesLeft { get; private set; }

        public GameObject MiniGamesToFinish { get; private set; }

        public MiniGameOnHUDEvent(int miniGamesLeft, GameObject miniGamesToFinish)
        {
            MiniGamesLeft = miniGamesLeft;
            MiniGamesToFinish = miniGamesToFinish;
        }
    }
}

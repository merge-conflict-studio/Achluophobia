﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/GamePauseEventChannel")]
    public class GamePauseEventChannel : EventChannel<GamePauseEvent>
    {
    }
}
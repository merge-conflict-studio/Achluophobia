﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/ZombieDeathEventPublisher")]
    public class ZombieDeathEventPublisher : GameScript
    {
        private ZombieDeathEventChannel eventChannel;
        private Health health;
        private ZombieController zombieController;

        private void InjectZombieDeathEventPublisher([EventChannelScope] ZombieDeathEventChannel eventChannel,
                                                     [EntityScope] Health health,
                                                     [EntityScope] ZombieController zombieController)
        {
            this.eventChannel = eventChannel;
            this.health = health;
            this.zombieController = zombieController;
        }

        private void Awake()
        {
            InjectDependencies("InjectZombieDeathEventPublisher");
        }

        private void OnEnable()
        {
            health.OnDeath += OnDeath;
        }

        private void OnDisable()
        {
            health.OnDeath -= OnDeath;
        }

        private void OnDeath(GameObject killer)
        {
            if (killer != null && killer.CompareTag(R.S.Tag.Player))
            {
                eventChannel.Publish(new ZombieDeathEvent(killer.GetComponent<PlayerController>().GetPlayerClass(), zombieController.ZombieClass.ZombieType));
            }

        }
    }
}

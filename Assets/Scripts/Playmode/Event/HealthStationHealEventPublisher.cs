﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/HealthStationHealEventPublisher")]
    public class HealthStationHealEventPublisher : GameScript
    {
        private SpecialItemEventChannel eventChannel;
        private Health health;

        private void InjectHealthStationHealEventPublisher([EventChannelScope] SpecialItemEventChannel eventChannel,
                                                           [EntityScope] Health health)
        {
            this.eventChannel = eventChannel;
            this.health = health;
        }

        private void Awake()
        {
            InjectDependencies("InjectHealthStationHealEventPublisher");
        }

        private void OnEnable()
        {
            health.OnHealthStationHealing += OnHealthStationHealing;
        }

        private void OnDisable()
        {
            health.OnHealthStationHealing -= OnHealthStationHealing;
        }

        private void OnHealthStationHealing(int ammountOfHealthGiven)
        {
            eventChannel.Publish(new SpecialItemEvent(PlayerClass.Medic, ammountOfHealthGiven));
        }
    }
}

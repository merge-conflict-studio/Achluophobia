﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/QuestItemsEventChannel")]
    public class QuestItemsEventChannel : EventChannel<QuestItemsEvent>
    {
    }
}
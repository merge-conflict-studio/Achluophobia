﻿using Harmony;

namespace Achluophobia
{
    public class AchievementSuccessEvent : IEvent
    {
        public string AchievementName { get; private set; }
        public bool IsGameAchievement { get; private set; }

        public AchievementSuccessEvent(string achievementName, bool isGameAchievement)
        {
            AchievementName = achievementName;
            IsGameAchievement = isGameAchievement;
        }
    }
}

﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/CreationEventChannel")]
    public class CreationEventChannel : EventChannel<CreationEvent>
    {
    }
}
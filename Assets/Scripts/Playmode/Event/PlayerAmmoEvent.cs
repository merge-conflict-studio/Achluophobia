﻿using Harmony;

namespace Achluophobia
{
    public class PlayerAmmoEvent : IEvent
    {
        public PlayerNumber PlayerNumber { get; private set; }

        public int AmmoInWeapon { get; private set; }

        public int AmmoOnPlayer { get; private set; }

        public bool IsAmmoUnlimited { get; private set; }

        public PlayerAmmoEvent(PlayerNumber playerNumber, int ammoInWeapon, int ammoOnPlayer, bool isAmmoUnlimited)
        {
            PlayerNumber = playerNumber;
            AmmoInWeapon = ammoInWeapon;
            AmmoOnPlayer = ammoOnPlayer;
            IsAmmoUnlimited = isAmmoUnlimited;
        }
    }
}
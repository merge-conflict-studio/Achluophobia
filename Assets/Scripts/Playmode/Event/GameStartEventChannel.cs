﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/GameStartEventChannel")]
    public class GameStartEventChannel : EventChannel<GameStartEvent>
    {
    }
}

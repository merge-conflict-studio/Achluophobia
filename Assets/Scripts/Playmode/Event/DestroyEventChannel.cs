﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/DestroyEventChannel")]
    public class DestroyEventChannel : EventChannel<DestroyEvent>
    {
    }
}
﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/PlayerSwitchWeaponEventChannel")]
    public class PlayerSwitchWeaponEventChannel : EventChannel<PlayerSwitchWeaponEvent>
    {
    }
}

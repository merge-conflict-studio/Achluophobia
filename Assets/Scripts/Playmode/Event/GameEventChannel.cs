﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/GameEventChannel")]
    public class GameEventChannel : EventChannel<GameEvent>
    {
    }
}
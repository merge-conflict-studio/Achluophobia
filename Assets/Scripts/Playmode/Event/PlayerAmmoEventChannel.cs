﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/PlayerAmmoEventChannel")]
    public class PlayerAmmoEventChannel : EventChannel<PlayerAmmoEvent>
    {
    }
}
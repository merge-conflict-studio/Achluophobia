﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public class PlayerRespawnEvent : IEvent
    {
        public GameObject DeadPlayer { get; private set; }
        public Vector3 PositionToRespawn { get; private set; }
        public bool IsReadyToRespawn { get; private set; }

        public PlayerRespawnEvent(GameObject deadPlayer, Vector3 positionToRespawn, bool isReadyToRespawn)
        {
            DeadPlayer = deadPlayer;
            PositionToRespawn = positionToRespawn;
            IsReadyToRespawn = isReadyToRespawn;
        }
    }
}

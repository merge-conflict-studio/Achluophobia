﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/ItemsOnHUDEventPublisher")]
    public class ItemsOnHUDEventPublisher : GameScript
    {
        private MiniGameController miniGameController;
        private ItemsOnHUDEventChannel eventChannel;

        private void InjectItemsOnHUDEventPublisher([EventChannelScope] ItemsOnHUDEventChannel eventChannel,
                                                    [EntityScope] MiniGameController miniGameController)
        {
            this.miniGameController = miniGameController;
            this.eventChannel = eventChannel;
        }

        private void Awake()
        {
            InjectDependencies("InjectItemsOnHUDEventPublisher");
        }

        private void OnEnable()
        {
            miniGameController.OnQuestItemRemovedFromLevel += OnQuestItemPickedUp;
        }

        private void OnDisable()
        {
            miniGameController.OnQuestItemRemovedFromLevel -= OnQuestItemPickedUp;
        }

        private void OnQuestItemPickedUp(int numberOfItemsLeft, GameObject itemToCollect)
        {
            eventChannel.Publish(new ItemsOnHUDEvent(numberOfItemsLeft, itemToCollect));
        }
    }
}
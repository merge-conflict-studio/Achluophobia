﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/PlayerAchievementSuccessEventPublisher")]
    public class PlayerAchievementSuccessEventPublisher : GameScript
    {
        private AchievementSuccessEventChannel eventChannel;
        private PlayerController playerController;

        private void InjectPlayerAchievementSuccessEventPublisher([EntityScope] PlayerController playerController,
                                                                  [EventChannelScope] AchievementSuccessEventChannel eventChannel)
        {
            this.playerController = playerController;
            this.eventChannel = eventChannel;
        }

        private void Awake()
        {
            InjectDependencies("InjectPlayerAchievementSuccessEventPublisher");
        }

        private void OnEnable()
        {
            playerController.OnGameAchievementSucceeded += OnAchievementSuccess;
        }

        private void OnDisable()
        {
            playerController.OnGameAchievementSucceeded -= OnAchievementSuccess;
        }

        private void OnAchievementSuccess(string achievementName)
        {
            eventChannel.Publish(new AchievementSuccessEvent(achievementName, true));
        }
    }
}

﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/MiniGameOnHUDEventPublisher")]
    public class MiniGameOnHUDEventPublisher : GameScript
    {
        private ChangeLevelOnMiniGamesFinished changeLevelOnMiniGamesFinished;
        private MiniGameOnHUDEventChannel eventChannel;

        private void InjectMiniGameOnHUDEventPublisher([EventChannelScope]MiniGameOnHUDEventChannel eventChannel,
                                                       [EntityScope] ChangeLevelOnMiniGamesFinished changeLevelOnMiniGamesFinished)
        {
            this.changeLevelOnMiniGamesFinished = changeLevelOnMiniGamesFinished;
            this.eventChannel = eventChannel;
        }

        private void Awake()
        {
            InjectDependencies("InjectMiniGameOnHUDEventPublisher");
        }

        private void OnEnable()
        {
            changeLevelOnMiniGamesFinished.OnMiniGameCompleted += OnMiniGameFinished;
        }

        private void OnDisable()
        {
            changeLevelOnMiniGamesFinished.OnMiniGameCompleted -= OnMiniGameFinished;
        }

        private void OnMiniGameFinished(int numbersOfMiniGameLeft, GameObject miniGamesToFinish)
        {
            eventChannel.Publish(new MiniGameOnHUDEvent(numbersOfMiniGameLeft, miniGamesToFinish));
        }
    }
}

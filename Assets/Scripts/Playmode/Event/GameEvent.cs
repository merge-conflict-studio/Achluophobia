﻿using Harmony;

namespace Achluophobia
{
    public abstract class GameEvent : IEvent
    {
        public bool HasGameEnded { get; private set; }

        protected GameEvent(bool hasGameEnded)
        {
            HasGameEnded = hasGameEnded;
        }
    }
}
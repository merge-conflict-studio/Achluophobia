﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/MiniGameEventPublisher")]
    public class MiniGameEventPublisher : GameScript
    {
        private MinigameSensor minigameSensor;
        private MiniGameEventChannel eventChannel;

        private void InjectMiniGameEventPublisher([EntityScope] MinigameSensor minigameSensor,
                                                  [EventChannelScope] MiniGameEventChannel eventChannel)
        {
            this.minigameSensor = minigameSensor;
            this.eventChannel = eventChannel;
        }

        private void Awake()
        {
            InjectDependencies("InjectMiniGameEventPublisher");
        }

        private void OnEnable()
        {
            minigameSensor.OnMinigameSuccessful += OnMinigameSuccessful;
        }

        private void OnDisable()
        {
            minigameSensor.OnMinigameSuccessful -= OnMinigameSuccessful;
        }

        private void OnMinigameSuccessful(GameObject minigame)
        {
            eventChannel.Publish(new MiniGameEvent(minigame));
        }
    }
}

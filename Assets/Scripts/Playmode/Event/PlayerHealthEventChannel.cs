﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/PlayerHealthEventChannel")]
    public class PlayerHealthEventChannel : EventChannel<PlayerHealthEvent>
    {
    }
}
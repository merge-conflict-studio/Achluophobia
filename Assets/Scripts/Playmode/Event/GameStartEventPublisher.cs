﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/GameStartEventPublisher")]
    public class GameStartEventPublisher : GameScript
    {
        private GameStartEventChannel eventChannel;
        private GameController gameController;

        private void InjectGameStartEventPublisher([EventChannelScope] GameStartEventChannel eventChannel,
                                                   [EntityScope] GameController gameController)
        {
            this.eventChannel = eventChannel;
            this.gameController = gameController;
        }

        private void Awake()
        {
            InjectDependencies("InjectGameStartEventPublisher");
        }

        private void OnEnable()
        {
            gameController.OnGameStart += OnGameStart;
        }

        private void OnDisable()
        {
            gameController.OnGameStart -= OnGameStart;
        }

        private void OnGameStart()
        {
            eventChannel.Publish(new GameStartEvent());
        }
    }
}

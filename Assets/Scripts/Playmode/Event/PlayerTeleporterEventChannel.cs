﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/PlayerTeleporterEventChannel")]
    public class PlayerTeleporterEventChannel : EventChannel<PlayerTeleporterEvent>
    {
    }
}
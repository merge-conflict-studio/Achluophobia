﻿using Harmony;

namespace Achluophobia
{
    public class GameEndEvent : IEvent
    {
        public EndingType Ending { get; private set; }
        public PlayerClass[] Players { get; private set; }

        public GameEndEvent(EndingType ending, PlayerClass[] players)
        {
            Ending = ending;
            Players = players;
        }
    }
}

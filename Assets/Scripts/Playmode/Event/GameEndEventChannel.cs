﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/GameEndEventChannel")]
    public class GameEndEventChannel : EventChannel<GameEndEvent>
    {
    }
}

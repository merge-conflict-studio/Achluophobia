﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/QuestItemsEventPublisher")]
    public class QuestItemsEventPublisher : GameScript
    {
        private QuestItemStimulus questItemStimulus;
        private QuestItemsEventChannel eventChannel;

        private void InjectQuestItemsEventPublisher([EntityScope] QuestItemStimulus questItemStimulus,
                                                    [EventChannelScope] QuestItemsEventChannel eventChannel)
        {
            this.questItemStimulus = questItemStimulus;
            this.eventChannel = eventChannel;
        }

        private void Awake()
        {
            InjectDependencies("InjectQuestItemsEventPublisher");
        }

        private void OnEnable()
        {
            questItemStimulus.OnQuestItemCollected += OnQuestItemPickedUp;
        }

        private void OnDisable()
        {
            questItemStimulus.OnQuestItemCollected -= OnQuestItemPickedUp;
        }

        private void OnQuestItemPickedUp(GameObject questItem)
        {
            eventChannel.Publish(new QuestItemsEvent(questItem));
        }
    }
}
﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public class QuestItemsEvent : IEvent
    {
        public GameObject QuestItem { get; private set; }

        public QuestItemsEvent(GameObject questItem)
        {
            QuestItem = questItem;
        }
    }
}
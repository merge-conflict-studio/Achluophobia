﻿using Harmony;

namespace Achluophobia
{
    public class ZombieDeathEvent : IEvent
    {
        public PlayerClass KillerPlayer { get; private set; }
        public ZombieType ZombieType { get; private set; }

        public ZombieDeathEvent(PlayerClass playerClass, ZombieType zombieType)
        {
            KillerPlayer = playerClass;
            ZombieType = zombieType;
        }
    }
}

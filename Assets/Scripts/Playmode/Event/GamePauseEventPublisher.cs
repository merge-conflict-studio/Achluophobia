﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/GamePauseEventPublisher")]
    public class GamePauseEventPublisher : GameScript
    {
        private GamePadsController gamePadsController;
        private GamePauseEventChannel eventChannel;

        private void InjectGamePauseEventPublisher([ApplicationScope] GamePadsController gamePadsController,
                                                   [EventChannelScope] GamePauseEventChannel eventChannel)
        {
            this.gamePadsController = gamePadsController;
            this.eventChannel = eventChannel;
        }

        private void Awake()
        {
            InjectDependencies("InjectGamePauseEventPublisher");
        }

        private void OnEnable()
        {
            gamePadsController.OnPause += OnPause;
        }

        private void OnDisable()
        {
            gamePadsController.OnPause -= OnPause;
        }

        private void OnPause()
        {
            eventChannel.Publish(new GamePauseEvent());
        }
    }
}
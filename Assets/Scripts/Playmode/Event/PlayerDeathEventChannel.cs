﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/PlayerDeathEventChannel")]
    public class PlayerDeathEventChannel : EventChannel<PlayerDeathEvent>
    {
    }
}
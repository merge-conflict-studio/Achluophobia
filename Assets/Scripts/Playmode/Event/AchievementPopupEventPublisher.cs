﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/AchievementPopupEventPublisher")]
    public class AchievementPopupEventPublisher : GameScript
    {
        private AchievementPopupEventChannel eventChannel;
        private AchievementsManagerController achievementsManagerController;

        private void InjectAchievementPopupEventPublisher([EventChannelScope] AchievementPopupEventChannel eventChannel,
                                                          [EntityScope] AchievementsManagerController achievementsManagerController)
        {
            this.eventChannel = eventChannel;
            this.achievementsManagerController = achievementsManagerController;
        }

        private void Awake()
        {
            InjectDependencies("InjectAchievementPopupEventPublisher");
        }

        private void OnEnable()
        {
            achievementsManagerController.OnGameAchievementToShow += OnAchievementShow;
            achievementsManagerController.OnClassAchievementToShow += OnAchievementShow;
        }

        private void OnDisable()
        {
            achievementsManagerController.OnGameAchievementToShow -= OnAchievementShow;
            achievementsManagerController.OnClassAchievementToShow -= OnAchievementShow;

        }

        private void OnAchievementShow(GameAchievements gameAchievements)
        {
            eventChannel.Publish(new AchievementPopupEvent(gameAchievements));
        }

        private void OnAchievementShow(ClassAchievements classAchievements)
        {
            eventChannel.Publish(new AchievementPopupEvent(classAchievements));
        }
    }
}
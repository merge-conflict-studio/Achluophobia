﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public class MiniGameEndEvent : IEvent
    {
        public bool IsCompleted { get; private set; }

        public MiniGameEndEvent(bool isCompleted)
        {
            IsCompleted = isCompleted;
        }
    }
}

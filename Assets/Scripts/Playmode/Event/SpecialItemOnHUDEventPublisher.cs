﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/SpecialItemOnHUDEventPublisher")]
    public class SpecialItemOnHUDEventPublisher : GameScript
    {
        private PlayerController playerController;
        private SpecialItemOnHUDEventChannel eventChannel;

        private void InjectSpecialItemOnHUDEventPublisher([EntityScope] PlayerController playerController,
                                                          [EventChannelScope] SpecialItemOnHUDEventChannel eventChannel)
        {
            this.playerController = playerController;
            this.eventChannel = eventChannel;
        }

        private void Awake()
        {
            InjectDependencies("InjectSpecialItemOnHUDEventPublisher");
        }

        private void OnEnable()
        {
            playerController.OnSpecialItemCountChanged += OnSpecialItemCountChanged;
        }

        private void OnDisable()
        {
            playerController.OnSpecialItemCountChanged -= OnSpecialItemCountChanged;
        }

        private void OnSpecialItemCountChanged(int specialItemCount, PlayerNumber playerNumber)
        {
            eventChannel.Publish(new SpecialItemOnHUDEvent(specialItemCount, playerNumber));
        }
    }

}
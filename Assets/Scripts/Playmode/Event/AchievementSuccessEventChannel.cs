﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/AchievementSuccessEventChannel")]
    public class AchievementSuccessEventChannel : EventChannel<AchievementSuccessEvent>
    {
    }
}
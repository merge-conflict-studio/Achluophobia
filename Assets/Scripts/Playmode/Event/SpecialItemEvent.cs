﻿using Harmony;

namespace Achluophobia
{
    public class SpecialItemEvent : IEvent
    {
        public PlayerClass PlayerClass { get; private set; }
        public int ValueToAdd { get; private set; }

        public SpecialItemEvent(PlayerClass playerClass, int valueToAdd)
        {
            PlayerClass = playerClass;
            ValueToAdd = valueToAdd;
        }
    }
}

﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public class PlayerTeleporterEvent : IEvent
    {
        public GameObject PlayerTeleporter { get; private set; }
        
        public GameObject Player { get; private set; }

        public PlayerTeleporterEvent(GameObject playerTeleporter, GameObject player)
        {
            PlayerTeleporter = playerTeleporter;
            Player = player;
        }
    }
}
﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/DeathEventChannel")]
    public class DeathEventChannel : EventChannel<DeathEvent>
    {
    }
}
﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/PlayerTeleporterEventPublisher")]
    public class PlayerTeleporterEventPublisher : GameScript
    {
        private PlayerTeleporterStimulus playerTeleporterStimulus;
        private PlayerTeleporterEventChannel eventChannel;

        private void InjectPlayerTeleporterEventPublisher([EntityScope] PlayerTeleporterStimulus playerTeleporterStimulus,
                                                          [EventChannelScope] PlayerTeleporterEventChannel eventChannel)
        {
            this.playerTeleporterStimulus = playerTeleporterStimulus;
            this.eventChannel = eventChannel;
        }

        private void Awake()
        {
            InjectDependencies("InjectPlayerTeleporterEventPublisher");
        }

        private void OnEnable()
        {
            playerTeleporterStimulus.OnPlayerTeleport += OnPlayerTeleporterTriggered;
        }

        private void OnDisable()
        {
            playerTeleporterStimulus.OnPlayerTeleport -= OnPlayerTeleporterTriggered;
        }

        private void OnPlayerTeleporterTriggered(GameObject playerTeleporter, GameObject player)
        {
            eventChannel.Publish(new PlayerTeleporterEvent(playerTeleporter, player));
        }
    }
}
﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/AchievementPopupEventChannel")]
    public class AchievementPopupEventChannel : EventChannel<AchievementPopupEvent>
    {
    }
}

﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/CreationEventPublisher")]
    public class CreationEventPublisher : GameScript
    {
        [SerializeField] private R.E.Prefab prefab;

        private GameObject rootGameObject;
        private CreationEventChannel eventChannel;

        private void InjectCreationEventPublisher([RootScope] GameObject rootGameObject,
                                                  [EventChannelScope] CreationEventChannel eventChannel)
        {
            this.rootGameObject = rootGameObject;
            this.eventChannel = eventChannel;
        }

        private void Awake()
        {
            InjectDependencies("InjectCreationEventPublisher");

            eventChannel.Publish(new CreationEvent(prefab, rootGameObject));
        }
    }
}
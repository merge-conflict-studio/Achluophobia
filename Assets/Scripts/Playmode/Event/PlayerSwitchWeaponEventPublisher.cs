﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/PlayerSwitchWeaponEventPublisher")]
    public class PlayerSwitchWeaponEventPublisher : GameScript
    {
        private PlayerController playerController;
        private PlayerSwitchWeaponEventChannel eventChannel;

        private void InjectPlayerSwitchWeaponEventPublisher([EntityScope] PlayerController playerController,
                                                            [EventChannelScope] PlayerSwitchWeaponEventChannel eventChannel)
        {
            this.playerController = playerController;
            this.eventChannel = eventChannel;
        }

        private void Awake()
        {
            InjectDependencies("InjectPlayerSwitchWeaponEventPublisher");
        }

        private void OnEnable()
        {
            playerController.OnSwitchedWeapon += OnWeaponSwitched;
        }

        private void OnDisable()
        {
            playerController.OnSwitchedWeapon -= OnWeaponSwitched;
        }

        private void OnWeaponSwitched(PlayerNumber playerNumber, WeaponType weaponType)
        {
            eventChannel.Publish(new PlayerSwitchWeaponEvent(playerNumber, weaponType));
        }
    }
}

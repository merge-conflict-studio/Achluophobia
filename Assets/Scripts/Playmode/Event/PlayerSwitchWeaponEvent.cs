﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public class PlayerSwitchWeaponEvent : IEvent
    {
        public PlayerNumber PlayerNumber { get; private set; }
        public WeaponType WeaponType { get; private set; }

        public PlayerSwitchWeaponEvent(PlayerNumber playerNumber, WeaponType weaponType)
        {
            PlayerNumber = playerNumber;
            WeaponType = weaponType;
        }
    }
}

﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public class SpecialItemOnHUDEvent : IEvent
    {
        public int SpecialItemsLeft { get; private set; }

        public PlayerNumber PlayerNumber { get; private set; }

        public SpecialItemOnHUDEvent(int specialItemsLeft, PlayerNumber playerNumber)
        {
            SpecialItemsLeft = specialItemsLeft;
            PlayerNumber = playerNumber;
        }
    }
}

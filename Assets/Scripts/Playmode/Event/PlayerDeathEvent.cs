﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public class PlayerDeathEvent : IEvent
    {
        public GameObject DeadPlayer { get; private set; }

        public PlayerDeathEvent(GameObject deadPlayer)
        {
            this.DeadPlayer = deadPlayer;
        }
    }
}
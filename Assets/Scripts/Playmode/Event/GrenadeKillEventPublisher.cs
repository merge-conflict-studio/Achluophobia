﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/GrenadeKillEventPublisher")]
    public class GrenadeKillEventPublisher : GameScript
    {
        private SpecialItemEventChannel eventChannel;
        private Health health;

        private void InjectGrenadeKillEventPublisher([EventChannelScope] SpecialItemEventChannel eventChannel,
                                                     [EntityScope] Health health)
        {
            this.eventChannel = eventChannel;
            this.health = health;
        }

        private void Awake()
        {
            InjectDependencies("InjectGrenadeKillEventPublisher");
        }

        private void OnEnable()
        {
            health.OnExplosionDeath += OnExplosionDeath;
        }

        private void OnDisable()
        {
            health.OnExplosionDeath -= OnExplosionDeath;
        }

        private void OnExplosionDeath()
        {
            eventChannel.Publish(new SpecialItemEvent(PlayerClass.Mastodonte, 1));
        }
    }
}
﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/MiniGameEventChannel")]
    public class MiniGameEventChannel : EventChannel<MiniGameEvent>
    {
    }
}

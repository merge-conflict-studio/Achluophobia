﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/MiniGameEndEventChannel")]
    public class MiniGameEndEventChannel : EventChannel<MiniGameEndEvent>
    {
    }
}
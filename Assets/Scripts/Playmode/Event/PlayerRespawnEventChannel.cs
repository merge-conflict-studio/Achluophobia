﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/PlayerRespawnEventChannel")]
    public class PlayerRespawnEventChannel : EventChannel<PlayerRespawnEvent>
    {
    }
}

﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Event/MastodonteDamageAchievementPublisher")]
    public class MastodonteDamageAchievementPublisher : GameScript
    {
        private AchievementSuccessEventChannel eventChannel;
        private KeepTrackOfMastodonteDamageTakenPerGame mastodonteDamageTracker;
        private PlayerDeathEventChannel playerDeathEventChannel;

        private void InjectMastodonteDamageAchievementPublisher(
            [EventChannelScope] AchievementSuccessEventChannel eventChannel,
            [EntityScope] KeepTrackOfMastodonteDamageTakenPerGame mastodonteDamageTracker)
        {
            this.eventChannel = eventChannel;
            this.mastodonteDamageTracker = mastodonteDamageTracker;
        }

        private void Awake()
        {
            InjectDependencies("InjectMastodonteDamageAchievementPublisher");
        }

        private void OnEnable()
        {
            mastodonteDamageTracker.OnSuccess += OnAchievementSuccess;
        }

        private void OnDisable()
        {
            mastodonteDamageTracker.OnSuccess -= OnAchievementSuccess;
        }

        private void OnAchievementSuccess()
        {
            eventChannel.Publish(new AchievementSuccessEvent(AchievementName.MastodonteAchievement3, false));
        }
    }
}

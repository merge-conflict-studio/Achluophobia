using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Menu/CharacterSelectionMenuController")]
    public class ClassSelectionMenuController : BaseClassSelectionMenuController, IMenuController
    {
        private List<PlayerClass> unavailablePlayerClasses;

        private void InjectCharacterSelectionMenuController(
            [ApplicationScope] ActivityStack activityStack,
            [ApplicationScope] GamePadsController gamePads,
            [ApplicationScope] SoundPlayer soundPlayer,
            [ApplicationScope] ImportantGameValues importantGameValues)
        {
            this.activityStack = activityStack;
            this.importantGameValues = importantGameValues;
            this.gamePads = gamePads;
            this.soundPlayer = soundPlayer;
        }

        protected override void Awake()
        {
            InjectDependencies("InjectCharacterSelectionMenuController");
            unavailablePlayerClasses = new List<PlayerClass>();
            canvasGroup = GetComponent<CanvasGroup>();
        }

        public override bool IsClassAvailable(Team team, PlayerClass wantedClass)
        {
            return !unavailablePlayerClasses.Contains(wantedClass);
        }

        public override void SetClassAvailable(Team team, PlayerClass currentClass)
        {
            unavailablePlayerClasses.Remove(currentClass);
        }

        public override void SetClassUnavailable(Team team, PlayerClass currentClass)
        {
            unavailablePlayerClasses.Add(currentClass);
        }

        public void OnCreate(params object[] parameters)
        {
            //Do nothing
        }

        public void OnResume()
        {
            numberOfPlayersReady = 0;
            gamePads.OnPause += TryStartingGame;
            importantGameValues.InitialPlayerClasses = new PlayerClass[importantGameValues.NumberOfPlayers];
            HideStartButton();

            for (int i = 0; i < importantGameValues.NumberOfPlayers; i++)
            {
                if (classSelectionViews[i] != null)
                {
                    gamePads.GetSensor((PlayerNumber)i).OnMenuUp += classSelectionViews[i].SetNextClass;
                    gamePads.GetSensor((PlayerNumber)i).OnMenuDown += classSelectionViews[i].SetPreviousClass;
                    gamePads.GetSensor((PlayerNumber)i).OnMenuConfirm += classSelectionViews[i].OnConfirm;
                    gamePads.OnMenuBack += ReturnToLastMenu;

                    classSelectionViews[i].OnReadyStateChanged += OnReadyStateChanged;
                    classSelectionViews[i].OnClassChanged += OnClassChanged;
                    classSelectionViews[i].OnClassUnavailable += OnClassUnavailable;
                }
            }
        }

        public void OnPause()
        {
            // Do nothing
        }

        public void OnStop()
        {
            gamePads.OnPause -= TryStartingGame;
            for (int i = 0; i < importantGameValues.NumberOfPlayers; i++)
            {
                if (classSelectionViews[i] != null)
                {
                    gamePads.GetSensor((PlayerNumber)i).OnMenuUp -= classSelectionViews[i].SetNextClass;
                    gamePads.GetSensor((PlayerNumber)i).OnMenuDown -= classSelectionViews[i].SetPreviousClass;
                    gamePads.GetSensor((PlayerNumber)i).OnMenuConfirm -= classSelectionViews[i].OnConfirm;
                    gamePads.OnMenuBack -= ReturnToLastMenu;

                    classSelectionViews[i].OnReadyStateChanged -= OnReadyStateChanged;
                    classSelectionViews[i].OnClassChanged -= OnClassChanged;
                    classSelectionViews[i].OnClassUnavailable -= OnClassUnavailable;
                    classSelectionViews[i].ResetState();
                }
            }
            ResetState();
        }

        protected override void ResetState()
        {
            unavailablePlayerClasses.Clear();
            numberOfPlayersReady = 0;
        }
    }
}
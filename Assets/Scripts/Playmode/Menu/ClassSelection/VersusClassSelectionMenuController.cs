﻿using System;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Menu/CharacterSelectionMenuController")]
    public class VersusClassSelectionMenuController : BaseClassSelectionMenuController, IMenuController
    {
        private List<PlayerClass> team1UnavailablePlayerClasses;
        private List<PlayerClass> team2UnavailablePlayerClasses;

        private void InjectVersusCharacterSelectionMenuController(
            [ApplicationScope] ActivityStack activityStack,
            [ApplicationScope] GamePadsController gamePads,
            [ApplicationScope] SoundPlayer soundPlayer,
            [ApplicationScope] ImportantGameValues importantGameValues)
        {
            this.activityStack = activityStack;
            this.importantGameValues = importantGameValues;
            this.gamePads = gamePads;
            this.soundPlayer = soundPlayer;
        }

        protected override void Awake()
        {
            InjectDependencies("InjectVersusCharacterSelectionMenuController");
            team1UnavailablePlayerClasses = new List<PlayerClass>();
            team2UnavailablePlayerClasses = new List<PlayerClass>();
            canvasGroup = GetComponent<CanvasGroup>();
        }

        public override bool IsClassAvailable(Team team, PlayerClass wantedClass)
        {
            bool isAvailable = false;

            switch (team)
            {
                case Team.One:
                    isAvailable = !team1UnavailablePlayerClasses.Contains(wantedClass);
                    break;
                case Team.Two:
                    isAvailable = !team2UnavailablePlayerClasses.Contains(wantedClass);
                    break;
                default:
                    throw new ArgumentException("The Team enum parameter is not defined");
            }
            return isAvailable;
        }

        public override void SetClassAvailable(Team team, PlayerClass currentClass)
        {
            switch (team)
            {
                case Team.One:
                    team1UnavailablePlayerClasses.Remove(currentClass);
                    break;
                case Team.Two:
                    team2UnavailablePlayerClasses.Remove(currentClass);
                    break;
            }
        }

        public override void SetClassUnavailable(Team team, PlayerClass currentClass)
        {
            switch (team)
            {
                case Team.One:
                    team1UnavailablePlayerClasses.Add(currentClass);
                    break;
                case Team.Two:
                    team2UnavailablePlayerClasses.Add(currentClass);
                    break;
            }
        }

        public void OnCreate(params object[] parameters)
        {
            //Do nothing
        }

        public void OnResume()
        {
            numberOfPlayersReady = 0;
            gamePads.OnPause += TryStartingGame;
            importantGameValues.InitialPlayerClasses = new PlayerClass[importantGameValues.NumberOfPlayers];
            HideStartButton();

            for (int i = 0; i < importantGameValues.NumberOfPlayers; i++)
            {
                if (classSelectionViews[i] != null)
                {
                    gamePads.GetSensor((PlayerNumber)i).OnMenuUp += classSelectionViews[i].SetNextClass;
                    gamePads.GetSensor((PlayerNumber)i).OnMenuDown += classSelectionViews[i].SetPreviousClass;
                    gamePads.GetSensor((PlayerNumber)i).OnMenuConfirm += classSelectionViews[i].OnConfirm;
                    gamePads.OnMenuBack += ReturnToLastMenu;

                    classSelectionViews[i].OnReadyStateChanged += OnReadyStateChanged;
                    classSelectionViews[i].OnClassChanged += OnClassChanged;
                    classSelectionViews[i].OnClassUnavailable += OnClassUnavailable;
                }
            }
        }

        public void OnPause()
        {
            // Do nothing
        }

        public void OnStop()
        {
            gamePads.OnPause -= TryStartingGame;
            for (int i = 0; i < importantGameValues.NumberOfPlayers; i++)
            {
                if (classSelectionViews[i] != null)
                {
                    gamePads.GetSensor((PlayerNumber)i).OnMenuUp -= classSelectionViews[i].SetNextClass;
                    gamePads.GetSensor((PlayerNumber)i).OnMenuDown -= classSelectionViews[i].SetPreviousClass;
                    gamePads.GetSensor((PlayerNumber)i).OnMenuConfirm -= classSelectionViews[i].OnConfirm;
                    gamePads.OnMenuBack -= ReturnToLastMenu;

                    classSelectionViews[i].OnReadyStateChanged -= OnReadyStateChanged;
                    classSelectionViews[i].OnClassChanged -= OnClassChanged;
                    classSelectionViews[i].OnClassUnavailable -= OnClassUnavailable;
                    classSelectionViews[i].ResetState();
                }
            }
            ResetState();
        }

        protected override void ResetState()
        {
            team1UnavailablePlayerClasses.Clear();
            team2UnavailablePlayerClasses.Clear();
            numberOfPlayersReady = 0;
        }
    }
}
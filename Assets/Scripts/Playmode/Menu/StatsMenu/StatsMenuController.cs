﻿using System;
using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Achluophobia
{
    public class StatsMenuController : GameScript, IMenuController
    {
        [SerializeField, Tooltip("The previous menu in hierarchy")]
        private Menu previousMenu;

        private ActivityStack activityStack;

        private enum StatsButtons { General, Assault, Medic, Mastodonte, Incinerator, Scout};

        private Text soloGamesFinished;
        private Text soloGamesNumber;
        private Text versusGamesFinished;
        private Text versusGamesNumber;
        private Text coopGamesFinished;
        private Text coopGamesNumber;
        private Text failedGames;
        private Text failedGamesNumber;

        private Text zombieKilled;
        private Text basicZombie;
        private Text basicZombieNumber;
        private Text runnerZombie;
        private Text runnerZombieNumber;
        private Text boomerZombie;
        private Text boomerZombieNumber;
        private Text hunterZombie;
        private Text hunterZombieNumber;
        private Text witchZombie;
        private Text witchZombieNumber;
        private Text timePlayed;
        private Text timePlayedNumber;
        private Text minutesText;
        private Text deaths;
        private Text deathsNumber;
        private Text ammoBoxDropped;
        private Text healthGiven;
        private Text glowsticksDropped;
        private Text burnerDropped;
        private Text grenadeKills;
        private Text specialStatsNumber;

        private GeneralStatsRepository generalStatsRepository;
        private GameStatsRepository gameStatsRepository;
        private AssaultStatsRepository assaultStatsRepository;
        private MedicStatsRepository medicStatsRepository;
        private MastodonteStatsRepository mastodonteStatsRepository;
        private IncineratorStatsRepository incineratorStatsRepository;
        private ScoutStatsRepository scoutStatsRepository;

        private GameStats gameStats;
        private AssaultStats assaultStats;
        private MedicStats medicStats;
        private MastodonteStats mastodonteStats;
        private IncineratorStats incineratorStats;
        private ScoutStats scoutStats;

        private Selectable defaultSelectedButton;

        private void InjectStatsMenuController([ApplicationScope] ActivityStack activityStack,
                                               [ChildScope, Named(R.S.GameObject.ZombieKilled)] GameObject zombieKilled,
                                               [ChildScope, Named(R.S.GameObject.SoloGamesFinished)] GameObject soloGamesFinished,
                                               [ChildScope, Named(R.S.GameObject.SoloGamesNumber)] GameObject soloGamesNumber,
                                               [ChildScope, Named(R.S.GameObject.VersusGamesFinished)] GameObject versusGamesFinished,
                                               [ChildScope, Named(R.S.GameObject.VersusGamesNumber)] GameObject versusGamesNumber,
                                               [ChildScope, Named(R.S.GameObject.CoopGamesFinished)] GameObject coopGamesFinished,
                                               [ChildScope, Named(R.S.GameObject.CoopGamesNumber)] GameObject coopGamesNumber,
                                               [ChildScope, Named(R.S.GameObject.FailedGames)] GameObject failedGames,
                                               [ChildScope, Named(R.S.GameObject.FailedGamesNumber)] GameObject failedGamesNumber,
                                               [ChildScope, Named(R.S.GameObject.BasicZombie)] GameObject basicZombie,
                                               [ChildScope, Named(R.S.GameObject.BasicZombieNumber)] GameObject basicZombieNumber,
                                               [ChildScope, Named(R.S.GameObject.RunnerZombie)] GameObject runnerZombie,
                                               [ChildScope, Named(R.S.GameObject.RunnerZombieNumber)] GameObject runnerZombieNumber,
                                               [ChildScope, Named(R.S.GameObject.BoomerZombie)] GameObject boomerZombie,
                                               [ChildScope, Named(R.S.GameObject.BoomerZombieNumber)] GameObject boomerZombieNumber,
                                               [ChildScope, Named(R.S.GameObject.HunterZombie)] GameObject hunterZombie,
                                               [ChildScope, Named(R.S.GameObject.HunterZombieNumber)] GameObject hunterZombieNumber,
                                               [ChildScope, Named(R.S.GameObject.WitchZombie)] GameObject witchZombie,
                                               [ChildScope, Named(R.S.GameObject.WitchZombieNumber)] GameObject witchZombieNumber,
                                               [ChildScope, Named(R.S.GameObject.TimePlayed)] GameObject timePlayed,
                                               [ChildScope, Named(R.S.GameObject.TimePlayedNumber)] GameObject timePlayedNumber,
                                               [ChildScope, Named(R.S.GameObject.Minutes)] GameObject minutesText,
                                               [ChildScope, Named(R.S.GameObject.Deaths)] GameObject deaths,
                                               [ChildScope, Named(R.S.GameObject.DeathsNumber)] GameObject deathsNumber,
                                               [ChildScope, Named(R.S.GameObject.AmmoBoxDropped)] GameObject ammoBoxDropped,
                                               [ChildScope, Named(R.S.GameObject.HealthGiven)] GameObject healthGiven,
                                               [ChildScope, Named(R.S.GameObject.GlowSticksDropped)] GameObject glowsticksDropped,
                                               [ChildScope, Named(R.S.GameObject.BurnerDropped)] GameObject burnerDropped,
                                               [ChildScope, Named(R.S.GameObject.GrenadeKill)] GameObject grenadeKills,
                                               [ChildScope, Named(R.S.GameObject.SpecialStatNumber)] GameObject specialStatsNumber,
                                               [ChildScope] GeneralStatsRepository generalStatsRepository,
                                               [ChildScope] GameStatsRepository gameStatsRepository,
                                               [ChildScope] AssaultStatsRepository assaultStatsRepository,
                                               [ChildScope] MedicStatsRepository medicStatsRepository,
                                               [ChildScope] MastodonteStatsRepository mastodonteStatsRepository,
                                               [ChildScope] IncineratorStatsRepository incineratorStatsRepository,
                                               [ChildScope] ScoutStatsRepository scoutStatsRepository,
                                               [ChildScope, Named(R.S.GameObject.General)] GameObject generalButton)
        {
            this.activityStack = activityStack;
            this.zombieKilled = zombieKilled.GetComponent<Text>();
            this.soloGamesFinished = soloGamesFinished.GetComponent<Text>();
            this.soloGamesNumber = soloGamesNumber.GetComponent<Text>();
            this.versusGamesFinished = versusGamesFinished.GetComponent<Text>();
            this.versusGamesNumber = versusGamesNumber.GetComponent<Text>();
            this.coopGamesFinished = coopGamesFinished.GetComponent<Text>();
            this.coopGamesNumber = coopGamesNumber.GetComponent<Text>();
            this.failedGames = failedGames.GetComponent<Text>();
            this.failedGamesNumber = failedGamesNumber.GetComponent<Text>();
            this.basicZombie = basicZombie.GetComponent<Text>();
            this.basicZombieNumber = basicZombieNumber.GetComponent<Text>();
            this.runnerZombie = runnerZombie.GetComponent<Text>();
            this.runnerZombieNumber = runnerZombieNumber.GetComponent<Text>();
            this.boomerZombie = boomerZombie.GetComponent<Text>();
            this.boomerZombieNumber = boomerZombieNumber.GetComponent<Text>();
            this.hunterZombie = hunterZombie.GetComponent<Text>();
            this.hunterZombieNumber = hunterZombieNumber.GetComponent<Text>();
            this.witchZombie = witchZombie.GetComponent<Text>();
            this.witchZombieNumber = witchZombieNumber.GetComponent<Text>();
            this.timePlayed = timePlayed.GetComponent<Text>();
            this.timePlayedNumber = timePlayedNumber.GetComponent<Text>();
            this.minutesText = minutesText.GetComponent<Text>();
            this.deaths = deaths.GetComponent<Text>();
            this.deathsNumber = deathsNumber.GetComponent<Text>();
            this.ammoBoxDropped = ammoBoxDropped.GetComponent<Text>();
            this.healthGiven = healthGiven.GetComponent<Text>();
            this.glowsticksDropped = glowsticksDropped.GetComponent<Text>();
            this.burnerDropped = burnerDropped.GetComponent<Text>();
            this.grenadeKills = grenadeKills.GetComponent<Text>();
            this.specialStatsNumber = specialStatsNumber.GetComponent<Text>();
            this.generalStatsRepository = generalStatsRepository;
            this.gameStatsRepository = gameStatsRepository;
            this.assaultStatsRepository = assaultStatsRepository;
            this.medicStatsRepository = medicStatsRepository;
            this.mastodonteStatsRepository = mastodonteStatsRepository;
            this.incineratorStatsRepository = incineratorStatsRepository;
            this.scoutStatsRepository = scoutStatsRepository;
            defaultSelectedButton = generalButton.GetComponent<Selectable>();
        }

        private void Awake()
        {
            InjectDependencies("InjectStatsMenuController");
        }

        public void OnCreate(params object[] parameters)
        {
            //Do nothing
        }

        public void OnResume()
        {
            gameStats = gameStatsRepository.GetStatsFromGame();
            assaultStats = assaultStatsRepository.GetAssaultStats();
            medicStats = medicStatsRepository.GetMedicStats();
            mastodonteStats = mastodonteStatsRepository.GetAllMastodonteStats();
            incineratorStats = incineratorStatsRepository.GetAllIncineratorStats();
            scoutStats = scoutStatsRepository.GetScoutStats();
            ChangeViewedStatsOnScreen(StatsButtons.General);
            defaultSelectedButton.Select();
        }

        public void OnPause()
        {
            //Do nothing
        }

        public void OnStop()
        {
            //Do nothing
        }

        [CalledOutsideOfCode]
        public void OnBackButtonPressed()
        {
            activityStack.StopCurrentMenu();
            activityStack.StartMenu(previousMenu);
        }

        [CalledOutsideOfCode]
        public void OnGeneralButtonPressed()
        {
            ChangeViewedStatsOnScreen(StatsButtons.General);
        }

        [CalledOutsideOfCode]
        public void OnAssaultButtonPressed()
        {
            ChangeViewedStatsOnScreen(StatsButtons.Assault);
        }

        [CalledOutsideOfCode]
        public void OnMedicButtonPressed()
        {
            ChangeViewedStatsOnScreen(StatsButtons.Medic);
        }

        [CalledOutsideOfCode]
        public void OnMastodonteButtonPressed()
        {
            ChangeViewedStatsOnScreen(StatsButtons.Mastodonte);
        }

        [CalledOutsideOfCode]
        public void OnIncineratorButtonPressed()
        {
            ChangeViewedStatsOnScreen(StatsButtons.Incinerator);
        }

        [CalledOutsideOfCode]
        public void OnScoutButtonPressed()
        {
            ChangeViewedStatsOnScreen(StatsButtons.Scout);
        }

        private void ChangeViewedStatsOnScreen(StatsButtons buttonSelected)
        {
            ClearView();
            switch (buttonSelected)
            {
                case StatsButtons.General:
                    soloGamesFinished.enabled = true;
                    soloGamesNumber.text = gameStats.SoloGamesFinished.ToString();
                    soloGamesNumber.enabled = true;
                    versusGamesFinished.enabled = true;
                    versusGamesNumber.text = gameStats.VersusGamesFinished.ToString();
                    versusGamesNumber.enabled = true;
                    coopGamesFinished.enabled = true;
                    coopGamesNumber.text = gameStats.CoopGamesFinished.ToString();
                    coopGamesNumber.enabled = true;
                    failedGames.enabled = true;
                    failedGamesNumber.text = gameStats.GameOvers.ToString();
                    failedGamesNumber.enabled = true;
                    break;
                case StatsButtons.Assault:
                    SetBasicStatsNumbers(assaultStats.GeneralStatsId);
                    specialStatsNumber.text = assaultStats.AmmoBoxDropped.ToString();
                    SetBasicClassStatsView();
                    ammoBoxDropped.enabled = true;
                    break;
                case StatsButtons.Medic:
                    SetBasicStatsNumbers(medicStats.GeneralStatsId);
                    specialStatsNumber.text = medicStats.AmmountOfHealthGiven.ToString();
                    SetBasicClassStatsView();
                    healthGiven.enabled = true;
                    break;
                case StatsButtons.Mastodonte:
                    SetBasicStatsNumbers(mastodonteStats.GeneralStatsId);
                    specialStatsNumber.text = mastodonteStats.ZombieKilledWithGrenade.ToString();
                    SetBasicClassStatsView();
                    grenadeKills.enabled = true;
                    break;
                case StatsButtons.Incinerator:
                    SetBasicStatsNumbers(incineratorStats.GeneralStatsId);
                    specialStatsNumber.text = incineratorStats.NumberOfBurnerDropped.ToString();
                    SetBasicClassStatsView();
                    burnerDropped.enabled = true;
                    break;
                case StatsButtons.Scout:
                    SetBasicStatsNumbers(scoutStats.GeneralStatsId);
                    specialStatsNumber.text = scoutStats.NumberOfGlowSticksDropped.ToString();
                    SetBasicClassStatsView();
                    glowsticksDropped.enabled = true;
                    break;

                default:
                    throw new ArgumentOutOfRangeException("buttonSelected", buttonSelected, null);
            }
        }

        private void SetBasicStatsNumbers(long id)
        {
            GeneralStats generalStats = generalStatsRepository.GetStatsFromId(id);

            basicZombieNumber.text = generalStats.BaseZombieKilled.ToString();
            runnerZombieNumber.text = generalStats.RunnerZombieKilled.ToString();
            hunterZombieNumber.text = generalStats.HunterZombieKilled.ToString();
            boomerZombieNumber.text = generalStats.BoomerZombieKilled.ToString();
            witchZombieNumber.text = generalStats.WitchZombieKilled.ToString();
            timePlayedNumber.text = generalStats.TimePlayed.ToString();
            deathsNumber.text = generalStats.NumberOfDeaths.ToString();
        }

        private void SetBasicClassStatsView()
        {
            zombieKilled.enabled = true;
            basicZombie.enabled = true;
            basicZombieNumber.enabled = true;
            runnerZombie.enabled = true;
            runnerZombieNumber.enabled = true;
            boomerZombie.enabled = true;
            boomerZombieNumber.enabled = true;
            hunterZombie.enabled = true;
            hunterZombieNumber.enabled = true;
            witchZombie.enabled = true;
            witchZombieNumber.enabled = true;
            timePlayed.enabled = true;
            timePlayedNumber.enabled = true;
            minutesText.enabled = true;
            deaths.enabled = true;
            deathsNumber.enabled = true;
            specialStatsNumber.enabled = true;
        }

        private void ClearView()
        {
            soloGamesFinished.enabled = false;
            soloGamesNumber.enabled = false;
            versusGamesFinished.enabled = false;
            versusGamesNumber.enabled = false;
            coopGamesFinished.enabled = false;
            coopGamesNumber.enabled = false;
            failedGames.enabled = false;
            failedGamesNumber.enabled = false;
            zombieKilled.enabled = false;
            basicZombie.enabled = false;
            basicZombieNumber.enabled = false;
            runnerZombie.enabled = false;
            runnerZombieNumber.enabled = false;
            boomerZombie.enabled = false;
            boomerZombieNumber.enabled = false;
            hunterZombie.enabled = false;
            hunterZombieNumber.enabled = false;
            witchZombie.enabled = false;
            witchZombieNumber.enabled = false;
            timePlayed.enabled = false;
            timePlayedNumber.enabled = false;
            minutesText.enabled = false;
            deaths.enabled = false;
            deathsNumber.enabled = false;
            ammoBoxDropped.enabled = false;
            healthGiven.enabled = false;
            glowsticksDropped.enabled = false;
            burnerDropped.enabled = false;
            grenadeKills.enabled = false;
            specialStatsNumber.enabled = false;
        }
    }
}

﻿using System;
using UnityEngine;
using Harmony;
using UnityEngine.UI;

namespace Achluophobia
{
    [AddComponentMenu("Game/Menu/GameOverMenuController")]
    public class GameOverMenuController : GameScript, IMenuController
    {
        [SerializeField, Tooltip("Button that makes you go back to the main menu")]
        private Button goBackToMenuButton;
        
        [SerializeField, Tooltip("Button that makes you exit")] 
        private Button quitButton;
        
        [SerializeField, Tooltip("Player1 Text")]
        private GameObject player1Text;
        
        [SerializeField, Tooltip("Player2 Text")]
        private GameObject player2Text;
        
        [SerializeField, Tooltip("Zombies Killed Team 1 Text")]
        private GameObject zombiesKilledTeam1Text;
        
        [SerializeField, Tooltip("Zombies Killed Team 2 Text")]
        private GameObject zombiesKilledTeam2Text;

        [SerializeField, Tooltip("Game Over Text")] 
        private GameObject gameOverText;

        private const string player1ChangedToTeam = "Team 1 :";
        private const string player2ChangedToTeam = "Team 2 :";
        private const string gameOverWin = "You escaped !";
        private const string gameOverLost = "You died";
        private const string gameOverWinVersusTeam1 = "Team 1 Escaped !";
        private const string gameOverWinVersusTeam2 = "Team 2 Escaped !";

        private ActivityStack activityStack;
        private ImportantGameValues importantGameValues;
        private PlayerValues playerValues;

        private void InjectGameOverMenuController([ApplicationScope] ActivityStack activityStack,
                                                  [ApplicationScope] ImportantGameValues importantGameValues,
                                                  [ApplicationScope] PlayerValues playerValues)
        {
            this.activityStack = activityStack;
            this.importantGameValues = importantGameValues;
            this.playerValues = playerValues;
        }

        private void Awake()
        {
            InjectDependencies("InjectGameOverMenuController");
        }

        public void OnCreate(params object[] parameters)
        {
            gameOverText.GetComponent<Text>().text = importantGameValues.EndingType == EndingType.GameFailed ? gameOverLost : gameOverWin;
            switch (importantGameValues.GameMode)
            {
                case GameMode.Solo:
                    player2Text.SetActive(false);
                    zombiesKilledTeam2Text.SetActive(false);
                    zombiesKilledTeam1Text.GetComponent<Text>().text = 
                        playerValues.ZombiesKilledPlayer0.ToString();
                    break;
                case GameMode.Coop:
                    zombiesKilledTeam1Text.GetComponent<Text>().text = 
                        playerValues.ZombiesKilledPlayer0.ToString();
                    zombiesKilledTeam2Text.GetComponent<Text>().text = 
                        playerValues.ZombiesKilledPlayer1.ToString();
                    break;
                case GameMode.Versus:
                    player2Text.SetActive(true);
                    zombiesKilledTeam2Text.SetActive(true);
                    player1Text.GetComponent<Text>().text = player1ChangedToTeam;
                    player2Text.GetComponent<Text>().text = player2ChangedToTeam;

                    zombiesKilledTeam1Text.GetComponent<Text>().text =
                    (playerValues.ZombiesKilledPlayer0 + playerValues.ZombiesKilledPlayer1).ToString();
                    zombiesKilledTeam2Text.GetComponent<Text>().text =
                        (playerValues.ZombiesKilledPlayer2 + playerValues.ZombiesKilledPlayer3).ToString();

                    if (importantGameValues.WinningTeam == 1)
                    {
                        gameOverText.GetComponent<Text>().text = gameOverWinVersusTeam1;
                    }
                    else if (importantGameValues.WinningTeam == 2)
                    {
                        gameOverText.GetComponent<Text>().text = gameOverWinVersusTeam2;
                    }
                    
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void OnResume()
        {
            goBackToMenuButton.Select();
            goBackToMenuButton.OnSelect(null);
            FreezeTime();
        }

        public void OnPause()
        {
            //Do nothing
        }

        public void OnStop()
        {
            //Do nothing
        }

        [CalledOutsideOfCode]
        public void OnGoBackToMenuButtonClicked()
        {
            GoBackToMainMenu();
        }

        [CalledOutsideOfCode]
        public void OnQuitButtonClicked()
        {
            StopGame();
        }

        private void StopGame()
        {
            activityStack.StopAllCoroutines();
            activityStack.StopCurrentMenu();
            activityStack.StopAllActivities();
        }

        private void GoBackToMainMenu()
        {
            importantGameValues.IsPlayerQuittingAGame = true;
            UnfreezeTime();
            activityStack.StopAllCoroutines();
            activityStack.StopCurrentMenu();
            activityStack.StopCurrentActivity();
        }

        private static void FreezeTime()
        {
            TimeExtensions.Pause();
        }

        private static void UnfreezeTime()
        {
            TimeExtensions.Resume();
        }
    }
}
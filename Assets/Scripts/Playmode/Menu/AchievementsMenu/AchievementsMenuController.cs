﻿using Harmony;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Achluophobia
{
    [AddComponentMenu("Game/Menu/AchievementsMenuController")]
    public class AchievementsMenuController : GameScript, IMenuController
    {
        [SerializeField, Tooltip("The previous menu in hierarchy")]
        private Menu previousMenu;
        [SerializeField, Tooltip("The first achievement box")]
        private GameObject achievementBox1;
        [SerializeField, Tooltip("The second achievement box")]
        private GameObject achievementBox2;
        [SerializeField, Tooltip("The third achievement box")]
        private GameObject achievementBox3;
        [SerializeField, Tooltip("The fourth achievement box")]
        private GameObject achievementBox4;
        [SerializeField, Tooltip("The fifth achievement box")]
        private GameObject achievementBox5;
        [SerializeField, Tooltip("The sixth achievement box")]
        private GameObject achievementBox6;

        private ActivityStack activityStack;

        private enum AchievementType { General, Assault, Medic, Mastodonte, Incinerator, Scout };

        private List<ClassAchievements> classAchievements;
        private List<GameAchievements> gameAchievements;

        private GameStats gameStats;
        private AssaultStats assaultStats;
        private MedicStats medicStats;
        private MastodonteStats mastodonteStats;
        private IncineratorStats incineratorStats;
        private ScoutStats scoutStats;
        private ClassAchievementsRepository classAchievementsRepository;
        private GameAchievementsRepository gameAchievementsRepository;

        private GameStatsRepository gameStatsRepository;
        private AssaultStatsRepository assaultStatsRepository;
        private MedicStatsRepository medicStatsRepository;
        private MastodonteStatsRepository mastodonteStatsRepository;
        private IncineratorStatsRepository incineratorStatsRepository;
        private ScoutStatsRepository scoutStatsRepository;

        private Selectable defaultSelectedButton;

        private GameObject[] achievementsTab;

        private void InjectAchievementsMenuController([ApplicationScope] ActivityStack activityStack,
                                                      [ChildScope] GameStatsRepository gameStatsRepository,
                                                      [ChildScope] AssaultStatsRepository assaultStatsRepository,
                                                      [ChildScope] MedicStatsRepository medicStatsRepository,
                                                      [ChildScope] MastodonteStatsRepository mastodonteStatsRepository,
                                                      [ChildScope] IncineratorStatsRepository incineratorStatsRepository,
                                                      [ChildScope] ScoutStatsRepository scoutStatsRepository,
                                                      [ChildScope] ClassAchievementsRepository classAchievementsRepository,
                                                      [ChildScope] GameAchievementsRepository gameAchievementsRepository,
                                                      [ChildScope, Named(R.S.GameObject.General)] GameObject generalButton)
        {
            this.activityStack = activityStack;
            this.gameAchievementsRepository = gameAchievementsRepository;
            this.classAchievementsRepository = classAchievementsRepository;
            this.gameStatsRepository = gameStatsRepository;
            this.assaultStatsRepository = assaultStatsRepository;
            this.medicStatsRepository = medicStatsRepository;
            this.mastodonteStatsRepository = mastodonteStatsRepository;
            this.incineratorStatsRepository = incineratorStatsRepository;
            this.scoutStatsRepository = scoutStatsRepository;
            defaultSelectedButton = generalButton.GetComponent<Selectable>();
        }

        private void Awake()
        {
            InjectDependencies("InjectAchievementsMenuController");
            gameAchievements = new List<GameAchievements>();
            classAchievements = new List<ClassAchievements>();
            achievementsTab = new GameObject[]
            {
                achievementBox1,
                achievementBox2,
                achievementBox3,
                achievementBox4,
                achievementBox5,
                achievementBox6
            };
        }

        public void OnCreate(params object[] parameters)
        {
            //Do nothing
        }

        public void OnResume()
        {
            defaultSelectedButton.Select();

            gameStats = gameStatsRepository.GetStatsFromGame();
            assaultStats = assaultStatsRepository.GetAssaultStats();
            medicStats = medicStatsRepository.GetMedicStats();
            mastodonteStats = mastodonteStatsRepository.GetAllMastodonteStats();
            incineratorStats = incineratorStatsRepository.GetAllIncineratorStats();
            scoutStats = scoutStatsRepository.GetScoutStats();

            SwitchAchievements(AchievementType.General);
            ShowAchievements(gameAchievements);
        }

        public void OnPause()
        {
            //Do nothing
        }

        public void OnStop()
        {
            //Do nothing
        }

        [CalledOutsideOfCode]
        public void OnBackButtonPressed()
        {
            activityStack.StopCurrentMenu();
            activityStack.StartMenu(previousMenu);
        }

        [CalledOutsideOfCode]
        public void OnGeneralButtonPressed()
        {
            SwitchAchievements(AchievementType.General);
            ShowAchievements(gameAchievements);
        }

        [CalledOutsideOfCode]
        public void OnAssaultButtonPressed()
        {
            SwitchAchievements(AchievementType.Assault);
            ShowAchievements(classAchievements);
        }

        [CalledOutsideOfCode]
        public void OnMedicButtonPressed()
        {
            SwitchAchievements(AchievementType.Medic);
            ShowAchievements(classAchievements);
        }

        [CalledOutsideOfCode]
        public void OnMastodonteButtonPressed()
        {
            SwitchAchievements(AchievementType.Mastodonte);
            ShowAchievements(classAchievements);
        }

        [CalledOutsideOfCode]
        public void OnIncineratorButtonPressed()
        {
            SwitchAchievements(AchievementType.Incinerator);
            ShowAchievements(classAchievements);
        }

        [CalledOutsideOfCode]
        public void OnScoutButtonPressed()
        {
            SwitchAchievements(AchievementType.Scout);
            ShowAchievements(classAchievements);
        }

        private void SwitchAchievements(AchievementType achievementType)
        {
            classAchievements.Clear();
            gameAchievements.Clear();
            ClearAchievementsOnScreen();
            LoadAchievements(achievementType, true);
            LoadAchievements(achievementType, false);
        }

        private void LoadAchievements(AchievementType achievementType, bool successOfAchievements)
        {
            switch (achievementType)
            {
                case AchievementType.General:
                    IList<GameAchievements> tempGameAchievements = successOfAchievements ? gameAchievementsRepository.GetAllSuccessfulAchievements(gameStats.Id) : 
                        gameAchievementsRepository.GetAllUnsuccessfulAchievements(gameStats.Id);
                    foreach(GameAchievements achievement in tempGameAchievements)
                    {
                        gameAchievements.Add(achievement);
                    }
                    break;
                case AchievementType.Assault:
                    AddClassAchievementsToList(successOfAchievements, assaultStats.GeneralStatsId);
                    break;
                case AchievementType.Medic:
                    AddClassAchievementsToList(successOfAchievements, medicStats.GeneralStatsId);
                    break;
                case AchievementType.Mastodonte:
                    AddClassAchievementsToList(successOfAchievements, mastodonteStats.GeneralStatsId);
                    break;
                case AchievementType.Incinerator:
                    AddClassAchievementsToList(successOfAchievements, incineratorStats.GeneralStatsId);
                    break;
                case AchievementType.Scout:
                    AddClassAchievementsToList(successOfAchievements, scoutStats.GeneralStatsId);
                    break;
            }
        }

        private void AddClassAchievementsToList(bool successOfAchievements, long statsId)
        {
            IList<ClassAchievements> tempClassAchievements = successOfAchievements ? classAchievementsRepository.GetAllSuccessfulAchievementsFromClass(statsId) 
                                                                                   : classAchievementsRepository.GetAllUnsuccessfulAchievementsFromClass(statsId);
            foreach (ClassAchievements achievement in tempClassAchievements)
            {
                classAchievements.Add(achievement);
            }
        }

        private void ShowAchievements(IList<GameAchievements> gameAchievements)
        {
            int counter = 0;
            foreach (GameAchievements achievement in gameAchievements)
            {
                achievementsTab[counter].SetActive(true);
                SetAchievementBoxValues(achievementsTab[counter].GetComponent<AchievementView>(), achievement);
                counter++;
            }
        }

        private void ShowAchievements(IList<ClassAchievements> classAchievements)
        {
            int counter = 0;
            foreach (ClassAchievements achievement in classAchievements)
            {
                achievementsTab[counter].SetActive(true);
                SetAchievementBoxValues(achievementsTab[counter].GetComponent<AchievementView>(), achievement);
                counter++;
            }
        }

        private void ClearAchievementsOnScreen()
        {
            achievementBox1.SetActive(false);
            achievementBox2.SetActive(false);
            achievementBox3.SetActive(false);
            achievementBox4.SetActive(false);
            achievementBox5.SetActive(false);
            achievementBox6.SetActive(false);
        }

        private void SetAchievementBoxValues(AchievementView achievementBoxValues, GameAchievements gameAchievements)
        {
            achievementBoxValues.ChangeTitle(gameAchievements.Name);
            achievementBoxValues.ChangeDescription(gameAchievements.Description);
            achievementBoxValues.ChangeSuccessColor(gameAchievements.Succeeded);
        }

        private void SetAchievementBoxValues(AchievementView achievementBoxValues, ClassAchievements classAchievements)
        {
            achievementBoxValues.ChangeTitle(classAchievements.Name);
            achievementBoxValues.ChangeDescription(classAchievements.Description);
            achievementBoxValues.ChangeSuccessColor(classAchievements.Succeeded);
        }
    }
}

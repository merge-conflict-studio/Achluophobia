﻿using System.Collections.Generic;
using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Achluophobia
{
    [AddComponentMenu("Game/Menu/OptionsMenuController")]
    public class OptionsMenuController : GameScript, IMenuController
    {
        private const string SoundValuePercentage = " %";
        private const string FullScreenOn = "Yes";
        private const string FullScreenOff = "No";

        [SerializeField, Tooltip("Previous menu")]
        private Menu previousMenu;

        [SerializeField, Tooltip("Screen Resolution to apply")]
        private GameObject screenResolutionToApply;

        [SerializeField, Tooltip("Fullscreen")]
        private GameObject fullScreenText;

        [SerializeField, Tooltip("Sound Text")]
        private GameObject soundText;

        [SerializeField, Tooltip("Sound Slider")]
        private Slider soundSlider;

        private List<Resolution> screenResolutions;

        private ActivityStack activityStack;

        private Selectable defaultSelectableButton;
        private int index;
        private bool isFullscreen;

        private void InjectOptionsMenuController([ApplicationScope] ActivityStack activityStack,
                                                 [ChildScope, Named(R.S.GameObject.FullscreenLeftArrow)] GameObject fullScreenLeftArrow)
        {
            this.activityStack = activityStack;
            defaultSelectableButton = fullScreenLeftArrow.GetComponent<Selectable>();
        }

        private void Awake()
        {
            InjectDependencies("InjectOptionsMenuController");

            screenResolutions = new List<Resolution>();

            foreach (Resolution resolution in Screen.resolutions)
            {
                screenResolutions.Add(resolution);
            }
            screenResolutionToApply.GetComponent<Text>().text = Screen.currentResolution.ToString();

            if (Screen.fullScreen)
            {
                fullScreenText.GetComponent<Text>().text = FullScreenOn;
                isFullscreen = true;
            }
            else
            {
                fullScreenText.GetComponent<Text>().text = FullScreenOff;
                isFullscreen = false;
            }
            index = screenResolutions.IndexOf(Screen.currentResolution);

            soundSlider.value = AudioListener.volume;
            soundText.GetComponent<Text>().text = soundText.GetComponent<Text>().text = (int)(soundSlider.value *100) + SoundValuePercentage;
        }

        [CalledOutsideOfCode]
        public void OnNextResolutionButtonPressed()
        {
            if (index < screenResolutions.Count-1)
            {
                screenResolutionToApply.GetComponent<Text>().text = screenResolutions[++index].ToString();
            }
        }

        [CalledOutsideOfCode]
        public void OnPreviousResolutionButtonPressed()
        {
            if (index > 0)
            {
                screenResolutionToApply.GetComponent<Text>().text = screenResolutions[--index].ToString();
            }
        }

        [CalledOutsideOfCode]
        public void OnFullscreenButtonPressed()
        {
            if (isFullscreen)
            {
                fullScreenText.GetComponent<Text>().text = FullScreenOff;
                isFullscreen = false;
            }
            else
            {
                fullScreenText.GetComponent<Text>().text = FullScreenOn;
                isFullscreen = true;
            }
        }

        [CalledOutsideOfCode]
        public void OnVolumeValueChanged()
        {
            soundText.GetComponent<Text>().text = Mathf.RoundToInt(soundSlider.value * 100) + SoundValuePercentage;
            AudioListener.volume = soundSlider.value;
        }

        [CalledOutsideOfCode]
        public void OnApplyButtonPressed()
        {
            Screen.SetResolution(screenResolutions[index].width, screenResolutions[index].height, isFullscreen);
        }

        [CalledOutsideOfCode]
        public void OnBackButtonPressed()
        {
            activityStack.StopCurrentMenu();
            activityStack.StartMenu(previousMenu);
        }

        public void OnCreate(params object[] parameters)
        {
            //Do nothing
        }

        public void OnResume()
        {
            defaultSelectableButton.Select();
        }

        public void OnPause()
        {
            //Do nothing
        }

        public void OnStop()
        {
            //Do nothing
        }
    }
}
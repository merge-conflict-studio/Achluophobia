﻿using System;
using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Achluophobia
{
    [AddComponentMenu("Game/Menu/DifficultyMenuController")]
    public class DifficultyMenuController : GameScript, IMenuController
    {
        [SerializeField, Tooltip("The previous menu in hierarchy")]
        private Menu previousMenu;

        [SerializeField, Tooltip("The Solo class Selection menu")]
        private Menu soloClassSelectionMenu;

        [SerializeField, Tooltip("The Coop class Selection menu")]
        private Menu coopClassSelectionMenu;

        [SerializeField, Tooltip("The Versus class Selection menu")]
        private Menu versusClassSelectionMenu;

        private ActivityStack activityStack;
        private ImportantGameValues importantGameValues;
        private GameObject spotlight;

        private Selectable defaultSelectedButton;

        private void InjectDifficultyMenuController([ApplicationScope] ActivityStack activityStack,
                                                    [ApplicationScope] ImportantGameValues importantGameValues,
                                                    [ChildScope, Named(R.S.GameObject.Easy)] GameObject easyButton,
                                                    [TagScope(R.S.Tag.Spotlight)] GameObject spotlight)

        {
            this.activityStack = activityStack;
            this.importantGameValues = importantGameValues;
            defaultSelectedButton = easyButton.GetComponent<Selectable>();
            this.spotlight = spotlight;
        }

        private void Awake()
        {
            InjectDependencies("InjectDifficultyMenuController");
        }

        [CalledOutsideOfCode]
        public void OnEasyButtonPressed()
        {
            spotlight.GetComponent<LightFlicker>().StopFlicker();
            importantGameValues.MapSize = MapSize.Small;
            StartClassSelectionMenu();
        }

        [CalledOutsideOfCode]
        public void OnNormalButtonPressed()
        {
            spotlight.GetComponent<LightFlicker>().StopFlicker();
            importantGameValues.MapSize = MapSize.Medium;
            StartClassSelectionMenu();
        }

        [CalledOutsideOfCode]
        public void OnHardButtonPressed()
        {
            spotlight.GetComponent<LightFlicker>().StopFlicker();
            importantGameValues.MapSize = MapSize.Large;
            StartClassSelectionMenu();
        }

        [CalledOutsideOfCode]
        public void OnBackButtonPressed()
        {
            activityStack.StopCurrentMenu();
            activityStack.StartMenu(previousMenu);
        }

        public void OnCreate(params object[] parameters)
        {
            spotlight.GetComponent<LightFlicker>().StartFlicker();
        }

        public void OnResume()
        {
            defaultSelectedButton.Select();
            defaultSelectedButton.OnSelect(null);
        }

        public void OnPause()
        {
            //Do nothing
        }

        public void OnStop()
        {
            //Do nothing
        }

        private void StartClassSelectionMenu()
        {
            activityStack.StopCurrentMenu();

            switch (importantGameValues.GameMode)
            {
                case GameMode.Solo:
                    activityStack.StartMenu(soloClassSelectionMenu);
                    break;
                case GameMode.Coop:
                    activityStack.StartMenu(coopClassSelectionMenu);
                    break;
                case GameMode.Versus:
                    activityStack.StartMenu(versusClassSelectionMenu);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
﻿using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Achluophobia
{
    [AddComponentMenu("Game/Menu/MainMenuController")]
    public class MainMenuController : GameScript, IMenuController
    {
        [SerializeField, Tooltip("Play mode menu")] 
        private Menu playMenu;
        
        [SerializeField, Tooltip("Options menu")] 
        private Menu optionsMenu;
        
        [SerializeField, Tooltip("Exit menu")]  
        private Menu exitMenu;

        [SerializeField, Tooltip("More menu")]
        private Menu moreMenu;

        private const float CameraXPostion = 567.3f;
        private const float CameraYPosition = 259.86f;
        private const float CameraZPosition = -11.01f;

        private ActivityStack activityStack;
        private Selectable defaultSelectedButton;
        private Camera mainCamera;
        private Camera secondCamera;

        private void InjectMainMenuController([ApplicationScope] ActivityStack activityStack,
                                              [ChildScope, Named(R.S.GameObject.PlayButton)] GameObject playButton,
                                              [TagScope(R.S.Tag.MainCamera)] Camera mainCamera,
                                              [TagScope(R.S.Tag.SecondCamera)] Camera secondCamera)
        {
            this.activityStack = activityStack;
            this.mainCamera = mainCamera;
            defaultSelectedButton = playButton.GetComponent<Selectable>();
            this.secondCamera = secondCamera;
        }

        private void Awake()
        {
            InjectDependencies("InjectMainMenuController");
            secondCamera.enabled = false;
            mainCamera.rect = new Rect(mainCamera.rect.x, mainCamera.rect.y, 1, 1);
            mainCamera.transform.position = new Vector3(CameraXPostion, CameraYPosition, CameraZPosition);
        }

        [CalledOutsideOfCode]
        public void OnPlayButtonPressed()
        {
            activityStack.StopCurrentMenu();
            activityStack.StartMenu(playMenu);
        }

        [CalledOutsideOfCode]
        public void OnOptionsButtonPressed()
        {
            activityStack.StopCurrentMenu();
            activityStack.StartMenu(optionsMenu);
        }

        [CalledOutsideOfCode]
        public void OnExitButtonPressed()
        {
            activityStack.StartMenu(exitMenu);
        }

        [CalledOutsideOfCode]
        public void OnMoreButtonPressed()
        {
            activityStack.StopCurrentMenu();
            activityStack.StartMenu(moreMenu);
        }

        public void OnCreate(params object[] parameters)
        {
            //Do nothing
        }

        public void OnResume()
        {
            defaultSelectedButton.Select();
        }

        public void OnPause()
        {
            //Nothing to do
        }

        public void OnStop()
        {
            //Nothing to do
        }
    }
}
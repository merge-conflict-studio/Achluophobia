﻿using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Achluophobia
{
    [AddComponentMenu("Game/Menu/CreditsMenuController")]
    public class CreditsMenuController : GameScript, IMenuController
    {
        [SerializeField, Tooltip("The previous menu")]
        private Menu previousMenu;

        [SerializeField, Tooltip("Emile's credit menu")]
        private Menu emileMenu;

        [SerializeField, Tooltip("Alexandre's credit menu")]
        private Menu alexandreMenu;

        [SerializeField, Tooltip("Philippe's credit menu")]
        private Menu philippeMenu;

        [SerializeField, Tooltip("Mathieu's credit menu")]
        private Menu mathieuMenu;

        [SerializeField, Tooltip("Jonathan's credit menu")]
        private Menu jonathanMenu;

        [SerializeField, Tooltip("Thierry's credit menu")]
        private Menu thierryMenu;

        [SerializeField, Tooltip("The button selected by default")]
        private Button defaultSelectedButton;

        private ActivityStack activityStack;

        private void InjectCreditsMenuController([ApplicationScope] ActivityStack activityStack)
        {
            this.activityStack = activityStack;
        }

        private void Awake()
        {
            InjectDependencies("InjectCreditsMenuController");
        }

        public void OnCreate(params object[] parameters)
        {
            //Do nothing
        }

        public void OnResume()
        {
            defaultSelectedButton.Select();
        }

        public void OnPause()
        {
            //Do nothing
        }

        public void OnStop()
        {
            //Do nothing
        }

        [CalledOutsideOfCode]
        public void OnBackButtonPressed()
        {
            activityStack.StopCurrentMenu();
            activityStack.StartMenu(previousMenu);
        }

        [CalledOutsideOfCode]
        public void OnEmileButtonPressed()
        {
            activityStack.StopCurrentMenu();
            activityStack.StartMenu(emileMenu);
        }

        [CalledOutsideOfCode]
        public void OnPhilippeButtonPressed()
        {
            activityStack.StopCurrentMenu();
            activityStack.StartMenu(philippeMenu);
        }

        [CalledOutsideOfCode]
        public void OnAlexandreButtonPressed()
        {
            activityStack.StopCurrentMenu();
            activityStack.StartMenu(alexandreMenu);
        }

        [CalledOutsideOfCode]
        public void OnMathieuButtonPressed()
        {
            activityStack.StopCurrentMenu();
            activityStack.StartMenu(mathieuMenu);
        }

        [CalledOutsideOfCode]
        public void OnJonathanButtonPressed()
        {
            activityStack.StopCurrentMenu();
            activityStack.StartMenu(jonathanMenu);
        }

        [CalledOutsideOfCode]
        public void OnThierryButtonPressed()
        {
            activityStack.StopCurrentMenu();
            activityStack.StartMenu(thierryMenu);
        }
    }
}

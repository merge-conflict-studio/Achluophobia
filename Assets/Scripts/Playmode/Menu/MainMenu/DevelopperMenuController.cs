﻿using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Achluophobia
{
    [AddComponentMenu("Game/Menu/DevelopperMenuController")]
    public class DevelopperMenuController : GameScript, IMenuController
    {
        [SerializeField, Tooltip("The previous menu")]
        private Menu previousMenu;

        [SerializeField, Tooltip("The button selected by default")]
        private Button defaultSelectedButton;

        [SerializeField, Tooltip("The menu's sound")]
        private AudioClip menuMusic;

        [SerializeField, Tooltip("The main theme")]
        private AudioClip mainTheme;

        private ActivityStack activityStack;
        private SoundPlayer soundPlayer;

        private void InjectDevelopperMenuController([ApplicationScope] ActivityStack activityStack,
                                               [ApplicationScope] SoundPlayer soundPlayer)
        {
            this.activityStack = activityStack;
            this.soundPlayer = soundPlayer;
        }

        private void Awake()
        {
            InjectDependencies("InjectDevelopperMenuController");
        }

        public void OnCreate(params object[] parameters)
        {
            //Do nothing
        }

        public void OnResume()
        {
            defaultSelectedButton.Select();
            soundPlayer.PlayMusicInLoop(menuMusic);
        }

        public void OnPause()
        {
            //Do nothing
        }

        public void OnStop()
        {
            soundPlayer.PlayMusicInLoop(mainTheme);
        }

        [CalledOutsideOfCode]
        public void OnBackButtonPressed()
        {
            activityStack.StopCurrentMenu();
            activityStack.StartMenu(previousMenu);
        }
    }
}
﻿using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Achluophobia
{
    [AddComponentMenu("Game/Menu/MoreMenuController")]
    public class MoreMenuController : GameScript, IMenuController
    {
        [SerializeField, Tooltip("The Stats menu")]
        private Menu statsMenu;

        [SerializeField, Tooltip("The Achievements menu")]
        private Menu achievementsMenu;

        [SerializeField, Tooltip("The Credits menu")]
        private Menu creditsMenu;

        [SerializeField, Tooltip("Main menu")]
        private Menu previousMenu;

        private ActivityStack activityStack;
        private Selectable defaultSelectedButton;
        private GameObject spotlight;

        private void InjectMoreMenuController([ApplicationScope] ActivityStack activityStack,
                                              [ChildScope, Named(R.S.GameObject.Stats)] GameObject statsButton,
                                              [TagScope(R.S.Tag.Spotlight)] GameObject spotlight)
        {
            this.activityStack = activityStack;
            defaultSelectedButton = statsButton.GetComponent<Selectable>();
            this.spotlight = spotlight;
        }

        private void Awake()
        {
            InjectDependencies("InjectMoreMenuController");
        }

        public void OnCreate(params object[] parameters)
        {
            spotlight.GetComponent<LightFlicker>().StartFlicker();
        }

        public void OnResume()
        {
            defaultSelectedButton.Select();
        }

        public void OnPause()
        {
            //Do nothing
        }

        public void OnStop()
        {
            //Do nothing
        }

        [CalledOutsideOfCode]
        public void OnStatsButtonPressed()
        {
            spotlight.GetComponent<LightFlicker>().StopFlicker();
            activityStack.StopCurrentMenu();
            activityStack.StartMenu(statsMenu);
        }

        [CalledOutsideOfCode]
        public void OnAchievementsButtonPressed()
        {
            spotlight.GetComponent<LightFlicker>().StopFlicker();
            activityStack.StopCurrentMenu();
            activityStack.StartMenu(achievementsMenu);
        }

        [CalledOutsideOfCode]
        public void OnCreditsButtonPressed()
        {
            spotlight.GetComponent<LightFlicker>().StopFlicker();
            activityStack.StopCurrentMenu();
            activityStack.StartMenu(creditsMenu);
        }

        [CalledOutsideOfCode]
        public void OnBackButtonPressed()
        {
            activityStack.StopCurrentMenu();
            activityStack.StartMenu(previousMenu);
        }
    }
}

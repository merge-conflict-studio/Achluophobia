﻿using UnityEngine;
using Harmony;
using UnityEngine.UI;

namespace Achluophobia
{
    [AddComponentMenu("Game/Menu/ExitMenuController")]
    public class ExitMenuController : GameScript, IMenuController
    {
        [SerializeField, Tooltip("The sound played on button press")]
        private AudioClip mainPressSound;

        private ActivityStack activityStack;
        private SoundPlayer soundPlayer;

        private Selectable defaultSelectedButton;

        private void InjectExitMenuController([ApplicationScope] ActivityStack activityStack,
                                              [ApplicationScope] SoundPlayer soundPlayer,
                                              [ChildScope, Named("No")] GameObject noButton)
        {
            this.activityStack = activityStack;
            this.soundPlayer = soundPlayer;
            defaultSelectedButton = noButton.GetComponent<Selectable>();
        }

        private void Awake()
        {
            InjectDependencies("InjectExitMenuController");
        }

        [CalledOutsideOfCode]
        public void OnYesButtonClicked()
        {
            soundPlayer.PlaySound(mainPressSound);
            activityStack.StopCurrentActivity();
        }

        [CalledOutsideOfCode]
        public void OnNoButtonClicked()
        {
            soundPlayer.PlaySound(mainPressSound);
            activityStack.StopCurrentMenu();
        }

        public void OnCreate(params object[] parameters)
        {
            //Do nothing
        }

        public void OnResume()
        {
            defaultSelectedButton.Select();
        }

        public void OnPause()
        {
            //Do nothing
        }

        public void OnStop()
        {
            //Do nothing
        }
    }
}
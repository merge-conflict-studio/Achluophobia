﻿using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Achluophobia
{
    [AddComponentMenu("Game/Menu/PlayMenuController")]
    public class PlayMenuController : GameScript, IMenuController
    {
        [SerializeField, Tooltip("Difficulty menu")]
        private Menu difficultyMenu;

        [SerializeField, Tooltip("Main menu")]
        private Menu mainMenu;

        private ActivityStack activityStack;
        private ImportantGameValues importantGameValues;
        private Selectable onePlayerButtonSelectable;

        private void InjectPlayMenuController([ApplicationScope] ActivityStack activityStack,
                                              [ApplicationScope] ImportantGameValues importantGameValues,
                                              [ChildScope, Named(R.S.GameObject.OnePSurvival)] GameObject onePlayerButton)
        {
            this.activityStack = activityStack;
            this.importantGameValues = importantGameValues;
            onePlayerButtonSelectable = onePlayerButton.GetComponent<Selectable>();
        }

        private void Awake()
        {
            InjectDependencies("InjectPlayMenuController");
        }

        [CalledOutsideOfCode]
        public void OnOnePlayerSurvivalButtonPressed()
        {
            importantGameValues.GameMode = GameMode.Solo;

            activityStack.StopCurrentMenu();
            activityStack.StartMenu(difficultyMenu);
        }

        [CalledOutsideOfCode]
        public void OnTwoPlayerSurvivalButtonPressed()
        {
            importantGameValues.GameMode = GameMode.Coop;

            activityStack.StopCurrentMenu();
            activityStack.StartMenu(difficultyMenu);
        }

        [CalledOutsideOfCode]
        public void OnTwoVsTwoRaceButtonPressed()
        {
            importantGameValues.GameMode = GameMode.Versus;

            activityStack.StopCurrentMenu();
            activityStack.StartMenu(difficultyMenu);
        }

        [CalledOutsideOfCode]
        public void OnBackButtonPressed()
        {
            activityStack.StopCurrentMenu();
            activityStack.StartMenu(mainMenu);
        }

        public void OnCreate(params object[] parameters)
        {
            //Do nothing
        }

        public void OnResume()
        {
            onePlayerButtonSelectable.Select();
        }

        public void OnPause()
        {
            //Do nothing
        }

        public void OnStop()
        {
            //Do nothing
        }
    }
}
﻿using UnityEngine;
using Harmony;
using UnityEngine.UI;

namespace Achluophobia
{
    [AddComponentMenu("Game/Menu/PauseMenuController")]
    public class PauseMenuController : GameScript, IMenuController
    {
        [SerializeField, Tooltip("Button that makes you go back to the main menu")]
        private Button resumeButton;

        [SerializeField, Tooltip("The main activity")]
        private Activity mainActivity;

        [SerializeField, Tooltip("Options Menu")]
        private Menu optionsMenu;

        [SerializeField, Tooltip("Quick Restart Menu")]
        private Menu quickRestartMenu;

        private ActivityStack activityStack;
        private ImportantGameValues importantGameValues;
        private Camera secondCamera;

        private void InjectPauseMenuController([ApplicationScope] ActivityStack activityStack,
                                               [TagScope(R.S.Tag.SecondCamera)] Camera secondCamera,
                                               [ApplicationScope] ImportantGameValues importantGameValues)

        {
            this.activityStack = activityStack;
            this.importantGameValues = importantGameValues;
            this.secondCamera = secondCamera;
        }

        private void Awake()
        {
            InjectDependencies("InjectPauseMenuController");
        }

        [CalledOutsideOfCode]
        public void OnResumeButtonClicked()
        {
            UnfreezeTime();
            activityStack.StopCurrentMenu();
        }

        [CalledOutsideOfCode]
        public void OnQuitButtonClicked()
        {
            importantGameValues.IsPlayerQuittingAGame = true;
            UnfreezeTime();
            activityStack.StopAllCoroutines();
            activityStack.StopCurrentMenu();
            activityStack.StopCurrentActivity();
            Destroy(secondCamera.gameObject.GetComponent<FollowingCamera>());
        }

        [CalledOutsideOfCode]
        public void OnQuickRestartButtonClicked()
        {
            activityStack.StopCurrentMenu();
            activityStack.StartMenu(quickRestartMenu);
        }

        [CalledOutsideOfCode]
        public void OnInGameOptionsButtonClicked()
        {
            activityStack.StopCurrentMenu();
            activityStack.StartMenu(optionsMenu);
        }

        public void OnCreate(params object[] parameters)
        {
            //Do nothing
        }

        public void OnResume()
        {
            resumeButton.Select();
            resumeButton.OnSelect(null);
            if (!TimeExtensions.IsPaused())
            {
                FreezeTime();
                foreach (GameObject player in importantGameValues.Players)
                {
                    player.GetComponent<PlayerController>().UnsubscribeFromBasicEvents(false);
                }
            }
        }

        public void OnPause()
        {
            //Do nothing
        }

        public void OnStop()
        {
            if (!TimeExtensions.IsPaused())
            {
                foreach (GameObject player in importantGameValues.Players)
                {
                    player.GetComponent<PlayerController>().SubscribeToBasicEvents(false);
                }
            }
        }

        private static void FreezeTime()
        {
            TimeExtensions.Pause();
        }

        private static void UnfreezeTime()
        {
            TimeExtensions.Resume();
        }
    }
}
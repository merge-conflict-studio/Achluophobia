﻿using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Achluophobia
{
    [AddComponentMenu("Game/Menu/QuickRestartMenuController")]
    public class QuickRestartMenuController : GameScript, IMenuController
    {
        [SerializeField, Tooltip("Main menu")] 
        private Menu pauseMenu;

        [SerializeField, Tooltip("Game activity")] 
        private Activity gameActivity;

        [SerializeField, Tooltip("Button for no")] 
        private Button noButton;

        private ActivityStack activityStack;
        private ImportantGameValues importantGameValues;

        private void InjectQuickRestartMenuController([ApplicationScope] ActivityStack activityStack,
                                                      [ApplicationScope] ImportantGameValues importantGameValues)
        {
            this.activityStack = activityStack;
            this.importantGameValues = importantGameValues;
        }

        private void Awake()
        {
            InjectDependencies("InjectQuickRestartMenuController");
        }

        [CalledOutsideOfCode]
        public void OnYesButtonPressed()
        {
            importantGameValues.IsPlayerQuittingAGame = false;
            Time.timeScale = 1; //Pour fix le probleme de initial Timescale dans le TimeExtension class
            activityStack.StopCurrentMenu();
            activityStack.StopAllCoroutines();
            activityStack.RestartCurrentActivity();
        }

        [CalledOutsideOfCode]
        public void OnNoButtonPressed()
        {
            activityStack.StopCurrentMenu();
            activityStack.StartMenu(pauseMenu);
        }

        public void OnCreate(params object[] parameters)
        {
            //Do nothing
        }

        public void OnResume()
        {
            noButton.Select();
        }

        public void OnPause()
        {
            //Do nothing
        }

        public void OnStop()
        {
            //Do nothing
        }
    }
}
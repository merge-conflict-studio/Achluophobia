﻿using System.Collections;
using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Achluophobia
{
    [AddComponentMenu("Game/World/Ui/View/TimeBeforeRespawnView")]
    public class TimeBeforeRespawnView : GameScript
    {
        private const string TimeBeforeRespawn = "Time before respawn : ";
        
        private Text textViewTeam1;
        private Text textViewTeam2;

        public void InjectTimeBeforeRespawnView([Named(R.S.GameObject.TimeBeforeRespawnTeam1), EntityScope] Text textViewTeam1,
                                                [Named(R.S.GameObject.TimeBeforeRespawnTeam2), EntityScope] Text textViewTeam2)
        {
            this.textViewTeam1 = textViewTeam1;
            textViewTeam1.color = Color.white;
            this.textViewTeam2 = textViewTeam2;
            textViewTeam2.color = Color.white;
        }

        private void Awake()
        {
            InjectDependencies("InjectTimeBeforeRespawnView");
            textViewTeam1.text = "";
            textViewTeam2.text = "";
        }

        public IEnumerator StartRespawnTimer(float timeInSeconds, Team team)
        {
            int timeLeft = (int) timeInSeconds;
            if (team == Team.One)
            {
                while (timeLeft > 0)
                {
                    textViewTeam1.text = TimeBeforeRespawn + timeLeft;
                    yield return new WaitForSeconds(1);
                    timeLeft--;
                }
                textViewTeam1.text = "";
            }
            else
            {
                while (timeLeft > 0)
                {
                    textViewTeam2.text = TimeBeforeRespawn + timeLeft;
                    yield return new WaitForSeconds(1);
                    timeLeft--;
                }
                textViewTeam2.text = "";
            }
        }
    }
}
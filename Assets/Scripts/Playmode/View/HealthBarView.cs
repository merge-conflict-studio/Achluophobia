﻿using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/World/Ui/View/HealthBarView")]
    public class HealthBarView : GameScript
    {
        public void SetHealthPercentage(float percentage)
        {
            if (transform != null)
            {
                transform.localScale = new Vector3(percentage, transform.localScale.y, transform.localScale.z);
            }
        }
    }
}
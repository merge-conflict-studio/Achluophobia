﻿using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Achluophobia
{
    [AddComponentMenu("Game/Controller/AchievementView")]
    public class AchievementView : GameScript
    {
        private readonly Color successful = new Color(0, 255, 0, 255);
        private readonly Color unsuccessful = new Color(255, 255, 255, 125);

        private Text title;
        private Text description;
        private Image successCircle;

        private void InjectAchievementsController([ChildScope, Named(R.S.GameObject.Title)] Text title,
                                                  [ChildScope, Named(R.S.GameObject.Description)] Text description,
                                                  [ChildScope, Named(R.S.GameObject.Success)] Image successCircle)
        {
            this.title = title;
            this.description = description;
            this.successCircle = successCircle;
        }

        private void Awake()
        {
            InjectDependencies("InjectAchievementsController");
        }

        public void ChangeTitle(string newTitle)
        {
            title.text = newTitle;
        }

        public void ChangeDescription(string newDescription)
        {
            description.text = newDescription;
        }

        public void ChangeSuccessColor(bool isSuccessful)
        {
            successCircle.color = isSuccessful ? successful : unsuccessful;
        }
    }
}

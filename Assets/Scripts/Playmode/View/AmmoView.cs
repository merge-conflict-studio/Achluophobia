﻿using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Achluophobia
{
    [AddComponentMenu("Game/View/AmmoView")]
    public class AmmoView : GameScript
    {
        private const string ItsASledgeHammer = "IT\'S A SLEDGEHAMMER"; //Texte afficher si le joueur utilise un mastodonte au lieu d'avoir aucun texte
        private Text textView;
        private ActiveWeaponView activeWeapon;

        public void InjectAmmoView([GameObjectScope] Text textView, 
                                   [ChildScope] ActiveWeaponView activeWeapon)
        {
            this.textView = textView;
            this.activeWeapon = activeWeapon;
            textView.color = Color.white;
        }

        public void Awake()
        {
            InjectDependencies("InjectAmmoView");
        }

        public void SetAmmoCount(int ammoLeftInWeapon, int ammoLeftOnPlayer, bool isUnlimited)
        {
            if (ammoLeftInWeapon == 0 && ammoLeftOnPlayer == 0)
            {
                textView.color = Color.red;
            }
            else if (textView.color == Color.red)
            {
                textView.color = Color.white;
            }

            string ammoToShow = isUnlimited ? ammoLeftInWeapon + " / ∞" : ammoLeftInWeapon + " / " + ammoLeftOnPlayer;
            activeWeapon.ChangeImage(isUnlimited, false);
            if (textView != null)
            {
                textView.text = ammoToShow;
            }
        }

        public void SetSledgeHammer()
        {
            textView.color = Color.white;
            textView.text = ItsASledgeHammer;
            activeWeapon.ChangeImage(false, true);
        }
    }
}
﻿using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Achluophobia
{
    [AddComponentMenu("Game/View/ItemView")]
    public class ItemView : GameScript
    {
        private const string StartingText = "Collect 3 Jerry Cans";
        private const string CollectText = "Collect ";
        private const string UseYourItemsToMoveOnText = "Use your items to move on";
        private const string GoToStairCaseText = "Go to the staircase";
        private const string MultipleLeftText = "s left";
        private const string LeftText = " left";

        private Text textView;

        public void InjectItemView([GameObjectScope] Text textView)
        {
            this.textView = textView;
            textView.color = Color.white;
        }

        public void Awake()
        {
            InjectDependencies("InjectItemView");
            textView.text = StartingText;
        }

        public void SetItemToFind(int numberOfItemsLeft, string itemCollected)
        {
            if (numberOfItemsLeft == 0)
            {
                textView.text = UseYourItemsToMoveOnText;
            }
            else if (numberOfItemsLeft > 1)
            {
                textView.text = CollectText + numberOfItemsLeft + " " + itemCollected + 's';
            }
            else
            {
                textView.text = CollectText + numberOfItemsLeft + " " + itemCollected;
            }
        }

        public void SetMiniGamesToComplete(int numberOfMiniGamesLeft, string activeMiniGame)
        {
            if (numberOfMiniGamesLeft == 0)
            {
                textView.text = GoToStairCaseText;
            }
            else if (numberOfMiniGamesLeft > 1)
            {
                textView.text = numberOfMiniGamesLeft + " " + activeMiniGame + MultipleLeftText;
            }
            else
            {
                textView.text = numberOfMiniGamesLeft + " " + activeMiniGame + LeftText;
            }
        }
    }
}
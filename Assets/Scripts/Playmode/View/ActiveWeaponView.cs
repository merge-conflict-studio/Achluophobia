﻿using UnityEngine;
using UnityEngine.UI;
using Harmony;

namespace Achluophobia
{
    [AddComponentMenu("Game/View/ActiveWeaponView")]
    public class ActiveWeaponView : GameScript
    {
        [SerializeField, Tooltip("Main weapon sprite")]
        private Sprite mainWeaponSprite;

        [SerializeField, Tooltip("Pistol sprite")]
        private Sprite pistolSprite;

        [SerializeField, Tooltip("Sledgehammer sprite")]
        private Sprite sledgeHammerSprite;

        private Image image;

        public void InjectActiveWeapon([GameObjectScope] Image image)
        {
            this.image = image;
        }

        public void Awake()
        {
            InjectDependencies("InjectActiveWeapon");
            image.sprite = sledgeHammerSprite;
        }

        public void ChangeImage(bool isPistol, bool isSledgeHammer)
        {
            if (!isSledgeHammer)
            {
                image.sprite =
                    isPistol
                        ? pistolSprite
                        : mainWeaponSprite;
            }
            else
            {
                image.sprite = sledgeHammerSprite;
            }
        }
    }
}
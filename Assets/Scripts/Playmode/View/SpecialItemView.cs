﻿using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Achluophobia
{
    [AddComponentMenu("Game/World/Ui/View/SpecialItemView")]
    public class SpecialItemView : GameScript
    {
        private const string SpecialItemsLeft = "Special items left: ";
        private Text specialItemText;

        private void InjectSpecialItemView([GameObjectScope] Text specialItemText)
        {
            this.specialItemText = specialItemText;
        }
        private void Awake()
        {
            InjectDependencies("InjectSpecialItemView");
            specialItemText.color = Color.white;
        }

        public void SetSpecialItemCount(int count)
        {
            if (transform != null)
            {
                if (count == 0)
                {
                    specialItemText.color = Color.red;
                }
                else if (specialItemText.color == Color.red)
                {
                    specialItemText.color = Color.white;
                }
                specialItemText.text = SpecialItemsLeft + count;
            }
        }
    }
}

﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Aspect/SpawnParticleOnHit")]
    public class SpawnParticleOnHit : GameScript
    {
        private RaycastShooter raycastShooter;
        private ParticleSpawner particleSpawner;

        private void InjectSpawnParticleOnHit([GameObjectScope] RaycastShooter raycastShooter,
                                              [ApplicationScope] ParticleSpawner particleSpawner)
        {
            this.raycastShooter = raycastShooter;
            this.particleSpawner = particleSpawner;
        }

        private void Awake()
        {
            InjectDependencies("InjectSpawnParticleOnHit");
        }

        private void OnEnable()
        {
            raycastShooter.OnRayCastHit += OnHit;
        }

        private void OnDisable()
        {
            raycastShooter.OnRayCastHit -= OnHit;
        }

        private void OnHit(RaycastHit hit)
        {
            ObjectMaterial material;

            switch (hit.collider.gameObject.GetRoot().tag)
            {
                case R.S.Tag.Enemy:
                    material = ObjectMaterial.Flesh;
                    break;
                default:
                    material = ObjectMaterial.Wood;
                    break;
            }
            particleSpawner.Spawn(hit.point, Quaternion.LookRotation(hit.normal), material);
        }
    }
}
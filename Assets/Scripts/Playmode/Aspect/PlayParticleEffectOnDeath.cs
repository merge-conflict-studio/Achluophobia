﻿using System.Security.Cryptography;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Aspect/PlayParticleEffectOnDeath")]
    public class PlayParticleEffectOnDeath : GameScript
    {
        [SerializeField, Tooltip("The effect to be played when the caracter dies")]
        private GameObject particleEffect;

        [SerializeField, Tooltip("Alternate particle effect with easter egg sound")]
        private GameObject alternateParticleEffect;

        [SerializeField, Tooltip("The lifetime of the effect in seconds")]
        private float particleLifetime;

        private Health health;

        private void InjectPlayParticleEffectOnDeath([EntityScope] Health health)
        {
            this.health = health;
        }

        private void Awake()
        {
            InjectDependencies("InjectPlayParticleEffectOnDeath");
        }

        private void OnEnable()
        {
            health.OnDeath += OnDeath;
        }

        private void OnDisable()
        {
            health.OnDeath -= OnDeath;
        }

        private void OnDeath(GameObject killer)
        {
            GameObject particle = Instantiate(SelectRandomSound(), transform.position, transform.rotation);
            Destroy(particle, particleLifetime);
        }

        private GameObject SelectRandomSound()
        {
            int percentage = Random.Range(0, 100);

            return percentage == 1 ? alternateParticleEffect : particleEffect;
        }
    }
}
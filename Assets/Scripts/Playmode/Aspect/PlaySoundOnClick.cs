﻿using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Achluophobia
{
    [AddComponentMenu("Game/Aspect/PlaySoundOnClick")]
    public class PlaySoundOnClick : GameScript
    {
        [SerializeField, Tooltip("AudioClip")] 
        private AudioClip audioClip;

        private SoundPlayer soundPlayer;
        private Button button;

        private void InjectPlaySoundOnClick([ApplicationScope] SoundPlayer soundPlayer,
                                            [GameObjectScope] Button button)
        {
            this.soundPlayer = soundPlayer;
            this.button = button;
        }

        private void Awake()
        {
            InjectDependencies("InjectPlaySoundOnClick");
        }

        private void Start()
        {
            button.Events().OnClick += OnClicked;
        }

        private void OnDestroy()
        {
            button.Events().OnClick -= OnClicked;
        }

        private void OnClicked()
        {
            soundPlayer.PlaySound(audioClip);
        }
    }
}
﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Aspect/PlaySoundOnRaycastHit")]
    public class PlaySoundOnRaycastHit : GameScript
    {
        [SerializeField, Tooltip("AudioClip")] 
        private AudioClip audioClip;

        private SoundPlayer soundPlayer;
        private RaycastShooter raycastShooter;

        private void InjectPlaySoundOnHit([ApplicationScope] SoundPlayer soundPlayer,
                                          [GameObjectScope] RaycastShooter raycastShooter)
        {
            this.soundPlayer = soundPlayer;
            this.raycastShooter = raycastShooter;
        }

        private void Awake()
        {
            InjectDependencies("InjectPlaySoundOnHit");
        }

        private void OnEnable()
        {
            raycastShooter.OnRayCastHit += OnRaycastHit;
        }

        private void OnDisable()
        {
            raycastShooter.OnRayCastHit -= OnRaycastHit;
        }

        private void OnRaycastHit(RaycastHit hit)
        {
            soundPlayer.PlaySound(audioClip);
        }
    }
}
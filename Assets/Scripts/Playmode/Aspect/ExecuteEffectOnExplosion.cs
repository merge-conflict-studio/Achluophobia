﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Aspect/ExecuteEffectOnExplosion")]
    public class ExecuteEffectOnExplosion : GameScript
    {
        private GameObject rootObject;
        private ExplosionSensor explosionSensor;

        private void InjectExecuteEffectOnExplosion([RootScope] GameObject rootObject,
                                                    [EntityScope] ExplosionSensor explosionSensor)
        {
            this.rootObject = rootObject;
            this.explosionSensor = explosionSensor;
        }

        private void Awake()
        {
            InjectDependencies("InjectExecuteEffectOnExplosion");
        }

        private void OnEnable()
        {
            explosionSensor.OnExplosionHitReceived += OnExplosionHit;
        }

        private void OnDisable()
        {
            explosionSensor.OnExplosionHitReceived -= OnExplosionHit;
        }

        private void OnExplosionHit(InstantEffect effect)
        {
            effect.ApplyOn(this, rootObject);
        }
    }
}

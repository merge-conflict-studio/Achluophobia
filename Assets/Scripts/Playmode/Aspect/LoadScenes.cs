﻿using Harmony;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Achluophobia
{
    [AddComponentMenu("Game/Aspect/LoadScenes")]
    public class LoadScenes : GameScript
    {
        [SerializeField, Tooltip("Scenes to load")] 
        private R.E.Scene[] scenes;

        [SerializeField, Tooltip("Load Scene Mode")] 
        private LoadSceneMode mode = LoadSceneMode.Additive;

        private void Start()
        {
            foreach (R.E.Scene scene in scenes)
            {
                SceneManager.LoadScene(R.S.Scene.ToString(scene), mode);
            }
        }
    }
}
﻿using UnityEngine;
using Harmony;

namespace Achluophobia
{
    [AddComponentMenu("Game/Aspect/InstanciateOnDeath")]
    public class InstanciateOnDeath : GameScript
    {
        [SerializeField, Tooltip("the prefab of the object we want to instantiate on death")] 
        private GameObject prefab;

        private Transform rootTranform;
        private Health health;

        private void InjectInstanciateOnDeath([RootScope] Transform rootTranform,
                                              [EntityScope] Health health)
        {
            this.rootTranform = rootTranform;
            this.health = health;
        }

        private void Awake()
        {
            InjectDependencies("InjectInstanciateOnDeath");
        }

        private void OnEnable()
        {
            health.OnDeath += OnDeath;
        }

        private void OnDisable()
        {
            health.OnDeath -= OnDeath;
        }

        private void OnDeath(GameObject killer)
        {
            Instantiate(prefab, rootTranform.position, Quaternion.Euler(Vector3.zero));
        }
    }
}
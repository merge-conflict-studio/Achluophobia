﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public class PlaySoundOnAmmoDepleted : GameScript
    {
        [SerializeField, Tooltip("Sound to play on ammo depleted")]
        private AudioClip audioClip;

        private SoundPlayer soundPlayer;
        private Ammo ammo;

        private void InjectPlaySoundOnAmmoDepleted(
            [ApplicationScope] SoundPlayer soundPlayer,
            [GameObjectScope] Ammo ammo)
        {
            this.soundPlayer = soundPlayer;
            this.ammo = ammo;
        }

        private void Awake()
        {
            InjectDependencies("InjectPlaySoundOnAmmoDepleted");
        }

        private void OnEnable()
        {
            ammo.OnAmmoDepleted += OnAmmoDepleted;
        }

        private void OnDisable()
        {
            ammo.OnAmmoDepleted -= OnAmmoDepleted;
        }

        private void OnAmmoDepleted()
        {
            soundPlayer.PlaySound(audioClip);
        }
    }
}


﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Aspect/DestroyOnPickableSpecialItemStimulus")]
    public class DestroyOnPickableSpecialItemStimulus : GameScript
    {
        private SpecialItemStimulus pickableSpecialItemStimulus;
        private EntityDestroyer entityDestroyer;
        private ImportantGameValues importantGameValues;

        private void InjectDestroyOnPickableSpecialItemStimulus([EntityScope] SpecialItemStimulus pickableSpecialItemStimulus,
                                                                [GameObjectScope] EntityDestroyer entityDestroyer,
                                                                [ApplicationScope] ImportantGameValues importantGameValues)
        {
            this.pickableSpecialItemStimulus = pickableSpecialItemStimulus;
            this.entityDestroyer = entityDestroyer;
            this.importantGameValues = importantGameValues;
        }

        private void Awake()
        {
            InjectDependencies("InjectDestroyOnPickableSpecialItemStimulus");
        }

        private void OnEnable()
        {
            pickableSpecialItemStimulus.OnSpecialItemPicked += OnPickableSpecialItemCollected;
        }

        private void OnDisable()
        {
            pickableSpecialItemStimulus.OnSpecialItemPicked -= OnPickableSpecialItemCollected;
        }

        private void OnPickableSpecialItemCollected()
        {
            entityDestroyer.Destroy();
            importantGameValues.SpecialItemsToPickup++;
        }
    }
}
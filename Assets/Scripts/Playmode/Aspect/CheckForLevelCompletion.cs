﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public class CheckForLevelCompletion : GameScript
    {
        private ImportantGameValues importantGameValues;

        private void InjectCheckForLevelCompletion([ApplicationScope] ImportantGameValues importantGameValues)
        {
            this.importantGameValues = importantGameValues;
        }

        private void Awake()
        {
            InjectDependencies("InjectCheckForLevelCompletion");
        }

        public bool IsLevelCompleted(MiniGameData miniGameData, GameObject playerToCheck)
        {
            return miniGameData.CheckAllMiniGameCompleted() && CheckIfAllPlayersAreAlive(playerToCheck);
        }

        private bool CheckIfAllPlayersAreAlive(GameObject playerToCheck)
        {
            if (importantGameValues.GameMode != GameMode.Versus)
            {
                return importantGameValues.NumberOfDeadPlayers == 0;
            }
            if (playerToCheck.GetComponent<PlayerController>().PlayerTeam == Team.One)
            {
                return importantGameValues.NumberOfDeadPlayersTeam1 == 0;
            }
            return importantGameValues.NumberOfDeadPlayersTeam2 == 0;
        }
    }
}

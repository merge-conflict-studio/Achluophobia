﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Aspect/ExecuteEffectOnCollect")]
    public class ExecuteEffectOnCollect : GameScript
    {
        private GameObject rootGameObject;
        private ItemSensor itemSensor;
        private SpecialItemSensor specialItemSensor;

        private void InjectExecuteEffectOnCollect([RootScope] GameObject rootGameObject,
                                                  [EntityScope] ItemSensor itemSensor,
                                                  [EntityScope] SpecialItemSensor specialItemSensor)
        {
            this.rootGameObject = rootGameObject;
            this.itemSensor = itemSensor;
            this.specialItemSensor = specialItemSensor;
        }

        private void Awake()
        {
            InjectDependencies("InjectExecuteEffectOnCollect");
        }

        private void OnEnable()
        {
            itemSensor.OnCollectItem += OnCollectItem;
            specialItemSensor.OnCollectItem += OnCollectItem;
        }

        private void OnDisable()
        {
            itemSensor.OnCollectItem -= OnCollectItem;
            specialItemSensor.OnCollectItem -= OnCollectItem;
        }

        private void OnCollectItem(InstantEffect effect)
        {
            effect.ApplyOn(this, rootGameObject);
        }
    }
}
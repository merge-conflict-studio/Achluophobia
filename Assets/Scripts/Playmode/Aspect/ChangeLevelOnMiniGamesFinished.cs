﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public delegate void MiniGamesChangedEventHandler(int numberOfMiniGamesLeft, GameObject miniGamesToFinish);

    [AddComponentMenu("Game/Aspect/ChangeLevelOnMiniGamesFinished")]
    public class ChangeLevelOnMiniGamesFinished : GameScript
    {
        private MiniGameEventChannel miniGameEventChannel;

        private MiniGameData miniGameData;

        public event MiniGamesChangedEventHandler OnMiniGameCompleted;

        private void InjectChangeLevelOnMiniGamesFinished([EventChannelScope] MiniGameEventChannel miniGameEventChannel,
                                                          [EntityScope] MiniGameData miniGameData)
        {
            this.miniGameEventChannel = miniGameEventChannel;
            this.miniGameData = miniGameData;
        }

        private void Awake()
        {
            InjectDependencies("InjectChangeLevelOnMiniGamesFinished");
        }

        private void OnEnable()
        {
            miniGameEventChannel.OnEventPublished += OnMiniGameComplete;
        }

        private void OnDisable()
        {
            miniGameEventChannel.OnEventPublished -= OnMiniGameComplete;
        }

        private void IncrementMiniGameCompletedCount(GameObject miniGame)
        {
            if (miniGame.CompareTag(miniGameData.LevelName))
            {
                miniGameData.MiniGamesCompleted++;
                int numberOfMiniGameFinishedInLevel = miniGameData.MiniGamesCompleted;
                int numberOfMiniGameToFinishInLevel = miniGameData.MiniGamesToFinish;

                if (OnMiniGameCompleted != null)
                    OnMiniGameCompleted(numberOfMiniGameToFinishInLevel - numberOfMiniGameFinishedInLevel,
                        miniGame);
            }
        }

        private void OnMiniGameComplete(MiniGameEvent miniGameEvent)
        {
            IncrementMiniGameCompletedCount(miniGameEvent.MissionProp);
        }
    }
}
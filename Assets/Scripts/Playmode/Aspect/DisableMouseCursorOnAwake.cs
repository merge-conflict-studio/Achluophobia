﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public class DisableMouseCursorOnAwake : Script
    {
        [SerializeField, Tooltip("The value if we want the mouse to be visible or not for debugging values")]
        private bool isDisabled;

        private void Awake()
        {
            if (isDisabled)
            {
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
            else
            {
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }
        }
    }
}
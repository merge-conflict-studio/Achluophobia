﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Aspect/DestroyOnQuestItemStimulus")]
    public class DestroyQuestItemOnStimulus : GameScript
    {
        private QuestItemStimulus questItemStimulus;
        private EntityDestroyer entityDestroyer;

        private void InjectDestroyOnQuestItemStimulus([EntityScope] QuestItemStimulus questItemStimulus,
                                                      [GameObjectScope] EntityDestroyer entityDestroyer)
        {
            this.questItemStimulus = questItemStimulus;
            this.entityDestroyer = entityDestroyer;
        }

        private void Awake()
        {
            InjectDependencies("InjectDestroyOnQuestItemStimulus");
        }

        private void OnEnable()
        {
            questItemStimulus.OnQuestItemCollected += OnQuestItemCollected;
        }

        private void OnDisable()
        {
            questItemStimulus.OnQuestItemCollected -= OnQuestItemCollected;
        }

        private void OnQuestItemCollected(GameObject questItem)
        {
            entityDestroyer.Destroy();
        }
    }
}
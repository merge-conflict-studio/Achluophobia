﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Aspect/SpawnOnButtonClick")]
    public class SpawnOnButtonClick : GameScript
    {
        private PlayerController playerController;
        private GameObjectSpawner specialItemSpawner;

        private void InjectSpawnOnButtonClick([EntityScope] PlayerController playerController,
                                              [EntityScope] GameObjectSpawner specialItemSpawner)
        {
            this.playerController = playerController;
            this.specialItemSpawner = specialItemSpawner;
        }

        private void Awake()
        {
            InjectDependencies("InjectSpawnOnButtonClick");
        }

        private void OnEnable()
        {
            playerController.OnTryingToSpawnSpecialItem += SpawnSpecialItem;
        }

        private void OnDisable()
        {
            playerController.OnTryingToSpawnSpecialItem -= SpawnSpecialItem;
        }

        private void SpawnSpecialItem()
        {
            specialItemSpawner.Spawn(playerController.GetWeaponTransform());
        }
    }
}

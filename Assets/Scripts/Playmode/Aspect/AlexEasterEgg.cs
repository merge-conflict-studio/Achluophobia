﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Achluophobia
{
    public class AlexEasterEgg : GameScript
    {
        [SerializeField, Tooltip("Profile image")]
        private GameObject mainImage;

        [SerializeField, Tooltip("Cat image")]
        private GameObject catImage;

        private void OnEnable()
        {
            StartCoroutine(EasterEgg());
        }

        private void OnDisable()
        {
            mainImage.SetActive(true);
            catImage.SetActive(false);
        }

        private IEnumerator EasterEgg()
        {
            yield return new WaitForSeconds(45);

            mainImage.SetActive(false);
            catImage.SetActive(true);
        }
    }
}

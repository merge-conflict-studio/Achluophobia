﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public delegate void MastodonteAchievementEventHandler();

    [AddComponentMenu("Game/Aspect/KeepTrackOfMastodonteDamageTakenPerGame")]
    public class KeepTrackOfMastodonteDamageTakenPerGame : GameScript
    {
        private const int AchievementDamageValue = 2000;

        private int damageTakenPerLife;
        private HitSensor hitSensor;
        private PlayerDeathEventChannel playerDeathEventChannel;

        public event MastodonteAchievementEventHandler OnSuccess;

        private void InjectKeepTrackOfMastodonteDamageTakenPerGame(
            [EntityScope] HitSensor hitSensor,
            [EventChannelScope] PlayerDeathEventChannel playerDeathEventChannel)
        {
            this.hitSensor = hitSensor;
            this.playerDeathEventChannel = playerDeathEventChannel;
        }

        private void Awake()
        {
            InjectDependencies("InjectKeepTrackOfMastodonteDamageTakenPerGame");
            damageTakenPerLife = 0;
        }

        private void OnEnable()
        {
            hitSensor.OnHit += IncrementDamageTakenPerGame;
            playerDeathEventChannel.OnEventPublished += ResetDamageTakenPerLife;
        }

        private void OnDisable()
        {
            hitSensor.OnHit -= IncrementDamageTakenPerGame;
            playerDeathEventChannel.OnEventPublished -= ResetDamageTakenPerLife;
        }

        private void IncrementDamageTakenPerGame(GameObject attacker, int damage, bool explosionHit)
        {
            damageTakenPerLife += damage;
            if(damageTakenPerLife >= AchievementDamageValue) NotifyAchievementSuccess();
        }

        private void ResetDamageTakenPerLife(PlayerDeathEvent playerDeathEvent)
        {
            if (playerDeathEvent.DeadPlayer == GetRoot())
            {
                damageTakenPerLife = 0;
            }
        }

        private void NotifyAchievementSuccess()
        {
            if (OnSuccess != null) OnSuccess();
        }
    }
}

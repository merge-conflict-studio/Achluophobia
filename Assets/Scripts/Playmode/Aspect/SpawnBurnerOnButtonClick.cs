﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Aspect/SpawnBurnerOnButtonClick")]
    public class SpawnBurnerOnButtonClick : GameScript
    {
        private PlayerController playerController;
        private GameObjectSpawner specialItemSpawner;

        private void InjectSpawnBurnerOnButtonClick([EntityScope] PlayerController playerController,
                                                    [EntityScope] GameObjectSpawner specialItemSpawner)
        {
            this.playerController = playerController;
            this.specialItemSpawner = specialItemSpawner;
        }

        private void Awake()
        {
            InjectDependencies("InjectSpawnBurnerOnButtonClick");
        }

        private void OnEnable()
        {
            playerController.OnTryingToSpawnSpecialItem += SpawnSpecialItem;
        }

        private void OnDisable()
        {
            playerController.OnTryingToSpawnSpecialItem -= SpawnSpecialItem;
        }

        private void SpawnSpecialItem()
        {
            if(CanSpawnSpecialItem())
            {
                specialItemSpawner.Spawn(playerController.GetPrimaryWeapon().transform);
            }
        }

        private bool CanSpawnSpecialItem()
        {
            return ((FireShootingWeapon)playerController.GetPrimaryWeapon()).GetAmmo().GetAmmosLeftInWeapon() > 0;
        }
    }
}

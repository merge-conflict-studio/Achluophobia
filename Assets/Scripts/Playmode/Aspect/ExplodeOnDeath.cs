﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Aspect/ExplodeOnDeath")]
    public class ExplodeOnDeath : GameScript
    {
        [SerializeField, Tooltip("The damage of the explosionSphere")]
        private int damage;

        private ExplosionSphere explosionSphere;
        private Health health;

        private void InjectExplodeOnDeath(
            [GameObjectScope] ExplosionSphere explosionSphere,
            [GameObjectScope] Health health)
        {
            this.explosionSphere = explosionSphere;
            this.health = health;
        }

        private void Awake()
        {
           InjectDependencies("InjectExplodeOnDeath"); 
        }

        private void OnEnable()
        {
            health.OnDeath += OnDeath;
        }

        private void OnDisable()
        {
            health.OnDeath -= OnDeath;
        }

        private void OnDeath(GameObject killer)
        {
            Collider[] colliders = explosionSphere.GetExplosionTargets();

            foreach (Collider collider in colliders)
            {
                HitSensor hitSensor = collider.GetComponent<HitSensor>();

                if (hitSensor != null) hitSensor.Hit(GetRoot(), damage, false);
            }
        }
    }
}


﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Aspect/ExecuteEffectOnAreaEnter")]
    public class ExecuteContinuousEffectOnAreaEnter : GameScript
    {
        private GameObject rootGameObject;
        private AreaEffectSensor areaEffectSensor;

        private void InjectExecuteContinuousEffectOnArea([RootScope] GameObject rootGameObject,
                                                         [EntityScope] AreaEffectSensor areaEffectSensor)
        {
            this.rootGameObject = rootGameObject;
            this.areaEffectSensor = areaEffectSensor;
        }

        private void Awake()
        {
            InjectDependencies("InjectExecuteContinuousEffectOnArea");
        }

        private void OnEnable()
        {
            areaEffectSensor.OnAreaEffectDetected += OnAreaEnter;
            areaEffectSensor.OnAreaEffectUndetected += OnAreaExit;
        }

        private void OnDisable()
        {
            areaEffectSensor.OnAreaEffectDetected -= OnAreaEnter;
            areaEffectSensor.OnAreaEffectUndetected -= OnAreaExit;
        }

        private void OnAreaEnter(ContinuousEffect effect)
        {
            effect.ApplyOn(this, rootGameObject);
        }

        private void OnAreaExit(ContinuousEffect effect)
        {
            effect.StopEffect(this);
        }
    }
}
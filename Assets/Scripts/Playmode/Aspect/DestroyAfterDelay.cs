﻿using UnityEngine;
using System.Collections;
using Harmony;

namespace Achluophobia
{
    public delegate void DestroyAfterDelayEventHandler();

    [AddComponentMenu("Game/Aspect/DestroyAfterDelay")]
    public class DestroyAfterDelay : GameScript
    {
        [SerializeField, Tooltip("The time before the object is destroyed in seconds")]
        private int delayBeforeDeathInSeconds;

        private EntityDestroyer entityDestroyer;

        public event DestroyAfterDelayEventHandler OnDestructionTimerOut;

        private void InjectDestroyAfterDelay([EntityScope] EntityDestroyer entityDestroyer)
        {
            this.entityDestroyer = entityDestroyer;
        }

        private void Awake()
        {
            InjectDependencies("InjectDestroyAfterDelay");
        }

        private void Start()
        {
            StartCoroutine(DestroyAfterDelayRoutine());
        }

        private IEnumerator DestroyAfterDelayRoutine()
        {
            yield return new WaitForSeconds(delayBeforeDeathInSeconds);
            if (OnDestructionTimerOut != null) OnDestructionTimerOut();
            entityDestroyer.Destroy();
        }
    }
}
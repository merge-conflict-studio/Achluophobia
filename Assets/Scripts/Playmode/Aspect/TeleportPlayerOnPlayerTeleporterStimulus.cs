﻿using Harmony;
using UnityEngine;

//Philippe Deschênes

namespace Achluophobia
{
    [AddComponentMenu("Game/Aspect/TeleportPlayerOnPlayerTeleporterStimulus")]
    public class TeleportPlayerOnPlayerTeleporterStimulus : GameScript
    {
        private PlayerTeleporterStimulus playerTeleporterStimulus;
        private ImportantGameValues importantGameValues;

        private void InjectTeleportOnPlayerTeleporterStimulus(
            [EntityScope] PlayerTeleporterStimulus playerTeleporterStimulus,
            [ApplicationScope] ImportantGameValues importantGameValues)
        {
            this.playerTeleporterStimulus = playerTeleporterStimulus;
            this.importantGameValues = importantGameValues;
        }

        private void Awake()
        {
            InjectDependencies("InjectTeleportOnPlayerTeleporterStimulus");
        }

        private void OnEnable()
        {
            playerTeleporterStimulus.OnPlayerTeleport += OnTeleportPlayer;
        }

        private void OnDisable()
        {
            playerTeleporterStimulus.OnPlayerTeleport -= OnTeleportPlayer;
        }

        private void OnTeleportPlayer(GameObject spawner, GameObject player)
        {
            if (importantGameValues.GameMode == GameMode.Versus)
            {
                if (player.GetComponent<PlayerController>().PlayerTeam == Team.One)
                {
                    Teleporter.Teleport(importantGameValues.Players[0].gameObject,
                                        teleportValuesFirstPlayer(player));

                    Teleporter.Teleport(importantGameValues.Players[1].gameObject,
                                        teleportValuesSecondPlayer(importantGameValues.Players[0]));
                    importantGameValues.CurrentLevelTeam1++;
                }
                else
                {
                    Teleporter.Teleport(importantGameValues.Players[2].gameObject,
                                        teleportValuesFirstPlayer(player));

                    Teleporter.Teleport(importantGameValues.Players[3].gameObject,
                        teleportValuesSecondPlayer(importantGameValues.Players[2]));
                    importantGameValues.CurrentLevelTeam2++;
                }

            }
            else
            {
                importantGameValues.CurrentLevelTeam1++;
                Teleporter.Teleport(player.gameObject, teleportValuesFirstPlayer(player));
                for (int i = 0; i < importantGameValues.Players.Length; i++)
                {
                    if (importantGameValues.Players[i] != player)
                    {
                        Teleporter.Teleport(importantGameValues.Players[i].gameObject,
                                            teleportValuesSecondPlayer(player));
                    }
                }
            }
        }

        private Vector3 teleportValuesSecondPlayer(GameObject player)
        {
            return new Vector3(
                               player.gameObject.transform.position.x + GameGlobalVariables.MinOffsetBetweenPlayerSpawn,
                               0,
                               player.gameObject.transform.position.z);
        }

        private Vector3 teleportValuesFirstPlayer(GameObject player)
        {
            return new Vector3(player.transform.position.x + GameGlobalVariables.SpaceBetweenFloors, 0,
                               player.transform.position.z);
        }
    }
}
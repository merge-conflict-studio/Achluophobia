﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Aspect/PlayHurtSoundOnHit")]
    public class PlayHurtSoundOnHit : GameScript
    {
        [SerializeField, Tooltip("Hurt Sound")]
        private AudioClip hurtSound;

        private SoundPlayer soundPlayer;
        private HitSensor hitSensor;

        private void InjectPlayHurtSoundOnHit([ApplicationScope] SoundPlayer soundPlayer,
            [EntityScope] HitSensor hitSensor)
        {
            this.soundPlayer = soundPlayer;
            this.hitSensor = hitSensor;
        }

        private void Awake()
        {
            InjectDependencies("InjectPlayHurtSoundOnHit");
        }

        private void OnEnable()
        {
            hitSensor.OnHit += OnHit;
        }

        private void OnDisable()
        {
            hitSensor.OnHit -= OnHit;
        }

        private void OnHit(GameObject attacker, int damage, bool isExplosiveHit)
        {
            soundPlayer.PlaySound(hurtSound);
        }
    }
}

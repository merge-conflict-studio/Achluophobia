﻿using System;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Aspect/ShowUiOnEnterTrigger")]
    public class ShowUiOnEnterTrigger : GameScript
    {
        private readonly Vector3 defaultPosition = new Vector3(0, 5, 1.2f);
        private readonly Quaternion defaultParentRotation = Quaternion.Euler(0, 0, 0);

        [SerializeField, Tooltip("The button to show when the sensor is interactable")]
        private Sprite interactableSprite;

        private new Collider collider;
        private SpriteRenderer interactButton;

        private void InjectShowUiOnEnterTrigger([GameObjectScope] Collider collider,
                                                [ChildScope][Named(R.S.GameObject.InteractButton)] SpriteRenderer interactButton)
        {
            this.collider = collider;
            this.interactButton = interactButton;
        }

        private void Awake()
        {
            InjectDependencies("InjectShowUiOnEnterTrigger");

            int layer = LayerMask.NameToLayer(R.S.Layer.MiniGameSensor);
            if (layer == -1)
            {
                throw new Exception("In order to use a MinigameSensor, you must have a " + R.S.Layer.MiniGameSensor + " layer.");
            }
            gameObject.layer = layer;
            interactButton.sprite = null;
            interactButton.transform.parent.rotation = defaultParentRotation;
            interactButton.transform.localPosition = defaultPosition;
        }

        private void OnEnable()
        {
            collider.Events().OnEnterTrigger += OnEnterTrigger;
            collider.Events().OnExitTrigger += OnExitTrigger;
        }

        private void OnDisable()
        {
            collider.Events().OnEnterTrigger -= OnEnterTrigger;
            collider.Events().OnExitTrigger -= OnExitTrigger;
        }

        private void OnEnterTrigger(Collider other)
        {
            interactButton.sprite = interactableSprite;
        }

        private void OnExitTrigger(Collider other)
        {
            interactButton.sprite = null;
        }
    }
}



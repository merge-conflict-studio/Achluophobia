﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Aspect/ResupplyAmmoOnPlayerRespawn")]
    public class ResupplyAmmoOnPlayerRespawn : GameScript
    {
        private PlayerRespawnEventChannel playerRespawnEventChannel;

        private void InjectResupplyAmmoOnPlayerRespawn([EventChannelScope] PlayerRespawnEventChannel playerRespawnEventChannel)
        {
            this.playerRespawnEventChannel = playerRespawnEventChannel;
        }

        private void Awake()
        {
            InjectDependencies("InjectResupplyAmmoOnPlayerRespawn");
        }

        private void OnEnable()
        {
            playerRespawnEventChannel.OnEventPublished += OnPlayerToRespawn;
        }

        private void OnDisable()
        {
            playerRespawnEventChannel.OnEventPublished -= OnPlayerToRespawn;
        }

        private void AddAmmoToRespawnedPlayer(GameObject player)
        {
            if (player.GetComponent<PlayerController>().GetPlayerClass() == PlayerClass.Mastodonte) return;
            Weapon weapon = player.GetComponent<PlayerController>().GetPrimaryWeapon();
            Ammo weaponAmmo = weapon.GetComponent<Ammo>();
            weaponAmmo.EmptyGun();
            if (weaponAmmo.GetAmmosLeftOnPlayer() <= weaponAmmo.GetMaxAmmo() / 2 &&
                weapon.WeaponType == WeaponType.Shooting)
            {
                if (player.GetComponent<PlayerController>().GetPlayerClass() == PlayerClass.Incinerator)
                {
                    weaponAmmo.PickUpAmmoOnRespawn(weaponAmmo.GetMaxAmmo() / 2, true);
                }
                else
                {
                    weaponAmmo.PickUpAmmoOnRespawn(weaponAmmo.GetMaxAmmo() / 2, false);                
                }
            }
        }

        private void OnPlayerToRespawn(PlayerRespawnEvent playerRespawnEvent)
        {
            if (playerRespawnEvent.IsReadyToRespawn)
            {
                if (playerRespawnEvent.DeadPlayer != null) AddAmmoToRespawnedPlayer(playerRespawnEvent.DeadPlayer);
            }
        }
    }
}

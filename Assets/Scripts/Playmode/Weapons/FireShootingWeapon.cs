﻿using System.Collections;
using UnityEngine;
using Harmony;

namespace Achluophobia
{
    [AddComponentMenu("Game/Weapons/FireShootingWeapon")]
    public class FireShootingWeapon : Weapon
    {
        [SerializeField, Tooltip("ScriptableObject containing the ShootingWeapon data")]
        private ShootingWeaponData shootingWeapon;
        
        [SerializeField, Tooltip("Addon for a shooting weapon")]
        private GameObject addonPrefab;

        [SerializeField, Tooltip("The sound of the flame weapon")]
        private AudioClip flameSound;

        [SerializeField, Tooltip("The sound of the flame weapon when you start it")]
        private AudioClip startFlameSound;

        [SerializeField, Tooltip("The sound of the flame weapon when it ends")]
        private AudioClip endFlameSound;

        private FlameShooter flameShooter;
        private PlayerController playerController;
        private Ammo ammo;
        private WaitForSeconds waitForRateOfFire;
        private IAddon addon;
        private bool isShooting;
        private bool isFuelLeft;
        private SoundPlayer soundPlayer;

        private void InjectFireShootingWeapon([GameObjectScope] FlameShooter flameShooter,
                                              [GameObjectScope] Ammo ammo,
                                              [RootScope] PlayerController playerController,
                                              [ApplicationScope] SoundPlayer soundPlayer)
        {
            this.flameShooter = flameShooter;
            this.ammo = ammo;
            this.playerController = playerController;
            this.soundPlayer = soundPlayer;
        }

        private void Awake()
        {
            InjectDependencies("InjectFireShootingWeapon");
            waitForRateOfFire = new WaitForSeconds(1.0f / shootingWeapon.RateOfFire);

            if (addonPrefab != null)
            {
                addonPrefab = Instantiate(addonPrefab, transform);
                addon = addonPrefab.GetComponent<IAddon>();
            }
            flameShooter.SetDamagePerParticle(shootingWeapon.MaxDamage);
            isFuelLeft = true;
        }

        private void Update()
        {
            if (isShooting && isFuelLeft) TryAttack();
        }

        public Ammo GetAmmo()
        {
            return ammo;
        }

        public ShootingWeaponData GetShootingWeaponData()
        {
            return shootingWeapon;
        }

        public override void Equip()
        {
            transform.parent.Find(R.S.GameObject.Visual).gameObject.SetActive(true);
            playerController.OnReload += Reload;
            playerController.OnStartAttack += StartAttack;
            playerController.OnStopAttack += StopAttack;

            ammo.NotifyAmmoChanged();

            if (addon != null) addon.Equip();
        }

        public override void Unequip()
        {
            transform.parent.Find(R.S.GameObject.Visual).gameObject.SetActive(false);
            playerController.OnReload -= Reload;
            playerController.OnStartAttack -= StartAttack;
            playerController.OnStopAttack -= StopAttack;

            ammo.StopReloading();

            if (addon != null) addon.Unequip();
        }
        
        private void Reload()
        {
            ammo.Reload(shootingWeapon.ReloadTime);
        }

        private IEnumerator WaitForRateOfFire()
        {
            SetReady(false);

            yield return waitForRateOfFire;
            soundPlayer.PlaySound(flameSound);
            SetReady(true);
        }

        private void StartAttack()
        {
            isShooting = true;
            if (isFuelLeft) flameShooter.On();
            else if (ammo.GetAmmosLeftInWeapon() > 0) isFuelLeft = true;
            soundPlayer.PlaySound(startFlameSound);
        }

        private void StopAttack()
        {
            isShooting = false;
            flameShooter.Off();
            soundPlayer.PlaySound(endFlameSound);
        }

        protected override void Attack()
        {
            if (!ammo.Deplete(shootingWeapon.ReloadTime))
            {
                isFuelLeft = false;
                flameShooter.Off();
                return;
            }
            isFuelLeft = true;
            StartCoroutine(WaitForRateOfFire());
        }
    }
}
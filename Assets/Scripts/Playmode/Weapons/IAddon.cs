﻿namespace Achluophobia
{
    public interface IAddon
    {
        void Equip();
        void Unequip();
    }
}


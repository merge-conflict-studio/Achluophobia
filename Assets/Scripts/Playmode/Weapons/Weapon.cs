﻿using UnityEngine;

namespace Achluophobia
{
    public abstract class Weapon : GameScript
    {
        [SerializeField, Tooltip("The weapon type")]
        private WeaponType weaponType;

        private bool isReady = true;

        protected void SetReady(bool isReady)
        {
            this.isReady = isReady;
        }

        protected void TryAttack()
        {
            if (isReady)
            {
                Attack();
            }
        }

        public WeaponType WeaponType { get { return weaponType; } }

        public abstract void Equip();

        public abstract void Unequip();

        protected abstract void Attack();
    }
}
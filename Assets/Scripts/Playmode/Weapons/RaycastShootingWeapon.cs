﻿using System.Collections;
using UnityEngine;
using Harmony;

namespace Achluophobia
{
    
    [AddComponentMenu("Game/Weapons/BulletShootingWeapon")]
    public class RaycastShootingWeapon : Weapon
    {
        [SerializeField, Tooltip("ScriptableObject containing the ShootingWeapon data")]
        private ShootingWeaponData shootingWeapon;
        [SerializeField, Tooltip("Addon for a shooting weapon")]
        private GameObject addonPrefab;

        private RaycastShooter raycastShooter;
        private PlayerController playerController;
        private Ammo ammo;
        private WaitForSeconds waitForRateOfFire;
        private IAddon addon;
        private bool isShooting;

        private void InjectShootingWeapon([GameObjectScope] RaycastShooter raycastShooter,
                                          [GameObjectScope] Ammo ammo,
                                          [RootScope] PlayerController playerController)
        {
            this.raycastShooter = raycastShooter;
            this.ammo = ammo;
            this.playerController = playerController;
        }

        private void Awake()
        {
            InjectDependencies("InjectShootingWeapon");
            waitForRateOfFire = new WaitForSeconds(1.0f / shootingWeapon.RateOfFire);

            if (addonPrefab != null)
            {
                addonPrefab = Instantiate(addonPrefab, transform);
                addon = addonPrefab.GetComponent<IAddon>();
            }

            raycastShooter.OnRayCastHit += OnRaycastHit;
        }

        private void Update()
        {
            if (isShooting) TryAttack();
        }

        private void OnDestroy()
        {
            raycastShooter.OnRayCastHit -= OnRaycastHit;
        }

        public Ammo GetAmmo()
        {
            return ammo;
        }

        public ShootingWeaponData GetShootingWeaponData()
        {
            return shootingWeapon;
        }

        public override void Equip()
        {
            transform.parent.Find(R.S.GameObject.Visual).gameObject.SetActive(true);
            playerController.OnReload += Reload;
            playerController.OnStartAttack += StartAttack;
            playerController.OnStopAttack += StopAttack;

            ammo.NotifyAmmoChanged();

            if (addon != null) addon.Equip();
        }

        public override void Unequip()
        {
            transform.parent.Find(R.S.GameObject.Visual).gameObject.SetActive(false);
            playerController.OnReload -= Reload;
            playerController.OnStartAttack -= StartAttack;
            playerController.OnStopAttack -= StopAttack;

            ammo.StopReloading();

            if (addon != null) addon.Unequip();
        }
        
        protected override void Attack()
        {
            if (!ammo.Deplete(shootingWeapon.ReloadTime)) return;

            raycastShooter.Shoot(shootingWeapon.FiringRange);

            StartCoroutine(WaitForRateOfFire());
        }
        
        private static int ReduceDamageWithDistance(float maxDamage, float hitDistance, float firingRange)
        {
            return Mathf.CeilToInt(maxDamage * ((firingRange - hitDistance) / firingRange));
        }
        
        private void Reload()
        {
            ammo.Reload(shootingWeapon.ReloadTime);
        }

        private void OnRaycastHit(RaycastHit hit)
        {
            switch (hit.collider.gameObject.GetRoot().tag)
            {
                case R.S.Tag.Enemy:

                    HitSensor hitSensor = hit.collider.gameObject.GetComponent<HitSensor>();

                    int damage = ReduceDamageWithDistance(shootingWeapon.MaxDamage, hit.distance, shootingWeapon.FiringRange);

                    hitSensor.Hit(transform.root.gameObject, damage, false);
                    break;
            }
        }

        private IEnumerator WaitForRateOfFire()
        {
            SetReady(false);

            yield return waitForRateOfFire;

            SetReady(true);
        }

        private void StartAttack()
        {
            isShooting = true;
        }

        private void StopAttack()
        {
            isShooting = false;
        }
    }
}
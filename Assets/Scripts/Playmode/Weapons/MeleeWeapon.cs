﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Weapons/MeleeWeapons")]
    public class MeleeWeapon : Weapon
    {
        [SerializeField, Tooltip("The scriptableobject that contains the melee weapon data")]
        private MeleeWeaponData meleeWeapon;

        [SerializeField, Tooltip("The audio sound of the melee weapon")]
        private AudioClip meleeWeaponSound;

        private PlayerController playerController;
        private MeshRenderer meleeWeaponVisual;
        private Collider hurtCollider;
        private Animator animator;
        private SoundPlayer soundPlayer;

        private void InjectMeleeWeapon([RootScope] PlayerController playerController,
                                       [ChildScope] MeshRenderer meleeWeaponVisual,
                                       [GameObjectScope] Collider hurtCollider,
                                       [GameObjectScope] Animator animator,
                                       [ApplicationScope] SoundPlayer soundPlayer)
        {
            this.playerController = playerController;
            this.meleeWeaponVisual = meleeWeaponVisual;
            this.hurtCollider = hurtCollider;
            this.animator = animator;
            this.soundPlayer = soundPlayer;
        }

        private void Awake()
        {
            InjectDependencies("InjectMeleeWeapon");

            meleeWeaponVisual.enabled = false;
            hurtCollider.enabled = false;
        }

        private void OnTriggerEnter(Collider other)
        {
            HitSensor hitSensor = other.GetComponent<HitSensor>();
            if (hitSensor) hitSensor.Hit(transform.root.gameObject, meleeWeapon.Damage, false);
        }

        public override void Equip()
        {
            transform.Find(R.S.GameObject.Visual).gameObject.SetActive(true);
            playerController.OnStartAttack += OnStartAttack;
            playerController.OnStopAttack += OnStopAttack;
            meleeWeaponVisual.enabled = true;
        }

        public override void Unequip()
        {
            transform.Find(R.S.GameObject.Visual).gameObject.SetActive(false);
            playerController.OnStartAttack -= OnStartAttack;
            playerController.OnStopAttack -= OnStopAttack;
            meleeWeaponVisual.enabled = false;
        }

        private void OnStartAttack()
        {
            animator.SetTrigger("StartAttack");
            soundPlayer.PlaySound(meleeWeaponSound);
        }

        private void OnStopAttack()
        {
            animator.SetTrigger("StopAttack");
        }

        protected override void Attack()
        {
           //Do nothing
        }
    }
}


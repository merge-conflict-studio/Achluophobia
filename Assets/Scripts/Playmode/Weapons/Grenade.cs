﻿using System.Collections;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public class Grenade : GameScript
    {
        [SerializeField, Tooltip("The explosionSphere prefab.")]
        private GameObject explosionPrefab;

        [SerializeField, Tooltip("Time before the explosionSphere in seconds.")]
        private float timeBeforeExplosion;

        [SerializeField, Tooltip("Damage of the explosionSphere")]
        private int damage;

        private GameObject owner;
        private ExplosionSphere explosionSphere;

        private void InjectGrenade([GameObjectScope] ExplosionSphere explosionSphere)
        {
            this.explosionSphere = explosionSphere;
        }

        private void Awake()
        {
            InjectDependencies("InjectGrenade");
        }

        public void StartTimedExplosion(GameObject owner)
        {
            this.owner = owner;
            StartCoroutine(Trigger());
        }

        private IEnumerator Trigger()
        {
            yield return new WaitForSeconds(timeBeforeExplosion);

            Explode();
        }

        private void Explode()
        {
            Collider[] colliders = explosionSphere.GetExplosionTargets();

            foreach (Collider collider in colliders)
            {
                HitSensor hitSensor = collider.GetComponent<HitSensor>();
                if (hitSensor != null) hitSensor.Hit(owner, damage, true);
            }
            Instantiate(explosionPrefab, transform.position, transform.rotation);
            GetComponent<EntityDestroyer>().Destroy();
        }
    }
}


﻿using System;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public delegate void HitSensorEventHandler(GameObject attacker, int hitPoints, bool isExplosionHit);

    [AddComponentMenu("Game/Sensor/HitSensor")]
    public class HitSensor : GameScript
    {
        private new Collider collider;

        public event HitSensorEventHandler OnHit;

        private void InjectHitSensor([GameObjectScope] Collider collider)
        {
            this.collider = collider;
        }

        private void Awake()
        {
            InjectDependencies("InjectHitSensor");

            int layer = LayerMask.NameToLayer(R.S.Layer.HitSensor);
            if (layer == -1)
            {
                throw new Exception("In order to use a HitSensor, you must have a " + R.S.Layer.HitSensor + " layer.");
            }
            gameObject.layer = layer;
            collider.isTrigger = false;
        }

        public void Hit(GameObject attacker, int hitPoints, bool isGrenadeHit)
        {
            if (OnHit != null) OnHit(attacker, hitPoints, isGrenadeHit);
        }
    }
}
﻿using System;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public delegate void ExplosionEventHandler(InstantEffect effect);

    [AddComponentMenu("Game/Sensor/ExplosionSensor")]
    public class ExplosionSensor : GameScript
    {
        private new Collider collider;

        public event ExplosionEventHandler OnExplosionHitReceived;

        private void InjectExplosionSensor([GameObjectScope] Collider collider)
        {
            this.collider = collider;
        }

        private void Awake()
        {
            InjectDependencies("InjectExplosionSensor");

            int layer = LayerMask.NameToLayer(R.S.Layer.ExplosionSensor);
            if (layer == -1)
            {
                throw new Exception("In order to use an explosion sensor, you must have a " +
                                    R.S.Layer.ExplosionSensor + " layer.");
            }
            gameObject.layer = layer;
            collider.isTrigger = true;
        }

        public void ExplosionHit(InstantEffect effect)
        {
            if (OnExplosionHitReceived != null) OnExplosionHitReceived(effect);
        }
    }
}
﻿using System;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public delegate void MinigameEndDiffusionEventHandler(GameObject minigame);
    public delegate void MinigameStartEventHandler();

    [AddComponentMenu("Game/Sensor/MinigameSensor")]
    public class MinigameSensor : GameScript
    {
        private const int MinigameXOffset = 5;
        private const int MinigameYOffset = 5;
        private const float MinigameZOffset = 2.5f;

        [SerializeField, Tooltip("Prefab of the minigame to launch")]
        private GameObject minigamePrefab;

        private QuestItemsData questItemsData;
        private new Collider collider;
        private bool canBeTriggered;

        public event MinigameStartEventHandler OnMinigameStart;
        public event MinigameEndEventHandler OnMinigameEnd;
        public event MinigameEndDiffusionEventHandler OnMinigameSuccessful;

        private void InjectMinigameSensor([GameObjectScope] Collider collider,
                                          [EntityScope] QuestItemsData questItemsData)
        {
            this.collider = collider;
            this.questItemsData = questItemsData;
        }

        private void Awake()
        {
            InjectDependencies("InjectMinigameSensor");

            int layer = LayerMask.NameToLayer(R.S.Layer.MiniGameSensor);
            if (layer == -1)
            {
                throw new Exception("In order to use a MinigameSensor, you must have a " + R.S.Layer.MiniGameSensor + " layer.");
            }
            gameObject.layer = layer;
            collider.isTrigger = true;
            canBeTriggered = true;
        }

        public void StartMiniGame(PlayerNumber playerNumber)
        {
            if (!canBeTriggered) return;
            if (!questItemsData.CheckAllItemsCollected()) return;

            canBeTriggered = false;

            GameObject minigame = Instantiate(minigamePrefab);
            Vector3 parentPosition = gameObject.transform.position;
            minigame.transform.parent = gameObject.transform;
            minigame.transform.position = new Vector3(parentPosition.x - MinigameXOffset, minigame.transform.position.y + MinigameYOffset, parentPosition.z + MinigameZOffset);

            MinigameLogicController minigameController = minigame.GetComponent<MinigameLogicController>();
            minigameController.OnMinigameEnd += NotifyMinigameEnd;
            minigameController.OnMinigameDestroy += OnMinigameDestroy;
            minigameController.LaunchMinigame(playerNumber);
            if (OnMinigameStart != null) OnMinigameStart();
        }

        private void NotifyMinigameEnd(bool isCompleted)
        {
            if (isCompleted && OnMinigameSuccessful != null) OnMinigameSuccessful(gameObject.GetRoot());
            if (OnMinigameEnd != null) OnMinigameEnd(isCompleted);
        }

        private void OnMinigameDestroy(bool isCompleted)
        {
            if (!isCompleted) canBeTriggered = true;
        }
    }
}
﻿using System;
using UnityEngine;
using XInputDotNetPure;
using GamePad = Harmony.GamePad;
using GamePadState = Harmony.GamePadState;

namespace Achluophobia
{
    public delegate void PlayerActionEventHandler();
    public delegate void GamePadEventHandler();
    public delegate void PlayerMovement(Vector2 inputValue);

    [AddComponentMenu("Game/Sensor/GamePadSensor")]
    public class GamePadSensor
    {
        private const float RightTriggerDeadzone = 0.15f;
        private const float LeftTriggerDeadzone = 0.15f;
        private readonly PlayerIndex playerIndex;
        private readonly GamePad gamePad;
        private GamePadState currentFrameGamePadState;
        private GamePadState previousFrameGamePadState;

        public event PlayerActionEventHandler OnMenuUp;
        public event PlayerActionEventHandler OnMenuDown;
        public event PlayerActionEventHandler OnMenuLeft;
        public event PlayerActionEventHandler OnMenuRight;
        public event PlayerActionEventHandler OnMenuConfirm;
        public event PlayerActionEventHandler OnBack;

        public event PlayerActionEventHandler OnPause;
        public event PlayerActionEventHandler OnTriggerPress;
        public event PlayerActionEventHandler OnTriggerRelease;
        public event PlayerActionEventHandler OnToggleFlashlight;
        public event PlayerActionEventHandler OnAction;
        public event PlayerActionEventHandler OnReload;
        public event PlayerActionEventHandler OnSwitchWeapon;
        public event PlayerActionEventHandler OnUseSpecialItem;
        public event PlayerActionEventHandler OnAim;
        public event PlayerActionEventHandler OnStopAiming;
        public event PlayerMovement OnMove;
        public event PlayerMovement OnRotate;

        public event GamePadEventHandler OnAPressed;
        public event GamePadEventHandler OnAReleased;
        public event GamePadEventHandler OnBPressed;
        public event GamePadEventHandler OnBReleased;
        public event GamePadEventHandler OnXPressed;
        public event GamePadEventHandler OnXReleased;
        public event GamePadEventHandler OnYPressed;
        public event GamePadEventHandler OnYReleased;

        public GamePadSensor(PlayerIndex playerIndex, GamePad gamePad)
        {
            this.playerIndex = playerIndex;
            this.gamePad = gamePad;
            previousFrameGamePadState = GetCurrentGamePadState();
        }

        public void Update()
        {
            currentFrameGamePadState = GetCurrentGamePadState();

            HandleUiInput();
            HandleDirectionInput();
            HandleRotationInput();
            HandleActionInput();

            previousFrameGamePadState = currentFrameGamePadState;
        }

        public bool IsConnected()
        {
            return gamePad.IsConnected(playerIndex);
        }
        
        private static bool IsPressedThisFrame(ButtonState previousState, ButtonState currentState)
        {
            return previousState == ButtonState.Released && currentState == ButtonState.Pressed;
        }

        private static bool IsReleasedThisFrame(ButtonState previousState, ButtonState currentState)
        {
            return previousState == ButtonState.Pressed && currentState == ButtonState.Released;
        }

        private void HandleUiInput()
        {
            if (IsPressedThisFrame(previousFrameGamePadState.DPad.Up, currentFrameGamePadState.DPad.Up))
            {
                NotifyMenuUp();
            }

            if (IsPressedThisFrame(previousFrameGamePadState.DPad.Down, currentFrameGamePadState.DPad.Down))
            {
                NotifyMenuDown();
            }

            if (IsPressedThisFrame(previousFrameGamePadState.DPad.Left, currentFrameGamePadState.DPad.Left))
            {
                NotifyMenuLeft();
            }

            if (IsPressedThisFrame(previousFrameGamePadState.DPad.Right, currentFrameGamePadState.DPad.Right))
            {
                NotifyMenuRight();
            }

            if (IsPressedThisFrame(previousFrameGamePadState.Buttons.A, currentFrameGamePadState.Buttons.A))
            {
                NotifyMenuConfirm();
            }

            if (IsPressedThisFrame(previousFrameGamePadState.Buttons.B, currentFrameGamePadState.Buttons.B))
            {
                NotifyMenuBack();
            }
        }

        private void HandleActionInput()
        {
            if (IsPressedThisFrame(previousFrameGamePadState.Buttons.Start, currentFrameGamePadState.Buttons.Start))
            {
                NotifyPause();
            }

            if (IsPressedThisFrame(Trigger.Left))
            {
                NotifyAim();
            }
            
            else if (IsReleasedThisFrame(Trigger.Left))
            {
                NotifyStopAiming();
            }

            if (IsPressedThisFrame(Trigger.Right))
            {
                NotifyTriggerPress();
            }

            else if (IsReleasedThisFrame(Trigger.Right))
            {
                NotifyTriggerRelease();
            }

            if (IsPressedThisFrame(previousFrameGamePadState.Buttons.B, currentFrameGamePadState.Buttons.B))
            {
                NotifyToggleFlashlight();
                if (OnBPressed != null) OnBPressed();

            }

            else if (IsReleasedThisFrame(previousFrameGamePadState.Buttons.B, currentFrameGamePadState.Buttons.B))
            {
                if (OnBReleased != null) OnBReleased();
            }

            if (IsPressedThisFrame(previousFrameGamePadState.Buttons.Y, currentFrameGamePadState.Buttons.Y))
            {
                NotifySwitchWeapon();
                if (OnYPressed != null) OnYPressed();
            }

            else if (IsReleasedThisFrame(previousFrameGamePadState.Buttons.Y, currentFrameGamePadState.Buttons.Y))
            {
                if (OnYReleased != null) OnYReleased();
            }

            if (IsPressedThisFrame(previousFrameGamePadState.Buttons.X, currentFrameGamePadState.Buttons.X))
            {
                NotifyReload();
                if (OnXPressed != null) OnXPressed();
            }

            else if (IsReleasedThisFrame(previousFrameGamePadState.Buttons.X, currentFrameGamePadState.Buttons.X))
            {
                if (OnXReleased != null) OnXReleased();
            }

            if (IsPressedThisFrame(previousFrameGamePadState.Buttons.A, currentFrameGamePadState.Buttons.A))
            {
                NotifyAction();
                if (OnAPressed != null) OnAPressed();
            }

            else if (IsReleasedThisFrame(previousFrameGamePadState.Buttons.A, currentFrameGamePadState.Buttons.A))
            {
                if (OnAReleased != null) OnAReleased();
            }

            if (IsPressedThisFrame(previousFrameGamePadState.Buttons.RightShoulder,
                currentFrameGamePadState.Buttons.RightShoulder))
            {
                NotifyUseSpecialItem();
            }
        }

        private void HandleDirectionInput()
        {
            Vector2 motion = GetLeftThumbStickValue();

            if (motion.sqrMagnitude > 0f)
            {
                NotifyMove(motion);
            }
        }

        private void HandleRotationInput()
        {
            NotifyRotate(GetRightThumbstickValue());
        }

        private Vector2 GetLeftThumbStickValue()
        {
            return new Vector2(
                GetCurrentGamePadState().ThumbSticks.Left.X,
                GetCurrentGamePadState().ThumbSticks.Left.Y
            );
        }

        private Vector2 GetRightThumbstickValue()
        {
            return new Vector2(
                GetCurrentGamePadState().ThumbSticks.Right.X,
                GetCurrentGamePadState().ThumbSticks.Right.Y);
        }

        private bool IsPressedThisFrame(Trigger trigger)
        {
            bool isPressedThisFrame = false;

            switch (trigger)
            {
                case Trigger.Left:
                    if (previousFrameGamePadState.Triggers.Left <= LeftTriggerDeadzone
                        && currentFrameGamePadState.Triggers.Left > LeftTriggerDeadzone)
                    {
                        isPressedThisFrame = true;
                    }
                    break;

                case Trigger.Right:
                    if (previousFrameGamePadState.Triggers.Right <= RightTriggerDeadzone
                        && currentFrameGamePadState.Triggers.Right > RightTriggerDeadzone)
                    {
                        isPressedThisFrame = true;
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException("trigger", trigger, null);
            }
            return isPressedThisFrame;
        }

        private bool IsReleasedThisFrame(Trigger trigger)
        {
            bool isReleasedThisFrame = false;

            switch (trigger)
            {
                case Trigger.Left:
                    if (previousFrameGamePadState.Triggers.Left > LeftTriggerDeadzone
                        && currentFrameGamePadState.Triggers.Left <= LeftTriggerDeadzone)
                    {
                        isReleasedThisFrame = true;
                    }
                    break;

                case Trigger.Right:
                    if (previousFrameGamePadState.Triggers.Right > RightTriggerDeadzone
                        && currentFrameGamePadState.Triggers.Right <= RightTriggerDeadzone)
                    {
                        isReleasedThisFrame = true;
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException("trigger", trigger, null);
            }
            return isReleasedThisFrame;
        }

        private GamePadState GetCurrentGamePadState()
        {
            return gamePad.GetGamepadState(playerIndex);
        }

        private void NotifyMenuUp()
        {
            if (OnMenuUp != null) OnMenuUp();
        }

        private void NotifyMenuDown()
        {
            if (OnMenuDown != null) OnMenuDown();
        }

        private void NotifyMenuLeft()
        {
            if (OnMenuLeft != null) OnMenuLeft();
        }

        private void NotifyMenuRight()
        {
            if (OnMenuRight != null) OnMenuRight();
        }

        private void NotifyMenuConfirm()
        {
            if (OnMenuConfirm != null) OnMenuConfirm();
        }

        private void NotifyMenuBack()
        {
            if (OnBack != null) OnBack();
        }

        private void NotifyPause()
        {
            if (OnPause != null) OnPause();
        }

        private void NotifyMove(Vector2 motion)
        {
            if (OnMove != null) OnMove(motion);
        }

        private void NotifyRotate(Vector2 direction)
        {
            if (OnRotate != null) OnRotate(direction);
        }

        private void NotifyTriggerPress()
        {
            if (OnTriggerPress != null) OnTriggerPress();
        }

        private void NotifyTriggerRelease()
        {
            if (OnTriggerRelease != null) OnTriggerRelease();
        }

        private void NotifyToggleFlashlight()
        {
            if (OnToggleFlashlight != null) OnToggleFlashlight();
        }

        private void NotifyReload()
        {
            if (OnReload != null) OnReload();
        }

        private void NotifyAction()
        {
            if (OnAction != null) OnAction();
        }

        private void NotifySwitchWeapon()
        {
            if (OnSwitchWeapon != null) OnSwitchWeapon();
        }

        private void NotifyUseSpecialItem()
        {
            if (OnUseSpecialItem != null) OnUseSpecialItem();
        }

        private void NotifyAim()
        {
            if (OnAim != null) OnAim();
        }

        private void NotifyStopAiming()
        {
            if (OnStopAiming != null) OnStopAiming();
        }
         
        private enum Trigger
        {
            Left,
            Right
        }
    }
}
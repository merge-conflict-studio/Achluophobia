﻿using System;
using System.Collections.Generic;
using System.Linq;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public delegate void LineOfSightSensorEventHandler(GameObject target);

    [AddComponentMenu("Game/Sensor/LineOfSightSensor")]
    public class LineOfSightSensor : GameScript
    {
        public event LineOfSightSensorEventHandler OnTargetEntersLOS;
        public event LineOfSightSensorEventHandler OnTargetLeavesLOS;

        [SerializeField, Tooltip("The angle of HALF the line of sight. For a peripherical view range of 90 degres, this has to have a value of 45.")]
        private int losAngle;

        [SerializeField, Tooltip("The distance in which the target is detectable from every angle.")]
        private float closeRangeDetectionDistance;

        private List<Collider> collidersInLOS;
        private new SphereCollider collider;

        private void InjectLineOfSightSensor([GameObjectScope] SphereCollider collider)
        {
            this.collider = collider;
        }

        private void Awake()
        {
            InjectDependencies("InjectLineOfSightSensor");

            collidersInLOS = new List<Collider>();

            int layer = LayerMask.NameToLayer(R.S.Layer.LineOfSightSensor);
            if (layer == -1)
            {
                throw new Exception("In order to use a LineOfSightSensor, you must have a " + R.S.Layer.HitSensor +
                                    " layer.");
            }
            gameObject.layer = layer;
            collider.isTrigger = true;
        }

        private void FixedUpdate()
        {
            foreach (Collider other in collidersInLOS)
            {
                float distanceToTarget = Vector3.Distance(other.transform.position, transform.position);
                
                if (distanceToTarget <= closeRangeDetectionDistance && OnTargetEntersLOS != null)
                {
                    OnTargetEntersLOS(other.gameObject);
                    break;
                }

                float angle = Vector3.Angle(other.transform.position - transform.position, transform.forward);
                if (!(angle <= losAngle)) continue;
                Ray ray = new Ray(transform.position, other.transform.position - transform.position);
                Debug.DrawRay(ray.origin, ray.direction * collider.radius, Color.blue);
                RaycastHit[] hits = Physics.RaycastAll(ray, distanceToTarget).OrderBy(hit => hit.distance).ToArray();

                if (hits.Length > 0 && hits[0].collider.CompareTag(R.S.Tag.Player))
                {
                    if (OnTargetEntersLOS != null) OnTargetEntersLOS(other.gameObject);
                }
                else if (OnTargetLeavesLOS != null) OnTargetLeavesLOS(other.gameObject);
            }
        }

        public void AddInLineOfSight(Collider other)
        {
            collidersInLOS.Add(other);
        }

        public void RemoveFromLineOfSight(Collider other)
        {
            collidersInLOS.Remove(other);
            if (OnTargetLeavesLOS != null) OnTargetLeavesLOS(other.gameObject);
        }

        public float GetLineOfSightRange()
        {
            return collider.radius;
        }
    }
}
﻿using Harmony;
using System;
using UnityEngine;

namespace Achluophobia
{
    public delegate void AreaDetectionEventHandler(ContinuousEffect effect);

    [AddComponentMenu("Game/Sensor/AreaEffectSensor")]
    public class AreaEffectSensor : GameScript
    {
        private new Collider collider;

        public event AreaDetectionEventHandler OnAreaEffectDetected;
        public event AreaDetectionEventHandler OnAreaEffectUndetected;

        private void InjectAreaEffectSensor([GameObjectScope] Collider collider)
        {
            this.collider = collider;
        }

        private void Awake()
        {
            InjectDependencies("InjectAreaEffectSensor");

            int layer = LayerMask.NameToLayer(R.S.Layer.ItemSensor);
            if (layer == -1)
            {
                throw new Exception("In order to use an area effect sensor, you must have a " + R.S.Layer.ItemSensor +
                                    " layer.");
            }
            gameObject.layer = layer;
            collider.isTrigger = true;
        }

        public void AreaEffectDetected(ContinuousEffect effect)
        {
            if (OnAreaEffectDetected != null) OnAreaEffectDetected(effect);
        }

        public void AreaEffectUndetected(ContinuousEffect effect)
        {
            if (OnAreaEffectUndetected != null) OnAreaEffectUndetected(effect);
        }
    }
}
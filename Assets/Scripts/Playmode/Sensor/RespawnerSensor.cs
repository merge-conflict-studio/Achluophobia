﻿using System;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public delegate void RespawnerSensorEventHandler(GameObject playerToRespawn, Vector3 respawnPosition, bool isReadyToRespawn);

    [AddComponentMenu("Game/Sensor/RespawnerSensor")]
    public class RespawnerSensor : GameScript
    {
        private new Collider collider;

        public event RespawnerSensorEventHandler OnRespawnPlayer;

        private void InjectRespawnerSensor([GameObjectScope] Collider collider)
        {
            this.collider = collider;
        }

        private void Awake()
        {
            InjectDependencies("InjectRespawnerSensor");

            int layer = LayerMask.NameToLayer(R.S.Layer.RespawnerSensor);
            if (layer == -1)
            {
                throw new Exception("In order to use a RespawnerSensor, you must have a " + R.S.Layer.RespawnerSensor +
                                    " layer.");
            }
            gameObject.layer = layer;
            collider.isTrigger = true;
        }

        public void RespawnPlayer(GameObject playerToRespawn, Vector3 respawnPosition, bool isReadyToRespawn)
        {
            if (OnRespawnPlayer != null) OnRespawnPlayer(playerToRespawn, respawnPosition, isReadyToRespawn);
        }
    }
}

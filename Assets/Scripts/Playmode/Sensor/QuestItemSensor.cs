﻿using System;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public delegate void QuestItemSensorEventHandler();

    [AddComponentMenu("Game/Sensor/QuestItemSensor")]
    public class QuestItemSensor : GameScript
    {
        private new Collider collider;

        public event QuestItemSensorEventHandler OnCollectQuestItem;

        private void InjectQuestItemSensor([GameObjectScope] Collider collider)
        {
            this.collider = collider;
        }

        private void Awake()
        {
            InjectDependencies("InjectQuestItemSensor");

            int layer = LayerMask.NameToLayer(R.S.Layer.QuestItemSensor);
            if (layer == -1)
            {
                throw new Exception("In order to use a QuestItemSensor, you must have a " + R.S.Layer.QuestItemSensor +
                                    " layer.");
            }
            gameObject.layer = layer;
            collider.isTrigger = true;
        }

        public void CollectQuestItem()
        {
            if (OnCollectQuestItem != null) OnCollectQuestItem();
        }
    }
}
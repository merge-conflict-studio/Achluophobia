﻿using System;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public delegate void PlayerTeleporterSensorEventHandler();

    [AddComponentMenu("Game/Sensor/PlayerTeleporterSensor")]
    public class PlayerTeleporterSensor : GameScript
    {
        private new Collider collider;

        public event PlayerTeleporterSensorEventHandler OnPlayerTeleport;

        private void InjectPlayerTeleporterSensor([GameObjectScope] Collider collider)
        {
            this.collider = collider;
        }

        private void Awake()
        {
            InjectDependencies("InjectPlayerTeleporterSensor");

            int layer = LayerMask.NameToLayer(R.S.Layer.PlayerTeleporterSensor);
            if (layer == -1)
            {
                throw new Exception("In order to use a PlayerTeleporterSensor, you must have a " + R.S.Layer.PlayerTeleporterSensor +
                                    " layer.");
            }
            gameObject.layer = layer;
            collider.isTrigger = true;
        }

        public void PlayerTeleport()
        {
            if (OnPlayerTeleport != null) OnPlayerTeleport();
        }
    }
}
﻿using System;
using System.Resources;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Sensor/HunterSpawnerSensor")]
    public class HunterSpawnerSensor : GameScript
    {
        [SerializeField, Tooltip("Hunter Prefab")]
        private GameObject hunterPrefab;
        
        private void Awake()
        {
            int layer = LayerMask.NameToLayer(R.S.Layer.HunterSpawnerSensor);
            if (layer == -1)
            {
                throw new Exception("In order to use a HunterSpawnerSensor, you must have a " + R.S.Layer.HunterSpawnerSensor +
                                    " layer.");
            }
            gameObject.layer = layer;
            gameObject.GetComponent<BoxCollider>().isTrigger = true;
        }

        private void OnTriggerExit(Collider other)
        {
            hunterPrefab = Instantiate(hunterPrefab, transform.position, Quaternion.identity);
            hunterPrefab.transform.SetParent(transform.parent);
            Destroy(gameObject);
        }
    }
}
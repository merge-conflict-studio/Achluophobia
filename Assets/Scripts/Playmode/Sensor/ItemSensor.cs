﻿using System;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public delegate void ItemSensorEventHandler(InstantEffect effect);

    [AddComponentMenu("Game/Sensor/ItemSensor")]
    public class ItemSensor : GameScript
    {
        private new Collider collider;

        public event ItemSensorEventHandler OnCollectItem;

        private void InjectItemSensor([GameObjectScope] Collider collider)
        {
            this.collider = collider;
        }

        private void Awake()
        {
            InjectDependencies("InjectItemSensor");

            int layer = LayerMask.NameToLayer(R.S.Layer.ItemSensor);
            if (layer == -1)
            {
                throw new Exception("In order to use a ItemSensor, you must have a " + R.S.Layer.ItemSensor +
                                    " layer.");
            }
            gameObject.layer = layer;
            collider.isTrigger = true;
        }

        public void CollectItem(InstantEffect effect)
        {
            if (OnCollectItem != null) OnCollectItem(effect);
        }
    }
}
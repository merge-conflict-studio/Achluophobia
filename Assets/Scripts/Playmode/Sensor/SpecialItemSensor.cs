﻿using System;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public delegate void ChangeCollectabilitySpecialItemEventHandler(GameObject specialItem);

    [AddComponentMenu("Game/Sensor/SpecialItemSensor")]
    public class SpecialItemSensor : GameScript
    {
        private new Collider collider;
        private List<SpecialItemStimulus> specialItemsInRange;

        public event ChangeCollectabilitySpecialItemEventHandler OnSpecialItemDetected;
        public event ChangeCollectabilitySpecialItemEventHandler OnSpecialItemUndetected;
        public event ItemSensorEventHandler OnCollectItem;

        private void InjectSpecialItemSensor([GameObjectScope] Collider collider)
        {
            this.collider = collider;
        }

        private void Awake()
        {
            InjectDependencies("InjectSpecialItemSensor");

            int layer = LayerMask.NameToLayer(R.S.Layer.ItemSensor);
            if (layer == -1)
            {
                throw new Exception("In order to use a special item sensor, you must have a " +
                                    R.S.Layer.ItemSensor + " layer.");
            }
            gameObject.layer = layer;
            collider.isTrigger = true;
            specialItemsInRange = new List<SpecialItemStimulus>();
        }

        public void SpecialItemDetected(SpecialItemStimulus specialItem)
        {
            specialItemsInRange.Add(specialItem);
            if (OnSpecialItemDetected != null) OnSpecialItemDetected(specialItem.GetRoot());
        }

        public void SpecialItemUndetected(SpecialItemStimulus specialItem)
        {
            specialItemsInRange.Remove(specialItem);
            if (OnSpecialItemUndetected != null) OnSpecialItemUndetected(specialItem.GetRoot());
        }

        public void CollectItem()
        {
            foreach (SpecialItemStimulus specialItems in specialItemsInRange)
            {
                if (OnCollectItem != null) OnCollectItem(specialItems.GetEffect());
                specialItems.OnItemCollected();
            }
        }
    }
}
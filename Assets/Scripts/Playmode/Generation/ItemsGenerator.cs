﻿using System.Collections.Generic;
using Harmony;
using UnityEngine;

//Partie fier: Philippe Deshênes
namespace Achluophobia
{
    [AddComponentMenu("Game/Generation/ItmesGenerator")]
    public class ItemsGenerator : GameScript
    {
        [SerializeField, Tooltip("Props array")] private GameObject[] propsArray;
        
        [SerializeField, Tooltip("Items array")] 
        private GameObject[] itemsArray;

        [SerializeField, Tooltip("Maximum number of props per level")]
        private int maxPropCount;
        
        [SerializeField, Tooltip("Maximum number of item per level")] 
        private int maxItemCount;

        public List<Vector3> ItemsSpawnPositions { get; private set; }
        public List<Vector3> PropsSpawnPositions { get; private set; }

        private List<GameObject> itemsList;
        private List<GameObject> propsList;
        private GameObject propsGameObject;
        private GameObject itemsGameObject;
        private Room[,] roomMap;
        private int roomCountX;
        private int roomCountZ;
        private int tileSize;
        private int spawnRoomPosition;
        private int levelModifier;

        public void Generate(Room[,] roomMap, int tileSize, int roomCountX, int roomCountZ, int spawnRoomPosition,
                             int levelModifier)
        {
            this.roomCountX = roomCountX;
            this.roomCountZ = roomCountZ;
            this.tileSize = tileSize;
            this.roomMap = roomMap;
            this.levelModifier = levelModifier;
            propsGameObject = new GameObject();
            propsGameObject.name = "Props";
            itemsGameObject = new GameObject();
            itemsGameObject.name = "Items";
            this.spawnRoomPosition = spawnRoomPosition;

            itemsGameObject.AddComponent<DontConsiderAsRoot>();
            propsGameObject.AddComponent<DontConsiderAsRoot>();
            
            SpawnProps();
            SpawnItems();
        }

        private void SpawnProps()
        {
            propsList = GetPropsList();
            PropsSpawnPositions = GetPropsSpawnPositions();
            for (int i = 0; i < maxPropCount; i++)
            {
                GameObject prop = Instantiate(propsList[i], PropsSpawnPositions[i], Quaternion.identity,
                                              propsGameObject.transform);
                prop.transform.Rotate(new Vector3(90, 0, 0));
                propsGameObject.transform.parent = GameObject.Find(GameGlobalVariables.Level + levelModifier).transform;
            }
        }

        private void SpawnItems()
        {
            itemsList = GetItemList();
            ItemsSpawnPositions = GetItemSpawnPositions();
            for (int i = 0; i < maxItemCount; i++)
            {
                Instantiate(itemsList[i], ItemsSpawnPositions[i], Quaternion.identity, 
                            itemsGameObject.transform);
                itemsGameObject.transform.parent = GameObject.Find(GameGlobalVariables.Level + levelModifier).transform;
            }
        }

        private List<GameObject> GetPropsList()
        {
            List<GameObject> propsListToSpawn = new List<GameObject>();
            for (int i = 0; i < maxPropCount; i++)
            {
                GameObject props = propsArray[Random.Range(0, propsArray.Length)];
                propsListToSpawn.Add(props);
            }
            return propsListToSpawn;
        }

        private List<Vector3> GetPropsSpawnPositions()
        {
            List<Vector3> propsSpawnPoints = new List<Vector3>();
            for (int i = 0; i < maxPropCount; i++)
            {
                Room randomRoom = roomMap[Random.Range(0, roomCountX), Random.Range(0, roomCountZ)];
                Vector3 position =
                    new Vector3(
                        randomRoom.BottomLeftCorner.x + Random.Range(0, tileSize) +
                            Random.Range(0, randomRoom.Width) * tileSize,
                        GameGlobalVariables.ItemYOffset,
                        randomRoom.BottomLeftCorner.z + Random.Range(0, tileSize) +
                            Random.Range(0, randomRoom.Height) * tileSize);
                while (propsSpawnPoints.Contains(position) || CheckIfOverSpawnPosition(position))
                {
                    position = new Vector3(
                        randomRoom.BottomLeftCorner.x + Random.Range(0, tileSize) +
                            Random.Range(0, randomRoom.Width) * tileSize, GameGlobalVariables.ItemYOffset,
                        randomRoom.BottomLeftCorner.z + Random.Range(0, tileSize) +
                        Random.Range(0, randomRoom.Height) * tileSize);
                }
                propsSpawnPoints.Add(position);
            }
            return propsSpawnPoints;
        }

        private List<Vector3> GetItemSpawnPositions()
        {
            List<Vector3> itemsSpawnPoints = new List<Vector3>();
            for (int i = 0; i < maxItemCount; i++)
            {
                Room randomRoom = roomMap[Random.Range(0, roomCountX), Random.Range(0, roomCountZ)];
                Vector3 position =
                    new Vector3(
                        randomRoom.BottomLeftCorner.x + Random.Range(0, tileSize) +
                        Random.Range(0, randomRoom.Width - 1) * tileSize, 0,
                        randomRoom.BottomLeftCorner.z + Random.Range(0, tileSize) +
                        Random.Range(0, randomRoom.Height - 1) * tileSize);

                while (PropsSpawnPositions.Contains(position) || itemsSpawnPoints.Contains(position) ||
                       CheckIfOverSpawnPosition(position))
                {
                    position = new Vector3(
                        randomRoom.BottomLeftCorner.x + Random.Range(0, tileSize) +
                        Random.Range(0, randomRoom.Width - 1) * tileSize, GameGlobalVariables.ItemYOffset,
                        randomRoom.BottomLeftCorner.z + Random.Range(0, tileSize) +
                        Random.Range(0, randomRoom.Height - 1) * tileSize);
                }
                itemsSpawnPoints.Add(position);
            }
            return itemsSpawnPoints;
        }

        private List<GameObject> GetItemList()
        {
            List<GameObject> itemListToSpawn = new List<GameObject>();
            for (int i = 0; i < maxItemCount; i++)
            {
                GameObject item = itemsArray[Random.Range(0, itemsArray.Length)];
                itemListToSpawn.Add(item);
            }
            return itemListToSpawn;
        }

        private bool CheckIfOverSpawnPosition(Vector3 position)
        {
            return (position.x >= spawnRoomPosition - GameGlobalVariables.FreeSpaceForSpawn &&
                    position.x <= spawnRoomPosition + GameGlobalVariables.FreeSpaceForSpawn &&
                    position.z >= spawnRoomPosition - GameGlobalVariables.FreeSpaceForSpawn && position.z <=
                    spawnRoomPosition + GameGlobalVariables.FreeSpaceForSpawn);
        }
    }
}
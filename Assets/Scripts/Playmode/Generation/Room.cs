﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Achluophobia
{
    public class Room
    {
        public int Height { get; private set; }
        
        public int Width { get; private set; }
        
        public int RoomNumber { get; set; }
        
        public Vector3 BottomLeftCorner { get; private set; }
        
        public Vector3 BottomRightCorner { get; private set; }
        
        public Vector3 TopLeftCorner { get; private set; }
        
        public Vector3 TopRightCorner { get; private set; }

        private readonly List<GameObject> floorTiles;
        private readonly GameObject roomGameObject;
        private readonly string roomName;

        public Room(List<GameObject> floorTiles, int width, int height, string roomName)
        {
            this.roomName = roomName;
            this.floorTiles = floorTiles;
            Width = width;
            Height = height;

            roomGameObject = new GameObject();

            SetCorners();
            BuildRoomGameObject();
        }

        public GameObject GetRoomGameObject()
        {
            return roomGameObject;
        }
        
        private void SetCorners()
        {
            BottomLeftCorner = floorTiles.ElementAt(0).transform.position;
            TopRightCorner = new Vector3(floorTiles.ElementAt(floorTiles.Count - 1).transform.position.x + 5,
                floorTiles.ElementAt(floorTiles.Count - 1).transform.position.y,
                floorTiles.ElementAt(floorTiles.Count - 1).transform.position.z + 5);
            TopLeftCorner = new Vector3(floorTiles.ElementAt(Height - 1).transform.position.x,
                floorTiles.ElementAt(Height - 1).transform.position.y,
                floorTiles.ElementAt(Height - 1).transform.position.z + 5);
            BottomRightCorner = new Vector3(floorTiles.ElementAt(floorTiles.Count - Height).transform.position.x + 5,
                floorTiles.ElementAt(floorTiles.Count - Height).transform.position.y,
                floorTiles.ElementAt(floorTiles.Count - Height).transform.position.z);
        }

        //Regroupe toutes les tuiles d'une salle pour ensuite créer un GameObject de cette salle complète
        private void BuildRoomGameObject()
        {
            GameObject gameObjectFloorTiles = new GameObject();
            gameObjectFloorTiles.name = "Floor Tiles";
            gameObjectFloorTiles.transform.parent = roomGameObject.transform;
            
            foreach (GameObject floorTile in floorTiles)
            {
                floorTile.transform.parent = gameObjectFloorTiles.transform;
            }
            roomGameObject.name = roomName;
        }
    }
}
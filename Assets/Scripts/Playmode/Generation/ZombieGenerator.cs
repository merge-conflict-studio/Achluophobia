﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Generation/ZombieGenerator")]
    public class ZombieGenerator : GameScript
    {
        [SerializeField, Tooltip("Zombie prefab array")]
        private GameObject[] zombiePrefabArray;

        [SerializeField, Tooltip("The maximum amount of zombies on a floor")]
        private int zombieCap;

        private const float BasicZombieProbabilities = 0.75f;
        private const float DelayBeforeCheckIfNeedToRespawnZombieInSeconds = 3.0f;
        private const float PercentageOfZombiesToMaintainOnAFloor = 0.85f;
        private const string Clone = "(Clone)";
        private const string Team2 = "Team2";

        private ItemsGenerator itemsGenerator;
        private string levelTag;
        private PlayerTeleporterEventChannel playerTeleporterEventChannel;
        private QuestItemGenerator questItemGenerator;
        private int roomCountX;
        private int roomCountZ;
        private Room[,] roomMap;
        private int spawnRoomPosition;
        private int tileSize;
        private int levelModifier;
        private List<GameObject> zombiesList;
        private List<Vector3> zombieSpawnPositions;
        private GameObject zombieGameObject;
        private List<GameObject> zombiesCopies;
        private ImportantGameValues importantGameValues;
        private WaitForSeconds delayCheckForZombieRespawn;

        private void InjectZombieGenerator([EventChannelScope] PlayerTeleporterEventChannel playerTeleporterEventChannel, [ApplicationScope] ImportantGameValues importantGameValues)
        {
            this.playerTeleporterEventChannel = playerTeleporterEventChannel;
            this.importantGameValues = importantGameValues;
        }

        public void Generate(Room[,] roomMap, int tileSize, int roomCountX, int roomCountZ, GameObject itemsGenerator,
                             GameObject questItemGenerator, int spawnRoomPosition, string levelTag,
                             int levelModifier)
        {
            this.roomCountX = roomCountX;
            this.roomCountZ = roomCountZ;
            this.tileSize = tileSize;
            this.roomMap = roomMap;
            this.spawnRoomPosition = spawnRoomPosition;
            this.itemsGenerator = itemsGenerator.GetComponent<ItemsGenerator>();
            this.questItemGenerator = questItemGenerator.GetComponent<QuestItemGenerator>();
            this.levelTag = levelTag;
            this.levelModifier = levelModifier;

            zombiesCopies = new List<GameObject>();
            zombieGameObject = new GameObject { name = "Zombies" };

            delayCheckForZombieRespawn = new WaitForSeconds(DelayBeforeCheckIfNeedToRespawnZombieInSeconds);

            InjectDependencies("InjectZombieGenerator");
            playerTeleporterEventChannel.OnEventPublished += OnLevelChanged;

            zombieGameObject.AddComponent<DontConsiderAsRoot>();
            SpawnZombies();
            if (levelTag == R.S.Tag.Level0) ActivateAllZombies();

            StartCoroutine(RespawnZombieUnderCertainThreshold());
        }

        public void AddZombieCopy(GameObject zombieToAdd)
        {
            zombiesCopies.Add(zombieToAdd);
        }

        public void DeactivateAllZombies()
        {
            foreach (GameObject zombie in zombiesList)
            {
                if (zombie != null) zombie.SetActive(false);
            }
            foreach (GameObject zombie in zombiesCopies)
            {
                if (zombie != null) zombie.SetActive(false);
            }
        }
        private void SpawnZombies()
        {
            zombiesList = GetZombiesList();
            zombieSpawnPositions = GenerateZombieSpawnPositions();

            for (int i = 0; i < zombiesList.Count; i++)
            {
                zombiesList[i] = Instantiate(zombiesList[i], zombieSpawnPositions[i], Quaternion.identity,
                    zombieGameObject.transform);
                zombieGameObject.transform.parent =
                    GameObject.Find(GameGlobalVariables.Level + levelModifier).transform;

                if (zombiesList[i].name != R.S.GameObject.HunterSpawner + Clone)
                {
                    zombiesList[i].GetComponent<ZombieController>().OnZombieDeath += OnZombieDeath;
                }
            }
        }

        private void DeactivateZombiesCopy()
        {
            foreach (GameObject zombie in zombiesCopies)
            {
                if (zombie != null) zombie.SetActive(false);
            }
        }

        private void DeactivateZombiesOriginal()
        {
            foreach (GameObject zombie in zombiesList)
            {
                if (zombie != null) zombie.SetActive(false);
            }
        }

        private void ActivateAllZombies()
        {
            foreach (GameObject zombie in zombiesList)
            {
                if (zombie != null) zombie.SetActive(true);
            }
            foreach (GameObject zombie in zombiesCopies)
            {
                if (zombie != null) zombie.SetActive(true);
            }
        }

        private void ActivateZombiesCopy()
        {
            foreach (GameObject zombie in zombiesCopies)
            {
                if (zombie != null) zombie.SetActive(true);
            }
        }

        private void ActivateZombiesOriginal()
        {
            foreach (GameObject zombie in zombiesList)
            {
                if (zombie != null) zombie.SetActive(true);
            }
        }

        private List<Vector3> GenerateZombieSpawnPositions()
        {
            List<Vector3> zombieSpawnPoints = new List<Vector3>();

            for (int i = 0; i < zombieCap; i++)
            {
                Room randomRoom = roomMap[Random.Range(0, roomCountX), Random.Range(0, roomCountZ)];
                Vector3 position = new Vector3(
                    randomRoom.BottomLeftCorner.x + Random.Range(0, tileSize) +
                    Random.Range(0, randomRoom.Width - 1) * tileSize, 0,
                    randomRoom.BottomLeftCorner.z + Random.Range(0, tileSize) +
                    Random.Range(0, randomRoom.Height - 1) * tileSize);
                while (itemsGenerator.ItemsSpawnPositions.Contains(position) ||
                       itemsGenerator.PropsSpawnPositions.Contains(position) ||
                       questItemGenerator.QuestItemsSpawnPositions.Contains(position) ||
                       questItemGenerator.MissionPropsSpawnPositions.Contains(position) ||
                       CheckIfOverSpawnPosition(position))
                {
                    position = new Vector3(
                        randomRoom.BottomLeftCorner.x + Random.Range(0, tileSize) +
                        Random.Range(0, randomRoom.Width - 1) * tileSize, 0,
                        randomRoom.BottomLeftCorner.z + Random.Range(0, tileSize) +
                        Random.Range(0, randomRoom.Height - 1) * tileSize);
                }
                zombieSpawnPoints.Add(position);
            }
            return zombieSpawnPoints;
        }

        private List<GameObject> GetZombiesList()
        {
            List<GameObject> zombiesToSpawnList = new List<GameObject>();
            for (int i = 0; i < zombieCap; i++)
            {
                GameObject zombieToAdd;
                if (Random.Range(0.0f, 1.0f) < BasicZombieProbabilities)
                {
                    zombieToAdd = zombiePrefabArray[0];
                    zombiesToSpawnList.Add(zombieToAdd);
                }
                else
                {
                    zombieToAdd = zombiePrefabArray[Random.Range(1, zombiePrefabArray.Length)];
                    zombiesToSpawnList.Add(zombieToAdd);
                }
            }
            return zombiesToSpawnList;
        }

        private bool CheckIfOverSpawnPosition(Vector3 position)
        {
            return (position.x >= spawnRoomPosition - GameGlobalVariables.FreeSpaceForSpawn &&
                    position.x <= spawnRoomPosition + GameGlobalVariables.FreeSpaceForSpawn &&
                    position.z >= spawnRoomPosition - GameGlobalVariables.FreeSpaceForSpawn &&
                    position.z <= spawnRoomPosition + GameGlobalVariables.FreeSpaceForSpawn);
        }

        private void OnLevelChanged(PlayerTeleporterEvent playerTeleporterEvent)
        {
            if (playerTeleporterEvent.PlayerTeleporter.tag.Contains(Team2))
            {
                ChangeLevelCopyZombies(playerTeleporterEvent);
            }
            else
            {
                ChangeLevelOriginalZombies(playerTeleporterEvent);
            }
        }

        private void ChangeLevelOriginalZombies(PlayerTeleporterEvent playerTeleporterEvent)
        {
            if (playerTeleporterEvent.PlayerTeleporter.CompareTag(levelTag))
            {
                DeactivateZombiesOriginal();
            }
            else
            {
                int generatorLevel = (int)char.GetNumericValue(levelTag[levelTag.Length - 1]);

                if (generatorLevel == importantGameValues.CurrentLevelTeam1 + 1)
                {
                    ActivateZombiesOriginal();
                }
            }
        }

        private void ChangeLevelCopyZombies(PlayerTeleporterEvent playerTeleporterEvent)
        {
            if (playerTeleporterEvent.PlayerTeleporter.CompareTag(levelTag + Team2))
            {
                DeactivateZombiesCopy();
            }
            else
            {
                int generatorLevel = (int)char.GetNumericValue(levelTag[5]);

                if (generatorLevel == importantGameValues.CurrentLevelTeam2 + 1)
                {
                    ActivateZombiesCopy();
                }
            }
        }

        private void OnZombieDeath(GameObject zombie)
        {
            zombie.GetComponent<ZombieController>().OnZombieDeath -= OnZombieDeath;

            if (zombiesList.Find(g => g.GetInstanceID() == zombie.GetInstanceID()) != null)
            {
                zombiesList.Remove(zombie);
            }
            else
            {
                zombiesCopies.Remove(zombie);
            }
        }

        private void SpawnNewZombie(bool isZombieInTeam2)
        {
            GameObject zombieToSpawn = Random.Range(0.0f, 1.0f) < BasicZombieProbabilities
                ? zombiePrefabArray[0]
                : zombiePrefabArray[Random.Range(1, zombiePrefabArray.Length)];

            if (isZombieInTeam2)
            {
                Vector3 newSpawnPosition = zombieSpawnPositions[Random.Range(0, zombieSpawnPositions.Count)];
                newSpawnPosition.z += GameGlobalVariables.DistanceNextMap2v2;
                zombiesCopies.Add(Instantiate(zombieToSpawn, newSpawnPosition, Quaternion.identity));
                zombiesCopies[zombiesCopies.Count - 1].SetActive(true);

                if (zombieToSpawn.name != R.S.GameObject.HunterSpawner + Clone)
                {
                    zombiesCopies[zombiesCopies.Count - 1].GetComponent<ZombieController>().OnZombieDeath += OnZombieDeath;
                }
            }
            else
            {
                zombiesList.Add(Instantiate(zombieToSpawn,
                    zombieSpawnPositions[Random.Range(0, zombieSpawnPositions.Count)], Quaternion.identity));
                zombiesList[zombiesList.Count - 1].SetActive(true);

                if (zombieToSpawn.name != R.S.GameObject.HunterSpawner + Clone)
                {
                    zombiesList[zombiesList.Count - 1].GetComponent<ZombieController>().OnZombieDeath += OnZombieDeath;
                }
                    
            }
        }

        private IEnumerator RespawnZombieUnderCertainThreshold()
        {
            while (true)
            {
                if (zombiesList[0].activeSelf)
                {
                    if ((float)zombiesList.Count / zombieCap < PercentageOfZombiesToMaintainOnAFloor)
                    {
                        SpawnNewZombie(false);
                    }
                }

                if (importantGameValues.GameMode == GameMode.Versus)
                {
                    if ((float)zombiesCopies.Count / zombieCap < PercentageOfZombiesToMaintainOnAFloor)
                    {
                        SpawnNewZombie(true);
                    }
                }
                yield return delayCheckForZombieRespawn;
            }
        }
    }
}
﻿using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Generation/QuestItemGenerator")]
    public class QuestItemGenerator : GameScript
    {
        [SerializeField, Tooltip("Quest items array")]
        private GameObject[] questItemsArray;

        [SerializeField, Tooltip("Mission props array")]
        private GameObject[] missionPropsArray;

        public List<Vector3> QuestItemsSpawnPositions { get; private set; }
        public List<Vector3> MissionPropsSpawnPositions { get; private set; }

        private List<GameObject> questItemsList;
        private List<GameObject> missionPropsList;

        private Room[,] roomMap;
        private int roomCountX;
        private int roomCountZ;
        private int tileSize;
        private ItemsGenerator itemsGenerator;
        private int spawnRoomPosition;
        private GameObject spawnedQuestItem;
        private GameObject spawnedMissionProp;
        private GameObject questItemsGameObject;
        private GameObject missionPropGameObject;
        private int levelModifier;
        private MapSize mapSize;

        public void Generate(Room[,] roomMap, int tileSize, int roomCountX, int roomCountZ, GameObject itemsGenerator,
            int spawnRoomPosition, int levelModifier, MapSize mapSize)
        {
            this.roomCountX = roomCountX;
            this.roomCountZ = roomCountZ;
            this.tileSize = tileSize;
            this.roomMap = roomMap;
            this.itemsGenerator = itemsGenerator.GetComponent<ItemsGenerator>();
            this.spawnRoomPosition = spawnRoomPosition;
            this.levelModifier = levelModifier;
            this.mapSize = mapSize;
            missionPropGameObject = new GameObject();
            missionPropGameObject.name = "Mission Props";
            questItemsGameObject = new GameObject();
            questItemsGameObject.name = "Quest Items";
            questItemsGameObject.AddComponent<DontConsiderAsRoot>();
            missionPropGameObject.AddComponent<DontConsiderAsRoot>();
            SpawnQuestItems();
            SpawnMissionProps();
        }

        /// <summary>
        /// For tests, don't need all the arguments
        /// </summary>
        /// <param name="levelModifier">Level Modifier</param>
        public void GenerateTest(int levelModifier)
        {
            this.levelModifier = levelModifier;
        }

        /// <summary>
        /// For tests
        /// </summary>
        /// <returns>returns LevelModifier</returns>
        public int GetLevelModifier()
        {
            return levelModifier;
        }

        private void SpawnQuestItems()
        {
            questItemsList = GetQuestItemsList();
            QuestItemsSpawnPositions = GetQuestItemSpawnPositions();
            for (int i = 0; i < questItemsList.Count; i++)
            {
                spawnedQuestItem = Instantiate(questItemsList[i], QuestItemsSpawnPositions[i], Quaternion.identity, 
                                               questItemsGameObject.transform);
                questItemsGameObject.transform.parent =
                    GameObject.Find(GameGlobalVariables.Level + levelModifier).transform;
                spawnedQuestItem.name = questItemsList[i].name;
            }
        }

        private void SpawnMissionProps()
        {
            missionPropsList = GetMissionPropsList();
            MissionPropsSpawnPositions = GetMissionPropsSpawnPositions();

            for (int i = 0; i < missionPropsList.Count; i++)
            {
                if (missionPropsList[i].name == R.S.GameObject.Fire)
                {
                    Vector3 fireSpawnPosition;
                    switch (mapSize)
                    {
                        case MapSize.Small:
                            fireSpawnPosition = GameGlobalVariables.EasyMissionPropLevel1Location;
                            break;
                        case MapSize.Medium:
                            fireSpawnPosition = GameGlobalVariables.NormalMissionPropLeve1Location;
                            break;
                        case MapSize.Large:
                            fireSpawnPosition = GameGlobalVariables.HardMissionPropLevel1Location;
                            break;
                        default:
                            fireSpawnPosition = GameGlobalVariables.EasyMissionPropLevel1Location;
                            break;
                    }
                    spawnedMissionProp = Instantiate(missionPropsList[i],
                        fireSpawnPosition,
                        Quaternion.identity,
                        missionPropGameObject.transform);
                }
                else
                {    
                    spawnedMissionProp = Instantiate(missionPropsList[i], MissionPropsSpawnPositions[i],
                                                 Quaternion.identity, 
                                                 missionPropGameObject.transform);
                }
                missionPropGameObject.transform.parent =
                    GameObject.Find(GameGlobalVariables.Level + levelModifier).transform;
                spawnedMissionProp.name = missionPropsList[i].name;
            }
        }

        private List<Vector3> GetQuestItemSpawnPositions()
        {
            List<Vector3> questItemsSpawnPoints = new List<Vector3>();

            for (int i = 0; i < questItemsArray.Length; i++)
            {
                Room randomRoom = roomMap[Random.Range(0, roomCountX), Random.Range(0, roomCountZ)];
                Vector3 position = new Vector3(
                    randomRoom.BottomLeftCorner.x + Random.Range(0, tileSize) +
                    Random.Range(0, randomRoom.Width - 1) * tileSize, 0.8f,
                    randomRoom.BottomLeftCorner.z + Random.Range(0, tileSize) +
                    Random.Range(0, randomRoom.Height - 1) * tileSize);
                while (itemsGenerator.ItemsSpawnPositions.Contains(position) ||
                       itemsGenerator.PropsSpawnPositions.Contains(position) || 
                       CheckIfOverSpawnPosition(position))
                {
                    position = new Vector3(
                        randomRoom.BottomLeftCorner.x + Random.Range(0, tileSize) +
                        Random.Range(0, randomRoom.Width - 1) * tileSize, 0.8f,
                        randomRoom.BottomLeftCorner.z + Random.Range(0, tileSize) +
                        Random.Range(0, randomRoom.Height - 1) * tileSize);
                }
                questItemsSpawnPoints.Add(position);
            }
            return questItemsSpawnPoints;
        }

        private List<Vector3> GetMissionPropsSpawnPositions()
        {
            List<Vector3> missionPropsSpawnPoints = new List<Vector3>();

            for (int i = 0; i < missionPropsArray.Length; i++)
            {
                Room randomRoom = roomMap[Random.Range(0, roomCountX), Random.Range(0, roomCountZ)];
                Vector3 position = new Vector3(
                    randomRoom.BottomLeftCorner.x + Random.Range(0, tileSize) +
                    Random.Range(0, randomRoom.Width - 1) * tileSize, 0.8f,
                    randomRoom.BottomLeftCorner.z + Random.Range(0, tileSize) +
                    Random.Range(0, randomRoom.Height - 1) * tileSize);
                while (itemsGenerator.ItemsSpawnPositions.Contains(position) ||
                       itemsGenerator.PropsSpawnPositions.Contains(position) ||
                       QuestItemsSpawnPositions.Contains(position) ||
                       CheckIfOverSpawnPosition(position))
                {
                    position = new Vector3(
                        randomRoom.BottomLeftCorner.x + Random.Range(0, tileSize) +
                        Random.Range(0, randomRoom.Width - 1) * tileSize, 0.8f,
                        randomRoom.BottomLeftCorner.z + Random.Range(0, tileSize) +
                        Random.Range(0, randomRoom.Height - 1) * tileSize);
                }
                missionPropsSpawnPoints.Add(position);
            }
            return missionPropsSpawnPoints;
        }

        private List<GameObject> GetQuestItemsList()
        {
            List<GameObject> questItemsListToSpawn = new List<GameObject>();

            foreach (GameObject questItem in questItemsArray)
            {
                questItemsListToSpawn.Add(questItem);
            }

            return questItemsListToSpawn;
        }

        private List<GameObject> GetMissionPropsList()
        {
            List<GameObject> missionPropsListToSpawn = new List<GameObject>();

            foreach (GameObject missionProp in missionPropsArray)
            {
                missionPropsListToSpawn.Add(missionProp);
            }

            return missionPropsListToSpawn;
        }

        private bool CheckIfOverSpawnPosition(Vector3 position)
        {
            return (position.x >= spawnRoomPosition - GameGlobalVariables.FreeSpaceForQuestItemSpawn &&
                    position.x <= spawnRoomPosition + GameGlobalVariables.FreeSpaceForQuestItemSpawn &&
                    position.z >= spawnRoomPosition - GameGlobalVariables.FreeSpaceForQuestItemSpawn &&
                    position.z <= spawnRoomPosition + GameGlobalVariables.FreeSpaceForQuestItemSpawn);
        }
    }
}
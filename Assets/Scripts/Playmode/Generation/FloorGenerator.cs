﻿using System;
using Harmony;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

// Partie fier: Alexandre Lachance
namespace Achluophobia
{
    /// <summary>
    /// Les prefabs de plancher et mur sont fait par Shekn Itrch (ref:https://www.assetstore.unity3d.com/en/#!/content/46732)
    /// Aucun code de Shekn Itrch n'a été utilisé dans ce projet 
    /// </summary>
    /// <remarks>
    /// Les items sont mocker pour l'instant
    /// Les commentaires sont fait pour un côté, le reste reste le même mais pour un côté différent, pas besoin de se répéter
    /// </remarks>
    [AddComponentMenu("Game/Generation/FloorGenerator")]
    public class FloorGenerator : GameScript
    {
        private const int GameSizeX = 2450;
        private const int GameSizeZ = 1450;
        
        [SerializeField, Header("Level")] [Tooltip("Level number to position floor")]
        private int levelModifier;

        [Header("Floor generation")]
        [SerializeField, Tooltip("Vertical wall prefab")] 
        private GameObject verticalWall;
        
        [SerializeField, Tooltip("Horizontal wall prefab")] 
        private GameObject horizontalWall;
        
        [SerializeField, Tooltip("Horizontal wall prefab with offset")] 
        private GameObject horizontalWallWithOffset;
        
        [SerializeField, Tooltip("Floor tile prefab")] 
        private GameObject floorTile;
        
        [SerializeField, Tooltip("Zone to spawn in next room")] 
        private GameObject playerTeleporter;

        [Header("Item generation")] 
        [SerializeField, Tooltip("Item Generator")] 
        private GameObject itemGenerator;
        
        [SerializeField, Tooltip("Item Generator")] 
        private GameObject questItemGenerator;

        [SerializeField, Tooltip("Player Respawner")]
        private GameObject playerRespawner;

        [SerializeField, Tooltip("Small Floor Plane")]
        private GameObject smallFloorPlane;

        [SerializeField, Tooltip("Medium Floor Plane")]
        private GameObject mediumFloorPlane;

        [SerializeField, Tooltip("Large Floor Plane")]
        private GameObject largeFloorPlane;

        private Room[,] roomMap;
        private List<Room> rooms;
        private List<GameObject> floors;
        private GameObject wallsGameObject;
        private GameObject corridorsGameObject;
        private GameObject corridorsFloorTiles;
        private GameObject corridorsWalls;

        private int roomSpacing;
        private int spawnRoomPosition;
        private int mapSize;
        private int roomCountX;
        private int roomCountZ;
        private int minRoomSize;
        private int maxRoomSize;

        private ImportantGameValues importantGameValues;
        private PlayerTeleporterEventChannel playerTeleporterEventChannel;

        private void InjectFloorGenerator([ApplicationScope] ImportantGameValues importantGameValues,
                                          [EventChannelScope] PlayerTeleporterEventChannel playerTeleporterEventChannel)
        {
            this.importantGameValues = importantGameValues;
            this.playerTeleporterEventChannel = playerTeleporterEventChannel;
        }

        private void Awake()
        {
            InjectDependencies("InjectFloorGenerator");
        }

        private void OnEnable()
        {
            playerTeleporterEventChannel.OnEventPublished += OnLevelChanged;
        }

        private void OnDisable()
        {
            playerTeleporterEventChannel.OnEventPublished -= OnLevelChanged;
        }

        public void Generate()
        {
            InitializeGameObjectsForHierarchy();

            rooms = new List<Room>();
            roomMap = new Room[roomCountX, roomCountZ];
            int x = 0;
            int z = 0;
            int roomCount = 0;
            //Création de la map et assignation de zone pour les Room
            for (int i = 0; i < mapSize; i += roomSpacing)
            {
                for (int j = 0; j < mapSize; j += roomSpacing)
                {
                    if (i == spawnRoomPosition && j == spawnRoomPosition)
                    {
                        rooms.Add(GenerateRoom(maxRoomSize, maxRoomSize, i, j, x, z, true, "SpawnRoom"));
                    }
                    else
                    {
                        rooms.Add(GenerateRoom(Random.Range(minRoomSize, maxRoomSize + 1),
                            Random.Range(minRoomSize, maxRoomSize + 1), i, j,
                            x, z, false, "Room : " + roomCount));
                    }
                    roomCount++;
                    z++;
                }
                x++;
                z = 0;
            }
            for (int i = 0; i < rooms.Count; i++)
            {
                rooms[i].RoomNumber = i;
            }
            GenerateWalls();
            SpawnItemsAndZombies();
            GenerateOuterWalls();
            SpawnFloorPlane();
        }

        public void GenerateNavMeshBuilder()
        {
            LocalNavMeshBuilder navMeshBuilder = gameObject.AddComponent<LocalNavMeshBuilder>();
            float halfMapSize = mapSize / 2.0f;
            int builderHeight = 10;
            GameObject levelCenter = new GameObject("levelCenter");
            levelCenter.transform.parent = transform;
            if (importantGameValues.GameMode == GameMode.Versus)
            {
                levelCenter.transform.position = new Vector3(
                    GameSizeX/2,
                    transform.position.y,
                    GameSizeZ/2);
                navMeshBuilder.m_Size = new Vector3(GameSizeX, builderHeight, GameSizeZ);
            }
            else
            {
                levelCenter.transform.position = new Vector3(
                    (transform.position.x + halfMapSize - GameGlobalVariables.HalfTileSize / 2.0f) +
                    (GameGlobalVariables.SpaceBetweenFloors * levelModifier),
                    transform.position.y,
                    transform.position.z + halfMapSize - GameGlobalVariables.HalfTileSize / 2.0f);
                navMeshBuilder.m_Size = new Vector3(mapSize + 50, builderHeight, mapSize + 50);
            }
            
            navMeshBuilder.m_Tracked = levelCenter.transform;
            navMeshBuilder.enabled = true;
        }

        public int GetSpawnRoomPosition()
        {
            switch (importantGameValues.MapSize)
            {
                case MapSize.Small:
                    return spawnRoomPosition + GameGlobalVariables.TileSize;

                case MapSize.Medium:
                    return spawnRoomPosition + (GameGlobalVariables.TileSize * 2);

                case MapSize.Large:
                    return spawnRoomPosition + (GameGlobalVariables.TileSize * 4);

                default:
                    return spawnRoomPosition + GameGlobalVariables.TileSize;
            }
        }

        private void RemoveNavMeshBuilder()
        {
            Destroy(GetComponent<LocalNavMeshBuilder>());
        }

        private void OnLevelChanged(PlayerTeleporterEvent playerTeleporterEvent)
        {
            if (playerTeleporterEvent.PlayerTeleporter.CompareTag(tag))
            {
                RemoveNavMeshBuilder();
            }
            else
            {
                string spawnerTag = playerTeleporterEvent.PlayerTeleporter.tag;
                int currentLevel = (int) char.GetNumericValue(spawnerTag[spawnerTag.Length - 1]);
                
                if (currentLevel == levelModifier - 1)
                {
                    GenerateNavMeshBuilder();
                }
            }
        }

        private Room GenerateRoom(int width, int height, int zoneModifierX, int zoneModifierZ, int x, int z,
            bool isSpawnRoom, string roomName)
        {
            floors = new List<GameObject>();
            //Remplis la room de tuile de plancher
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    floors.Add(
                        Instantiate(floorTile,
                            new Vector3(i * GameGlobalVariables.TileSize + zoneModifierX +
                                       (levelModifier * GameGlobalVariables.SpaceBetweenFloors),
                                        0,
                                        j * GameGlobalVariables.TileSize + zoneModifierZ),
                            Quaternion.identity));
                }
            }
            
            Room room = new Room(floors, width, height, roomName);
            roomMap[x, z] = room;
            
            if (isSpawnRoom)
            {
                Instantiate(playerTeleporter,
                            new Vector3(room.TopLeftCorner.x + (room.Width / 2) * GameGlobalVariables.TileSize, 1,
                                        room.TopLeftCorner.z - (room.Height / 2) * GameGlobalVariables.TileSize), 
                                        Quaternion.identity, GameObject.Find(GameGlobalVariables.Level + levelModifier).transform);
                Instantiate(playerRespawner,
                            new Vector3(room.TopLeftCorner.x + (room.Width / 2) * GameGlobalVariables.TileSize, 1,
                                        room.TopLeftCorner.z - (room.Height / 2) * GameGlobalVariables.TileSize),
                                        Quaternion.identity, GameObject.Find(GameGlobalVariables.Level + levelModifier).transform);

                playerTeleporter.GetComponent<BoxCollider>().isTrigger = false;
                playerRespawner.GetComponent<SphereCollider>().isTrigger = false;
            }

            room.GetRoomGameObject().transform.parent =
                GameObject.Find(GameGlobalVariables.Level + levelModifier).transform;
            return room;
        }

        private void GenerateWalls()
        {
            for (int i = 0; i < roomCountX; i++)
            {
                for (int j = 0; j < roomCountZ; j++)
                {
                    if (i > 0)
                    {
                        BuildLeftRoom(i, j);
                    }
                    else
                    {
                        FillLeftWalls(i, j);
                    }
                    if (i < roomCountX - 1)
                    {
                        BuildRightRoom(i, j);
                    }
                    else
                    {
                        FillRightWalls(i, j);
                    }
                    if (j > 0)
                    {
                        BuildBottomRoom(i, j);
                    }
                    else
                    {
                        FillBottomWalls(i, j);
                    }
                    if (j < roomCountZ - 1)
                    {
                        BuildTopRoom(i, j);
                    }
                    else
                    {
                        FillTopWalls(i, j);
                    }
                }
            }
        }

        private void BuildLeftRoom(int i, int j)
        {
            //Construction d'un corridor si la room de gauche n'est pas collée 
            if (roomMap[i, j].BottomLeftCorner.x != roomMap[i - 1, j].BottomRightCorner.x)
            {
                BuildLeftCorridor(i, j);
            }
            //Sinon on le peuple de mur
            else
            {
                for (int k = 0;
                    k < (int) roomMap[i, j].TopLeftCorner.z - (int) roomMap[i - 1, j].TopRightCorner.z;
                    k += GameGlobalVariables.TileSize)
                {
                    Instantiate(
                        verticalWall,
                        new Vector3(
                            roomMap[i, j].BottomLeftCorner.x - GameGlobalVariables.HalfTileSize,
                            0,
                            roomMap[i - 1, j].TopRightCorner.z + k),
                        Quaternion.identity,
                        wallsGameObject.transform);
                }
            }
        }

        private void BuildLeftCorridor(int i, int j)
        {
            //Vérification de conditions pour ne pas créer de mur de corridor dans le vide
            if (i > 0 && j > 0 && roomMap[i - 1, j - 1].Height == maxRoomSize &&
                roomMap[i - 1, j - 1].TopRightCorner.x >= roomMap[i - 1, j].BottomRightCorner.x)
            {
                //Création du corridor
                for (int k = 0;
                     k < roomMap[i, j].BottomLeftCorner.x - roomMap[i - 1, j - 1].TopRightCorner.x;
                     k += GameGlobalVariables.TileSize)
                {
                    Instantiate(
                        horizontalWall,
                        new Vector3(
                            roomMap[i - 1, j - 1].TopRightCorner.x + k,
                            0,
                            roomMap[i - 1, j - 1].TopRightCorner.z - GameGlobalVariables.HalfTileSize),
                        Quaternion.identity,
                        corridorsWalls.transform);
                }
                for (int l = 0;
                     l < roomMap[i, j].BottomLeftCorner.x - roomMap[i - 1, j].BottomRightCorner.x;
                     l += GameGlobalVariables.TileSize)
                {
                    GameObject floor = Instantiate(
                        floorTile,
                        new Vector3(
                            roomMap[i - 1, j].BottomRightCorner.x + l,
                            0,
                            roomMap[i - 1, j].BottomRightCorner.z),
                        Quaternion.identity,
                        corridorsFloorTiles.transform);

                    Instantiate(
                        horizontalWallWithOffset,
                        new Vector3(
                            floor.transform.localPosition.x - GameGlobalVariables.HorizontalWallOffset,
                            0,
                            floor.transform.localPosition.z + GameGlobalVariables.HalfTileSize),
                        Quaternion.identity,
                        corridorsWalls.transform);
                }
            }
            //Création du corridor
            else
            {
                for (int k = 0;
                     k < (int) roomMap[i, j].BottomLeftCorner.x - (int) roomMap[i - 1, j].BottomRightCorner.x;
                     k += GameGlobalVariables.TileSize)
                {
                    GameObject floor = Instantiate(
                        floorTile,
                        new Vector3(
                            roomMap[i - 1, j].BottomRightCorner.x + k,
                            0,
                            roomMap[i - 1, j].BottomRightCorner.z),
                        Quaternion.identity,
                        corridorsFloorTiles.transform);

                    Instantiate(
                        horizontalWall,
                        new Vector3(
                            floor.transform.localPosition.x,
                            0,
                            floor.transform.localPosition.z - GameGlobalVariables.HalfTileSize),
                            Quaternion.identity,
                            corridorsWalls.transform);

                    Instantiate(
                        horizontalWallWithOffset,
                        new Vector3(
                            floor.transform.localPosition.x - GameGlobalVariables.HorizontalWallOffset,
                            0,
                            floor.transform.localPosition.z + GameGlobalVariables.HalfTileSize),
                         Quaternion.identity,
                        corridorsWalls.transform);
                }
            }
            FillLeftWalls(i, j);
        }

        //Méthode pour remplir les murs de gauche s'il y a création d'un corridor
        private void FillLeftWalls(int i, int j)
        {
            for (int k = i > 0 ? GameGlobalVariables.TileSize : 0;
                 k < (int) roomMap[i, j].TopLeftCorner.z - (int) roomMap[i, j].BottomLeftCorner.z;
                 k += GameGlobalVariables.TileSize)
            {
                Instantiate(
                    verticalWall,
                    new Vector3(
                        roomMap[i, j].BottomLeftCorner.x - GameGlobalVariables.HalfTileSize,
                        0,
                        roomMap[i, j].BottomLeftCorner.z + k),
                    Quaternion.identity,
                    wallsGameObject.transform);
            }
        }

        private void BuildRightRoom(int i, int j)
        {
            if (roomMap[i, j].BottomRightCorner.x != roomMap[i + 1, j].BottomLeftCorner.x)
            {
                FillRightWalls(i, j);
            }
            else
            {
                if (j != roomCountZ - 1) return;
                for (int k = 0;
                    k < (int) roomMap[i, j].TopRightCorner.z - (int) roomMap[i + 1, j].TopLeftCorner.z;
                    k += GameGlobalVariables.TileSize)
                {
                    Instantiate(
                        verticalWall,
                        new Vector3(
                            roomMap[i + 1, j].BottomLeftCorner.x - GameGlobalVariables.HalfTileSize +
                            GameGlobalVariables.VerticalWallOffset,
                            0,
                            roomMap[i + 1, j].TopLeftCorner.z + k),
                        Quaternion.identity,
                        wallsGameObject.transform);
                }
            }
        }

        private void FillRightWalls(int i, int j)
        {
            for (int k = i < roomCountX - 1 ? GameGlobalVariables.TileSize : 0;
                 k < (int) roomMap[i, j].TopRightCorner.z - (int) roomMap[i, j].BottomRightCorner.z;
                 k += GameGlobalVariables.TileSize)
            {
                Instantiate(
                    verticalWall,
                    new Vector3(
                        roomMap[i, j].BottomRightCorner.x - GameGlobalVariables.HalfTileSize,
                        0,
                        roomMap[i, j].BottomRightCorner.z + k),
                    Quaternion.identity,
                    wallsGameObject.transform);
            }
        }

        private void BuildBottomRoom(int i, int j)
        {
            if (roomMap[i, j].BottomRightCorner.z != roomMap[i, j - 1].TopRightCorner.z)
            {
                BuildBottomCorridor(i, j);
            }
            else
            {
                for (int k = 0;
                     k < (int) roomMap[i, j].BottomRightCorner.x - (int) roomMap[i, j - 1].TopRightCorner.x;
                     k += GameGlobalVariables.TileSize)
                {
                    Instantiate(
                        horizontalWall,
                        new Vector3(
                            roomMap[i, j - 1].TopRightCorner.x + k,
                            0,
                            roomMap[i, j].BottomLeftCorner.z - GameGlobalVariables.HalfTileSize),
                        Quaternion.identity,
                        wallsGameObject.transform);
                }
            }
        }

        private void BuildBottomCorridor(int i, int j)
        {
            if (i > 0 && j > 0 && roomMap[i - 1, j - 1].Width == maxRoomSize &&
                roomMap[i - 1, j - 1].TopRightCorner.z >= roomMap[i, j - 1].TopLeftCorner.z)
            {
                for (int k = 0;
                     k < roomMap[i, j].BottomLeftCorner.z - roomMap[i - 1, j - 1].TopRightCorner.z;
                     k += GameGlobalVariables.TileSize)
                {
                    Instantiate(
                        verticalWall,
                        new Vector3(
                            roomMap[i - 1, j - 1].TopRightCorner.x - GameGlobalVariables.HalfTileSize,
                            0,
                            roomMap[i - 1, j - 1].TopRightCorner.z + k),
                        Quaternion.identity,
                        corridorsWalls.transform);
                }
                for (int l = 0;
                     l < roomMap[i, j].BottomLeftCorner.z - roomMap[i, j - 1].TopLeftCorner.z;
                     l += GameGlobalVariables.TileSize)
                {
                    GameObject floor = Instantiate(
                        floorTile,
                        new Vector3(
                            roomMap[i, j - 1].TopLeftCorner.x,
                            0,
                            roomMap[i, j - 1].TopLeftCorner.z + l),
                        Quaternion.identity,
                        corridorsFloorTiles.transform);

                    Instantiate(
                        verticalWall,
                        new Vector3(
                            floor.transform.localPosition.x + GameGlobalVariables.HalfTileSize +
                                GameGlobalVariables.VerticalWallOffset,
                            0,
                            floor.transform.localPosition.z),
                        Quaternion.identity,
                        corridorsWalls.transform);
                }
            }
            else
            {
                for (int k = 0;
                     k < (int) roomMap[i, j].BottomLeftCorner.z - (int) roomMap[i, j - 1].TopLeftCorner.z;
                     k += GameGlobalVariables.TileSize)
                {
                    GameObject floor = Instantiate(
                        floorTile,
                        new Vector3(
                            roomMap[i, j - 1].TopLeftCorner.x,
                            0,
                            roomMap[i, j - 1].TopLeftCorner.z + k),
                        Quaternion.identity,
                        corridorsFloorTiles.transform);

                    Instantiate(
                        verticalWall,
                        new Vector3(
                            floor.transform.localPosition.x - GameGlobalVariables.HalfTileSize,
                            0,
                            floor.transform.localPosition.z),
                        Quaternion.identity,
                        corridorsWalls.transform);

                    Instantiate(
                        verticalWall,
                        new Vector3(
                            floor.transform.localPosition.x + GameGlobalVariables.HalfTileSize +
                                GameGlobalVariables.VerticalWallOffset,
                            0,
                            floor.transform.localPosition.z),
                        Quaternion.identity,
                        corridorsWalls.transform);
                }
            }
            
            FillBottomWalls(i, j);
        }

        private void FillBottomWalls(int i, int j)
        {
            for (int k = j > 0 ? GameGlobalVariables.TileSize : 0;
                 k < (int) roomMap[i, j].BottomRightCorner.x - (int) roomMap[i, j].BottomLeftCorner.x;
                 k += GameGlobalVariables.TileSize)
            {
                Instantiate(
                    horizontalWall,
                    new Vector3(
                        roomMap[i, j].BottomLeftCorner.x + k,
                        0,
                        roomMap[i, j].BottomLeftCorner.z - GameGlobalVariables.HalfTileSize),
                    Quaternion.identity,
                    wallsGameObject.transform);
            }
        }

        private void BuildTopRoom(int i, int j)
        {
            if (roomMap[i, j].TopRightCorner.z != roomMap[i, j + 1].BottomRightCorner.z)
            {
                FillTopWalls(i, j);
            }
            else
            {
                if (i == roomCountX - 1)
                {
                    for (int k = 0;
                         k < (int) roomMap[i, j].TopRightCorner.x - (int) roomMap[i, j + 1].BottomRightCorner.x;
                         k += GameGlobalVariables.TileSize)
                    {
                        Instantiate(
                            horizontalWall,
                            new Vector3(
                                roomMap[i, j + 1].BottomRightCorner.x + k,
                                0,
                                roomMap[i, j + 1].BottomRightCorner.z - GameGlobalVariables.HalfTileSize),
                            Quaternion.identity,
                            wallsGameObject.transform);
                    }
                }
            }
        }

        private void FillTopWalls(int i, int j)
        {
            for (int k = j < roomCountZ - 1 ? GameGlobalVariables.TileSize : 0;
                 k < (int) roomMap[i, j].TopRightCorner.x - (int) roomMap[i, j].TopLeftCorner.x;
                 k += GameGlobalVariables.TileSize)
            {
                Instantiate(
                    horizontalWall,
                    new Vector3(
                        roomMap[i, j].TopLeftCorner.x + k,
                        0,
                        roomMap[i, j].TopLeftCorner.z - GameGlobalVariables.HalfTileSize),
                    Quaternion.identity,
                    wallsGameObject.transform);
            }
        }

        private void GenerateOuterWalls()
        {
            for (int i = 0; i < mapSize; i += GameGlobalVariables.TileSize)
            {
                Instantiate(
                    verticalWall,
                    new Vector3(
                        mapSize - GameGlobalVariables.HalfTileSize +
                            (levelModifier * GameGlobalVariables.SpaceBetweenFloors),
                        0,
                        i),
                    Quaternion.identity,
                    wallsGameObject.transform);

                Instantiate(
                    horizontalWall,
                    new Vector3(
                        i + (levelModifier * GameGlobalVariables.SpaceBetweenFloors),
                        0,
                        0 - GameGlobalVariables.HalfTileSize),
                    Quaternion.identity,
                    wallsGameObject.transform);

                Instantiate(
                    verticalWall,
                    new Vector3(
                        (levelModifier * GameGlobalVariables.SpaceBetweenFloors) - GameGlobalVariables.HalfTileSize,
                        0,
                        i),
                    Quaternion.identity,
                    wallsGameObject.transform);

                Instantiate(
                    horizontalWall,
                    new Vector3(
                        i + (levelModifier * GameGlobalVariables.SpaceBetweenFloors),
                        0,
                        mapSize - GameGlobalVariables.HalfTileSize),
                    Quaternion.identity,
                    wallsGameObject.transform);
            }
        }

        private void InitializeGameObjectsForHierarchy()
        {
            switch (importantGameValues.MapSize)
            {
                case MapSize.Small:
                    roomSpacing = 15;
                    spawnRoomPosition = 60;
                    mapSize = 150;
                    roomCountX = 10;
                    roomCountZ = 10;
                    minRoomSize = 2;
                    maxRoomSize = 3;
                    break;

                case MapSize.Medium:
                    roomSpacing = 25;
                    spawnRoomPosition = 100;
                    mapSize = 250;
                    roomCountX = 10;
                    roomCountZ = 10;
                    minRoomSize = 3;
                    maxRoomSize = 5;
                    break;

                case MapSize.Large:
                    roomSpacing = 45;
                    spawnRoomPosition = 180;
                    mapSize = 450;
                    roomCountX = 10;
                    roomCountZ = 10;
                    minRoomSize = 7;
                    maxRoomSize = 9;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            wallsGameObject = new GameObject();
            wallsGameObject.transform.parent = GameObject.Find(GameGlobalVariables.Level + levelModifier).transform;
            wallsGameObject.name = "Walls";
            corridorsGameObject = new GameObject();
            corridorsGameObject.transform.parent = GameObject.Find(GameGlobalVariables.Level + levelModifier).transform;
            corridorsGameObject.name = "Corridors";
            corridorsFloorTiles = new GameObject();
            corridorsFloorTiles.transform.parent =
                GameObject.Find(GameGlobalVariables.Level + levelModifier + "/Corridors").transform;
            corridorsFloorTiles.name = "Floor Tiles";
            corridorsWalls = new GameObject();
            corridorsWalls.transform.parent =
                GameObject.Find(GameGlobalVariables.Level + levelModifier + "/Corridors").transform;
            corridorsWalls.name = "Walls";
        }


        private void SpawnItemsAndZombies()
        {
            //Check pour enlever les GetComponent
            itemGenerator.GetComponent<ItemsGenerator>().Generate(roomMap, GameGlobalVariables.TileSize, roomCountX,
                roomCountZ,
                spawnRoomPosition * levelModifier, levelModifier);
            
            questItemGenerator.GetComponent<QuestItemGenerator>().Generate(roomMap, GameGlobalVariables.TileSize,
                roomCountX, roomCountZ,
                itemGenerator, spawnRoomPosition * levelModifier, levelModifier, importantGameValues.MapSize);
            
            GetComponent<ZombieGenerator>().Generate(roomMap, GameGlobalVariables.TileSize, roomCountX,
                roomCountZ,
                itemGenerator, questItemGenerator, spawnRoomPosition, tag, levelModifier);
        }

        private void SpawnFloorPlane()
        {
            switch (importantGameValues.MapSize)
            {
                    case MapSize.Small:
                        Instantiate(smallFloorPlane, 
                            new Vector3(71.1f + (levelModifier * GameGlobalVariables.SpaceBetweenFloors), 0, 70.7f), 
                            Quaternion.identity,
                            GameObject.Find(GameGlobalVariables.Level + levelModifier).transform);
                        break;
                    case MapSize.Medium:
                        Instantiate(mediumFloorPlane,
                            new Vector3(121 + (levelModifier * GameGlobalVariables.SpaceBetweenFloors), 0, 122),
                            Quaternion.identity,
                            GameObject.Find(GameGlobalVariables.Level + levelModifier).transform);
                        break;
                    case MapSize.Large:
                        Instantiate(largeFloorPlane,
                            new Vector3(227 + (levelModifier * GameGlobalVariables.SpaceBetweenFloors), 0, 223),
                            Quaternion.identity,
                            GameObject.Find(GameGlobalVariables.Level + levelModifier).transform);
                        break;
            }
        }
    }
}
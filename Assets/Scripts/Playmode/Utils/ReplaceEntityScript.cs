﻿using UnityEngine;

namespace Achluophobia
{
    public class ReplaceEntityScript : GameScript
    {
        private const float baseYaxisValue = 0.083333333f;

        private void LateUpdate()
        {
            transform.position = new Vector3(transform.position.x, baseYaxisValue, transform.position.z);
        }
    }
}
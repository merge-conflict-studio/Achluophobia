﻿using UnityEngine;

namespace Achluophobia
{
    public class ReplacePlayerDownToZeroInYAxis : GameScript
    {
        private const float baseYaxisValue = 0f;

        private void LateUpdate()
        {
            transform.position = new Vector3(transform.position.x, baseYaxisValue, transform.position.z);
        }
    }
}

﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public class StopAllVelocity : GameScript
    {
        private Rigidbody rigidbody;

        private void InjectStopAllVelocity([EntityScope] Rigidbody rigidbody)
        {
            this.rigidbody = rigidbody;
        }

        private void Awake()
        {
            InjectDependencies("InjectStopAllVelocity");
        }

        private void LateUpdate()
        {
            rigidbody.velocity = Vector3.zero;
            rigidbody.angularVelocity = Vector3.zero;
        }
    }
}

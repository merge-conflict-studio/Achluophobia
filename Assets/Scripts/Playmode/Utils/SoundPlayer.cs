﻿using UnityEngine;

namespace Achluophobia
{
    public class SoundPlayer : MonoBehaviour
    {
        private AudioSource audioSource;

        public void PlayMusicInLoop(AudioClip clip, float volume = 0.45f)
        {
            audioSource.clip = clip;
            audioSource.loop = true;
            audioSource.Play();
            audioSource.volume = volume;
        }

        public void PlaySound(AudioClip clip)
        {
            audioSource.PlayOneShot(clip);
        }

        public void Stop()
        {
            audioSource.Stop();
            audioSource.loop = false;
        }

        //Doit être setter a deux endroits différents, on doit setter le audioSource
        public void SetAudioSource(AudioSource audioSource)
        {
            this.audioSource = audioSource;
        }
    }
}



﻿namespace Achluophobia
{
    public class GameAchievements
    {
        public long Id { get; set; }
        public bool Succeeded { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public long GameStatsId { get; set; }
    }
}
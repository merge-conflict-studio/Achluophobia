﻿using System;
using Harmony;

namespace Achluophobia
{
    public class PlayerValues : Script
    {
        public int ZombiesKilledPlayer0 { get; private set; }
        public int ZombiesKilledPlayer1 { get; private set; }
        public int ZombiesKilledPlayer2 { get; private set; }
        public int ZombiesKilledPlayer3 { get; private set; }
        
        public bool Player0HasShotOnLevel { get; private set; }
        public bool Player1HasShotOnLevel { get; private set; }
        public bool Player2HasShotOnLevel { get; private set; }
        public bool Player3HasShotOnLevel { get; private set; }

        public void IncrementPlayerKills(PlayerNumber playerNumber)
        {
            switch (playerNumber)
            {
                case PlayerNumber.One :
                    ZombiesKilledPlayer0++;
                    break;
                case PlayerNumber.Two :
                    ZombiesKilledPlayer1++;
                    break;
                case PlayerNumber.Three :
                    ZombiesKilledPlayer2++;
                    break;
                case PlayerNumber.Four :
                    ZombiesKilledPlayer3++;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("playerNumber", playerNumber, null);
            }
        }

        public void SetPlayerHasShotOnLevel(PlayerNumber playerNumber, bool hasShot)
        {
            switch (playerNumber)
            {
                case PlayerNumber.One :
                    Player0HasShotOnLevel = hasShot;
                    break;
                case PlayerNumber.Two :
                    Player1HasShotOnLevel = hasShot;
                    break;
                case PlayerNumber.Three :
                    Player2HasShotOnLevel = hasShot;
                    break;
                case PlayerNumber.Four :
                    Player3HasShotOnLevel = hasShot;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("playerNumber", playerNumber, null);
            }
        }
    }
}
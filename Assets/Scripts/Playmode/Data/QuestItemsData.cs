﻿using System;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public class QuestItemsData : GameScript
    {
        [SerializeField, Tooltip("Number of items to collect in the level")] 
        private int itemsToCollect;

        [SerializeField, Tooltip("Name of the level")] 
        private string levelName;

        public int ItemsCollected;

        public bool CheckAllItemsCollected()
        {
            return ItemsCollected == itemsToCollect;
        }

        public int ItemsToCollect
        {
            get { return itemsToCollect; }
            set { itemsToCollect = value; }
        }
        
        public string LevelName
        {
            get { return levelName; }
            set { levelName = value; }
        }

        public void AddOneCollectedQuestItemsForTest()
        {
            if (CompareTag(R.S.Tag.Untagged))
            {
                throw new ArgumentNullException();
            }
            if (ItemsCollected > itemsToCollect)
            {
                throw new ArgumentOutOfRangeException();
            }
            if (CompareTag(levelName))
            {
                ItemsCollected++;
            }
        }
    }
}
﻿using UnityEngine;
namespace Achluophobia
{
    public class GameGlobalVariables : GameScript
    {
        public static int TileSize = 5;
        public static float HalfTileSize = 2.5f;
        public static int SpaceBetweenFloors = 500;
        public static float HorizontalWallOffset = 0.25f;
        public static float VerticalWallOffset = 0.5f;
        public static string Level = "Level";

        public static Vector3 EasyMissionPropLevel1Location = new Vector3(565, 1, 68.24f);
        public static Vector3 NormalMissionPropLeve1Location = new Vector3(610, 1, 113.06f);
        public static Vector3 HardMissionPropLevel1Location = new Vector3(700, 1, 203.14f);

        public static float FreeSpaceForSpawn = 8f;
        public static float ItemYOffset = 0.83f;

        public static float FreeSpaceForQuestItemSpawn = 0.75f;
        public static int DistanceNextMap2v2 = 1000;

        public static int MinOffsetBetweenPlayerSpawn = 4;
        public static int RespawnTime = 10;

        public static int DeadYPosition = 300;
    }
}
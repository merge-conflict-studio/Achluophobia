﻿using System.Collections.Generic;
using Harmony;
using JetBrains.Annotations;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Data/GameAchievementsRepository")]
    public sealed class GameAchievementsRepository : GameScript
    {
        private Repository repository;

        private void InjectGameAchievementsRepository([ApplicationScope] SqLiteConnectionFactory connectionFactory,
                                                      [ApplicationScope] SqLiteParameterFactory parameterFactory)
        {
            repository = new Repository(connectionFactory, parameterFactory, new GameAchievementsMapper());
        }

        private void Awake()
        {
            InjectDependencies("InjectGameAchievementsRepository");
        }

        public GameAchievements GetAchievementByName(string achievementName)
        {
            return repository.SelectAchievementByName(achievementName);
        }

        public void ChangeSuccessOfGameAchievement(string achievementName, bool success)
        {
            repository.UpdateSuccessValueOfAchievement(achievementName, success);
        }

        public IList<GameAchievements> GetAllSuccessfulAchievements(long id)
        {
            return repository.GetAllSuccessfulAchievements(id);
        }

        public IList<GameAchievements> GetAllUnsuccessfulAchievements(long id)
        {
            return repository.GetAllUncsucessfulAchievements(id);
        }

        private class Repository : DbRepository<GameAchievements>
        {
            public Repository([NotNull] SqLiteConnectionFactory connectionFactory,
                              [NotNull] SqLiteParameterFactory parameterFactory,
                              [NotNull] SqLiteDataMapper<GameAchievements> dataMapper)
                              : base(connectionFactory, parameterFactory, dataMapper)
            {
            }

            public void UpdateSuccessValueOfAchievement(string achievementName, bool success)
            {
                ExecuteUpdate("UPDATE GameAchievements SET succeeded = ? WHERE name = ? ;", new object[]
                {
                    success,
                    achievementName
                });
            }

            public GameAchievements SelectAchievementByName(string achievementName)
            {
                return ExecuteSelectOne("SELECT * FROM GameAchievements WHERE name = ? ;", new object[]
                {
                    achievementName
                });
            }

            public IList<GameAchievements> GetAllSuccessfulAchievements(long id)
            {
                return ExecuteSelectAll("SELECT * FROM GameAchievements WHERE succeeded = 1 AND gamestatsid = ? ;", new object[] 
                {
                    id
                });
            }

            public IList<GameAchievements> GetAllUncsucessfulAchievements(long id)
            {
                return ExecuteSelectAll("SELECT * FROM GameAchievements WHERE succeeded = 0 AND gamestatsid = ? ;", new object[] 
                {
                    id
                });
            }
        }
    }
}
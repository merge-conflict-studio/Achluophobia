﻿namespace Achluophobia
{
    public class GeneralStats
    {
        public long Id { get; set; }
        public int NumberOfDeaths { get; set; }
        public long TimePlayed { get; set; }
        public int BaseZombieKilled { get; set; }
        public int RunnerZombieKilled { get; set; }
        public int HunterZombieKilled { get; set; }
        public int BoomerZombieKilled { get; set; }
        public int WitchZombieKilled { get; set; }
    }
}
﻿using Harmony;
using JetBrains.Annotations;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Data/GeneralStatsRepository")]
    public sealed class GeneralStatsRepository : GameScript
    {
        private Repository repository;

        private void InjectGeneralStatsRepository([ApplicationScope] SqLiteConnectionFactory connectionFactory,
                                                  [ApplicationScope] SqLiteParameterFactory parameterFactory)
        {
            repository = new Repository(connectionFactory, parameterFactory, new GeneralStatsMapper());
        }

        public void Awake()
        {
            InjectDependencies("InjectGeneralStatsRepository");
        }

        public void ChangePlayerGeneralStats(GeneralStats generalStats)
        {
            repository.UpdateStats(generalStats);
        }

        public GeneralStats GetStatsFromId(long id)
        {
            return repository.SelectStatsFromId(id);
        }

        public GeneralStats GetStats()
        {
            return repository.SelectStats();
        }

        public void AddStats(GeneralStats generalStats)
        {
            repository.InsertStats(generalStats);
        }

        private class Repository : DbRepository<GeneralStats>
        {
            public Repository([NotNull] SqLiteConnectionFactory connectionFactory,
                              [NotNull] SqLiteParameterFactory parameterFactory,
                              [NotNull] SqLiteDataMapper<GeneralStats> dataMapper)
                              : base(connectionFactory, parameterFactory, dataMapper)
            {
            }

            public void UpdateStats(GeneralStats generalStats)
            {
                ExecuteUpdate("UPDATE GeneralStats SET " +
                              "numberofdeaths = ?, " +
                              "timeplayed = ?, " +
                              "basezombie_killed = ?, " +
                              "runnerzombie_killed = ?, " +
                              "boomerzombie_killed = ?, " +
                              "hunterzombie_killed = ?, " +
                              "witchzombie_killed = ? " +
                              "WHERE id = ? ", new object[]
                {
                    generalStats.NumberOfDeaths,
                    generalStats.TimePlayed,
                    generalStats.BaseZombieKilled,
                    generalStats.RunnerZombieKilled,
                    generalStats.BoomerZombieKilled,
                    generalStats.HunterZombieKilled,
                    generalStats.WitchZombieKilled,
                    generalStats.Id
                });
            }

            public GeneralStats SelectStatsFromId(long id)
            {
                return ExecuteSelectOne("SELECT * FROM GeneralStats WHERE id = ? ;", new object[]
                {
                    id
                });
            }

            public GeneralStats SelectStats()
            {
                return ExecuteSelectOne("SELECT * FROM GeneralStats ;", new object[] { });
            }

            public void InsertStats(GeneralStats generalStats)
            {
                ExecuteInsert("INSERT INTO GeneralStats (id, numberofdeaths, timeplayed, basezombie_killed, " +
                              "runnerzombie_killed, hunterzombie_killed, boomerzombie_killed, witchzombie_killed) " +
                              "VALUES (?, ?, ?, ?, ?, ?, ?, ?) ;", new object[]
                {
                    generalStats.Id,
                    generalStats.NumberOfDeaths,
                    generalStats.TimePlayed,
                    generalStats.BaseZombieKilled,
                    generalStats.RunnerZombieKilled,
                    generalStats.HunterZombieKilled,
                    generalStats.BoomerZombieKilled,
                    generalStats.WitchZombieKilled
                });
            }
        }
    }
}
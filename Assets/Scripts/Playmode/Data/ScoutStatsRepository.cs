﻿using Harmony;
using JetBrains.Annotations;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Data/ScoutStatsRepository")]
    public sealed class ScoutStatsRepository : GameScript
    {
        private Repository repository;

        private void InjectScoutStatsRepository([ApplicationScope] SqLiteConnectionFactory connectionFactory,
                                                [ApplicationScope] SqLiteParameterFactory parameterFactory)
        {
            repository = new Repository(connectionFactory, parameterFactory, new ScoutStatsMapper());
        }

        private void Awake()
        {
            InjectDependencies("InjectScoutStatsRepository");
        }

        public void ChangeNumberOfGlowSticksDropped(ScoutStats scoutStats)
        {
            repository.UpdateNumberOfGlowSticksDropped(scoutStats);
        }

        public ScoutStats GetScoutStats()
        {
            return repository.SelectScoutStats();
        }

        private class Repository : DbRepository<ScoutStats>
        {
            public Repository([NotNull] SqLiteConnectionFactory connectionFactory,
                              [NotNull] SqLiteParameterFactory parameterFactory,
                              [NotNull] SqLiteDataMapper<ScoutStats> dataMapper)
                              : base (connectionFactory, parameterFactory, dataMapper)
            {
            }

            public void UpdateNumberOfGlowSticksDropped(ScoutStats scoutStats)
            {
                ExecuteUpdate("UPDATE ScoutStats SET numberofglowsticksdropped = ? WHERE idscoutstats = ? ;", new object[]
                    {
                        scoutStats.NumberOfGlowSticksDropped,
                        scoutStats.Id
                    });
            }

            public ScoutStats SelectScoutStats()
            {
                return ExecuteSelectOne("SELECT * FROM ScoutStats ;", new object[] { });
            }
        }
    }
}

﻿using System;
using UnityEngine;

namespace Achluophobia
{
    public class RespawnerData : GameScript
    {
        public GameObject PlayerToRespawnTeam1 { get; private set; }
        public GameObject PlayerToRespawnTeam2 { get; private set; }
        public Vector3 RespawnPosition1 { get; private set; }
        public Vector3 RespawnPosition2 { get; private set; }

        private void Awake()
        {
            PlayerToRespawnTeam1 = null;
            PlayerToRespawnTeam2 = null;
            RespawnPosition1 = Vector3.zero;
            RespawnPosition2 = Vector3.zero;
        }

        public void SetPlayerAndPosition(GameObject playerToRespawn, Vector3 respawnPosition)
        {
            switch (playerToRespawn.GetComponent<PlayerController>().PlayerTeam)
            {
                case Team.One:
                    PlayerToRespawnTeam1 = playerToRespawn;
                    RespawnPosition1 = respawnPosition;
                    break;
                case Team.Two:
                    PlayerToRespawnTeam2 = playerToRespawn;
                    RespawnPosition2 = respawnPosition;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            
        }

        public void RemovePlayerAndPosition(Team team)
        {
            switch (team)
            {
                case Team.One:
                    PlayerToRespawnTeam1 = null;
                    RespawnPosition1 = Vector3.zero;
                    break;
                case Team.Two:
                    PlayerToRespawnTeam2 = null;
                    RespawnPosition2 = Vector3.zero;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("team", team, null);
            }
        }
    }
}

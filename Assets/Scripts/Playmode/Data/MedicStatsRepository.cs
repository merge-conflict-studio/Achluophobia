﻿using Harmony;
using JetBrains.Annotations;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("MedicStatsRepository")]
    public sealed class MedicStatsRepository : GameScript
    {
        private Repository repository;

        private void InjectMedicStatsRepository([ApplicationScope] SqLiteConnectionFactory connectionFactory,
                                                [ApplicationScope] SqLiteParameterFactory parameterFactory)
        {
            repository = new Repository(connectionFactory, parameterFactory, new MedicStatsMapper());
        }

        private void Awake()
        {
            InjectDependencies("InjectMedicStatsRepository");
        }

        public void ChangeHealthGiven(MedicStats medicStats)
        {
            repository.UpdateHealthGiven(medicStats);
        }

        public MedicStats GetMedicStats()
        {
            return repository.SelectMedicStats();
        }

        private class Repository : DbRepository<MedicStats>
        {
            public Repository([NotNull] SqLiteConnectionFactory connectionFactory,
                              [NotNull] SqLiteParameterFactory parameterFactory,
                              [NotNull] SqLiteDataMapper<MedicStats> dataMapper)
                              : base (connectionFactory, parameterFactory, dataMapper)
            {
            }

            public void UpdateHealthGiven(MedicStats medicStats)
            {
                ExecuteUpdate("UPDATE MedicStats SET ammountofhealthgiven = ? WHERE idmedicstats = ? ;", new object[]
                {
                    medicStats.AmmountOfHealthGiven,
                    medicStats.Id
                });
            }

            public MedicStats SelectMedicStats()
            {
                return ExecuteSelectOne("SELECT * FROM MedicStats ;", new object[] { });
            }
        }
    }
}

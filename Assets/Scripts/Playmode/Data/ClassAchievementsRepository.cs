﻿using System.Collections.Generic;
using Harmony;
using JetBrains.Annotations;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Data/ClassAchievementsRepository")]
    public sealed class ClassAchievementsRepository : GameScript
    {
        private Repository repository;

        private void InjectClassAchievementsRepository([ApplicationScope] SqLiteConnectionFactory connectionFactory,
                                                       [ApplicationScope] SqLiteParameterFactory parameterFactory)
        {
            repository = new Repository(connectionFactory, parameterFactory, new ClassAchievementsMapper());
        }

        private void Awake()
        {
            InjectDependencies("InjectClassAchievementsRepository");
        }

        public ClassAchievements GetAchievementByName(string achievementName)
        {
            return repository.SelectAchievementByName(achievementName);
        }

        public void ChangeSuccessValueOfClassAchievement(string achievementName, bool success)
        {
            repository.UpdateSucessValueOfAchievement(achievementName, success);
        }

        public IList<ClassAchievements> GetAllSuccessfulAchievementsFromClass(long id)
        {
            return repository.GetAllSuccessfulAchievements(id);
        }

        public IList<ClassAchievements> GetAllUnsuccessfulAchievementsFromClass(long id)
        {
            return repository.GetAllUnsuccessfulAchievements(id);
        }

        private class Repository : DbRepository<ClassAchievements>
        {
            public Repository([NotNull] SqLiteConnectionFactory connectionFactory,
                              [NotNull] SqLiteParameterFactory parameterFactory,
                              [NotNull] SqLiteDataMapper<ClassAchievements> dataMapper)
                              : base(connectionFactory, parameterFactory, dataMapper)
            {
            }

            public void UpdateSucessValueOfAchievement(string achievementName, bool success)
            {
                ExecuteUpdate("UPDATE ClassAchievements SET succeeded = ? WHERE name = ? ;", new object[]
                {
                    success,
                    achievementName
                });
            }

            public ClassAchievements SelectAchievementByName(string achievementName)
            {
                return ExecuteSelectOne("SELECT * FROM ClassAchievements WHERE name = ? ;", new object[]
                {
                    achievementName
                });
            }

            public IList<ClassAchievements> GetAllSuccessfulAchievements(long id)
            {
                return ExecuteSelectAll("SELECT * FROM ClassAchievements WHERE succeeded = 1 AND generalstatsid = ? ;",
                    new object[]
                    {
                        id
                    });
            }

            public IList<ClassAchievements> GetAllUnsuccessfulAchievements(long id)
            {
                return ExecuteSelectAll("SELECT * FROM ClassAchievements WHERE succeeded = 0 AND generalstatsid = ? ;",
                    new object[]
                    {
                        id
                    });
            }
        }
    }
}
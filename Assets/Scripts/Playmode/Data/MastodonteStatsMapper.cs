﻿using System.Data.Common;

namespace Achluophobia
{
    public class MastodonteStatsMapper : SqLiteDataMapper<MastodonteStats>
    {
        public override MastodonteStats GetObjectFromReader(DbDataReader reader)
        {
            return new MastodonteStats
            {
                Id = reader.GetInt64(reader.GetOrdinal("idmastodontestats")),
                ZombieKilledWithGrenade = reader.GetInt64(reader.GetOrdinal("zombiekilledwithgrenade")),
                GeneralStatsId = reader.GetInt64(reader.GetOrdinal("generalstatsid"))
            };
        }
    }
}

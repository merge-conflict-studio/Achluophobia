﻿using Harmony;
using JetBrains.Annotations;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Data/AssaultStatsRepository")]
    public sealed class AssaultStatsRepository : GameScript
    {
        private Repository repository;

        private void InjectAssaultStatsRepository([ApplicationScope] SqLiteConnectionFactory connectionFactory,
                                                  [ApplicationScope] SqLiteParameterFactory parameterFactory)
        {
            repository = new Repository(connectionFactory, parameterFactory, new AssaultStatsMapper());
        }

        private void Awake()
        {
            InjectDependencies("InjectAssaultStatsRepository");
        }

        public void ChangeAmmoBoxDropped(AssaultStats assaultStats)
        {
            repository.UpdateAmmoBoxDropped(assaultStats);
        }

        public AssaultStats GetAssaultStats()
        {
            return repository.SelectAssaultStats();
        }

        private class Repository : DbRepository<AssaultStats>
        {
            public Repository([NotNull] SqLiteConnectionFactory connectionFactory,
                              [NotNull] SqLiteParameterFactory parameterFactory,
                              [NotNull] SqLiteDataMapper<AssaultStats> dataMapper)
                              : base (connectionFactory, parameterFactory, dataMapper)
            {
            }

            public void UpdateAmmoBoxDropped(AssaultStats assaultStats)
            {
                ExecuteUpdate("UPDATE AssaultStats SET ammoboxdropped = ? WHERE idassaultstats = ? ;", new object[]
                {
                    assaultStats.AmmoBoxDropped,
                    assaultStats.Id
                });         
            }

            public AssaultStats SelectAssaultStats()
            {
                return ExecuteSelectOne("SELECT * FROM AssaultStats ;", new object[] { });
            }
        }
    }
}

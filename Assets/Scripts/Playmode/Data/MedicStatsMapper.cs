﻿using System.Data.Common;

namespace Achluophobia
{
    public class MedicStatsMapper : SqLiteDataMapper<MedicStats>
    {
        public override MedicStats GetObjectFromReader(DbDataReader reader)
        {
            return new MedicStats
            {
                Id = reader.GetInt64(reader.GetOrdinal("idmedicstats")),
                AmmountOfHealthGiven = reader.GetInt64(reader.GetOrdinal("ammountofhealthgiven")),
                GeneralStatsId = reader.GetInt64(reader.GetOrdinal("generalstatsid"))
            };
        }
    }
}

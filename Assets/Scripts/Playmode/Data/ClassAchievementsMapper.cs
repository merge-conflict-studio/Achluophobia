﻿using System.Data.Common;

namespace Achluophobia
{
    public class ClassAchievementsMapper : SqLiteDataMapper<ClassAchievements>
    {
        public override ClassAchievements GetObjectFromReader(DbDataReader reader)
        {
            return new ClassAchievements
            {
                Id = reader.GetInt64(reader.GetOrdinal("id")),
                Succeeded = reader.GetBoolean(reader.GetOrdinal("succeeded")),
                Description = reader.GetString(reader.GetOrdinal("description")),
                Name = reader.GetString(reader.GetOrdinal("name")),
                GeneralStatsId = reader.GetInt64(reader.GetOrdinal("generalstatsid"))
            };
        }
    }
}
﻿using UnityEngine;

namespace Achluophobia
{
    [CreateAssetMenu(fileName = "ClassInfo", menuName = "Game/Data/ClassInfo")]
    public class ClassInfo : ScriptableObject
    {   
        [SerializeField, Tooltip("The sprite representing the class")]
        private Sprite sprite;
        
        [SerializeField, Tooltip("The color associated with the class")]
        private Color color;

        public Sprite Sprite
        {
            get { return sprite; }
        }

        public Color Color
        {
            get { return color; }
        }
    }
}



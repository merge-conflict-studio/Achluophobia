﻿using System.Data.Common;

namespace Achluophobia
{
    public class GameAchievementsMapper : SqLiteDataMapper<GameAchievements>
    {
        public override GameAchievements GetObjectFromReader(DbDataReader reader)
        {
            return new GameAchievements
            {
                Id = reader.GetInt64(reader.GetOrdinal("id")),
                Succeeded = reader.GetBoolean(reader.GetOrdinal("succeeded")),
                Description = reader.GetString(reader.GetOrdinal("description")),
                Name = reader.GetString(reader.GetOrdinal("name")),
                GameStatsId = reader.GetInt64(reader.GetOrdinal("gamestatsid"))
            };
        }
    }
}
﻿using System.Collections.Generic;
using Harmony;
using JetBrains.Annotations;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Data/GameStatsRepository")]
    public class GameStatsRepository : GameScript
    {
        private Repository repository;

        private void InjectGameStatsRepository([ApplicationScope] SqLiteConnectionFactory connectionFactory,
                                               [ApplicationScope] SqLiteParameterFactory parameterFactory)
        {
            repository = new Repository(connectionFactory, parameterFactory, new GameStatsMapper());
        }

        private void Awake()
        {
            InjectDependencies("InjectGameStatsRepository");
        }

        public virtual void ChangeAllGameStats(GameStats gameStats)
        {
            repository.UpdateAllGameStats(gameStats);
        }

        public virtual GameStats GetStatsFromGame()
        {
            return repository.SelectStatsFromGame();
        }

        private class Repository : DbRepository<GameStats>
        {
            public Repository([NotNull] SqLiteConnectionFactory connectionFactory,
                              [NotNull] SqLiteParameterFactory parameterFactory,
                              [NotNull] SqLiteDataMapper<GameStats> dataMapper)
                              : base(connectionFactory, parameterFactory, dataMapper)
            {
            }

            public void UpdateAllGameStats(GameStats gameStats)
            {
                ExecuteUpdate("UPDATE GameStats SET mapsize = ? , " +
                              "versusgamesfinished = ? , " +
                              "sologamesfinished = ? , " +
                              "coopgamesfinished = ? , " +
                              "gameovers = ? " +
                              "WHERE id = ? ;", new object[]
                {
                    gameStats.MapSize,
                    gameStats.VersusGamesFinished,
                    gameStats.SoloGamesFinished,
                    gameStats.CoopGamesFinished,
                    gameStats.GameOvers,
                    gameStats.Id
                });
            }

            public GameStats SelectStatsFromGame()
            {
                return ExecuteSelectOne("SELECT * FROM GameStats ;", new object[] { });
            }
        }
    }
}
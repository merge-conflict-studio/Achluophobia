﻿using System.Data.Common;

namespace Achluophobia
{
    public class ScoutStatsMapper : SqLiteDataMapper<ScoutStats>
    {
        public override ScoutStats GetObjectFromReader(DbDataReader reader)
        {
            return new ScoutStats
            {
                Id = reader.GetInt64(reader.GetOrdinal("idscoutstats")),
                NumberOfGlowSticksDropped = reader.GetInt64(reader.GetOrdinal("numberofglowsticksdropped")),
                GeneralStatsId = reader.GetInt64(reader.GetOrdinal("generalstatsid"))
            };
        }
    }
}

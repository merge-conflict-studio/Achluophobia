﻿using UnityEngine;

namespace Achluophobia
{
    public class MiniGameData : GameScript
    {
        [SerializeField, Tooltip("Number of minigames to finish in the level")]
        private int miniGamesToFinish;

        [SerializeField, Tooltip("Name of the level")]
        private string levelName;

        public int MiniGamesCompleted;
        public int MiniGamesToFinish { get; private set; }
        public string LevelName;

        private void Awake()
        {
            MiniGamesToFinish = miniGamesToFinish;
            MiniGamesCompleted = 0;
            LevelName = levelName;
        }

        public bool CheckAllMiniGameCompleted()
        {
            return miniGamesToFinish == MiniGamesCompleted;
        }
    }
}

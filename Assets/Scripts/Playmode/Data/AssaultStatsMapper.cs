﻿using System.Data.Common;

namespace Achluophobia
{
    public class AssaultStatsMapper : SqLiteDataMapper<AssaultStats>
    {
        public override AssaultStats GetObjectFromReader(DbDataReader reader)
        {
            return new AssaultStats
            {
                Id = reader.GetInt64(reader.GetOrdinal("idassaultstats")),
                AmmoBoxDropped = reader.GetInt64(reader.GetOrdinal("ammoboxdropped")),
                GeneralStatsId = reader.GetInt64(reader.GetOrdinal("generalstatsid"))
            };
        }
    }
}

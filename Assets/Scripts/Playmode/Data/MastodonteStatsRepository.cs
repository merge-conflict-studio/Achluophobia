﻿using Harmony;
using JetBrains.Annotations;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Data/MastodonteStatsRepository")]
    public sealed class MastodonteStatsRepository : GameScript
    {
        private Repository repository;

        private void InjectMastodonteStatsRepository([ApplicationScope] SqLiteConnectionFactory connectionFactory,
                                                     [ApplicationScope] SqLiteParameterFactory parameterFactory)
        {
            repository = new Repository(connectionFactory, parameterFactory, new MastodonteStatsMapper());
        }

        private void Awake()
        {
            InjectDependencies("InjectMastodonteStatsRepository");
        }

        public void ChangeZombieKilledWithGrenade(MastodonteStats mastodonteStats)
        {
            repository.UpdateZombieKilledWithGrenade(mastodonteStats);
        }

        public MastodonteStats GetAllMastodonteStats()
        {
            return repository.SelectMastodonteStats();
        }

        private class Repository : DbRepository<MastodonteStats>
        {
            public Repository([NotNull] SqLiteConnectionFactory connectionFactory,
                              [NotNull] SqLiteParameterFactory parameterFactory,
                              [NotNull] SqLiteDataMapper<MastodonteStats> dataMapper)
                              : base (connectionFactory, parameterFactory, dataMapper)
            {
            }

            public void UpdateZombieKilledWithGrenade(MastodonteStats mastodonteStats)
            {
                ExecuteUpdate("UPDATE MastodonteStats SET zombiekilledwithgrenade = ? WHERE idmastodontestats = ? ;", new object[]
                {
                    mastodonteStats.ZombieKilledWithGrenade,
                    mastodonteStats.Id
                });
            }

            public MastodonteStats SelectMastodonteStats()
            {
                return ExecuteSelectOne("SELECT * FROM MastodonteStats ;", new object[] { });
            }
        }
    }
}

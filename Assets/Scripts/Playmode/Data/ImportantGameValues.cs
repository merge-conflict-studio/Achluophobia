﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public class ImportantGameValues : Script
    {
        public GameMode GameMode { get; set; }
        
        public EndingType EndingType { get; set; }

        public PlayerClass[] InitialPlayerClasses { get; set; }

        public GameObject[] Players { get; set; }

        public MapSize MapSize { get; set; }

        public int NumberOfDeadPlayers { get; set; }

        public int NumberOfDeadPlayersTeam1 { get; set; }

        public int NumberOfDeadPlayersTeam2 { get; set; }

        public int CurrentLevelTeam1 { get; set; }

        public int CurrentLevelTeam2 { get; set; }

        public bool IsPlayerQuittingAGame { get; set; }

        public int NumberOfPlayers
        {
            get { return (int)GameMode; }
        }
        
        public int WinningTeam { get; set; }
        
        public int SpecialItemsToPickup { get; set; }
    }
}
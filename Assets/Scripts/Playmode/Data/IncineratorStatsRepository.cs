﻿using Harmony;
using JetBrains.Annotations;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Data/IncineratorStatsRepository")]
    public sealed class IncineratorStatsRepository : GameScript
    {
        private Repository repository;
        
        private void InjectIncineratorStatsRepository([ApplicationScope] SqLiteConnectionFactory connectionFactory,
                                                      [ApplicationScope] SqLiteParameterFactory parameterFactory)
        {
            repository = new Repository(connectionFactory, parameterFactory, new IncineratorStatsMapper());
        }

        private void Awake()
        {
            InjectDependencies("InjectIncineratorStatsRepository");
        }

        public void ChangeNumberOfBurnerDropped(IncineratorStats incineratorStats)
        {
            repository.UpdateBurnerDropped(incineratorStats);
        }

        public IncineratorStats GetAllIncineratorStats()
        {
            return repository.SelectIncineratorStats();
        }

        private class Repository : DbRepository<IncineratorStats>
        {
            public Repository([NotNull] SqLiteConnectionFactory connectionFactory,
                              [NotNull] SqLiteParameterFactory parameterFactory,
                              [NotNull] SqLiteDataMapper<IncineratorStats> dataMapper)
                              : base (connectionFactory, parameterFactory, dataMapper)
            {
            }

            public void UpdateBurnerDropped(IncineratorStats incineratorStats)
            {
                ExecuteUpdate("UPDATE IncineratorStats SET numberofburnerdropped = ? WHERE idincineratorstats = ? ;", new object[]
                    {
                        incineratorStats.NumberOfBurnerDropped,
                        incineratorStats.Id
                    });
            }

            public IncineratorStats SelectIncineratorStats()
            {
                return ExecuteSelectOne("SELECT * FROM IncineratorStats ;", new object[] { });
            }
        }
    }
}

﻿namespace Achluophobia
{
    public class GameStats
    {
        public long Id { get; set; }
        public int MapSize { get; set; }
        public long VersusGamesFinished { get; set; }
        public long SoloGamesFinished { get; set; }
        public long CoopGamesFinished { get; set; }
        public long GameOvers { get; set; }
    }
}
﻿using System.Data.Common;
using Harmony;

namespace Achluophobia
{
    public abstract class SqLiteDataMapper<T> : IDbDataMapper<T> where T : class
    {
        public long GetPrimaryKeyFromConnection(DbConnection connection)
        {
            DbCommand command = connection.CreateCommand();
            command.CommandText = "SELECT last_insert_rowid()";
            long result = (long)command.ExecuteScalar();
            return result;
        }

        public abstract T GetObjectFromReader(DbDataReader reader);
    }
}
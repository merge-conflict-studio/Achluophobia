﻿using System.Data.Common;

namespace Achluophobia
{
    public class GameStatsMapper : SqLiteDataMapper<GameStats>
    {
        public override GameStats GetObjectFromReader(DbDataReader reader)
        {
            return new GameStats
            {
                Id = reader.GetInt64(reader.GetOrdinal("id")),
                MapSize = reader.GetInt32(reader.GetOrdinal("mapsize")),
                VersusGamesFinished = reader.GetInt64(reader.GetOrdinal("versusgamesfinished")),
                SoloGamesFinished = reader.GetInt64(reader.GetOrdinal("sologamesfinished")),
                CoopGamesFinished = reader.GetInt64(reader.GetOrdinal("coopgamesfinished")),
                GameOvers = reader.GetInt64(reader.GetOrdinal("gameovers"))
            };
        }
    }
}
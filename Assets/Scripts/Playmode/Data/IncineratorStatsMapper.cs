﻿using System.Data.Common;

namespace Achluophobia
{
    public class IncineratorStatsMapper : SqLiteDataMapper<IncineratorStats>
    {
        public override IncineratorStats GetObjectFromReader(DbDataReader reader)
        {
            return new IncineratorStats
            {
                Id = reader.GetInt64(reader.GetOrdinal("idincineratorstats")),
                NumberOfBurnerDropped = reader.GetInt64(reader.GetOrdinal("numberofburnerdropped")),
                GeneralStatsId = reader.GetInt64(reader.GetOrdinal("generalstatsid"))
            };
        }
    }
}

﻿using System.Data.Common;

namespace Achluophobia
{
    public class GeneralStatsMapper : SqLiteDataMapper<GeneralStats>
    {
        public override GeneralStats GetObjectFromReader(DbDataReader reader)
        {
            return new GeneralStats
            {
                Id = reader.GetInt64(reader.GetOrdinal("id")),
                NumberOfDeaths = reader.GetInt32(reader.GetOrdinal("numberofdeaths")),
                TimePlayed = reader.GetInt32(reader.GetOrdinal("timeplayed")),
                BaseZombieKilled = reader.GetInt32(reader.GetOrdinal("basezombie_killed")),
                RunnerZombieKilled = reader.GetInt32(reader.GetOrdinal("runnerzombie_killed")),
                BoomerZombieKilled = reader.GetInt32(reader.GetOrdinal("boomerzombie_killed")),
                HunterZombieKilled = reader.GetInt32(reader.GetOrdinal("hunterzombie_killed")),
                WitchZombieKilled = reader.GetInt32(reader.GetOrdinal("witchzombie_killed"))
            };
        }
    }
}
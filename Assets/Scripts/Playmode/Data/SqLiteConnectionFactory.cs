﻿using System;
using System.Data.Common;
using System.Data.SQLite;
using System.IO;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Data/SqLiteConnectionFactory")]
    public class SqLiteConnectionFactory : GameScript, IDbConnectionFactory
    {
        private const string SqliteConnectionTemplate = "URI=file:{0}";

        [SerializeField, Tooltip("The name of the database")]
        private string databaseFileName = "AchluophobiaDB.db";
        private DbTransaction currentTransaction;
        //private DbConnection connection;

        private string connexionString;

        public void Awake()
        {
            CreateDatabaseIfDoesntExits();

            connexionString = GetConnexionString();
            //connection = GetConnection();
            //connection = new SQLiteConnection(connexionString);
            //connection.Open();
        }

        private void OnDestroy()
        {
            //connection.Close();
            //connection.Dispose();
        }

        public DbConnection GetConnection()
        {
            //return connection;
            return new SQLiteConnection(connexionString);
        }

        public string GetCurrentDatabaseFilePath()
        {
            return Path.Combine(ApplicationExtensions.PersistentDataPath, databaseFileName);
        }

        public string GetSourceDatabaseFilePath()
        {
            return Path.Combine(ApplicationExtensions.ApplicationDataPath, databaseFileName);
        }

        public void CreateDatabaseIfDoesntExits()
        {
            if (IsCurrentDatabaseDoesntExists())
            {
                File.Copy(GetSourceDatabaseFilePath(), GetCurrentDatabaseFilePath(), true);
            }
        }

        public void ResetDatabase()
        {
            if (IsCurrentDatabaseExists())
            {
                File.Delete(GetCurrentDatabaseFilePath());
            }
        }

        public bool IsCurrentDatabaseExists()
        {
            return File.Exists(GetCurrentDatabaseFilePath());
        }

        public bool IsCurrentDatabaseDoesntExists()
        {
            return !IsCurrentDatabaseExists();
        }

        public bool IsSourceDatabaseExists()
        {
            return File.Exists(GetSourceDatabaseFilePath());
        }

        public bool IsSourceDatabaseDoesntExists()
        {
            return !IsSourceDatabaseExists();
        }

        public void BeginTransaction()
        {
            currentTransaction = GetConnection().BeginTransaction();
        }

        public void RollbackTransaction()
        {
            if (currentTransaction != null)
            {
                currentTransaction.Rollback();
                currentTransaction.Dispose();
            }
            currentTransaction = null;
        }

        public void EndTransaction()
        {
            if (currentTransaction != null)
            {
                currentTransaction.Commit();
                currentTransaction.Dispose();
            }
            currentTransaction = null;
        }

        private string GetConnexionString()
        {
            return String.Format(SqliteConnectionTemplate, GetCurrentDatabaseFilePath());
        }
    }
}
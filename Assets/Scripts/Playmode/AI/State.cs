﻿namespace Achluophobia
{
    public abstract class State
    {
        public abstract void Update();
        public abstract void OnDeath();
    }
}
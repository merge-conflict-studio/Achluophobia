﻿using UnityEngine;

namespace Achluophobia
{
    class AttackState : State
    {
        private readonly GameObject target;
        private readonly ZombieController zombie;

        private float lastTimeAttacked;

        public AttackState(ZombieController zombie, GameObject target)
        {
            this.zombie = zombie;
            this.target = target;

            zombie.IsStopped(true);
            lastTimeAttacked = Time.time;
            zombie.OnSightOnTargetChanged -= OnSightOnTargetChanged;
            zombie.SetDestination(target.transform.position);
        }

        public override void Update()
        {
            if (Vector3.Distance(target.transform.position, zombie.transform.position) > zombie.ZombieClass.Range + 1)
            {
                zombie.State = new ChaseState(zombie, target);
            }
            else if (lastTimeAttacked <= Time.time)
            {
                if (zombie.ZombieClass.name == "Witch")
                {
                    zombie.transform.LookAt(target.transform);
                }
                zombie.Attack();
                lastTimeAttacked = Time.time + zombie.ZombieClass.AttackSpeed;
            }
            else
            {
                zombie.SetDestination(target.transform.position);
            }
        }

        public override void OnDeath()
        {
            zombie.State = new DeadState(zombie);
        }

        private void OnSightOnTargetChanged(GameObject other, bool canSeeTarget)
        {
            if (target != other) return;
            zombie.State = new ChaseState(zombie, other);
        }
    }
}
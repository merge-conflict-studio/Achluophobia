﻿using Harmony;

namespace Achluophobia
{
    public class DeadState : State
    {
        public DeadState(ZombieController zombie)
        {
            zombie.gameObject.GetRoot().Destroy();
        }

        public override void Update()
        {
            // Do nothing
        }

        public override void OnDeath()
        {
            // Do nothing
        }
    }
}
﻿using UnityEngine;

namespace Achluophobia
{
    public class ChaseState : State
    {
        private readonly GameObject target;
        private readonly ZombieController zombie;

        public ChaseState(ZombieController zombie, GameObject target)
        {
            this.zombie = zombie;
            this.target = target;

            zombie.IsStopped(false);
            zombie.OnSightOnTargetChanged += ReactToSightOnTargetChanged;
        }

        public override void Update()
        {
            if (!zombie.TargetIsInRange())
            {
                zombie.State = new PatrolState(zombie);
                zombie.OnSightOnTargetChanged -= ReactToSightOnTargetChanged;
            }
            else
            {
                float distanceToTarget = Vector3.Distance(target.transform.position,
                    zombie.gameObject.transform.position);

                if (distanceToTarget <= zombie.ZombieClass.Range)
                {
                    zombie.State = new AttackState(zombie, target);
                    zombie.OnSightOnTargetChanged -= ReactToSightOnTargetChanged;
                }
                else
                {
                    zombie.SetDestination(target.transform.position);
                }
            }
        }

        public override void OnDeath()
        {
            zombie.OnSightOnTargetChanged -= ReactToSightOnTargetChanged;
            zombie.State = new DeadState(zombie);
        }

        private void ReactToSightOnTargetChanged(GameObject other, bool canSeeTarget)
        {
            if (target != other) return;
            if (canSeeTarget) zombie.SetDestination(other.transform.position);
            else
            {
                Vector3 lastSeenPosition = other.transform.position;
                zombie.SetDestination(new Vector3(lastSeenPosition.x, lastSeenPosition.y, lastSeenPosition.z));
            }
        }
    }
}
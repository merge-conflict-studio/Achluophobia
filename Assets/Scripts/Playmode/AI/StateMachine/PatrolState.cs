﻿using UnityEngine;
using UnityEngine.AI;

namespace Achluophobia
{
    public class PatrolState : State
    {
        private const int NumberOfTries = 30;
        private const float Range = 10.0f;

        private readonly ZombieController zombie;

        private NavMeshHit hit;

        public PatrolState(ZombieController zombie)
        {
            this.zombie = zombie;
            zombie.OnSightOnTargetChanged += OnNewTargetInSight;
        }

        public override void Update()
        {
            if (!zombie.HasReachedDestination()) return;
            Vector3 nextDestination;
            if (RandomPointOnNavMesh(zombie.transform.position, Range, out nextDestination))
            {
                zombie.SetDestination(nextDestination);
            }
        }

        public override void OnDeath()
        {
            zombie.OnSightOnTargetChanged -= OnNewTargetInSight;
            zombie.State = new DeadState(zombie);
        }

        private void OnNewTargetInSight(GameObject target, bool canSeeTarget)
        {
            zombie.OnSightOnTargetChanged -= OnNewTargetInSight;
            zombie.State = new ChaseState(zombie, target);
        }

        /// <summary>
        /// Return a random point on the NavMesh for the zombies to patrol.
        /// SOURCE : https://docs.unity3d.com/ScriptReference/AI.NavMesh.SamplePosition.html
        /// </summary>
        /// <param name="center">The center point of the area to search.</param>
        /// <param name="range">The distance from the center point to calculate.</param>
        /// <param name="result">The result will be stored in this parameter.</param>
        /// <returns>True if it found a point, false otherwise.</returns>
        private bool RandomPointOnNavMesh(Vector3 center, float range, out Vector3 result)
        {
            for (int i = 0; i < NumberOfTries; i++)
            {
                Vector3 randomPoint = center + Random.insideUnitSphere * range;
                if (!NavMesh.SamplePosition(randomPoint, out hit, range, NavMesh.AllAreas)) continue;
                result = hit.position;
                return true;
            }
            result = Vector3.zero;
            return false;
        }
    }
}
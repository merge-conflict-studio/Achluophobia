﻿using System;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public delegate void QuestItemStimulusEventHandler(GameObject questItem);

    [AddComponentMenu("Game/Stimulus/QuestItemStimulus")]
    public class QuestItemStimulus : GameScript
    {
        private new Collider collider;

        public event QuestItemStimulusEventHandler OnQuestItemCollected;

        private void InjectQuestItemStimulus([GameObjectScope] Collider collider)
        {
            this.collider = collider;
        }

        private void Awake()
        {
            InjectDependencies("InjectQuestItemStimulus");

            int layer = LayerMask.NameToLayer(R.S.Layer.QuestItemSensor);
            if (layer == -1)
            {
                throw new Exception("In order to use a ItemStimulus, you must have a " + R.S.Layer.QuestItemSensor +
                                    " layer.");
            }
            gameObject.layer = layer;
            collider.isTrigger = true;
        }

        private void OnEnable()
        {
            collider.Events().OnEnterTrigger += OnEnterTrigger;
        }

        private void OnDisable()
        {
            collider.Events().OnEnterTrigger -= OnEnterTrigger;
        }

        private void OnEnterTrigger(Collider other)
        {
            QuestItemSensor questItemSensor = other.GetComponentInChildren<QuestItemSensor>();
            if (questItemSensor == null) return;
            questItemSensor.CollectQuestItem();

            if (OnQuestItemCollected != null) OnQuestItemCollected(gameObject);
        }
    }
}
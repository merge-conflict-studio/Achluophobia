﻿using System;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public delegate void HitSensorDetectedEventHandler();

    [AddComponentMenu("Game/Stimulus/ActiveHitStimulus")]
    public class ActiveHitStimulus : GameScript
    {
        private new Collider collider;
        private static HitSensor hitSensor;

        public event HitSensorDetectedEventHandler OnHitSensorDetected;

        private void InjectActiveHitStimulus([GameObjectScope] Collider collider)
        {
            this.collider = collider;
        }

        private void Awake()
        {
            InjectDependencies("InjectActiveHitStimulus");

            int layer = LayerMask.NameToLayer(R.S.Layer.HitSensor);
            if (layer == -1)
            {
                throw new Exception("In order to use a ActiveHitStimulus, you must have a " + R.S.Layer.HitSensor +
                                    " layer.");
            }
            gameObject.layer = layer;
            collider.isTrigger = true;
        }

        private void OnEnable()
        {
            collider.Events().OnEnterTrigger += OnEnterTrigger;
            collider.Events().OnExitTrigger += OnExitTrigger;
        }

        private void OnDisable()
        {
            collider.Events().OnEnterTrigger -= OnEnterTrigger;
            collider.Events().OnExitTrigger -= OnExitTrigger;
            hitSensor = null;
        }

        public void Hit(GameObject attacker, int hitPoints)
        {
            if (hitSensor != null) hitSensor.Hit(attacker, hitPoints, false);
        }

        private void OnEnterTrigger(Collider other)
        {
            if (!other.CompareTag(R.S.Tag.Player)) return;
            if (hitSensor == null) hitSensor = other.gameObject.GetComponentInChildren<HitSensor>();
            if (OnHitSensorDetected != null) OnHitSensorDetected();
        }

        private void OnExitTrigger(Collider other)
        {
            if (!other.CompareTag(R.S.Tag.Player)) return;
            if (other.gameObject.GetComponentInChildren<HitSensor>() == hitSensor) hitSensor = null;
        }
    }
}
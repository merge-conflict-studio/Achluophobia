﻿using System;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public delegate void ItemStimulusEventHandler(InstantEffect effect);

    [AddComponentMenu("Game/Stimulus/ItemStimulus")]
    public class ItemStimulus : GameScript
    {
        [SerializeField, Tooltip("The effect this item will apply")] 
        private InstantEffect effect;

        private new Collider collider;

        public event ItemStimulusEventHandler OnItemCollected;

        private void InjectItemStimulus([GameObjectScope] Collider collider)
        {
            this.collider = collider;
        }

        private void Awake()
        {
            InjectDependencies("InjectItemStimulus");

            int layer = LayerMask.NameToLayer(R.S.Layer.ItemSensor);
            if (layer == -1)
            {
                throw new Exception("In order to use a ItemStimulus, you must have a " + R.S.Layer.ItemSensor +
                                    " layer.");
            }
            gameObject.layer = layer;
            collider.isTrigger = true;
        }

        private void OnEnable()
        {
            collider.Events().OnEnterTrigger += OnEnterTrigger;
        }

        private void OnDisable()
        {
            collider.Events().OnEnterTrigger -= OnEnterTrigger;
        }

        private void OnEnterTrigger(Collider other)
        {
            ItemSensor itemSensor = other.GetComponentInChildren<ItemSensor>();
            if (itemSensor != null)
            {
                CheckIfItemIsNeeded(other.gameObject, effect, itemSensor);
            }
        }

        private void CheckIfItemIsNeeded(GameObject player, InstantEffect effect, ItemSensor itemSensor)
        {
            if ((!(effect is Resupply) || player.GetComponentInChildren<Ammo>().GetAmmosLeftOnPlayer() ==
                 player.GetComponentInChildren<Ammo>().GetMaxAmmo()) &&
                (!(effect is Heal) || player.GetComponent<Health>().GetHealth() ==
                 player.GetComponent<Health>().GetMaxHealth())) return;
            
            itemSensor.CollectItem(effect);
            if (OnItemCollected != null) OnItemCollected(effect);
        }
    }
}
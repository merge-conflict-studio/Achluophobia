﻿using System;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public delegate void ExplosionSensorDetectedEventHandler(ExplosionSensor explosionSensor);

    [AddComponentMenu("Game/Stimulus/ExplosionStimulus")]
    public class ExplosionStimulus : GameScript
    {
        private new Collider collider;

        public event ExplosionSensorDetectedEventHandler OnExplosionSensorDetected;
        public event ExplosionSensorDetectedEventHandler OnExplosionSensorUndetected;

        private void InjectExplosionStimulus([GameObjectScope] Collider collider)
        {
            this.collider = collider;
        }

        private void Awake()
        {
            InjectDependencies("InjectExplosionStimulus");

            int layer = LayerMask.NameToLayer(R.S.Layer.ExplosionSensor);
            if (layer == -1)
            {
                throw new Exception("In order to use an explosion stimulus, you must have a " +
                                    R.S.Layer.ExplosionSensor + " layer.");
            }
            gameObject.layer = layer;
            collider.isTrigger = true;

        }

        private void OnEnable()
        {
            collider.Events().OnEnterTrigger += OnEnterTrigger;
            collider.Events().OnExitTrigger += OnExitTrigger;
        }

        private void OnDisable()
        {
            collider.Events().OnEnterTrigger -= OnEnterTrigger;
            collider.Events().OnExitTrigger -= OnExitTrigger;
        }

        private void OnEnterTrigger(Collider other)
        {
            ExplosionSensor explosionSensor = other.GetComponent<ExplosionSensor>();
            if (explosionSensor != null)
            {
                if (OnExplosionSensorDetected != null) OnExplosionSensorDetected(explosionSensor);
            }
        }

        private void OnExitTrigger(Collider other)
        {
            ExplosionSensor explosionSensor = other.GetComponent<ExplosionSensor>();
            if (explosionSensor == null) return;
            if (OnExplosionSensorUndetected != null) OnExplosionSensorUndetected(explosionSensor);
        }
    }
}

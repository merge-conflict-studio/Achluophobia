﻿using System;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public delegate void LineOfSightStimulusEventHandler();

    [AddComponentMenu("Game/Stimulus/LineOfSightStimulus")]
    public class LineOfSightStimulus : GameScript
    {
        private new Collider collider;

        private void InjectLineOfSightStimulus([GameObjectScope] Collider collider)
        {
            this.collider = collider;
        }

        private void Awake()
        {
            InjectDependencies("InjectLineOfSightStimulus");

            int layer = LayerMask.NameToLayer(R.S.Layer.LineOfSightSensor);
            if (layer == -1)
            {
                throw new Exception("In order to use a LineOfSightSensor, you must have a " + R.S.Layer.HitSensor +
                                    " layer.");
            }
            gameObject.layer = layer;
            collider.isTrigger = true;
        }

        private void OnEnable()
        {
            collider.Events().OnEnterTrigger += OnEnterTrigger;
            collider.Events().OnExitTrigger += OnExitTrigger;
        }

        private void OnDisable()
        {
            collider.Events().OnEnterTrigger -= OnEnterTrigger;
            collider.Events().OnExitTrigger -= OnExitTrigger;
        }

        private void OnEnterTrigger(Collider other)
        {
            LineOfSightSensor lineOfSightSensor = other.GetComponent<LineOfSightSensor>();
            if (lineOfSightSensor != null) lineOfSightSensor.AddInLineOfSight(collider);
        }

        private void OnExitTrigger(Collider other)
        {
            LineOfSightSensor lineOfSightSensor = other.GetComponent<LineOfSightSensor>();
            if (lineOfSightSensor != null) lineOfSightSensor.RemoveFromLineOfSight(collider);
        }
    }
}
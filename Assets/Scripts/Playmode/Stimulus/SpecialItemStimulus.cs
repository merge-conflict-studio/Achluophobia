﻿using System;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public delegate void SpecialItemEventHandler();
    public delegate void IncineratorDetectionEventHandler(SpecialItemSensor specialItemSensor);

    [AddComponentMenu("Game/Stimulus/SpecialItemStimulus")]
    public class SpecialItemStimulus : GameScript
    {
        private new Collider collider;
        private bool isFirstSensorSeen;

        [SerializeField, Tooltip("The effect of the special item")]
        private InstantEffect specialItemEffect;

        public event SpecialItemEventHandler OnSpecialItemPicked;
        public event IncineratorDetectionEventHandler OnIncineratorDetected;

        private void InjectSpecialItemStimulus([GameObjectScope] Collider collider)
        {
            this.collider = collider;
        }

        private void Awake()
        {
            InjectDependencies("InjectSpecialItemStimulus");

            int layer = LayerMask.NameToLayer(R.S.Layer.ItemSensor);
            if (layer == -1)
            {
                throw new Exception("In order to use a special item stimulus, you must have a " +
                                    R.S.Layer.ItemSensor + " layer.");
            }
            gameObject.layer = layer;
            collider.isTrigger = true;
            isFirstSensorSeen = true;
        }

        private void OnEnable()
        {
            collider.Events().OnEnterTrigger += OnEnterTrigger;
            collider.Events().OnExitTrigger += OnExitTrigger;
        }

        private void OnDisable()
        {
            collider.Events().OnEnterTrigger -= OnEnterTrigger;
            collider.Events().OnExitTrigger -= OnExitTrigger;
        }

        public void OnItemCollected()
        {
            if (OnSpecialItemPicked != null) OnSpecialItemPicked();
        }

        public InstantEffect GetEffect()
        {
            return specialItemEffect;
        }

        private void OnEnterTrigger(Collider other)
        {
            SpecialItemSensor specialItemSensor =
                other.GetComponentInChildren<SpecialItemSensor>();
            if (isFirstSensorSeen)
            {
                OnIncineratorPlayerDetected(specialItemSensor);
                isFirstSensorSeen = false;
            }

            if (specialItemSensor != null)
            {
                specialItemSensor.SpecialItemDetected(this);
            }
        }

        private void OnExitTrigger(Collider other)
        {
            SpecialItemSensor specialItemSensor =
                other.GetComponentInChildren<SpecialItemSensor>();

            if (specialItemSensor != null)
            {
                specialItemSensor.SpecialItemUndetected(this);
            }
        }

        private void OnIncineratorPlayerDetected(SpecialItemSensor incinerator)
        {
            if (OnIncineratorDetected != null) OnIncineratorDetected(incinerator);
        }
    }
}
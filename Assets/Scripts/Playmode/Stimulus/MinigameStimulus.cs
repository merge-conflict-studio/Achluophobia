﻿using System;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public delegate void MinigameStimulusEventHandler(GameObject minigame);

    [AddComponentMenu("Game/Stimulus/MinigameStimulus")]
    public class MinigameStimulus : GameScript
    {
        private new Collider collider;
        private MinigameSensor minigameSensor;
        private GamePadSensor playerGamePadSensor;
        private PlayerController playerController;

        private void InjectMinigameStimulus([GameObjectScope] Collider collider,
                                            [EntityScope] PlayerController playerController,
                                            [ApplicationScope] GamePadsController gamePadsController)
        {
            this.collider = collider;
            this.playerController = playerController;
            playerGamePadSensor = gamePadsController.GetSensor(playerController.PlayerNumber);
        }

        private void Awake()
        {
            InjectDependencies("InjectMinigameStimulus");

            int layer = LayerMask.NameToLayer(R.S.Layer.MiniGameSensor);
            if (layer == -1)
            {
                throw new Exception("In order to use a MinigameStimulus, you must have a " +
                                    R.S.Layer.MiniGameSensor + " layer.");
            }
            gameObject.layer = layer;
            collider.isTrigger = true;
        }

        private void OnEnable()
        {
            collider.Events().OnEnterTrigger += OnEnterTrigger;
            collider.Events().OnExitTrigger += OnExitTrigger;
            playerGamePadSensor.OnAction += OnActionButtonPressed;
        }

        private void OnDisable()
        {
            collider.Events().OnEnterTrigger -= OnEnterTrigger;
            collider.Events().OnExitTrigger -= OnExitTrigger;
            playerGamePadSensor.OnAction -= OnActionButtonPressed;
        }

        private void OnEnterTrigger(Collider other)
        {
            if (other.CompareTag(R.S.Tag.Player) ||
                other.gameObject.layer != LayerMask.NameToLayer(R.S.Layer.MiniGameSensor)) return;

            if (minigameSensor != null)
            {
                minigameSensor.OnMinigameStart -= OnMinigameStart;
                minigameSensor.OnMinigameEnd -= OnMinigameEnd;
                minigameSensor = null;
            }

            minigameSensor = other.GetComponent<MinigameSensor>();

            if (minigameSensor == null) return;
            minigameSensor.OnMinigameStart += OnMinigameStart;
            minigameSensor.OnMinigameEnd += OnMinigameEnd;
        }

        private void OnExitTrigger(Collider other)
        {
            if (other.CompareTag(R.S.Tag.Player) ||
                other.gameObject.layer != LayerMask.NameToLayer(R.S.Layer.MiniGameSensor)) return;

            if (minigameSensor == null) return;
            minigameSensor.OnMinigameStart -= OnMinigameStart;
            minigameSensor.OnMinigameEnd -= OnMinigameEnd;
            minigameSensor = null;
        }

        private void OnActionButtonPressed()
        {
            if (minigameSensor != null) minigameSensor.StartMiniGame(playerController.PlayerNumber);
        }

        private void OnMinigameStart()
        {
            playerController.UnsubscribeToEventsEndGameMinigame(false);
        }

        private void OnMinigameEnd(bool isCompleted)
        {
            playerController.SubscribeToEvents(false);
        }
    }
}
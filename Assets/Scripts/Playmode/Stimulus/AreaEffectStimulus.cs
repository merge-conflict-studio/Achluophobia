﻿using System;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Stimulus/AreaEffectStimulus")]
    public class AreaEffectStimulus : GameScript
    {
        [SerializeField, Tooltip("The effect of the area effect stimulus")]
        private ContinuousEffect effect;

        private new Collider collider;
        private AreaEffectSensor areaEffectSensor;

        private void InjectAreaEffectStimulus([GameObjectScope] Collider collider)
        {
            this.collider = collider;
        }

        private void Awake()
        {
            InjectDependencies("InjectAreaEffectStimulus");

            int layer = LayerMask.NameToLayer(R.S.Layer.ItemSensor);
            if (layer == -1)
            {
                throw new Exception("In order to use an area effect stimulus, you must have a " +
                                    R.S.Layer.ItemSensor + " layer.");
            }
            gameObject.layer = layer;
            collider.isTrigger = true;
        }

        private void OnEnable()
        {
            collider.Events().OnEnterTrigger += OnEnterTrigger;
            collider.Events().OnExitTrigger += OnExitTrigger;
        }

        private void OnDestroy()
        {
            if (areaEffectSensor != null) areaEffectSensor.AreaEffectUndetected(effect);
            collider.Events().OnEnterTrigger -= OnEnterTrigger;
            collider.Events().OnExitTrigger -= OnExitTrigger;
        }

        private void OnEnterTrigger(Collider other)
        {
            areaEffectSensor = other.GetComponentInChildren<AreaEffectSensor>();

            if (areaEffectSensor != null)
            {
                areaEffectSensor.AreaEffectDetected(effect);
            }
        }

        private void OnExitTrigger(Collider other)
        {
            areaEffectSensor = other.GetComponentInChildren<AreaEffectSensor>();

            if (areaEffectSensor != null)
            {
                areaEffectSensor.AreaEffectUndetected(effect);
            }
            areaEffectSensor = null;
        }
    }
}
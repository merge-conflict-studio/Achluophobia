﻿using System;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public delegate void PlayerTeleporterStimulusEventHandler(GameObject playerTeleporter, GameObject player);

    [AddComponentMenu("Game/Stimulus/PlayerTeleporterStimulus")]
    public class PlayerTeleporterStimulus : GameScript
    {
        private new Collider collider;
        private CheckForLevelCompletion checkForLevelCompletion;
        private MiniGameData miniGameData;

        public event PlayerTeleporterStimulusEventHandler OnPlayerTeleport;

        private void InjectPlayerTeleporterStimulus([GameObjectScope] Collider collider,
                                                    [GameObjectScope] CheckForLevelCompletion checkForLevelCompletion,
                                                    [EntityScope] MiniGameData miniGameData)
        {
            this.collider = collider;
            this.checkForLevelCompletion = checkForLevelCompletion;
            this.miniGameData = miniGameData;
        }

        private void Awake()
        {
            InjectDependencies("InjectPlayerTeleporterStimulus");

            int layer = LayerMask.NameToLayer(R.S.Layer.PlayerTeleporterSensor);
            if (layer == -1)
            {
                throw new Exception("In order to use a PlayerTeleporterStimulus, you must have a " +
                                    R.S.Layer.PlayerTeleporterSensor + " layer.");
            }
            gameObject.layer = layer;
            collider.isTrigger = true;
        }

        private void OnEnable()
        {
            collider.Events().OnEnterTrigger += OnEnterTrigger;
        }

        private void OnDisable()
        {
            collider.Events().OnEnterTrigger -= OnEnterTrigger;
        }

        private void OnEnterTrigger(Collider other)
        {
            PlayerTeleporterSensor playerTeleporterSensor = other.GetComponentInChildren<PlayerTeleporterSensor>();
            if (playerTeleporterSensor == null) return;
            if(checkForLevelCompletion.IsLevelCompleted(miniGameData,other.gameObject.GetRoot()))
            {
                playerTeleporterSensor.PlayerTeleport();

                if (OnPlayerTeleport != null) OnPlayerTeleport(this.gameObject, other.gameObject);
            }
        }
    }
}
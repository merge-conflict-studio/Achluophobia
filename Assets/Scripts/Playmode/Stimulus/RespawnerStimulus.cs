﻿using System;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Stimulus/RespawnerStimulus")]
    public class RespawnerStimulus : GameScript
    {
        private new Collider collider;
        private RespawnerData respawnerData;
        private ImportantGameValues importantGameValues;

        private void InjectRespawnerStimulus([GameObjectScope] Collider collider,
            [ApplicationScope] RespawnerData respawnerData,
            [ApplicationScope] ImportantGameValues importantGameValues)
        {
            this.collider = collider;
            this.respawnerData = respawnerData;
            this.importantGameValues = importantGameValues;
        }

        private void Awake()
        {
            InjectDependencies("InjectRespawnerStimulus");

            int layer = LayerMask.NameToLayer(R.S.Layer.RespawnerSensor);
            if (layer == -1)
            {
                throw new Exception("In order to use a RespawnerStimulus, you must have a " +
                                    R.S.Layer.RespawnerSensor + " layer.");
            }
            gameObject.layer = layer;
            collider.isTrigger = true;
        }

        private void OnEnable()
        {
            collider.Events().OnEnterTrigger += OnEnterTrigger;
        }

        private void OnDisable()
        {
            collider.Events().OnEnterTrigger -= OnEnterTrigger;
        }

        private void OnEnterTrigger(Collider other)
        {
            RespawnerSensor respawnerSensor = other.GetComponentInChildren<RespawnerSensor>();
            if (respawnerSensor == null) return;
            if (importantGameValues.GameMode != GameMode.Versus)
            {
                if (importantGameValues.NumberOfDeadPlayers == 0) return;
                if (respawnerData.PlayerToRespawnTeam1 != null && respawnerData.RespawnPosition1 != Vector3.zero)
                {
                    respawnerSensor.RespawnPlayer(respawnerData.PlayerToRespawnTeam1, respawnerData.RespawnPosition1,
                        true);
                    respawnerData.RemovePlayerAndPosition(Team.One);
                    if (importantGameValues.NumberOfDeadPlayers != 0) importantGameValues.NumberOfDeadPlayers--;
                }
            }
            else
            {
                switch (other.gameObject.GetComponent<PlayerController>().PlayerTeam)
                {
                    case Team.One:
                        if (importantGameValues.NumberOfDeadPlayersTeam1 == 0) return;
                        if (respawnerData.PlayerToRespawnTeam1 != null &&
                            respawnerData.RespawnPosition1 != Vector3.zero)
                        {
                            respawnerSensor.RespawnPlayer(respawnerData.PlayerToRespawnTeam1,
                                respawnerData.RespawnPosition1, true);
                            respawnerData.RemovePlayerAndPosition(Team.One);
                            if (importantGameValues.NumberOfDeadPlayersTeam1 != 0)
                                importantGameValues.NumberOfDeadPlayersTeam1--;
                        }
                        break;
                    case Team.Two:
                        if (importantGameValues.NumberOfDeadPlayersTeam2 == 0) return;
                        if (respawnerData.PlayerToRespawnTeam2 != null &&
                            respawnerData.RespawnPosition2 != Vector3.zero)
                        {
                            respawnerSensor.RespawnPlayer(respawnerData.PlayerToRespawnTeam2,
                                respawnerData.RespawnPosition2, true);
                            respawnerData.RemovePlayerAndPosition(Team.Two);
                            if (importantGameValues.NumberOfDeadPlayersTeam2 != 0)
                                importantGameValues.NumberOfDeadPlayersTeam2--;
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
    }
}

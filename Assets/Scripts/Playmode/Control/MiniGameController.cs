﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public delegate void QuestItemIncrementEventHandler(int numberOfQuestItemsLeft, GameObject itemToCollect);

    [AddComponentMenu("Game/Control/MiniGameController")]
    public class MiniGameController : GameScript
    {
        private QuestItemsEventChannel questItemsEventChannel;
        private QuestItemsData questItemsData;

        private int numberOfQuestItemsToCollectInLevel;
        private int numberOfQuestItemsCollectedInLevel;

        public event QuestItemIncrementEventHandler OnQuestItemRemovedFromLevel;

        private void InjectMiniGameController([EventChannelScope] QuestItemsEventChannel questItemsEventChannel,
                                              [EntityScope] QuestItemsData questItemsData)
        {
            this.questItemsEventChannel = questItemsEventChannel;
            this.questItemsData = questItemsData;
        }

        private void Awake()
        {
            InjectDependencies("InjectMiniGameController");
        }

        private void OnEnable()
        {
            questItemsEventChannel.OnEventPublished += OnQuestItemPickedUp;
        }

        private void OnDisable()
        {
            questItemsEventChannel.OnEventPublished -= OnQuestItemPickedUp;
        }

        private void IncrementCollectedItemCount(GameObject questItem)
        {
            if (!questItem.CompareTag(questItemsData.LevelName)) return;
            questItemsData.ItemsCollected++;
            numberOfQuestItemsCollectedInLevel = questItemsData.ItemsCollected;
            numberOfQuestItemsToCollectInLevel = questItemsData.ItemsToCollect;

            if (OnQuestItemRemovedFromLevel != null)
                OnQuestItemRemovedFromLevel(numberOfQuestItemsToCollectInLevel - numberOfQuestItemsCollectedInLevel,
                    questItem);
        }

        private void OnQuestItemPickedUp(QuestItemsEvent questItemsEvent)
        {
            IncrementCollectedItemCount(questItemsEvent.QuestItem);
        }
    }
}

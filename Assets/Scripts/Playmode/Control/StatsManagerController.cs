﻿using Harmony;
using UnityEngine;
using System;

//Partie fier: Mathieu Carrier
namespace Achluophobia
{

    public delegate void AchievementSuccessfullEventHandler(string achievementName);

    [AddComponentMenu("Game/Controller/StatsManagerController")]
    public class StatsManagerController : GameScript
    {
        private ZombieDeathEventChannel zombieDeathEventChannel;
        private GameStartEventChannel gameStartEventChannel;
        private GameEndEventChannel gameEndEventChannel;
        private PlayerDeathEventChannel playerDeathEventChannel;
        private SpecialItemEventChannel specialItemEventChannel;
        private MiniGameEndEventChannel miniGameEndEventChannel;
        private PlayerTeleporterEventChannel playerTeleporterEventChannel;
        //Stats Repositories
        private AssaultStatsRepository assaultStatsRepository;
        private MedicStatsRepository medicStatsRepository;
        private MastodonteStatsRepository mastodonteStatsRepository;
        private IncineratorStatsRepository incineratorStatsRepository;
        private ScoutStatsRepository scoutStatsRepository;
        private GeneralStatsRepository generalStatsRepository;
        private GameStatsRepository gameStatsRepository;
        //Class specifics stats
        private AssaultStats assaultStats;
        private MedicStats medicStats;
        private MastodonteStats mastodonteStats;
        private IncineratorStats incineratorStats;
        private ScoutStats scoutStats;
        //Temporaries class specifics stats to add 
        private AssaultStats assaultStatsToAdd;
        private MedicStats medicStatsToAdd;
        private MastodonteStats mastodonteStatsToAdd;
        private IncineratorStats incineratorStatsToAdd;
        private ScoutStats scoutStatsToAdd;
        //General stats for every class
        private GeneralStats assaultGeneralStats;
        private GeneralStats medicGeneralStats;
        private GeneralStats mastodonteGeneralStats;
        private GeneralStats incineratorGeneralStats;
        private GeneralStats scoutGeneralStats;
        //Temporaries general stats for every class to add
        private GeneralStats assaultGeneralStatsToAdd;
        private GeneralStats medicGeneralStatsToAdd;
        private GeneralStats mastodonteGeneralStatsToAdd;
        private GeneralStats incineratorGeneralStatsToAdd;
        private GeneralStats scoutGeneralStatsToAdd;
        //Stats of the game in general
        private GameStats gameStats;
        private DateTime initialTime;

        private bool allMiniGamesCompleted;
        private bool noHealthStationDropped;
        private bool noAmmoBoxDropped;

        public event AchievementSuccessfullEventHandler OnClassAchievementSucceeded;
        public event AchievementSuccessfullEventHandler OnGameAchievementSucceeded;

        private void InjectStatsManagerController([EventChannelScope] ZombieDeathEventChannel zombieDeathEventChannel,
                                                  [EventChannelScope] GameStartEventChannel gameStartEventChannel,
                                                  [EventChannelScope] GameEndEventChannel gameEndEventChannel,
                                                  [EventChannelScope] PlayerDeathEventChannel playerDeathEventChannel,
                                                  [EventChannelScope] SpecialItemEventChannel specialItemEventChannel,
                                                  [EventChannelScope] MiniGameEndEventChannel miniGameEndEventChannel,
                                                  [EventChannelScope] PlayerTeleporterEventChannel playerTeleporterEventChannel,
                                                  [EntityScope] AssaultStatsRepository assaultStatsRepository,
                                                  [EntityScope] MedicStatsRepository medicStatsRepository,
                                                  [EntityScope] MastodonteStatsRepository mastodonteStatsRepository,
                                                  [EntityScope] IncineratorStatsRepository incineratorStatsRepository,
                                                  [EntityScope] ScoutStatsRepository scoutStatsRepository,
                                                  [EntityScope] GeneralStatsRepository generalStatsRepository,
                                                  [EntityScope] GameStatsRepository gameStatsRepository)
        {
            this.zombieDeathEventChannel = zombieDeathEventChannel;
            this.gameStartEventChannel = gameStartEventChannel;
            this.gameEndEventChannel = gameEndEventChannel;
            this.miniGameEndEventChannel = miniGameEndEventChannel;
            this.playerDeathEventChannel = playerDeathEventChannel;
            this.specialItemEventChannel = specialItemEventChannel;
            this.playerTeleporterEventChannel = playerTeleporterEventChannel;
            this.assaultStatsRepository = assaultStatsRepository;
            this.medicStatsRepository = medicStatsRepository;
            this.mastodonteStatsRepository = mastodonteStatsRepository;
            this.incineratorStatsRepository = incineratorStatsRepository;
            this.scoutStatsRepository = scoutStatsRepository;
            this.generalStatsRepository = generalStatsRepository;
            this.gameStatsRepository = gameStatsRepository;
        }

        private void Awake()
        {
            InjectDependencies("InjectStatsManagerController");

            assaultStats = assaultStatsRepository.GetAssaultStats();
            assaultGeneralStats = generalStatsRepository.GetStatsFromId(assaultStats.GeneralStatsId);
            medicStats = medicStatsRepository.GetMedicStats();
            medicGeneralStats = generalStatsRepository.GetStatsFromId(medicStats.GeneralStatsId);
            mastodonteStats = mastodonteStatsRepository.GetAllMastodonteStats();
            mastodonteGeneralStats = generalStatsRepository.GetStatsFromId(mastodonteStats.GeneralStatsId);
            incineratorStats = incineratorStatsRepository.GetAllIncineratorStats();
            incineratorGeneralStats = generalStatsRepository.GetStatsFromId(incineratorStats.GeneralStatsId);
            scoutStats = scoutStatsRepository.GetScoutStats();
            scoutGeneralStats = generalStatsRepository.GetStatsFromId(scoutStats.GeneralStatsId);
            gameStats = gameStatsRepository.GetStatsFromGame();
            noAmmoBoxDropped = true;
            noHealthStationDropped = true;
        }

        private void OnEnable()
        {
            gameStartEventChannel.OnEventPublished += OnGameStart;
            zombieDeathEventChannel.OnEventPublished += OnZombieKilled;
            gameEndEventChannel.OnEventPublished += OnGameEnd;
            playerDeathEventChannel.OnEventPublished += OnPlayerDeath;
            specialItemEventChannel.OnEventPublished += OnSpecialItemStatToAdd;
            miniGameEndEventChannel.OnEventPublished += OnMiniGameEnd;
            playerTeleporterEventChannel.OnEventPublished += CheckIfNotAffraidOfTheDarkAchievementSucceeded;
        }

        private void OnDisable()
        {
            gameStartEventChannel.OnEventPublished -= OnGameStart;
            zombieDeathEventChannel.OnEventPublished -= OnZombieKilled;
            gameEndEventChannel.OnEventPublished -= OnGameEnd;
            playerDeathEventChannel.OnEventPublished -= OnPlayerDeath;
            specialItemEventChannel.OnEventPublished -= OnSpecialItemStatToAdd;
            miniGameEndEventChannel.OnEventPublished -= OnMiniGameEnd;
            playerTeleporterEventChannel.OnEventPublished -= CheckIfNotAffraidOfTheDarkAchievementSucceeded;
        }

        private void OnGameStart(GameStartEvent gameStartEvent)
        {
            assaultGeneralStatsToAdd = new GeneralStats();
            medicGeneralStatsToAdd = new GeneralStats();
            mastodonteGeneralStatsToAdd = new GeneralStats();
            incineratorGeneralStatsToAdd = new GeneralStats();
            scoutGeneralStatsToAdd = new GeneralStats();

            assaultStatsToAdd = new AssaultStats();
            medicStatsToAdd = new MedicStats();
            mastodonteStatsToAdd = new MastodonteStats();
            incineratorStatsToAdd = new IncineratorStats();
            scoutStatsToAdd = new ScoutStats();

            initialTime = DateTime.Now;
            allMiniGamesCompleted = true;
        }

        private void OnGameEnd(GameEndEvent gameEndEvent)
        {
            EndingType ending = gameEndEvent.Ending;
            PlayerClass[] players = gameEndEvent.Players;
            DateTime finishTime = DateTime.Now;
            TimeSpan playtime = finishTime - initialTime;

            foreach (PlayerClass player in players)
            {
                IncrementPlayTimeOnClass(player, playtime);
                if (player == PlayerClass.Medic && noHealthStationDropped && ending != EndingType.GameFailed)
                {
                    if (OnClassAchievementSucceeded != null) OnClassAchievementSucceeded(AchievementName.MedicAchievement3);
                }
                if (player == PlayerClass.Assault && noAmmoBoxDropped && ending != EndingType.GameFailed)
                {
                    if (OnClassAchievementSucceeded != null) OnClassAchievementSucceeded(AchievementName.AssaultAchievement3);
                }
            }

            if (ending != EndingType.GameFailed && CheckIfTortoiseAndTheHareAchievementCompleted(players))
            {
                if (OnGameAchievementSucceeded != null) OnGameAchievementSucceeded(AchievementName.GameAchievement4);
            }

            CalculateAllClassStats();
            CalculateGameStats(ending);
            UpdateAllClassStats();
            UpdateGameStats();
        }

        private void OnMiniGameEnd(MiniGameEndEvent miniGameEndEvent)
        {
            if (!miniGameEndEvent.IsCompleted)
            {
                allMiniGamesCompleted = false;
            }
        }

        private void OnSpecialItemStatToAdd(SpecialItemEvent specialItemEvent)
        {
            PlayerClass playerClass = specialItemEvent.PlayerClass;
            int valueToAdd = specialItemEvent.ValueToAdd;
            switch (playerClass)
            {
                case PlayerClass.Assault:
                    assaultStatsToAdd.AmmoBoxDropped += valueToAdd;
                    noAmmoBoxDropped = false;
                    if (assaultStats.AmmoBoxDropped + assaultStatsToAdd.AmmoBoxDropped == 25)
                    {
                        if (OnClassAchievementSucceeded != null)
                            OnClassAchievementSucceeded(AchievementName.AssaultAchievement2);
                    }
                    break;
                case PlayerClass.Medic:
                    medicStatsToAdd.AmmountOfHealthGiven += valueToAdd;
                    noHealthStationDropped = false;
                    if (medicStats.AmmountOfHealthGiven + medicStatsToAdd.AmmountOfHealthGiven >= 1500)
                    {
                        if (OnClassAchievementSucceeded != null)
                            OnClassAchievementSucceeded(AchievementName.MedicAchievement2);
                    }
                    break;
                case PlayerClass.Mastodonte:
                    mastodonteStatsToAdd.ZombieKilledWithGrenade += valueToAdd;
                    if (mastodonteStats.ZombieKilledWithGrenade + mastodonteStatsToAdd.ZombieKilledWithGrenade == 50)
                    {
                        if (OnClassAchievementSucceeded != null)
                            OnClassAchievementSucceeded(AchievementName.MastodonteAchievement2);
                    }
                    break;
                case PlayerClass.Incinerator:
                    incineratorStatsToAdd.NumberOfBurnerDropped += valueToAdd;
                    break;
                case PlayerClass.Scout:
                    scoutStatsToAdd.NumberOfGlowSticksDropped += valueToAdd;
                    if (scoutStats.NumberOfGlowSticksDropped + scoutStatsToAdd.NumberOfGlowSticksDropped == 50)
                    {
                        if (OnClassAchievementSucceeded != null)
                            OnClassAchievementSucceeded(AchievementName.ScoutAchievement2);
                    }
                    break;
            }
        }

        private void OnZombieKilled(ZombieDeathEvent zombieDeathEvent)
        {
            switch (zombieDeathEvent.ZombieType)
            {
                case ZombieType.Basic:
                    IncrementBasicZombieKill(zombieDeathEvent.KillerPlayer);
                    break;
                case ZombieType.Boomer:
                    IncrementBoomerZombieKill(zombieDeathEvent.KillerPlayer);
                    break;
                case ZombieType.Hunter:
                    IncrementHunterZombieKill(zombieDeathEvent.KillerPlayer);
                    break;
                case ZombieType.Runner:
                    IncrementRunnerZombieKill(zombieDeathEvent.KillerPlayer);
                    break;
                case ZombieType.Witch:
                    IncrementWitchZombieKill(zombieDeathEvent.KillerPlayer);
                    break;
                case ZombieType.None:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void OnPlayerDeath(PlayerDeathEvent playerDeathEvent)
        {
            PlayerClass playerClass = playerDeathEvent.DeadPlayer.GetComponent<PlayerController>().GetPlayerClass();
            switch (playerClass)
            {
                case PlayerClass.Assault:
                    IncrementClassDeath(assaultGeneralStatsToAdd);
                    break;
                case PlayerClass.Medic:
                    IncrementClassDeath(medicGeneralStatsToAdd);
                    break;
                case PlayerClass.Mastodonte:
                    IncrementClassDeath(mastodonteGeneralStatsToAdd);
                    break;
                case PlayerClass.Incinerator:
                    IncrementClassDeath(incineratorGeneralStatsToAdd);
                    break;
                case PlayerClass.Scout:
                    IncrementClassDeath(scoutGeneralStatsToAdd);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void CalculateAllClassStats()
        {
            CalculateNewGeneralStats(assaultGeneralStats, assaultGeneralStatsToAdd);
            CalculateNewGeneralStats(medicGeneralStats, medicGeneralStatsToAdd);
            CalculateNewGeneralStats(mastodonteGeneralStats, mastodonteGeneralStatsToAdd);
            CalculateNewGeneralStats(incineratorGeneralStats, incineratorGeneralStatsToAdd);
            CalculateNewGeneralStats(scoutGeneralStats, scoutGeneralStatsToAdd);
            CalculateNewClassSpecificStats();
        }

        private void UpdateAllClassStats()
        {
            UpdateGeneralStats(assaultGeneralStats);
            UpdateGeneralStats(medicGeneralStats);
            UpdateGeneralStats(mastodonteGeneralStats);
            UpdateGeneralStats(incineratorGeneralStats);
            UpdateGeneralStats(scoutGeneralStats);
            UpdateAllClassSpecificStats();
        }

        private void IncrementPlayTimeOnClass(PlayerClass playerClass, TimeSpan playTime)
        {
            switch (playerClass)
            {
                case PlayerClass.Assault:
                    assaultGeneralStatsToAdd.TimePlayed += (long)playTime.TotalMinutes;
                    break;
                case PlayerClass.Medic:
                    medicGeneralStatsToAdd.TimePlayed += (long)playTime.TotalMinutes;
                    break;
                case PlayerClass.Mastodonte:
                    mastodonteGeneralStatsToAdd.TimePlayed += (long)playTime.TotalMinutes;
                    break;
                case PlayerClass.Incinerator:
                    incineratorGeneralStatsToAdd.TimePlayed += (long)playTime.TotalMinutes;
                    break;
                case PlayerClass.Scout:
                    scoutGeneralStatsToAdd.TimePlayed += (long)playTime.TotalMinutes;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("playerClass", playerClass, null);
            }
        }

        private static void IncrementClassDeath(GeneralStats classStats)
        {
            classStats.NumberOfDeaths++;
        }

        private void IncrementBasicZombieKill(PlayerClass killerPlayer)
        {
            switch (killerPlayer)
            {
                case PlayerClass.Assault:
                    assaultGeneralStatsToAdd.BaseZombieKilled++;
                    break;
                case PlayerClass.Medic:
                    medicGeneralStatsToAdd.BaseZombieKilled++;
                    break;
                case PlayerClass.Mastodonte:
                    mastodonteGeneralStatsToAdd.BaseZombieKilled++;
                    if (mastodonteGeneralStats.BaseZombieKilled + mastodonteGeneralStatsToAdd.BaseZombieKilled == 100)
                    {
                        if (OnClassAchievementSucceeded != null)
                            OnClassAchievementSucceeded(AchievementName.MastodonteAchievement1);
                    }
                    break;
                case PlayerClass.Incinerator:
                    incineratorGeneralStatsToAdd.BaseZombieKilled++;
                    break;
                case PlayerClass.Scout:
                    scoutGeneralStatsToAdd.BaseZombieKilled++;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("killerPlayer", killerPlayer, null);
            }
        }

        private void IncrementRunnerZombieKill(PlayerClass killerPlayer)
        {
            switch (killerPlayer)
            {
                case PlayerClass.Assault:
                    assaultGeneralStatsToAdd.RunnerZombieKilled++;
                    break;
                case PlayerClass.Medic:
                    medicGeneralStatsToAdd.RunnerZombieKilled++;
                    break;
                case PlayerClass.Mastodonte:
                    mastodonteGeneralStatsToAdd.RunnerZombieKilled++;
                    break;
                case PlayerClass.Incinerator:
                    incineratorGeneralStatsToAdd.RunnerZombieKilled++;
                    break;
                case PlayerClass.Scout:
                    scoutGeneralStatsToAdd.RunnerZombieKilled++;
                    if (scoutGeneralStats.RunnerZombieKilled + scoutGeneralStatsToAdd.RunnerZombieKilled == 75)
                    {
                        if (OnClassAchievementSucceeded != null)
                            OnClassAchievementSucceeded(AchievementName.ScoutAchievement1);
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException("killerPlayer", killerPlayer, null);
            }
        }

        private void IncrementHunterZombieKill(PlayerClass killerPlayer)
        {
            switch (killerPlayer)
            {
                case PlayerClass.Assault:
                    assaultGeneralStatsToAdd.HunterZombieKilled++;
                    if (assaultGeneralStats.HunterZombieKilled + assaultGeneralStatsToAdd.HunterZombieKilled == 75)
                    {
                        if (OnClassAchievementSucceeded != null)
                            OnClassAchievementSucceeded(AchievementName.AssaultAchievement1);
                    }
                    break;
                case PlayerClass.Medic:
                    medicGeneralStatsToAdd.HunterZombieKilled++;
                    break;
                case PlayerClass.Mastodonte:
                    mastodonteGeneralStatsToAdd.HunterZombieKilled++;
                    break;
                case PlayerClass.Incinerator:
                    incineratorGeneralStatsToAdd.HunterZombieKilled++;
                    break;
                case PlayerClass.Scout:
                    scoutGeneralStatsToAdd.HunterZombieKilled++;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("killerPlayer", killerPlayer, null);
            }
        }

        private void IncrementBoomerZombieKill(PlayerClass killerPlayer)
        {
            switch (killerPlayer)
            {
                case PlayerClass.Assault:
                    assaultGeneralStatsToAdd.BoomerZombieKilled++;
                    break;
                case PlayerClass.Medic:
                    medicGeneralStatsToAdd.BoomerZombieKilled++;
                    break;
                case PlayerClass.Mastodonte:
                    mastodonteGeneralStatsToAdd.BoomerZombieKilled++;
                    break;
                case PlayerClass.Incinerator:
                    incineratorGeneralStatsToAdd.BoomerZombieKilled++;
                    if (incineratorGeneralStats.BoomerZombieKilled + incineratorGeneralStatsToAdd.BoomerZombieKilled ==
                        75)
                    {
                        if (OnClassAchievementSucceeded != null)
                            OnClassAchievementSucceeded(AchievementName.IncineratorAchievement1);
                    }
                    break;
                case PlayerClass.Scout:
                    scoutGeneralStatsToAdd.BoomerZombieKilled++;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("killerPlayer", killerPlayer, null);
            }
        }

        private void IncrementWitchZombieKill(PlayerClass killerPlayer)
        {
            switch (killerPlayer)
            {
                case PlayerClass.Assault:
                    assaultGeneralStatsToAdd.WitchZombieKilled++;
                    break;
                case PlayerClass.Medic:
                    medicGeneralStatsToAdd.WitchZombieKilled++;
                    if (medicGeneralStats.WitchZombieKilled + medicGeneralStatsToAdd.WitchZombieKilled == 50)
                    {
                        if (OnClassAchievementSucceeded != null)
                            OnClassAchievementSucceeded(AchievementName.MedicAchievement1);
                    }
                    break;
                case PlayerClass.Mastodonte:
                    mastodonteGeneralStatsToAdd.WitchZombieKilled++;
                    break;
                case PlayerClass.Incinerator:
                    incineratorGeneralStatsToAdd.WitchZombieKilled++;
                    break;
                case PlayerClass.Scout:
                    scoutGeneralStatsToAdd.WitchZombieKilled++;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("killerPlayer", killerPlayer, null);
            }
        }

        private static void CalculateNewGeneralStats(GeneralStats currentStats, GeneralStats statsToAdd)
        {
            currentStats.BaseZombieKilled += statsToAdd.BaseZombieKilled;
            currentStats.BoomerZombieKilled += statsToAdd.BoomerZombieKilled;
            currentStats.HunterZombieKilled += statsToAdd.HunterZombieKilled;
            currentStats.RunnerZombieKilled += statsToAdd.RunnerZombieKilled;
            currentStats.WitchZombieKilled += statsToAdd.WitchZombieKilled;

            currentStats.NumberOfDeaths += statsToAdd.NumberOfDeaths;
            currentStats.TimePlayed += statsToAdd.TimePlayed;
        }

        private void CalculateNewClassSpecificStats()
        {
            assaultStats.AmmoBoxDropped += assaultStatsToAdd.AmmoBoxDropped;
            medicStats.AmmountOfHealthGiven += medicStatsToAdd.AmmountOfHealthGiven;
            mastodonteStats.ZombieKilledWithGrenade += mastodonteStatsToAdd.ZombieKilledWithGrenade;
            incineratorStats.NumberOfBurnerDropped += incineratorStatsToAdd.NumberOfBurnerDropped;
            scoutStats.NumberOfGlowSticksDropped += scoutStatsToAdd.NumberOfGlowSticksDropped;
        }

        private void CalculateGameStats(EndingType endingType)
        {
            switch (endingType)
            {
                case EndingType.SoloFinished:
                    gameStats.SoloGamesFinished++;
                    if (gameStats.SoloGamesFinished == 1)
                    {
                        if (OnGameAchievementSucceeded != null)
                        {
                            if (allMiniGamesCompleted) OnGameAchievementSucceeded(AchievementName.GameAchievement5);
                            OnGameAchievementSucceeded(AchievementName.GameAchievement1);
                        }
                    }
                    break;
                case EndingType.CoopFinished:
                    gameStats.CoopGamesFinished++;
                    if (gameStats.CoopGamesFinished == 1)
                    {
                        if (OnGameAchievementSucceeded != null)
                        {
                            if (allMiniGamesCompleted) OnGameAchievementSucceeded(AchievementName.GameAchievement5);
                            OnGameAchievementSucceeded(AchievementName.GameAchievement2);
                        }
                    }
                    break;
                case EndingType.VersusFinished:
                    gameStats.VersusGamesFinished++;
                    if (gameStats.VersusGamesFinished == 20)
                    {
                        if (OnGameAchievementSucceeded != null)
                        {
                            if (allMiniGamesCompleted) OnGameAchievementSucceeded(AchievementName.GameAchievement5);
                            OnGameAchievementSucceeded(AchievementName.GameAchievement3);
                        }
                    }
                    break;
                case EndingType.GameFailed:
                    gameStats.GameOvers++;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("endingType", endingType, null);
            }
        }

        private bool CheckIfTortoiseAndTheHareAchievementCompleted(PlayerClass[] players)
        {
            if (players.Length == 2)
            {
                if (players[0] == PlayerClass.Mastodonte && players[1] == PlayerClass.Scout) return true;
                else if (players[0] == PlayerClass.Scout && players[1] == PlayerClass.Mastodonte) return true;
                else return false;
            }
            else if (players.Length == 4)
            {
                if (players[0] == PlayerClass.Mastodonte && players[1] == PlayerClass.Scout) return true;
                else if (players[0] == PlayerClass.Scout && players[1] == PlayerClass.Mastodonte) return true;
                else if (players[2] == PlayerClass.Mastodonte && players[3] == PlayerClass.Scout) return true;
                else if (players[2] == PlayerClass.Scout && players[3] == PlayerClass.Mastodonte) return true;
                else return false;
            }
            return false;
        }

        private void CheckIfNotAffraidOfTheDarkAchievementSucceeded(PlayerTeleporterEvent playerTeleporterEvent)
        {
            PlayerController playerController = playerTeleporterEvent.Player.GetComponent<PlayerController>();
            if(playerController != null)
            {
                if(playerController.GetPlayerClass() == PlayerClass.Incinerator && playerController.GetSpecialItemCount() == 0)
                {
                    if (OnClassAchievementSucceeded != null) OnClassAchievementSucceeded(AchievementName.IncineratorAchievement2);
                }
            }
        }

        private void UpdateGeneralStats(GeneralStats statsToUpdate)
        {
            generalStatsRepository.ChangePlayerGeneralStats(statsToUpdate);
        }

        private void UpdateAllClassSpecificStats()
        {
            assaultStatsRepository.ChangeAmmoBoxDropped(assaultStats);
            medicStatsRepository.ChangeHealthGiven(medicStats);
            mastodonteStatsRepository.ChangeZombieKilledWithGrenade(mastodonteStats);
            incineratorStatsRepository.ChangeNumberOfBurnerDropped(incineratorStats);
            scoutStatsRepository.ChangeNumberOfGlowSticksDropped(scoutStats);
        }

        private void UpdateGameStats()
        {
            gameStatsRepository.ChangeAllGameStats(gameStats);
        }
    }
}

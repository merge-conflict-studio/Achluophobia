﻿using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Control/BoomerController")]
    public class BoomerController : ZombieController
    {
        private const int SelfDestructDamage = 1000;
        
        private new void Awake()
        {
            base.Awake();
        }

        public override void Attack()            
        {
            //On s'assure que le boomer se suicide et déclenche l'explosion par le fait même.
             health.Hit(transform.root.gameObject, SelfDestructDamage, false);
        }
    }
}



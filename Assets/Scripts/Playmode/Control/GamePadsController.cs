﻿using XInputDotNetPure;
using GamePad = Harmony.GamePad;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    /// <summary>
    /// Represents a GamePadInputSensor controller. 
    /// It contains a collection of GamePadInputSensor and it continuously
    /// update their state.
    /// </summary>
    [AddComponentMenu("Game/Control/GamePadsController")]
    public sealed class GamePadsController : GameScript
    {
        private GamePad gamePad;
        private GamePadSensor[] GamePads;

        //These events are for global events.
        //They will trigger if any GamePadInputSensor triggers it.
        //Use them for menus or anywhere that each GamePad can trigger the action.
        public event PlayerActionEventHandler OnMenuUp;
        public event PlayerActionEventHandler OnMenuDown;
        public event PlayerActionEventHandler OnMenuLeft;
        public event PlayerActionEventHandler OnMenuRight;
        public event PlayerActionEventHandler OnMenuConfirm;
        public event PlayerActionEventHandler OnMenuBack;
        public event PlayerActionEventHandler OnPause;

        private void InjectGamePad([GameObjectScope] GamePad gamePad)
        {
            this.gamePad = gamePad;
        }

        private void Awake()
        {
            InjectDependencies("InjectGamePad");

            GamePads = new[]
            {
                new GamePadSensor(PlayerIndex.One, gamePad),
                new GamePadSensor(PlayerIndex.Two, gamePad),
                new GamePadSensor(PlayerIndex.Three, gamePad),
                new GamePadSensor(PlayerIndex.Four, gamePad)
            };
        }

        private void OnEnable()
        {
            foreach (GamePadSensor gamePadSensor in GamePads)
            {
                gamePadSensor.OnMenuUp += NotifyMenuUp;
                gamePadSensor.OnMenuDown += NotifyMenuDown;
                gamePadSensor.OnMenuLeft += NotifyMenuLeft;
                gamePadSensor.OnMenuRight += NotifyMenuRight;
                gamePadSensor.OnMenuConfirm += NotifyMenuConfirm;
                gamePadSensor.OnBack += NotifyMenuBack;
                gamePadSensor.OnPause += NotifyPause;
            }
        }

        private void OnDisable()
        {
            foreach (GamePadSensor gamePadSensor in GamePads)
            {
                gamePadSensor.OnMenuUp -= NotifyMenuUp;
                gamePadSensor.OnMenuDown -= NotifyMenuDown;
                gamePadSensor.OnMenuLeft -= NotifyMenuLeft;
                gamePadSensor.OnMenuRight -= NotifyMenuRight;
                gamePadSensor.OnMenuConfirm -= NotifyMenuConfirm;
                gamePadSensor.OnBack -= NotifyMenuBack;
                gamePadSensor.OnPause -= NotifyPause;
            }
        }

        private void Update()
        {
            foreach (GamePadSensor gamePadSensor in GamePads)
            {
                if (gamePadSensor.IsConnected())
                {
                    gamePadSensor.Update();
                }
            }
        }

        public GamePadSensor GetSensor(PlayerNumber playerNumber)
        {
            return GamePads[(int) playerNumber];
        }

        public GamePadSensor[] GetAllSensors()
        {
            return GamePads;
        }

        private void NotifyMenuUp()
        {
            if (OnMenuUp != null) OnMenuUp();
        }

        private void NotifyMenuDown()
        {
            if (OnMenuDown != null) OnMenuDown();
        }

        private void NotifyMenuLeft()
        {
            if (OnMenuLeft != null) OnMenuLeft();
        }

        private void NotifyMenuRight()
        {
            if (OnMenuRight != null) OnMenuRight();
        }

        private void NotifyMenuBack()
        {
            if (OnMenuBack != null) OnMenuBack();
        }

        private void NotifyPause()
        {
            if (OnPause != null) OnPause();
        }

        private void NotifyMenuConfirm()
        {
            if (OnMenuConfirm != null) OnMenuConfirm();
        }
    }
}
﻿using System;
using Harmony;
using UnityEngine;
using System.Collections;

// Partie fier: Alexandre Lachance
namespace Achluophobia
{
    [AddComponentMenu("Game/World/Ui/Control/HeadUpMenuController")]
    public class HeadUpMenuController : GameScript
    {
        [SerializeField, Tooltip("Item for Level 1")] 
        private string itemLevel1;

        [SerializeField, Tooltip("Item for Level 2")] 
        private string itemLevel2;

        [SerializeField, Tooltip("Item for Level 3")] 
        private string itemLevel3;

        [SerializeField, Tooltip("Item for Level 4")] 
        private string itemLevel4;

        [SerializeField, Tooltip("The time the achievement popup will be shown on screen")]
        private float achievementPopupLifetime;

        private const float MaxHealthViewScale = 0.6f;

        private AmmoView ammoView1;
        private AmmoView ammoView2;
        private AmmoView ammoView3;
        private AmmoView ammoView4;
        private HealthBarView healthBarView1;
        private HealthBarView healthBarView2;
        private HealthBarView healthBarView3;
        private HealthBarView healthBarView4;
        private SpecialItemView specialItemView1;
        private SpecialItemView specialItemView2;
        private SpecialItemView specialItemView3;
        private SpecialItemView specialItemView4;
        private ItemView itemView1;
        private ItemView itemView2;
        private TimeBeforeRespawnView timeBeforeRespawnView;
        private PlayerHealthEventChannel playerHealthEventChannel;
        private PlayerAmmoEventChannel playerAmmoEventChannel;
        private ItemsOnHUDEventChannel itemsOnHudEventChannel;
        private PlayerTeleporterEventChannel playerTeleporterEventChannel;
        private MiniGameOnHUDEventChannel miniGameOnHudEventChannel;
        private PlayerDeathEventChannel playerDeathEventChannel;
        private PlayerSwitchWeaponEventChannel playerSwitchWeaponEventChannel;
        private SpecialItemOnHUDEventChannel specialItemOnHudEventChannel;
        private AchievementPopupEventChannel achievementPopupEventChannel;
        private ImportantGameValues importantGameValues;
        private GameObject achievementBox;

        private QuestItemsData dataLevel1;
        private QuestItemsData dataLevel2;
        private QuestItemsData dataLevel3;
        private QuestItemsData dataLevel4;
        private string playerTeleporterTag;
        private Coroutine currentCoroutineTeam1;
        private Coroutine currentCoroutineTeam2;

        public void InjectHudController([Named(R.S.GameObject.AmmoCount1)] [EntityScope] AmmoView ammoView1,
                                        [Named(R.S.GameObject.AmmoCount2)] [EntityScope] AmmoView ammoView2,
                                        [Named(R.S.GameObject.AmmoCount3)] [EntityScope] AmmoView ammoView3,
                                        [Named(R.S.GameObject.AmmoCount4)] [EntityScope] AmmoView ammoView4,
                                        [Named(R.S.GameObject.HealthBar1)] [EntityScope] HealthBarView healthBarView1,
                                        [Named(R.S.GameObject.HealthBar2)] [EntityScope] HealthBarView healthBarView2,
                                        [Named(R.S.GameObject.HealthBar3)] [EntityScope] HealthBarView healthBarView3,
                                        [Named(R.S.GameObject.HealthBar4)] [EntityScope] HealthBarView healthBarView4,
                                        [Named(R.S.GameObject.SpecialItemsCount1)] [EntityScope] SpecialItemView specialItemView1,
                                        [Named(R.S.GameObject.SpecialItemsCount2)] [EntityScope] SpecialItemView specialItemView2,
                                        [Named(R.S.GameObject.SpecialItemsCount3)] [EntityScope] SpecialItemView specialItemView3,
                                        [Named(R.S.GameObject.SpecialItemsCount4)] [EntityScope] SpecialItemView specialItemView4,
                                        [Named(R.S.GameObject.ItemToFind1)] [EntityScope] ItemView itemView1,
                                        [Named(R.S.GameObject.ItemToFind2)] [EntityScope] ItemView itemView2,
                                        [Named(R.S.GameObject.AchievementCase), EntityScope] GameObject achievementBox,
                                        [EntityScope] TimeBeforeRespawnView timeBeforeRespawnView,
                                        [EventChannelScope] ItemsOnHUDEventChannel itemsOnHudEventChannel,
                                        [EventChannelScope] MiniGameOnHUDEventChannel miniGameOnHudEventChannel,
                                        [EventChannelScope] PlayerAmmoEventChannel playerAmmoEventChannel,
                                        [EventChannelScope] PlayerDeathEventChannel playerDeathEventChannel,
                                        [EventChannelScope] PlayerHealthEventChannel playerHealthEventChannel,
                                        [EventChannelScope] PlayerSwitchWeaponEventChannel playerSwitchWeaponEventChannel,
                                        [EventChannelScope] PlayerTeleporterEventChannel playerTeleporterEventChannel,
                                        [EventChannelScope] SpecialItemOnHUDEventChannel specialItemOnHudEventChannel,
                                        [EventChannelScope] AchievementPopupEventChannel achievementPopupEventChannel,
                                        [ApplicationScope] ImportantGameValues importantGameValues)
        {
            this.ammoView1 = ammoView1;
            this.ammoView2 = ammoView2;
            this.ammoView3 = ammoView3;
            this.ammoView4 = ammoView4;
            this.healthBarView1 = healthBarView1;
            this.healthBarView2 = healthBarView2;
            this.healthBarView3 = healthBarView3;
            this.healthBarView4 = healthBarView4;
            this.specialItemView1 = specialItemView1;
            this.specialItemView2 = specialItemView2;
            this.specialItemView3 = specialItemView3;
            this.specialItemView4 = specialItemView4;
            this.itemView1 = itemView1;
            this.itemView2 = itemView2;
            this.achievementBox = achievementBox;
            this.timeBeforeRespawnView = timeBeforeRespawnView;
            this.itemsOnHudEventChannel = itemsOnHudEventChannel;
            this.miniGameOnHudEventChannel = miniGameOnHudEventChannel;
            this.playerAmmoEventChannel = playerAmmoEventChannel;
            this.playerDeathEventChannel = playerDeathEventChannel;
            this.playerHealthEventChannel = playerHealthEventChannel;
            this.playerSwitchWeaponEventChannel = playerSwitchWeaponEventChannel;
            this.playerTeleporterEventChannel = playerTeleporterEventChannel;
            this.specialItemOnHudEventChannel = specialItemOnHudEventChannel;
            this.achievementPopupEventChannel = achievementPopupEventChannel;
            this.importantGameValues = importantGameValues;
        }

        private void Awake()
        {
            InjectDependencies("InjectHudController");

            switch (importantGameValues.NumberOfPlayers)
            {
                case 1:
                    healthBarView2.GameObject.Destroy();
                    ammoView2.gameObject.Destroy();
                    healthBarView3.GameObject.Destroy();
                    ammoView3.gameObject.Destroy();
                    healthBarView4.GameObject.Destroy();
                    ammoView4.gameObject.Destroy();
                    itemView2.GameObject.Destroy();
                    break;
                case 2:
                    healthBarView3.GameObject.Destroy();
                    ammoView3.gameObject.Destroy();
                    healthBarView4.GameObject.Destroy();
                    ammoView4.gameObject.Destroy();
                    itemView2.gameObject.Destroy();
                    break;
            }
        }

        private void OnEnable()
        {
            itemsOnHudEventChannel.OnEventPublished += OnItemPickedUp;
            miniGameOnHudEventChannel.OnEventPublished += OnMiniGameComplete;
            playerAmmoEventChannel.OnEventPublished += OnPlayerAmmoChanged;
            playerDeathEventChannel.OnEventPublished += OnPlayerDeath;
            playerHealthEventChannel.OnEventPublished += OnPlayerHealthChanged;
            playerSwitchWeaponEventChannel.OnEventPublished += OnWeaponSwitched;
            playerTeleporterEventChannel.OnEventPublished += OnLevelChanged;
            specialItemOnHudEventChannel.OnEventPublished += OnSpecialItemCountChanged;
            achievementPopupEventChannel.OnEventPublished += OnShowAchievementPopup;
        }

        private void OnDisable()
        {
            itemsOnHudEventChannel.OnEventPublished -= OnItemPickedUp;
            miniGameOnHudEventChannel.OnEventPublished -= OnMiniGameComplete;
            playerAmmoEventChannel.OnEventPublished -= OnPlayerAmmoChanged;
            playerHealthEventChannel.OnEventPublished -= OnPlayerHealthChanged;
            playerDeathEventChannel.OnEventPublished -= OnPlayerDeath;
            playerSwitchWeaponEventChannel.OnEventPublished -= OnWeaponSwitched;
            playerTeleporterEventChannel.OnEventPublished -= OnLevelChanged;
            specialItemOnHudEventChannel.OnEventPublished -= OnSpecialItemCountChanged;
            achievementPopupEventChannel.OnEventPublished -= OnShowAchievementPopup;
        }

        private void OnSpecialItemCountChanged(SpecialItemOnHUDEvent specialItemOnHudEvent)
        {
            UpdateSpecialItemView(specialItemOnHudEvent.PlayerNumber, specialItemOnHudEvent.SpecialItemsLeft);
        }

        private void UpdateSpecialItemView(PlayerNumber playerNumber, int specialItemCount)
        {
            switch (playerNumber)
            {
                case PlayerNumber.One:
                     specialItemView1.SetSpecialItemCount(specialItemCount);
                     break;
                case PlayerNumber.Two:
                    specialItemView2.SetSpecialItemCount(specialItemCount);
                    break;
                case PlayerNumber.Three:
                    specialItemView3.SetSpecialItemCount(specialItemCount);
                    break;
                case PlayerNumber.Four:
                    specialItemView4.SetSpecialItemCount(specialItemCount);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("playerNumber", playerNumber, null);
            }
        }
        private void UpdateHealthBarView(PlayerNumber playerNumber, Health playerHealth)
        {
            switch (playerNumber)
            {
                case PlayerNumber.One:
                    healthBarView1.SetHealthPercentage((playerHealth.HealthPoints * MaxHealthViewScale) /
                                                       playerHealth.GetMaxHealth());
                    break;
                case PlayerNumber.Two:
                    healthBarView2.SetHealthPercentage((playerHealth.HealthPoints * MaxHealthViewScale) /
                                                       playerHealth.GetMaxHealth());
                    break;
                case PlayerNumber.Three:
                    healthBarView3.SetHealthPercentage((playerHealth.HealthPoints * MaxHealthViewScale) /
                                                       playerHealth.GetMaxHealth());
                    break;
                case PlayerNumber.Four:
                    healthBarView4.SetHealthPercentage((playerHealth.HealthPoints * MaxHealthViewScale) /
                                                       playerHealth.GetMaxHealth());
                    break;
                default:
                    throw new ArgumentOutOfRangeException("playerNumber", playerNumber, null);
            }
        }

        private void OnPlayerHealthChanged(PlayerHealthEvent healthEvent)
        {
            UpdateHealthBarView(healthEvent.PlayerNumber, healthEvent.PlayerHealth);
        }

        private void UpdateAmmoCountView(PlayerNumber playerNumber, int ammoInWeapon, int ammoOnPlayer,
            bool isAmmoUnlimited)
        {
            switch (playerNumber)
            {
                case PlayerNumber.One:
                    ammoView1.SetAmmoCount(ammoInWeapon, ammoOnPlayer, isAmmoUnlimited);
                    break;
                case PlayerNumber.Two:
                    ammoView2.SetAmmoCount(ammoInWeapon, ammoOnPlayer, isAmmoUnlimited);
                    break;
                case PlayerNumber.Three:
                    ammoView3.SetAmmoCount(ammoInWeapon, ammoOnPlayer, isAmmoUnlimited);
                    break;
                case PlayerNumber.Four:
                    ammoView4.SetAmmoCount(ammoInWeapon, ammoOnPlayer, isAmmoUnlimited);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("playerNumber", playerNumber, null);
            }
        }

        private void OnPlayerAmmoChanged(PlayerAmmoEvent ammoEvent)
        {
            UpdateAmmoCountView(ammoEvent.PlayerNumber, ammoEvent.AmmoInWeapon, ammoEvent.AmmoOnPlayer,
                ammoEvent.IsAmmoUnlimited);
        }

        private void UpdateItemToFindView(int numberOfItemsLeft, string itemCollected, Team team)
        {
            switch (team)
            {
                case Team.One:
                    itemView1.SetItemToFind(numberOfItemsLeft, itemCollected);
                    break;
                case Team.Two:
                    itemView2.SetItemToFind(numberOfItemsLeft, itemCollected);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("team", team, null);
            }
        }

        private void UpdateMiniGameOnItemToFindView(int numberOfMiniGamesLeft, string activeMiniGame, Team team)
        {
            switch (team)
            {
                case Team.One:
                    itemView1.SetMiniGamesToComplete(numberOfMiniGamesLeft, activeMiniGame);
                    break;
                case Team.Two:
                    itemView2.SetMiniGamesToComplete(numberOfMiniGamesLeft, activeMiniGame);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("team", team, null);
            }
        }

        private void OnMiniGameComplete(MiniGameOnHUDEvent miniGameOnHudEventEvent)
        {
            UpdateMiniGameOnItemToFindView(miniGameOnHudEventEvent.MiniGamesLeft,
                                           miniGameOnHudEventEvent.MiniGamesToFinish.GetRoot().name,
                                           miniGameOnHudEventEvent.MiniGamesToFinish.tag.Contains("Team2") ? Team.Two : Team.One);
        }

        private void OnItemPickedUp(ItemsOnHUDEvent itemsOnHudEvent)
        {
            UpdateItemToFindView(itemsOnHudEvent.ItemsLeft, 
                                 itemsOnHudEvent.ItemToCollect.GetRoot().name,
                                 itemsOnHudEvent.ItemToCollect.tag.Contains("Team2") ? Team.Two : Team.One);
        }

        private void OnLevelChanged(PlayerTeleporterEvent playerTeleporterEvent)
        {
            playerTeleporterTag = playerTeleporterEvent.PlayerTeleporter.tag.Substring(0,6);

            dataLevel1 = GameObject.FindGameObjectWithTag(R.S.Tag.DataLevel1).GetComponent<QuestItemsData>();
            dataLevel2 = GameObject.FindGameObjectWithTag(R.S.Tag.DataLevel2).GetComponent<QuestItemsData>();
            dataLevel3 = GameObject.FindGameObjectWithTag(R.S.Tag.DataLevel3).GetComponent<QuestItemsData>();
            dataLevel4 = GameObject.FindGameObjectWithTag(R.S.Tag.DataLevel4).GetComponent<QuestItemsData>();

            switch (playerTeleporterTag)
            {
                case R.S.Tag.Level0:
                        UpdateItemToFindView(dataLevel1.ItemsToCollect, itemLevel1,
                            playerTeleporterEvent.Player.GetComponent<PlayerController>().PlayerTeam == Team.One
                                ? Team.One
                                : Team.Two);
                    break;
                case R.S.Tag.Level1:
                        UpdateItemToFindView(dataLevel2.ItemsToCollect, itemLevel2,
                            playerTeleporterEvent.Player.GetComponent<PlayerController>().PlayerTeam == Team.One
                                ? Team.One
                                : Team.Two);
                    break;
                case R.S.Tag.Level2:
                        UpdateItemToFindView(dataLevel3.ItemsToCollect, itemLevel3,
                            playerTeleporterEvent.Player.GetComponent<PlayerController>().PlayerTeam == Team.One
                                ? Team.One
                                : Team.Two);
                    break;
                case R.S.Tag.Level3:
                        UpdateItemToFindView(dataLevel4.ItemsToCollect, itemLevel4,
                            playerTeleporterEvent.Player.GetComponent<PlayerController>().PlayerTeam == Team.One
                                ? Team.One
                                : Team.Two);
                    break;
            }
        }

        private void OnPlayerDeath(PlayerDeathEvent playerDeathEvent)
        {
            switch (importantGameValues.GameMode)
            {
                case GameMode.Solo:
                    return;
                case GameMode.Coop:
                {
                    if (importantGameValues.NumberOfDeadPlayers == 1)
                        currentCoroutineTeam1 = StartCoroutine(timeBeforeRespawnView.StartRespawnTimer(
                            GameGlobalVariables.RespawnTime,
                            playerDeathEvent.DeadPlayer.GetComponent<PlayerController>().PlayerTeam));
                    break;
                }
                case GameMode.Versus:
                    if (importantGameValues.NumberOfDeadPlayersTeam1 == 1 && 
                        playerDeathEvent.DeadPlayer.GetComponent<PlayerController>().PlayerTeam == Team.One)
                    {
                        currentCoroutineTeam1 = StartCoroutine(timeBeforeRespawnView.StartRespawnTimer(
                            GameGlobalVariables.RespawnTime,
                            playerDeathEvent.DeadPlayer.GetComponent<PlayerController>().PlayerTeam));
                    }
                    else if (importantGameValues.NumberOfDeadPlayersTeam1 == 2 &&
                             playerDeathEvent.DeadPlayer.GetComponent<PlayerController>().PlayerTeam == Team.One)
                    {
                        StopCoroutine(currentCoroutineTeam1);
                        currentCoroutineTeam1 = StartCoroutine(timeBeforeRespawnView.StartRespawnTimer(
                            GameGlobalVariables.RespawnTime,
                            playerDeathEvent.DeadPlayer.GetComponent<PlayerController>().PlayerTeam));
                    }
                    if (importantGameValues.NumberOfDeadPlayersTeam2 == 1 && 
                        playerDeathEvent.DeadPlayer.GetComponent<PlayerController>().PlayerTeam == Team.Two)
                    {
                        currentCoroutineTeam2 = StartCoroutine(timeBeforeRespawnView.StartRespawnTimer(
                            GameGlobalVariables.RespawnTime,
                            playerDeathEvent.DeadPlayer.GetComponent<PlayerController>().PlayerTeam));
                    }
                    else if (importantGameValues.NumberOfDeadPlayersTeam2 == 2 && 
                             playerDeathEvent.DeadPlayer.GetComponent<PlayerController>().PlayerTeam == Team.Two)
                    {
                        StopCoroutine(currentCoroutineTeam2);
                        currentCoroutineTeam2 = StartCoroutine(timeBeforeRespawnView.StartRespawnTimer(
                            GameGlobalVariables.RespawnTime,
                            playerDeathEvent.DeadPlayer.GetComponent<PlayerController>().PlayerTeam));
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void OnWeaponSwitched(PlayerSwitchWeaponEvent playerSwitchWeaponEvent)
        {
            //Utilis seulement pour le Masto
            switch (playerSwitchWeaponEvent.PlayerNumber)
            {
                case PlayerNumber.One:
                    ammoView1.SetSledgeHammer();
                    break;
                case PlayerNumber.Two:
                    ammoView2.SetSledgeHammer();
                    break;
                case PlayerNumber.Three:
                    ammoView3.SetSledgeHammer();
                    break;
                case PlayerNumber.Four:
                    ammoView4.SetSledgeHammer();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void OnShowAchievementPopup(AchievementPopupEvent achievementPopupEvent)
        {
            StartCoroutine(ShowAchievementOnScreen());
            if (achievementPopupEvent.ClassAchievement != null)
            {
                SetAchievementBoxValues(achievementBox.GetComponent<AchievementView>(), achievementPopupEvent.ClassAchievement);
            }
            else if (achievementPopupEvent.GameAchievement != null)
            {
                SetAchievementBoxValues(achievementBox.GetComponent<AchievementView>(), achievementPopupEvent.GameAchievement);
            }
        }

        private IEnumerator ShowAchievementOnScreen()
        {
            achievementBox.gameObject.SetActive(true);

            yield return new WaitForSeconds(achievementPopupLifetime);

            achievementBox.gameObject.SetActive(false);
        }

        private void SetAchievementBoxValues(AchievementView achievementBoxValues, GameAchievements gameAchievements)
        {
            achievementBoxValues.ChangeTitle(gameAchievements.Name);
            achievementBoxValues.ChangeDescription(gameAchievements.Description);
            achievementBoxValues.ChangeSuccessColor(true);
        }

        private void SetAchievementBoxValues(AchievementView achievementBoxValues, ClassAchievements classAchievements)
        {
            achievementBoxValues.ChangeTitle(classAchievements.Name);
            achievementBoxValues.ChangeDescription(classAchievements.Description);
            achievementBoxValues.ChangeSuccessColor(true);
        }
    }
}
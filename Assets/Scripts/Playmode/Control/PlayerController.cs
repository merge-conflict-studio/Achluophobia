﻿using System;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public delegate void SwitchedWeaponEventHandler(PlayerNumber playerNumber, WeaponType newWeaponType);
    public delegate void ActionTriggerEventHandler();
    public delegate void SpecialItemSpawningEventHandler();
    public delegate void SpecialItemCountOnHUDEventHandler(int specialItemCount, PlayerNumber playerNumber);

    [AddComponentMenu("Game/Control/PlayerController")]
    public class PlayerController : GameScript
    {
        private static PlayerNumber currentPlayerNumber = 0;
        private const float MovementSpeedMultiplier = 0.5f;
        private const string Clone = "(Clone)";

        [SerializeField, Tooltip("ScriptableObject containing the player data")] private PlayerData player;

        [SerializeField, Tooltip("The primary weapon prefab")] private GameObject primaryWeaponPrefab;

        [SerializeField, Tooltip("The secondary weapon prefab")] private GameObject secondaryWeaponPrefab;

        [SerializeField, Tooltip("The maximum number of SpecialItem the player can carry")]
        private int specialItemLimit;

        [SerializeField, Tooltip("The class of the player")] private PlayerClass playerClass;

        public PlayerNumber PlayerNumber { get; private set; }

        public Team PlayerTeam { get; private set; }

        private Health health;
        private GamePadSensor gamePadInputSensor;
        private CharacterController characterController;
        private Light flashlight;
        private Light auraLight;
        private EntityRotater entityRotater;
        private Weapon[] weapons;
        private Weapon currentWeapon;
        private Transform weaponTransform;
        private new Camera camera;
        private Camera camera2;
        private SpecialItemSensor pickableSpecialItemSensor;
        private PlayerTeleporterSensor playerTeleporterSensor;
        private bool canCollectSpecialItem;
        private FollowingCamera followingCamera;
        private FollowingCamera followingCamera2;
        private GameObjectSpawner gameObjectSpawner;
        private bool isAiming;
        private bool isPlayerDead;
        private bool isClassSpecificItemCollectable;
        private ImportantGameValues importantGameValues;
        private PlayerValues playerValues;
        private int specialItemCount;
        private bool isPlayerAttacking; 
        private bool wasHit;

        private PlayerDeathEventChannel playerDeathEventChannel;
        private PlayerRespawnEventChannel playerRespawnEventChannel;
        private ZombieDeathEventChannel zombieDeathEventChannel;

        public event PlayerActionEventHandler OnReload;
        public event SwitchedWeaponEventHandler OnSwitchedWeapon;
        public event ActionTriggerEventHandler OnAim;
        public event ActionTriggerEventHandler OnStopAiming;
        public event ActionTriggerEventHandler OnStartAttack;
        public event ActionTriggerEventHandler OnStopAttack;
        public event SpecialItemSpawningEventHandler OnTryingToSpawnSpecialItem;
        public event SpecialItemCountOnHUDEventHandler OnSpecialItemCountChanged;
        public event AchievementSuccessfullEventHandler OnGameAchievementSucceeded;

        private void InjectPlayerController([GameObjectScope] Health health,
            [ApplicationScope] GamePadsController gamePadsController,
            [GameObjectScope] CharacterController characterController,
            [GameObjectScope] EntityRotater entityRotater,
            [Named(R.S.GameObject.Spotlight)] [EntityScope] Light flashlight,
            [Named(R.S.GameObject.LightAura)] [EntityScope] Light auraLight,
            [TagScope(R.S.Tag.MainCamera)] Camera camera,
            [TagScope(R.S.Tag.SecondCamera)] Camera camera2,
            [ChildScope] SpecialItemSensor pickableSpecialItemSensor,
            [EventChannelScope] PlayerDeathEventChannel playerDeathEventChannel,
            [EventChannelScope] PlayerRespawnEventChannel playerRespawnEventChannel,
            [EventChannelScope] ZombieDeathEventChannel zombieDeathEventChannel,
            [ApplicationScope] ImportantGameValues importantGameValues,
            [ChildScope] PlayerTeleporterSensor playerTeleporterSensor,
            [ApplicationScope] PlayerValues playerValues)

        {
            gamePadInputSensor = gamePadsController.GetSensor(PlayerNumber);
            this.health = health;
            this.characterController = characterController;
            this.entityRotater = entityRotater;
            this.flashlight = flashlight;
            this.auraLight = auraLight;
            this.camera = camera;
            this.camera2 = camera2;
            this.pickableSpecialItemSensor = pickableSpecialItemSensor;
            this.playerDeathEventChannel = playerDeathEventChannel;
            this.playerRespawnEventChannel = playerRespawnEventChannel;
            this.zombieDeathEventChannel = zombieDeathEventChannel;
            this.importantGameValues = importantGameValues;
            this.playerTeleporterSensor = playerTeleporterSensor;
            this.playerValues = playerValues;
        }

        private void Awake()
        {
            PlayerNumber = currentPlayerNumber;
            InjectDependencies("InjectPlayerController");
            currentPlayerNumber++;
            weapons = new Weapon[2];
            InstantiateWeapons();

            weapons[0] = primaryWeaponPrefab.GetComponentInChildren<Weapon>();
            weapons[1] = secondaryWeaponPrefab.GetComponentInChildren<Weapon>();

            canCollectSpecialItem = false;
            currentWeapon = weapons[0];
            currentWeapon.Equip();
            isPlayerDead = false;
            specialItemCount = specialItemLimit;
            isClassSpecificItemCollectable = false;
            isPlayerAttacking = false;
            playerValues.SetPlayerHasShotOnLevel(PlayerNumber, true);
        }

        private void OnEnable()
        {
            isPlayerDead = false;
            SubscribeToEvents(true);
            if (followingCamera == null && followingCamera2 == null)
            {
                if (importantGameValues.GameMode != GameMode.Versus)
                {
                    PlayerTeam = Team.One;
                    camera2.enabled = false;
                    followingCamera = camera.GetComponent<FollowingCamera>() ??
                                      camera.gameObject.AddComponent<FollowingCamera>();
                    followingCamera.AddTarget(gameObject);
                }
                else
                {
                    camera2.enabled = true;
                    if (PlayerNumber == PlayerNumber.One || PlayerNumber == PlayerNumber.Two)
                    {
                        PlayerTeam = Team.One;
                        camera.rect = new Rect(camera.rect.x, camera.rect.y, 0.5f, 1);
                        followingCamera = camera.GetComponent<FollowingCamera>() ??
                                          camera.gameObject.AddComponent<FollowingCamera>();
                        followingCamera.AddTarget(gameObject);
                    }
                    else
                    {
                        PlayerTeam = Team.Two;
                        followingCamera2 = camera2.GetComponent<FollowingCamera>() ??
                                           camera2.gameObject.AddComponent<FollowingCamera>();
                        followingCamera2.AddTarget(gameObject);
                    }
                }
            }

            camera.transform.rotation = Quaternion.Euler(90, 0, 0);
            camera.transform.Translate(Vector3.up * 100);
            camera2.transform.rotation = Quaternion.Euler(90, 0, 0);
            camera2.transform.Translate(Vector3.up * 100);
        }

        private void OnDisable()
        {
            isPlayerDead = true;
            currentPlayerNumber = 0;
            if (camera != null && followingCamera != null)
            {
                followingCamera.RemoveAllTargets();
                Destroy(camera.gameObject.GetComponent<FollowingCamera>());
                camera.transform.position = new Vector3(0, 20.96f, -11.01f); //Vector3 ne peux pas etre un const
                followingCamera = null;
            }

            if (camera2 != null && followingCamera2 != null)
            {
                followingCamera2.RemoveAllTargets();
                Destroy(camera2.gameObject.GetComponent<FollowingCamera>());
                camera2.transform.position = new Vector3(0, 20.96f, -11.01f); //Vector3 ne peux pas etre un const
                followingCamera2 = null;
            }

            if (gamePadInputSensor == null) return;

            UnsubscribeToEventsEndGameMinigame(true);

            currentWeapon.Unequip();
        }

        public void Initialize(Color playerColor)
        {
            health.Reset();
            auraLight.color = playerColor;
            NotifySpecialItemCountChanged();
        }

        public void SubscribeToEvents(bool subscribeToDeathAndRespawn)
        {
            if (isPlayerDead) return;
            SubscribeToBasicEvents(subscribeToDeathAndRespawn);

            gamePadInputSensor.OnTriggerPress += OnTriggerPress;
            gamePadInputSensor.OnTriggerRelease += OnTriggerRelease;
            gamePadInputSensor.OnAim += Aim;
            gamePadInputSensor.OnStopAiming += StopAiming;
        }
        
        public void SubscribeToBasicEvents(bool subscribeToDeathAndRespawn)
        {
            gamePadInputSensor.OnMove += OnMove;
            gamePadInputSensor.OnRotate += OnRotate;
            gamePadInputSensor.OnUseSpecialItem += OnUseSpecialItem;
            gamePadInputSensor.OnToggleFlashlight += OnToggleFlashlight;
            gamePadInputSensor.OnReload += ReloadShootingWeapon;
            gamePadInputSensor.OnSwitchWeapon += SwitchWeapon;
            gamePadInputSensor.OnAction += OnAction;
            health.OnHit += OnHit;
            playerTeleporterSensor.OnPlayerTeleport += OnLevelFinished;

            pickableSpecialItemSensor.OnSpecialItemDetected += OnPickableSpecialItemDetected;
            pickableSpecialItemSensor.OnSpecialItemUndetected += OnPickableSpecialItemUnDetected;

            if (!subscribeToDeathAndRespawn) return;
            playerDeathEventChannel.OnEventPublished += OnPlayerDead;
            playerRespawnEventChannel.OnEventPublished += OnPlayerRespawn;
            zombieDeathEventChannel.OnEventPublished += OnZombieKilled;
        }
        
        public void UnsubscribeToEventsEndGameMinigame(bool unsubscribeToDeathAndRespawn)
        {
            UnsubscribeFromBasicEvents(unsubscribeToDeathAndRespawn);
            
            gamePadInputSensor.OnTriggerPress -= OnTriggerPress;
            gamePadInputSensor.OnTriggerRelease -= OnTriggerRelease;
            gamePadInputSensor.OnAim -= Aim;
            gamePadInputSensor.OnStopAiming -= StopAiming;
        }

        private void UnsubscribeToEventsRespawn(bool unsubscribeToDeathAndRespawn)
        {
            UnsubscribeFromBasicEvents(unsubscribeToDeathAndRespawn);
            
            gamePadInputSensor.OnTriggerPress -= OnTriggerPress;
            gamePadInputSensor.OnTriggerRelease -= OnTriggerRelease;
        }
        
        public void UnsubscribeFromBasicEvents(bool unsubscribeFromDeathAndRespawn)
        {
            gamePadInputSensor.OnMove -= OnMove;
            gamePadInputSensor.OnRotate -= OnRotate;
            gamePadInputSensor.OnUseSpecialItem -= OnUseSpecialItem;
            gamePadInputSensor.OnToggleFlashlight -= OnToggleFlashlight;
            gamePadInputSensor.OnReload -= ReloadShootingWeapon;
            gamePadInputSensor.OnSwitchWeapon -= SwitchWeapon;
            gamePadInputSensor.OnAction -= OnAction;
            health.OnHit -= OnHit;
            playerTeleporterSensor.OnPlayerTeleport -= OnLevelFinished;

            pickableSpecialItemSensor.OnSpecialItemDetected -= OnPickableSpecialItemDetected;
            pickableSpecialItemSensor.OnSpecialItemUndetected -= OnPickableSpecialItemUnDetected;

            if (!unsubscribeFromDeathAndRespawn) return;
            playerDeathEventChannel.OnEventPublished -= OnPlayerDead;
            playerRespawnEventChannel.OnEventPublished -= OnPlayerRespawn;
            zombieDeathEventChannel.OnEventPublished -= OnZombieKilled;
        }

        public Weapon GetCurrentWeapon()
        {
            return currentWeapon;
        }

        public Weapon GetPrimaryWeapon()
        {
            return weapons[0];
        }

        public Transform GetWeaponTransform()
        {
            return weaponTransform;
        }

        public PlayerClass GetPlayerClass()
        {
            return playerClass;
        }

        public bool GetWasHit()
        {
            return wasHit;
        }

        public int GetSpecialItemCount()
        {
            return specialItemCount;
        }

        private void OnMove(Vector2 direction)
        {
            Vector3 newMotion = new Vector3(direction.x, 0f, direction.y) * player.MovementSpeed;

            if (isAiming) newMotion *= MovementSpeedMultiplier;

            characterController.Move(newMotion * Time.deltaTime);
        }

        private void PickedUpSpecialItem()
        {
            specialItemCount += importantGameValues.SpecialItemsToPickup;
            if (specialItemCount > specialItemLimit) specialItemCount = specialItemLimit;
            NotifySpecialItemCountChanged();
            importantGameValues.SpecialItemsToPickup = 0;
        }

        private void OnAction()
        {
            if (canCollectSpecialItem)
            {
                pickableSpecialItemSensor.CollectItem();
                if (isClassSpecificItemCollectable) PickedUpSpecialItem();
                isClassSpecificItemCollectable = false;
            }
            canCollectSpecialItem = false;
        }

        private void OnTriggerPress()
        {
            if (OnStartAttack == null) return;
            OnStartAttack();
            isPlayerAttacking = true;
            playerValues.SetPlayerHasShotOnLevel(PlayerNumber, false);
        }

        private void OnTriggerRelease()
        {
            if (OnStopAttack == null) return;
            isPlayerAttacking = false;
            OnStopAttack();
        }

        private void OnUseSpecialItem()
        {
            if (specialItemCount <= 0) return;
            if (OnTryingToSpawnSpecialItem != null) OnTryingToSpawnSpecialItem();
            --specialItemCount;
            NotifySpecialItemCountChanged();
        }

        private void OnRotate(Vector2 direction)
        {
            entityRotater.LookTowards(direction, player.RotationSpeed);
        }

        private void OnToggleFlashlight()
        {
            flashlight.enabled = !flashlight.enabled;
        }

        private void OnPickableSpecialItemDetected(GameObject specialItem)
        {
            switch (specialItem.name)
            {
                case R.S.GameObject.Burner + Clone:
                    if (playerClass == PlayerClass.Incinerator)
                    {
                        canCollectSpecialItem = true;
                        isClassSpecificItemCollectable = true;
                    }
                    break;
                case R.S.GameObject.FlareStick + Clone:
                    if (playerClass == PlayerClass.Scout)
                    {
                        canCollectSpecialItem = true;
                        isClassSpecificItemCollectable = true;
                    }
                    break;
                case R.S.GameObject.AssaultAmmoBox + Clone:
                    canCollectSpecialItem = gameObject.GetComponentInChildren<Ammo>().GetMaxAmmo() !=
                                            gameObject.GetComponentInChildren<Ammo>().GetAmmosLeftOnPlayer();
                    break;
                default:
                    canCollectSpecialItem = false;
                    isClassSpecificItemCollectable = false;
                    break;
            }
        }

        private void OnPickableSpecialItemUnDetected(GameObject specialItem)
        {
            canCollectSpecialItem = false;
        }

        private void ReloadShootingWeapon()
        {
            if (OnReload != null) OnReload();
        }

        private void InstantiateWeapons()
        {
            GameObject weapon = transform.Find(R.S.GameObject.Weapons).gameObject;

            if (weapon != null)
            {
                weaponTransform = weapon.transform;
                primaryWeaponPrefab = Instantiate(primaryWeaponPrefab, weaponTransform);
                secondaryWeaponPrefab = Instantiate(secondaryWeaponPrefab, weaponTransform);
            }
        }

        private void Aim()
        {
            if (OnAim != null) OnAim();
            isAiming = true;
        }

        private void StopAiming()
        {
            if (OnStopAiming != null) OnStopAiming();
            isAiming = false;
        }

        private void SwitchWeapon()
        {
            if (isPlayerAttacking) return;

            Weapon oldWeapon = currentWeapon;
            oldWeapon.Unequip();

            currentWeapon = currentWeapon == weapons[1] ? weapons[0] : weapons[1];

            NotifySwitchedWeapon(currentWeapon.WeaponType);

            currentWeapon.Equip();
        }

        private void NotifySpecialItemCountChanged()
        {
            if (OnSpecialItemCountChanged != null) OnSpecialItemCountChanged(specialItemCount, PlayerNumber);
        }

        private void NotifySwitchedWeapon(WeaponType newWeaponType)
        {
            if (OnSwitchedWeapon != null) OnSwitchedWeapon(PlayerNumber, newWeaponType);
        }

        private void OnPlayerDead(PlayerDeathEvent playerDeathEvent)
        {
            if (playerDeathEvent.DeadPlayer != GetRoot()) return;
            if (isPlayerDead) return;
            OnTriggerRelease();
            if (playerDeathEvent.DeadPlayer.GetComponent<PlayerController>().PlayerTeam == Team.One)
            {
                if (camera != null && followingCamera != null)
                {
                    followingCamera.RemoveTarget(playerDeathEvent.DeadPlayer);
                }
            }
            else
            {
                if (camera2 != null && followingCamera2 != null)
                {
                    followingCamera2.RemoveTarget(playerDeathEvent.DeadPlayer);
                }
            }

            if (gamePadInputSensor == null) return;

            UnsubscribeToEventsRespawn(false);

            transform.GetComponent<ReplacePlayerDownToZeroInYAxis>().enabled = false;
            transform.position =
                new Vector3(playerDeathEvent.DeadPlayer.transform.position.x, GameGlobalVariables.DeadYPosition,
                    playerDeathEvent.DeadPlayer.transform.position.z); //valeur en dehors de la map, comme sa pas d'instanciation/destruction au runtime
            isPlayerDead = true;
        }

        private void OnPlayerRespawn(PlayerRespawnEvent playerRespawnEvent)
        {
            if (playerRespawnEvent.DeadPlayer.GetComponent<PlayerController>().PlayerNumber != PlayerNumber || !playerRespawnEvent.IsReadyToRespawn) return;
            if (!isPlayerDead && GetRoot().transform.position.y == GameGlobalVariables.DeadYPosition)
            {
                isPlayerDead = true;
            }
            if (!isPlayerDead) return;
            isPlayerDead = false;
            SubscribeToEvents(false);

            if (playerRespawnEvent.DeadPlayer.GetComponent<PlayerController>().PlayerTeam == Team.One)
            {
                if (importantGameValues.NumberOfDeadPlayersTeam1 != 0) importantGameValues.NumberOfDeadPlayersTeam1--;
                if (followingCamera != null) followingCamera.AddTarget(gameObject);
            }
            else
            {
                if (importantGameValues.NumberOfDeadPlayersTeam2 != 0) importantGameValues.NumberOfDeadPlayersTeam2--;
                if (followingCamera2 != null) followingCamera2.AddTarget(gameObject);
            }

            health.Reset();

            transform.position = playerRespawnEvent.PositionToRespawn;
            transform.GetComponent<ReplacePlayerDownToZeroInYAxis>().enabled = true;

            if (importantGameValues.GameMode != GameMode.Versus) importantGameValues.NumberOfDeadPlayers--;
        }

        private void OnLevelFinished()
        {
            if (playerValues.Player0HasShotOnLevel && OnGameAchievementSucceeded != null)
                OnGameAchievementSucceeded(AchievementName.GameAchievement6);
            if (playerValues.Player1HasShotOnLevel && OnGameAchievementSucceeded != null)
                OnGameAchievementSucceeded(AchievementName.GameAchievement6);
            if (playerValues.Player2HasShotOnLevel && OnGameAchievementSucceeded != null)
                OnGameAchievementSucceeded(AchievementName.GameAchievement6);
            if (playerValues.Player3HasShotOnLevel && OnGameAchievementSucceeded != null)
                OnGameAchievementSucceeded(AchievementName.GameAchievement6);

            playerValues.SetPlayerHasShotOnLevel(PlayerNumber, true);
        }

        private void OnZombieKilled(ZombieDeathEvent zombieDeathEvent)
        {
            if (zombieDeathEvent.KillerPlayer == playerClass)
            {
                playerValues.IncrementPlayerKills(PlayerNumber);
            }
        }

        private void OnHit(GameObject attacker, int hitPoints, bool isExplosionHit)
        {
            switch (PlayerTeam)
            {
                case Team.One:
                    wasHit = true;
                    break;

                case Team.Two:
                    wasHit = true;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
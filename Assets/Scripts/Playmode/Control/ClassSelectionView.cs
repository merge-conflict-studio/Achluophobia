﻿using System;
using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Achluophobia
{
    public delegate void ReadyStateChangedEventhandler(bool isReady);
    public delegate void Action();

    [AddComponentMenu("Game/Control/ClassSelectionView")]
    public class ClassSelectionView : GameScript
    {
        private static readonly Color ReadyColor = Color.green;
        private static readonly Color NotReadyColor = Color.white;

        [SerializeField, Tooltip("The player team")]
        private Team team;
        
        private PlayerClass currentClass;
        private Image characterImage;
        private bool isReady;
        private ClassesInfo classesInfo;

        private BaseClassSelectionMenuController controller;

        public event ReadyStateChangedEventhandler OnReadyStateChanged;
        public event Action OnClassUnavailable;
        public event Action OnClassChanged;

        private void InjectCharacterSelectionBoxController([ApplicationScope] ClassesInfo classesInfo,
                                                           [EntityScope] BaseClassSelectionMenuController controller)
        {
            this.classesInfo = classesInfo;
            this.controller = controller;
        }

        private void Awake()
        {
            InjectDependencies("InjectCharacterSelectionBoxController");
            isReady = false;
            characterImage = GetComponentInParent<Image>();
            currentClass = 0;
            characterImage.sprite = classesInfo.GetInfo(currentClass).Sprite;
        }

        public void SetNextClass()
        {
            if (isReady) return;
            currentClass = SetNextClass(currentClass);
            characterImage.sprite = classesInfo.GetInfo(currentClass).Sprite;
            if (OnClassChanged != null) OnClassChanged();
        }

        public void SetPreviousClass()
        {
            if (isReady) return;
            currentClass = SetPreviousClass(currentClass);
            characterImage.sprite = classesInfo.GetInfo(currentClass).Sprite;
            if (OnClassChanged != null) OnClassChanged();
        }

        public void OnConfirm()
        {
            if (isReady)
            {
                isReady = false;
                controller.SetClassAvailable(team, currentClass);
                characterImage.color = NotReadyColor;
                if (OnReadyStateChanged != null) OnReadyStateChanged(isReady);
            }
            else if (!isReady)
            {
                if (controller.IsClassAvailable(team, currentClass))
                {
                    isReady = true;
                    characterImage.color = ReadyColor;
                    controller.SetClassUnavailable(team, currentClass);
                    if (OnReadyStateChanged != null) OnReadyStateChanged(isReady);
                }
                else
                {
                    if (OnClassUnavailable != null) OnClassUnavailable();
                }
            }
        }

        public PlayerClass GetCurrentPlayerClass()
        {
            return currentClass;
        }

        public void ResetState()
        {
            isReady = false;
            characterImage.color = NotReadyColor;
            currentClass = PlayerClass.Assault;
            characterImage.sprite = classesInfo.GetInfo(currentClass).Sprite;
        }

        private static PlayerClass SetNextClass(PlayerClass currentClass)
        {
            PlayerClass newClass;

            if ((int)currentClass == Enum.GetNames(typeof(PlayerClass)).Length - 1)
            {
                newClass = 0;
            }
            else newClass = currentClass + 1;

            return newClass;
        }

        private static PlayerClass SetPreviousClass(PlayerClass currentClass)
        {
            PlayerClass newClass;

            if (currentClass == 0)
            {
                newClass = (PlayerClass)(Enum.GetNames(typeof(PlayerClass)).Length - 1);
            }
            else newClass = currentClass - 1;

            return newClass;
        }
    }
}
﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Control/WitchProjectileController")]
    public class WitchProjectileController : GameScript
    {
        [SerializeField, Tooltip("The projectile data")]
        private ProjectileData projectileData;

        private ActiveHitStimulus activeHitStimulus;
        private EntityDestroyer entityDestroyer;

        private void InjectWitchProjectileController([EntityScope] ActiveHitStimulus activeHitStimulus,
                                                     [EntityScope] EntityDestroyer entityDestroyer)
        {
            this.activeHitStimulus = activeHitStimulus;
            this.entityDestroyer = entityDestroyer;
        }

        private void Awake()
        {
            InjectDependencies("InjectWitchProjectileController");
        }

        private void OnEnable()
        {
            activeHitStimulus.OnHitSensorDetected += OnWitchProjectileHit;
        }

        private void OnDisable()
        {
            activeHitStimulus.OnHitSensorDetected -= OnWitchProjectileHit;
        }

        private void OnWitchProjectileHit()
        {
            activeHitStimulus.Hit(null, projectileData.Damage);
            entityDestroyer.Destroy();
        }
    }
}
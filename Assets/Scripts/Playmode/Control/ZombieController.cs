﻿using Harmony;
using UnityEngine;
using UnityEngine.AI;

namespace Achluophobia
{
    public delegate void TargetSightEventHandler(GameObject target, bool canSeeTarget);
    public delegate void ZombieDeathEventHandler(GameObject zombie);

    [AddComponentMenu("Game/Control/ZombieController")]
    public class ZombieController : GameScript
    {
        [SerializeField, Tooltip("The zombie class contains all the information related to the zombie's statistics.")]
        private ZombieClass zombieClass;

        [SerializeField, Tooltip("The type of zombie")]
        private ZombieType zombieType;

        private const float DistanceLimit = 0.75f;

        public State State { get; set; }

        public ZombieClass ZombieClass
        {
            get { return zombieClass; }
        }

        private NavMeshAgent agent;
        protected Health health;
        private LineOfSightSensor LOSsensor;

        private ActiveHitStimulus hitStimulus;
        private HitSensor hitSensor;

        public event TargetSightEventHandler OnSightOnTargetChanged;
        public event ZombieDeathEventHandler OnZombieDeath;

        private void InjectZombieController([GameObjectScope] NavMeshAgent agent,
                                            [GameObjectScope] Health health,
                                            [ChildScope] LineOfSightSensor LOSsensor,
                                            [ChildScope] HitSensor hitSensor,
                                            [ChildScope] ActiveHitStimulus hitStimulus)
        {
            this.agent = agent;
            this.health = health;
            this.LOSsensor = LOSsensor;
            this.hitSensor = hitSensor;
            this.hitStimulus = hitStimulus;
        }

        protected void Awake()
        {
            InjectDependencies("InjectZombieController");
            State = new PatrolState(this);

            agent.speed = ZombieClass.Speed;
        }

        private void OnEnable()
        {
            LOSsensor.OnTargetEntersLOS += TargetIsInLineOfSight;
            LOSsensor.OnTargetLeavesLOS += TargetLeaveLineOfSight;
            health.OnDeath += Die;
            hitSensor.OnHit += OnHit;
        }

        private void OnHit(GameObject attacker, int hitPoints, bool isExplosionHit)
        {
            if (State is PatrolState && !isExplosionHit)
            {
                agent.SetDestination(attacker.transform.position);
            }
        }

        private void OnDisable()
        {
            LOSsensor.OnTargetEntersLOS -= TargetIsInLineOfSight;
            LOSsensor.OnTargetLeavesLOS -= TargetLeaveLineOfSight;
            health.OnDeath -= Die;
            hitSensor.OnHit -= OnHit;
        }

        private void Update()
        {
            State.Update();
        }

        public virtual void Attack()
        {
            hitStimulus.Hit(transform.root.gameObject, Random.Range(ZombieClass.MinDamage, ZombieClass.MaxDamage));
        }

        public void SetDestination(Vector3 destination)
        {
            agent.destination = destination;
        }

        public bool HasReachedDestination()
        {
            return agent.remainingDistance <= DistanceLimit || !agent.hasPath;
        }

        public void IsStopped(bool isStopped)
        {
            agent.isStopped = isStopped;
        }

        public bool TargetIsInRange()
        {
            return agent.remainingDistance < LOSsensor.GetLineOfSightRange();
        }

        private void TargetIsInLineOfSight(GameObject target)
        {
            if (OnSightOnTargetChanged != null) OnSightOnTargetChanged(target, true);
        }

        private void TargetLeaveLineOfSight(GameObject target)
        {
            if (OnSightOnTargetChanged != null) OnSightOnTargetChanged(target, false);
        }

        private void Die(GameObject killer)
        {
            State.OnDeath();
            if (OnZombieDeath != null) OnZombieDeath(gameObject);
        }
    }
}
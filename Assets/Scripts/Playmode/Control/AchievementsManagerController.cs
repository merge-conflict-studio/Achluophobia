﻿using Harmony;
using UnityEngine;

//Partie fier: Mathieu Carrier
namespace Achluophobia
{
    public delegate void GameAchievementPopupEventHandler(GameAchievements achievements);
    public delegate void ClassAchievementPopupEventHandler(ClassAchievements achievements);

    [AddComponentMenu("Game/Controller/AchievementsManagerController")]
    public class AchievementsManagerController : GameScript
    {
        private StatsManagerController statsManagerController;
        private GameController gameController;
        private GameAchievementsRepository gameAchievementsRepository;
        private ClassAchievementsRepository classAchievementsRepository;
        private AchievementSuccessEventChannel achievementSuccessEventChannel;

        public event GameAchievementPopupEventHandler OnGameAchievementToShow;
        public event ClassAchievementPopupEventHandler OnClassAchievementToShow;

        private void InjectAchievementsManagerController([EntityScope] StatsManagerController statsManagerController,
                                                         [EntityScope] GameAchievementsRepository gameAchievementsRepository,
                                                         [EntityScope] ClassAchievementsRepository classAchievementsRepository,
                                                         [SceneScope] GameController gameController,
                                                         [EventChannelScope] AchievementSuccessEventChannel achievementSuccessEventChannel)
        {
            this.statsManagerController = statsManagerController;
            this.gameAchievementsRepository = gameAchievementsRepository;
            this.classAchievementsRepository = classAchievementsRepository;
            this.gameController = gameController;
            this.achievementSuccessEventChannel = achievementSuccessEventChannel;
        }

        private void Awake()
        {
            InjectDependencies("InjectAchievementsManagerController");
        }

        private void OnEnable()
        {
            statsManagerController.OnGameAchievementSucceeded += OnGameAchievementSucceeded;
            statsManagerController.OnClassAchievementSucceeded += OnAchievementSucceeded;
            gameController.OnAchivementSuccess += OnAchievementSucceeded;
            achievementSuccessEventChannel.OnEventPublished += OnAchievementSuccess;
        }

        private void OnDisable()
        {
            statsManagerController.OnGameAchievementSucceeded -= OnGameAchievementSucceeded;
            statsManagerController.OnClassAchievementSucceeded -= OnAchievementSucceeded;
            gameController.OnAchivementSuccess -= OnAchievementSucceeded;
            achievementSuccessEventChannel.OnEventPublished -= OnAchievementSuccess;
        }

        private void OnAchievementSucceeded(string achievementName)
        {
            ClassAchievements classAchievements = classAchievementsRepository.GetAchievementByName(achievementName);
            if (!classAchievements.Succeeded)
            {
                classAchievementsRepository.ChangeSuccessValueOfClassAchievement(achievementName, true);

                if (OnClassAchievementToShow != null) OnClassAchievementToShow(classAchievements);
            }
        }

        private void OnGameAchievementSucceeded(string achievementName)
        {
            GameAchievements achievements = gameAchievementsRepository.GetAchievementByName(achievementName);
            if (!achievements.Succeeded)
            {
                gameAchievementsRepository.ChangeSuccessOfGameAchievement(achievementName, true);
                
                if (OnGameAchievementToShow != null) OnGameAchievementToShow(achievements);
            }
        }

        private void OnAchievementSuccess(AchievementSuccessEvent achievementSuccessEvent)
        {
            if (achievementSuccessEvent.IsGameAchievement)
            {
                OnGameAchievementSucceeded(achievementSuccessEvent.AchievementName);
            }
            else
            {
                OnAchievementSucceeded(achievementSuccessEvent.AchievementName);
            }
        }
    }
}

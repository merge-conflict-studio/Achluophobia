﻿using System.Collections;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public abstract class BaseClassSelectionMenuController : GameScript
    {
        [SerializeField, Tooltip("The Game Activity")]
        protected Activity gameActivity;

        [SerializeField, Tooltip("The previous menu in hierarchy")]
        protected Menu previousMenu;

        [SerializeField, Tooltip("CharacterSelectionControllers of each player")]
        protected ClassSelectionView[] classSelectionViews;

        [SerializeField, Tooltip("The sound played when the game starts")]
        protected AudioClip startGameSound;

        [SerializeField, Tooltip("The sound played when the class is unavailable")]
        protected AudioClip classUnavailableSound;

        [SerializeField, Tooltip("The sound played when the class is changed")]
        protected AudioClip classChangedSound;

        [SerializeField, Tooltip("The sound played when the player is ready")]
        protected AudioClip playerReadySound;

        [SerializeField, Tooltip("The main button press sound")]
        protected AudioClip mainButtonPressSound;

        [SerializeField, Tooltip("The start button gameobject")]
        protected GameObject startButton;

        protected int numberOfPlayersReady;
        protected GamePadsController gamePads;
        protected ActivityStack activityStack;
        protected SoundPlayer soundPlayer;
        protected ImportantGameValues importantGameValues;
        protected CanvasGroup canvasGroup;
        protected CanvasGroupFader canvasGroupFader;

        protected abstract void Awake();

        [CalledOutsideOfCode]
        public void OnBackButtonPressed()
        {
            soundPlayer.PlaySound(mainButtonPressSound);
            ReturnToLastMenu();
        }

        protected void OnClassUnavailable()
        {
            soundPlayer.PlaySound(classUnavailableSound);
        }

        protected void OnClassChanged()
        {
            soundPlayer.PlaySound(classChangedSound);
        }

        protected void ShowStartButton()
        {
            startButton.SetActive(true);
        }

        protected void HideStartButton()
        {
            startButton.SetActive(false);
        }

        protected void TryStartingGame()
        {
            if (numberOfPlayersReady != importantGameValues.NumberOfPlayers) return;

            for (int i = 0; i < importantGameValues.NumberOfPlayers; i++)
            {
                if (classSelectionViews[i] != null)
                {
                    gamePads.GetSensor((PlayerNumber)i).OnMenuUp -= classSelectionViews[i].SetNextClass;
                    gamePads.GetSensor((PlayerNumber)i).OnMenuDown -= classSelectionViews[i].SetPreviousClass;
                    gamePads.GetSensor((PlayerNumber)i).OnMenuConfirm -= classSelectionViews[i].OnConfirm;
                    gamePads.OnMenuBack -= ReturnToLastMenu;
                    importantGameValues.InitialPlayerClasses[i] = classSelectionViews[i].GetCurrentPlayerClass();
                }
            }
            importantGameValues.IsPlayerQuittingAGame = false;
            soundPlayer.PlaySound(startGameSound);

            canvasGroupFader = GetComponent<CanvasGroupFader>();
            canvasGroupFader.OnFadingFinished += StartGame;
            canvasGroupFader.FadeOut(canvasGroup);
        }

        private void StartGame()
        {
            canvasGroupFader.OnFadingFinished -= StartGame;
            activityStack.StopCurrentMenu();
            soundPlayer.Stop();
            activityStack.StartActivity(gameActivity);
        }

        protected void ReturnToLastMenu()
        {
            activityStack.StopCurrentMenu();
            activityStack.StartMenu(previousMenu);
        }

        protected void OnReadyStateChanged(bool isReady)
        {
            if (isReady)
            {
                ++numberOfPlayersReady;
                soundPlayer.PlaySound(playerReadySound);
            }
            else --numberOfPlayersReady;

            if (numberOfPlayersReady == importantGameValues.NumberOfPlayers)
            {
                ShowStartButton();
            }
            else if (startButton.activeSelf)
            {
                HideStartButton();
            }
        }

        public abstract bool IsClassAvailable(Team team, PlayerClass wantedClass);

        public abstract void SetClassAvailable(Team team, PlayerClass currentClass);

        public abstract void SetClassUnavailable(Team team, PlayerClass currentClass);

        protected abstract void ResetState();
    }
}


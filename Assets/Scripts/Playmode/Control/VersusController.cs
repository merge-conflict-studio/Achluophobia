﻿using System;
using System.Collections;
using Harmony;
using UnityEngine;

// Partie fier: Alexandre Lachance
namespace Achluophobia
{
    public delegate void PlayerRespawnEventHandler(GameObject playerToRespawn, Vector3 positionToRespawn, bool isReadyToRespawn);
    public class VersusController : GameScript
    {
        private ImportantGameValues importantGameValues;
        private const int DistanceMapTeam1ToRespawn = 0;
        private const int DistanceMapTeam2ToRespawn = 1000;
        private RespawnerData respawnerData;

        public event PlayerRespawnEventHandler OnPlayerRespawn;

        private void InjectVersusController([ApplicationScope] ImportantGameValues importantGameValues,
                                            [ApplicationScope] RespawnerData respawnerData)
        {
            this.importantGameValues = importantGameValues;
            this.respawnerData = respawnerData;
        }

        private void Awake()
        {
            InjectDependencies("InjectVersusController");
        }

        public void BothPlayerDied(Team playerTeam, int positionX, int positionZ)
        {
            StartCoroutine(RespawnBothPlayersWithDelay(playerTeam, positionX, positionZ));
        }

        public void OnePlayerDied(GameObject deadPlayer, int positionX, int positionZ)
        {
            StartCoroutine(RespawnPlayerWithDelay(deadPlayer, positionX, positionZ));
        }

        private bool CheckIfNumberOfDeadPlayerIsInaccurateOnBothPlayerDied(Team team)
        {
            switch (team)
            {
                case Team.One:
                    if (importantGameValues.NumberOfDeadPlayersTeam1 == 2 &&
                        (importantGameValues.Players[0].gameObject.transform.position.y !=
                         GameGlobalVariables.DeadYPosition ||
                         importantGameValues.Players[1].gameObject.transform.position.y !=
                         GameGlobalVariables.DeadYPosition))
                    {
                        return true;
                    }
                    break;

                case Team.Two:
                    if (importantGameValues.NumberOfDeadPlayersTeam2 == 2 &&
                        (importantGameValues.Players[2].gameObject.transform.position.y !=
                         GameGlobalVariables.DeadYPosition ||
                         importantGameValues.Players[3].gameObject.transform.position.y !=
                         GameGlobalVariables.DeadYPosition))
                    {
                        return true;
                    }
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
            return false;
        }

        private void SetCorrectDeathValuesWhenOnePlayerShouldBeAlive(Team team, int positionX, int positionZ)
        {
            switch (team)
            {
                case Team.One:
                    importantGameValues.NumberOfDeadPlayersTeam1 = 1;
                    respawnerData.RemovePlayerAndPosition(Team.One);
                    respawnerData.SetPlayerAndPosition(
                        importantGameValues.Players[0].gameObject.transform.position.y !=
                        GameGlobalVariables.DeadYPosition
                            ? importantGameValues.Players[1]
                            : importantGameValues.Players[0],
                        new Vector3(
                            positionX + (GameGlobalVariables.SpaceBetweenFloors *
                                         importantGameValues.CurrentLevelTeam1),
                            0,
                            positionZ + DistanceMapTeam1ToRespawn));
                    break;

                case Team.Two:
                    importantGameValues.NumberOfDeadPlayersTeam2 = 1;
                    respawnerData.RemovePlayerAndPosition(Team.Two);
                    respawnerData.SetPlayerAndPosition(
                        importantGameValues.Players[2].gameObject.transform.position.y !=
                        GameGlobalVariables.DeadYPosition
                            ? importantGameValues.Players[3]
                            : importantGameValues.Players[2],
                        new Vector3(
                            positionX + (GameGlobalVariables.SpaceBetweenFloors *
                                         importantGameValues.CurrentLevelTeam2),
                            0,
                            positionZ + DistanceMapTeam2ToRespawn));
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void SetBothPlayerRespawn(Team team, int positionX, int positionZ)
        {
            switch (team)
            {
                case Team.One:
                    importantGameValues.NumberOfDeadPlayersTeam1 = 0;
                    respawnerData.RemovePlayerAndPosition(Team.One);
                    NotifyPlayerRespawn(importantGameValues.Players[0],
                        new Vector3(
                            positionX + (GameGlobalVariables.SpaceBetweenFloors *
                                         importantGameValues.CurrentLevelTeam1),
                            0,
                            positionZ + DistanceMapTeam1ToRespawn),
                        true);
                    NotifyPlayerRespawn(importantGameValues.Players[1],
                        new Vector3(
                            positionX + (GameGlobalVariables.SpaceBetweenFloors *
                                         importantGameValues.CurrentLevelTeam1) +
                            GameGlobalVariables.MinOffsetBetweenPlayerSpawn,
                            0,
                            positionZ + DistanceMapTeam1ToRespawn),
                        true);
                    break;

                case Team.Two:
                    importantGameValues.NumberOfDeadPlayersTeam2 = 0;
                    respawnerData.RemovePlayerAndPosition(Team.Two);
                    NotifyPlayerRespawn(importantGameValues.Players[2],
                        new Vector3(
                            positionX + (GameGlobalVariables.SpaceBetweenFloors *
                                         importantGameValues.CurrentLevelTeam2),
                            0,
                            positionZ + DistanceMapTeam2ToRespawn),
                        true);
                    NotifyPlayerRespawn(importantGameValues.Players[3],
                        new Vector3(
                            positionX + (GameGlobalVariables.SpaceBetweenFloors *
                                         importantGameValues.CurrentLevelTeam2) +
                            GameGlobalVariables.MinOffsetBetweenPlayerSpawn,
                            0,
                            positionZ + DistanceMapTeam2ToRespawn),
                        true);
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private IEnumerator RespawnBothPlayersWithDelay(Team playerTeam, int positionX, int positionZ)
        {
            yield return new WaitForSeconds(GameGlobalVariables.RespawnTime);
           
            switch (playerTeam)
            {
                case Team.One:
                    if (CheckIfNumberOfDeadPlayerIsInaccurateOnBothPlayerDied(Team.One))
                    {
                        SetCorrectDeathValuesWhenOnePlayerShouldBeAlive(Team.One, positionX, positionZ);
                    }
                    else
                    {
                        SetBothPlayerRespawn(Team.One, positionX, positionZ);
                    }
                    break;

                case Team.Two:
                    if (CheckIfNumberOfDeadPlayerIsInaccurateOnBothPlayerDied(Team.Two))
                    {
                        SetCorrectDeathValuesWhenOnePlayerShouldBeAlive(Team.Two, positionX, positionZ);
                    }
                    else
                    {
                       SetBothPlayerRespawn(Team.Two, positionX, positionZ);
                    }
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private bool CheckIfNumberOfDeadPlayerIsInaccurateOnOnePlayerDied(Team playerTeam)
        {
            switch (playerTeam)
            {
                case Team.One:
                    if (importantGameValues.Players[0].gameObject.transform.position.y ==
                        GameGlobalVariables.DeadYPosition &&
                        importantGameValues.Players[1].gameObject.transform.position.y ==
                        GameGlobalVariables.DeadYPosition && importantGameValues.NumberOfDeadPlayersTeam1 == 1)
                    {
                        return true;
                    }
                    break;
                
               case Team.Two:
                   if (importantGameValues.Players[2].gameObject.transform.position.y ==
                       GameGlobalVariables.DeadYPosition &&
                       importantGameValues.Players[3].gameObject.transform.position.y ==
                       GameGlobalVariables.DeadYPosition && importantGameValues.NumberOfDeadPlayersTeam2 == 1)
                   {
                       return true;
                   }
                   break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
            return false;
        }

        private IEnumerator RespawnPlayerWithDelay(GameObject playerToSpawn, int positionX, int positionZ)
        {
            yield return new WaitForSeconds(GameGlobalVariables.RespawnTime);

            switch (playerToSpawn.GetComponent<PlayerController>().PlayerTeam)
            {
                case Team.One:
                    if (CheckIfNumberOfDeadPlayerIsInaccurateOnOnePlayerDied(Team.One))
                    {
                        SetBothPlayerRespawn(Team.One, positionX, positionZ);
                    }
                    else
                    {
                        respawnerData.SetPlayerAndPosition(playerToSpawn,
                            new Vector3(
                                positionX +
                                (GameGlobalVariables.SpaceBetweenFloors * importantGameValues.CurrentLevelTeam1) +
                                GameGlobalVariables.MinOffsetBetweenPlayerSpawn, 0,
                                positionZ + DistanceMapTeam1ToRespawn));
                    }
                    break;

                case Team.Two:
                    if (CheckIfNumberOfDeadPlayerIsInaccurateOnOnePlayerDied(Team.Two))
                    {
                        SetBothPlayerRespawn(Team.Two, positionX, positionZ);
                    }
                    else
                    {
                        respawnerData.SetPlayerAndPosition(playerToSpawn,
                            new Vector3(
                                positionX +
                                (GameGlobalVariables.SpaceBetweenFloors * importantGameValues.CurrentLevelTeam2) +
                                GameGlobalVariables.MinOffsetBetweenPlayerSpawn, 0, positionZ + DistanceMapTeam2ToRespawn));
                    }
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void NotifyPlayerRespawn(GameObject playerToSpawn, Vector3 position, bool isReadyToRespawn)
        {
            if (OnPlayerRespawn != null) OnPlayerRespawn(playerToSpawn, position, isReadyToRespawn);
        }
    }
}

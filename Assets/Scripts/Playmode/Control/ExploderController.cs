﻿using Harmony;
using System.Collections.Generic;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Controller/ExploderController")]
    public class ExploderController : GameScript
    {
        [SerializeField, Tooltip("The effect of the explosion")]
        private InstantEffect effect;

        [SerializeField, Tooltip("The explosion prefab")]
        private GameObject explosionPrefab;

        private ExplosionStimulus explosionStimulus;
        private DestroyAfterDelay destroyer;
        private List<ExplosionSensor> explosionTargets;
        public GameObject Owner { get; set; }

        private void InjectExploderController([EntityScope] ExplosionStimulus explosionStimulus,
                                              [EntityScope] DestroyAfterDelay destroyer)
        {
            this.explosionStimulus = explosionStimulus;
            this.destroyer = destroyer;
        }

        private void Awake()
        {
            InjectDependencies("InjectExploderController");
            explosionTargets = new List<ExplosionSensor>();
        }

        private void OnEnable()
        {
            explosionStimulus.OnExplosionSensorDetected += AddSensorToList;
            explosionStimulus.OnExplosionSensorUndetected += RemoveSensorFromList;
            destroyer.OnDestructionTimerOut += DetonateExplosion;
        }

        private void OnDisable()
        {
            explosionStimulus.OnExplosionSensorDetected -= AddSensorToList;
            explosionStimulus.OnExplosionSensorUndetected -= RemoveSensorFromList;
            destroyer.OnDestructionTimerOut -= DetonateExplosion;
        }

        

        private void AddSensorToList(ExplosionSensor explosionSensor)
        {
            explosionTargets.Add(explosionSensor);
        }

        private void RemoveSensorFromList(ExplosionSensor explosionSensor)
        {
            explosionTargets.Remove(explosionSensor);
        }

        private void DetonateExplosion()
        {
            Instantiate(explosionPrefab, transform.position, transform.rotation);
            foreach (ExplosionSensor explosionSensor in explosionTargets)
            {
                explosionSensor.ExplosionHit(effect);
            }
        }
    }
}

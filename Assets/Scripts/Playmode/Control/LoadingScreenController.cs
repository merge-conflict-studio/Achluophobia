﻿using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Achluophobia
{
    [AddComponentMenu("Game/Control/LoadingScreenController")]
    public class LoadingScreenController : GameScript
    {
        [SerializeField, Tooltip("The start button gameobject")]
        private GameObject startButton;

        [SerializeField, Tooltip("The loading text")]
        private Text loadingText;

        [SerializeField, Tooltip("The control image")]
        private Image controlImage;

        private Canvas loadingScreenCanvas;
        private ActivityStack activityStack;
        private ImportantGameValues importantGameValues;
        private GamePadsController gamePads;

        private void InjectLoadingScreenController([EntityScope] Canvas loadingScreenCanvas,
                                                   [ApplicationScope] GamePadsController gamePads,
                                                   [ApplicationScope] ImportantGameValues importantGameValues,
                                                   [ApplicationScope] ActivityStack activityStack)
        {
            this.loadingScreenCanvas = loadingScreenCanvas;
            this.gamePads = gamePads;
            this.importantGameValues = importantGameValues;
            this.activityStack = activityStack;
        }

        private void Awake()
        {
            InjectDependencies("InjectLoadingScreenController");

            loadingScreenCanvas.enabled = false;
            activityStack.OnActivityLoadingStarted += OnActivityLoadStart;
            activityStack.OnActivityLoadingEnded += OnActivityLoadEnd;
            HideStartButton();
        }

        private void OnDestroy()
        {
            activityStack.OnActivityLoadingStarted -= OnActivityLoadStart;
            activityStack.OnActivityLoadingEnded -= OnActivityLoadEnd;
        }

        private void OnActivityLoadStart()
        {
            controlImage.enabled = !importantGameValues.IsPlayerQuittingAGame;
            loadingScreenCanvas.enabled = true;
            loadingText.enabled = true;
            FreezeTime();
        }

        private void OnActivityLoadEnd()
        {
            if (!importantGameValues.IsPlayerQuittingAGame)
            {
                gamePads.OnPause += OnGameStartButtonClicked;
                ShowStartButton();
            }
            else
            {
                HideLoadingScreen();
            }
        }

        private void HideLoadingScreen()
        {
            loadingScreenCanvas.enabled = false;
            UnFreezeTime();
        }
        private void OnGameStartButtonClicked()
        {
            gamePads.OnPause -= OnGameStartButtonClicked;
            HideStartButton();
            HideLoadingScreen();
        }

        private void ShowStartButton()
        {
            startButton.SetActive(true);
            loadingText.enabled = false;
        }

        private void HideStartButton()
        {
            startButton.SetActive(false);
        }

        private void FreezeTime()
        {
            TimeExtensions.Pause();
        }

        private void UnFreezeTime()
        {
            TimeExtensions.Resume();
        }
    }
}
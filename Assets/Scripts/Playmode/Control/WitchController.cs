﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Control/WitchController")]
    public class WitchController : ZombieController
    {
        private GameObjectSpawner gameObjectSpawner;

        private new void Awake()
        {
            base.Awake();

            if (gameObject.GetRoot().GetComponentInChildren<GameObjectSpawner>() != null)
            {
                gameObjectSpawner = gameObject.GetRoot().GetComponentInChildren<GameObjectSpawner>();
            }
        }

        public override void Attack()
        {
            gameObjectSpawner.Spawn(gameObject.transform);
        }
    }
}

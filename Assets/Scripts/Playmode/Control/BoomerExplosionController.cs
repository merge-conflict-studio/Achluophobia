﻿using Harmony;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Achluophobia
{
    [AddComponentMenu("Game/Controller/BoomerExplosionController")]
    public class BoomerExplosionController : GameScript
    {
        [SerializeField, Tooltip("The effect of the explosion")]
        private InstantEffect effect;
        
        private ExplosionStimulus explosionStimulus;
        private List<ExplosionSensor> explosionTargets;
        private Health health;

        private void InjectBoomerExplosionController([EntityScope] ExplosionStimulus explosionStimulus,
                                                     [EntityScope] Health health)
        {
            this.explosionStimulus = explosionStimulus;
            this.health = health;
        }

        private void Awake()
        {
            InjectDependencies("InjectBoomerExplosionController");
            explosionTargets = new List<ExplosionSensor>();
        }

        private void OnEnable()
        {
            explosionStimulus.OnExplosionSensorDetected += AddSensorToList;
            explosionStimulus.OnExplosionSensorUndetected += RemoveSensorFromList;
            health.OnDeath += DetonateExplosion;
        }
        
        private void OnDisable()
        {
            explosionStimulus.OnExplosionSensorDetected -= AddSensorToList;
            explosionStimulus.OnExplosionSensorUndetected -= RemoveSensorFromList;
            health.OnDeath -= DetonateExplosion;
        }

        private void AddSensorToList(ExplosionSensor explosionSensor)
        {
            explosionTargets.Add(explosionSensor);
        }

        private void RemoveSensorFromList(ExplosionSensor explosionSensor)
        {
            explosionTargets.Remove(explosionSensor);
        }

        private void DetonateExplosion(GameObject killer)
        {
            foreach (ExplosionSensor explosionSensor in explosionTargets)
            {
                explosionSensor.ExplosionHit(effect);
            }
        }
    }
}
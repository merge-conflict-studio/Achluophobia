﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Control/MainActivityController")]
    public class MainActivityController : GameScript, IActivityController
    {
        [SerializeField, Tooltip("The main menu, which will be loaded first")] 
        private Menu mainMenu;

        [SerializeField, Tooltip("The main menu, which will be loaded first")]
        private AudioClip mainMenuSoundtrack;

        private ActivityStack activityStack;
        private SoundPlayer soundPlayer;

        private void InjectMainActivityController([ApplicationScope] ActivityStack activityStack,
                                                  [ApplicationScope] SoundPlayer soundPlayer,
                                                  [TagScope(R.S.Tag.MainCamera)] AudioSource audioSource)
        {
            this.activityStack = activityStack;
            this.soundPlayer = soundPlayer;
            soundPlayer.SetAudioSource(audioSource);
        }

        private void Awake()
        {
            InjectDependencies("InjectMainActivityController");
        }

        private void Start()
        {
            soundPlayer.PlayMusicInLoop(mainMenuSoundtrack);
        }

        public void OnCreate()
        {
            activityStack.StartMenu(mainMenu);
        }

        public void OnStop()
        {
            //Do nothing
        }
    }
}
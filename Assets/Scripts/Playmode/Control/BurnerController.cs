﻿using Harmony;
using System.Collections;
using UnityEngine;

namespace Achluophobia
{
    [AddComponentMenu("Game/Controller/BurnerController")]
    public class BurnerController : GameScript
    {
        [SerializeField, Tooltip("The ammount of fuel depleted per seconds")]
        private int fuelDepletedPerSeconds;

        [SerializeField, Tooltip("The max ammount of fuel taken from the ammos for the burner")]
        private int maxFuelTakenFromAmmos;

        private SpecialItemStimulus pickableSpecialItemStimulus;
        private SpecialItemSensor incineratorSensor;
        private Light burnerLight;
        private int fuelLeftInBurner;
        private MeshRenderer meshRenderer;

        private void InjectBurnerController([EntityScope] SpecialItemStimulus pickableSpecialItemStimulus,
                                            [EntityScope] Light burnerLight,
                                            [Named(R.S.GameObject.ViewVisualisation)][EntityScope] MeshRenderer meshRenderer)
        {
            this.pickableSpecialItemStimulus = pickableSpecialItemStimulus;
            this.burnerLight = burnerLight;
            this.meshRenderer = meshRenderer;
        }

        private void Awake()
        {
            InjectDependencies("InjectBurnerController");
            fuelLeftInBurner = 0;
            burnerLight.enabled = false;
        }

        private void OnEnable()
        {
            pickableSpecialItemStimulus.OnIncineratorDetected += StartBurner;
            pickableSpecialItemStimulus.OnSpecialItemPicked += ReCollectAmmos;
        }

        private void OnDisable()
        {
            pickableSpecialItemStimulus.OnIncineratorDetected -= StartBurner;
            pickableSpecialItemStimulus.OnSpecialItemPicked -= ReCollectAmmos;
        }

        private void StartBurner(SpecialItemSensor incinerator)
        {
            meshRenderer.enabled = true;
            incineratorSensor = incinerator;
            Ammo fuel = incineratorSensor.GetRoot().GetComponentInChildren<Ammo>();
            fuelLeftInBurner = fuel.DepleteForBurner(maxFuelTakenFromAmmos);
            if (fuelLeftInBurner > 0)
            {
                StartCoroutine(BurnerLighting());
            }
        }

        private void ReCollectAmmos()
        {
            meshRenderer.enabled = false;
            if (incineratorSensor == null) return;
            Ammo fuel = incineratorSensor.GetRoot().GetComponentInChildren<Ammo>();
            if (fuelLeftInBurner > 0)
            {
                fuel.PickUpAmmo(fuelLeftInBurner);
            }
        }
        
        private IEnumerator BurnerLighting()
        {
            burnerLight.enabled = true;
            while (fuelLeftInBurner > 0)
            {
                yield return new WaitForSeconds(1);
                fuelLeftInBurner -= fuelDepletedPerSeconds;
            }
            meshRenderer.enabled = false;
            burnerLight.enabled = false;
            fuelLeftInBurner = 0;
        }
    }
}

﻿using System;
using Harmony;
using System.Collections;
using UnityEngine;

// Philippe Deschênes : Ma partie du GameController

namespace Achluophobia
{
    public delegate void GameFinishedEvendHandler(EndingType endingType, PlayerClass[] players);
    public delegate void GameStartEventHandler();
    public delegate void AchivementSuccess(string achievementName);

    [AddComponentMenu("Game/Control/GameController")]
    public class GameController : GameScript, IFragmentController
    {
        private const string Level = "Level";

        private const string Team2 = "Team2";

        [SerializeField, Tooltip("The floor generators")]
        private FloorGenerator[] floorGenerators;

        [SerializeField, Tooltip("The player spawner")]
        private PlayerSpawner playerSpawner;

        [SerializeField, Tooltip("The game over menu")]
        private Menu gameOverMenu;

        [SerializeField, Tooltip("The pause menu")]
        private Menu pauseMenu;

        [SerializeField, Tooltip("The menu used at the end of the game")]
        private Menu endGameMenu;

        [SerializeField, Tooltip("The ambiant soundtrack")]
        private AudioClip ambiantSoundtrack;

        private ActivityStack activityStack;
        private ImportantGameValues importantGameValues;
        private GameObject[] mapCopy;
        private GamePauseEventChannel gamePauseEventChannel;
        private PlayerDeathEventChannel playerDeathEventChannel;
        private PlayerTeleporterEventChannel playerTeleporterEventChannel;
        private SoundPlayer soundPlayer;
        private VersusController versusController2;
        private VersusController versusController1;
        private RespawnerData respawnerData;

        public event GameFinishedEvendHandler OnGameOver;
        public event GameStartEventHandler OnGameStart;
        public event AchivementSuccess OnAchivementSuccess;

        private void InjectMapGenerationController([ApplicationScope] ActivityStack activityStack,
                                                   [ApplicationScope] ImportantGameValues importantGameValues,
                                                   [ApplicationScope] SoundPlayer soundPlayer,
                                                   [EventChannelScope] GamePauseEventChannel gamePauseEventChannel,
                                                   [EventChannelScope] PlayerDeathEventChannel playerDeathEventChannel,
                                                   [EventChannelScope] PlayerTeleporterEventChannel playerTeleporterEventChannel,
                                                   [Named(R.S.GameObject.VersusController1)][SceneScope] VersusController versusController1,
                                                   [Named(R.S.GameObject.VersusController2)][SceneScope] VersusController versusController2,
                                                   [ApplicationScope] RespawnerData respawnerData)
        {
            this.activityStack = activityStack;
            this.importantGameValues = importantGameValues;
            this.soundPlayer = soundPlayer;
            this.gamePauseEventChannel = gamePauseEventChannel;
            this.playerDeathEventChannel = playerDeathEventChannel;
            this.playerTeleporterEventChannel = playerTeleporterEventChannel;
            this.versusController1 = versusController1;
            this.versusController2 = versusController2;
            this.respawnerData = respawnerData;
        }

        private void Awake()
        {
            InjectDependencies("InjectMapGenerationController");

            importantGameValues.NumberOfDeadPlayersTeam1 = 0;
            importantGameValues.NumberOfDeadPlayersTeam2 = 0;
            importantGameValues.NumberOfDeadPlayers = 0;
        }

        private void OnEnable()
        {
            playerDeathEventChannel.OnEventPublished += OnPlayerDeath;
            gamePauseEventChannel.OnEventPublished += OnGamePause;
            playerTeleporterEventChannel.OnEventPublished += OnGameFinished;
        }

        private void OnDisable()
        {
            playerDeathEventChannel.OnEventPublished -= OnPlayerDeath;
            gamePauseEventChannel.OnEventPublished -= OnGamePause;
            playerTeleporterEventChannel.OnEventPublished -= OnGameFinished;
        }

        public void OnCreate()
        {
            if (OnGameStart != null) OnGameStart();
            importantGameValues.NumberOfDeadPlayers = 0;
            importantGameValues.CurrentLevelTeam1 = 0;
            importantGameValues.CurrentLevelTeam2 = 0;
            foreach (FloorGenerator floorGenerator in floorGenerators)
            {
                floorGenerator.Generate();
            }

            switch (importantGameValues.GameMode)
            {
                case GameMode.Solo:
                    importantGameValues.Players = new GameObject[1];
                    playerSpawner.Spawn(importantGameValues.InitialPlayerClasses[0],
                                        new Vector3(floorGenerators[0].GetSpawnRoomPosition(),
                                                    0,
                                                    floorGenerators[0].GetSpawnRoomPosition()));
                    break;

                case GameMode.Coop:
                    importantGameValues.Players = new GameObject[2];
                    playerSpawner.Spawn(importantGameValues.InitialPlayerClasses[0],
                        new Vector3(floorGenerators[0].GetSpawnRoomPosition(),
                            0,
                            floorGenerators[0].GetSpawnRoomPosition()));
                    playerSpawner.Spawn(importantGameValues.InitialPlayerClasses[1],
                        new Vector3(floorGenerators[0].GetSpawnRoomPosition() + GameGlobalVariables.TileSize,
                            0,
                            floorGenerators[0].GetSpawnRoomPosition() + GameGlobalVariables.TileSize));
                    break;
                case GameMode.Versus:

                    CopyMap();

                    importantGameValues.Players = new GameObject[4];
                    playerSpawner.Spawn(importantGameValues.InitialPlayerClasses[0],
                        new Vector3(floorGenerators[0].GetSpawnRoomPosition(),
                            0,
                            floorGenerators[0].GetSpawnRoomPosition()));
                    playerSpawner.Spawn(importantGameValues.InitialPlayerClasses[1],
                        new Vector3(floorGenerators[0].GetSpawnRoomPosition() + GameGlobalVariables.TileSize,
                            0,
                            floorGenerators[0].GetSpawnRoomPosition() + GameGlobalVariables.TileSize));
                    playerSpawner.Spawn(importantGameValues.InitialPlayerClasses[2],
                        new Vector3(floorGenerators[0].GetSpawnRoomPosition(),
                            0,
                            floorGenerators[0].GetSpawnRoomPosition() + GameGlobalVariables.DistanceNextMap2v2));
                    playerSpawner.Spawn(importantGameValues.InitialPlayerClasses[3],
                        new Vector3(floorGenerators[0].GetSpawnRoomPosition() + GameGlobalVariables.TileSize,
                            0,
                            floorGenerators[0].GetSpawnRoomPosition() + GameGlobalVariables.DistanceNextMap2v2 + GameGlobalVariables.TileSize));

                    ChangeTag();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            floorGenerators[0].GenerateNavMeshBuilder();
            for (int i = 1; i < floorGenerators.Length; i++)
            {
                floorGenerators[i].GetComponent<ZombieGenerator>().DeactivateAllZombies();
            }
            soundPlayer.PlayMusicInLoop(ambiantSoundtrack);
        }

        public void OnStop()
        {
            // Do nothing
        }

        private void OnGamePause(GamePauseEvent gamePauseEvent)
        {
            if (Time.timeScale != 0) activityStack.StartMenu(pauseMenu);
        }

        private void OnPlayerDeath(PlayerDeathEvent playerDeathEvent)
        {
            if (importantGameValues.GameMode != GameMode.Versus)
            {
                importantGameValues.NumberOfDeadPlayers++;
            }
            if (importantGameValues.NumberOfDeadPlayers >= importantGameValues.NumberOfPlayers &&
                importantGameValues.GameMode != GameMode.Versus)
            {
                activityStack.StopCurrentMenu();
                importantGameValues.EndingType = EndingType.GameFailed;
                activityStack.StartMenu(gameOverMenu);
                if (OnGameOver != null) OnGameOver(EndingType.GameFailed, importantGameValues.InitialPlayerClasses);
            }
            else if (importantGameValues.GameMode == GameMode.Versus)
            {
                if (playerDeathEvent.DeadPlayer.GetComponent<PlayerController>().PlayerTeam == Team.One)
                {
                    importantGameValues.NumberOfDeadPlayersTeam1++;
                    if (importantGameValues.NumberOfDeadPlayersTeam1 > 2)
                        importantGameValues.NumberOfDeadPlayersTeam1 = 2;
                    switch (importantGameValues.NumberOfDeadPlayersTeam1)
                    {
                        case 1:
                            versusController1.OnePlayerDied(playerDeathEvent.DeadPlayer,
                                                            floorGenerators[importantGameValues.CurrentLevelTeam1].GetSpawnRoomPosition(),
                                                            floorGenerators[0].GetSpawnRoomPosition());
                            break;
                        case 2:
                            versusController1.BothPlayerDied(Team.One, 
                                                             floorGenerators[importantGameValues.CurrentLevelTeam1].GetSpawnRoomPosition(),
                                                             floorGenerators[0].GetSpawnRoomPosition());
                            break;
                    }
                }
                else
                {
                    importantGameValues.NumberOfDeadPlayersTeam2++;
                    if (importantGameValues.NumberOfDeadPlayersTeam2 > 2)
                        importantGameValues.NumberOfDeadPlayersTeam2 = 2;
                    switch (importantGameValues.NumberOfDeadPlayersTeam2)
                    {
                        case 1:
                            versusController2.OnePlayerDied(playerDeathEvent.DeadPlayer,
                                                            floorGenerators[importantGameValues.CurrentLevelTeam2].GetSpawnRoomPosition(),
                                                            floorGenerators[0].GetSpawnRoomPosition());
                            break;
                        case 2:
                            versusController2.BothPlayerDied(Team.Two, 
                                                             floorGenerators[importantGameValues.CurrentLevelTeam2].GetSpawnRoomPosition(),
                                                             floorGenerators[0].GetSpawnRoomPosition());
                            break;
                    }
                }
            }
            else
            {
                StartCoroutine(RespawnPlayerWithDelay(playerDeathEvent.DeadPlayer));
            }
        }

        private void OnGameFinished(PlayerTeleporterEvent playerTeleporterEvent)
        {
            if (playerTeleporterEvent.PlayerTeleporter.tag.Substring(0, 6) != R.S.Tag.Level4) return;
            switch (importantGameValues.GameMode)
            {
                case GameMode.Solo:
                    importantGameValues.EndingType = EndingType.SoloFinished;
                    if (OnGameOver != null) OnGameOver(EndingType.SoloFinished, importantGameValues.InitialPlayerClasses);
                    break;
                case GameMode.Coop:
                    importantGameValues.EndingType = EndingType.CoopFinished;
                    if (OnGameOver != null) OnGameOver(EndingType.CoopFinished, importantGameValues.InitialPlayerClasses);
                    break;
                case GameMode.Versus:
                    importantGameValues.EndingType = EndingType.VersusFinished;
                    importantGameValues.WinningTeam = playerTeleporterEvent.Player.GetComponent<PlayerController>().PlayerTeam == Team.One ? 1 : 2;
                    if (OnGameOver != null) OnGameOver(EndingType.VersusFinished, importantGameValues.InitialPlayerClasses);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            foreach (GameObject player in importantGameValues.Players)
            {
                if (!player.GetComponent<PlayerController>().GetWasHit()
                    && player.GetComponent<PlayerController>().GetPlayerClass() == PlayerClass.Scout)
                {
                    if (OnAchivementSuccess != null) OnAchivementSuccess(AchievementName.ScoutAchievement3);
                }
            }
            
            activityStack.StartMenu(endGameMenu);
        }

        private void CopyMap()
        {
            mapCopy = new GameObject[floorGenerators.Length];
            for (int i = 0; i < floorGenerators.Length; i++)
            {
                GameObject levelCopy = GameObject.Find(GameGlobalVariables.Level + i);
                mapCopy[i] = Instantiate(levelCopy, new Vector3(0, 0, GameGlobalVariables.DistanceNextMap2v2), Quaternion.identity);
                ZombieController[] zombieControllers = mapCopy[i].GetComponentsInChildren<ZombieController>();

                foreach (ZombieController zombieController in zombieControllers)
                {
                    floorGenerators[i].GetComponent<ZombieGenerator>().AddZombieCopy(zombieController.GetRoot());
                }
            }
        }

        private void ChangeTag()
        {
            for (int i = 0; i < mapCopy.Length; i++)
            {
                mapCopy[i].tag = Level + i + Team2;

                QuestItemStimulus[] questItemStimulus = mapCopy[i].GetComponentsInChildren<QuestItemStimulus>();

                QuestItemsData[] questItemsDatas = mapCopy[i].GetComponentsInChildren<QuestItemsData>();

                MiniGameData[] miniGameDatas = mapCopy[i].GetComponentsInChildren<MiniGameData>();

                PlayerTeleporterStimulus[] playerTeleporterStimulus = mapCopy[i].GetComponentsInChildren<PlayerTeleporterStimulus>();

                MiniGameController[] miniGameControllers = mapCopy[i].GetComponentsInChildren<MiniGameController>();

                foreach (QuestItemStimulus questItem in questItemStimulus)
                {
                    questItem.gameObject.GetRoot().tag = Level + i + Team2;
                    questItem.gameObject.tag = Level + i + Team2;
                }

                foreach (QuestItemsData questItem in questItemsDatas)
                {
                    questItem.LevelName = Level + i + Team2;
                }

                foreach (MiniGameData miniGameData in miniGameDatas)
                {
                    miniGameData.LevelName = Level + i + Team2;
                }

                foreach (PlayerTeleporterStimulus playerTeleporterStimulu in playerTeleporterStimulus)
                {
                    playerTeleporterStimulu.GetRoot().tag = Level + i + Team2;
                    playerTeleporterStimulu.tag = Level + i + Team2;
                }

                foreach (MiniGameController minigame in miniGameControllers)
                {
                    minigame.GetRoot().tag = Level + i + Team2;
                    minigame.tag = Level + i + Team2;
                }
            }
        }

        private IEnumerator RespawnPlayerWithDelay(GameObject playerToSpawn)
        {
            yield return new WaitForSeconds(GameGlobalVariables.RespawnTime);
            
            respawnerData.SetPlayerAndPosition(playerToSpawn,
                new Vector3(
                    floorGenerators[0].GetSpawnRoomPosition() +
                    (GameGlobalVariables.SpaceBetweenFloors * importantGameValues.CurrentLevelTeam1) +
                    GameGlobalVariables.MinOffsetBetweenPlayerSpawn, 0,
                    floorGenerators[importantGameValues.CurrentLevelTeam1].GetSpawnRoomPosition()));
        }
    }
}
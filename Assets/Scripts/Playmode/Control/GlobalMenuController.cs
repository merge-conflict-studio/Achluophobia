﻿using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Achluophobia
{
    [AddComponentMenu("Game/Control/GlobalMenuController")]
    public class GlobalMenuController : GameScript
    {
        private GamePadsController gamePadsController;

        private void InjectGlobalMenuController([ApplicationScope] GamePadsController gamePadsController)
        {
            this.gamePadsController = gamePadsController;
        }

        private void Awake()
        {
            InjectDependencies("InjectGlobalMenuController");

            gamePadsController.OnMenuUp += OnUp;
            gamePadsController.OnMenuDown += OnDown;
            gamePadsController.OnMenuLeft += OnLeft;
            gamePadsController.OnMenuRight += OnRight;
            gamePadsController.OnMenuConfirm += OnConfirm;
        }

        private void OnDestroy()
        {
            gamePadsController.OnMenuUp -= OnUp;
            gamePadsController.OnMenuDown -= OnDown;
            gamePadsController.OnMenuLeft -= OnLeft;
            gamePadsController.OnMenuRight -= OnRight;
            gamePadsController.OnMenuConfirm -= OnConfirm;
        }

        private static void OnUp()
        {
            Selectable currentSelectable = SelectableExtensions.GetCurrentlySelected();
            if (currentSelectable != null)
            {
                currentSelectable.SelectUp();
            }
        }

        private static void OnDown()
        {
            Selectable currentSelectable = SelectableExtensions.GetCurrentlySelected();
            if (currentSelectable != null)
            {
                currentSelectable.SelectDown();
            }
        }

        private static void OnLeft()
        {
            Selectable currentSelectable = SelectableExtensions.GetCurrentlySelected();

            if (currentSelectable == null) return;
            if (currentSelectable is Slider)
            {
                currentSelectable.GetComponent<Slider>().value -= 0.1f;
            }
            else
            {
                currentSelectable.SelectLeft();
            }
        }

        private static void OnRight()
        {
            Selectable currentSelectable = SelectableExtensions.GetCurrentlySelected();
            if (currentSelectable == null) return;
            if (currentSelectable is Slider)
            {
                currentSelectable.GetComponent<Slider>().value += 0.1f;
            }
            else
            {
                currentSelectable.SelectRight();
            }
        }

        private void OnConfirm()
        {
            Selectable currentSelectable = SelectableExtensions.GetCurrentlySelected();
            if (currentSelectable != null)
            {
                currentSelectable.Click();
            }
        }
    }
}
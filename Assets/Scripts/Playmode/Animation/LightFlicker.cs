﻿using System.Collections;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public class LightFlicker : MonoBehaviour
    {
        [SerializeField, Tooltip("The sound played when the light is on")]
        private AudioClip lightOnSound;

        private const float MaximumLightOffDelay = 0.2f;
        private const float MinimumDelay = 0.05f;
        private const float MaximumLightOnDelay = 1.8f;

        private Light light;
        private float defaultIntensity;
        private SoundPlayer soundPlayer;
        private Coroutine flickerCoroutine;

        private void Awake()
        {
            light = GetComponent<Light>();
            soundPlayer = GetComponent<SoundPlayer>();
            soundPlayer.SetAudioSource(GetComponent<AudioSource>());

            defaultIntensity = light.intensity;
            StartFlicker();
        }

        public AudioClip GetAudioClip()
        {
            return lightOnSound;
        }

        public void StopFlicker()
        {
            StopCoroutine(flickerCoroutine);
            gameObject.GetRoot().SetActive(false);
        }

        public void StartFlicker()
        {
            gameObject.GetRoot().SetActive(true);
            flickerCoroutine = StartCoroutine(FlickerAtRandom());
        }

        private IEnumerator FlickerAtRandom()
        {
            while(true)
            {                
                light.intensity = defaultIntensity;
                soundPlayer.PlayMusicInLoop(lightOnSound);
                yield return new WaitForSeconds(Random.Range(MinimumDelay, MaximumLightOnDelay));

                light.intensity = 0;
                soundPlayer.Stop();
                yield return new WaitForSeconds(Random.Range(MinimumDelay, MaximumLightOffDelay));
            }
        }
    }
}
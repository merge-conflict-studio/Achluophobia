﻿using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Achluophobia
{
    //SOURCE : https://forum.unity.com/threads/waiting-for-seconds-in-update.19347/
    public class GifAnimation : GameScript
    {
        [SerializeField, Tooltip("The list of sprites making the gif")] 
        private Sprite[] sprites;

        private const int FramesPerSecond = 10;
        
        private Image logo;
        private int index;

        public void InjectGifAnimation([GameObjectScope] Image logo)
        {
            this.logo = logo;
        }

        private void Awake()
        {
            InjectDependencies("InjectGifAnimation");
        }

        private void Update()
        {
            index = (int) (Time.time * FramesPerSecond) % sprites.Length;
            logo.sprite = sprites[index];
        }
    }
}
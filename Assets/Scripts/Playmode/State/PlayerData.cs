﻿using System;
using UnityEngine;

[CreateAssetMenu(menuName = "Game/Data/Player")]
[Serializable]
public class PlayerData : ScriptableObject
{
    [Range(0, 12), Tooltip("Movement speed in meters/s")] 
    public float MovementSpeed;
    
    [Range(0, 20), Tooltip("Rotation speed in Radians/s")]
    public float RotationSpeed;
}
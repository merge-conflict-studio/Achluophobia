﻿namespace Achluophobia
{
    public enum GameMode
    {
        Solo = 1,
        Coop = 2,
        Versus = 4
    }
}


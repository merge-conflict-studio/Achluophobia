﻿namespace Achluophobia
{
    public enum EndingType
    {
        SoloFinished,
        CoopFinished,
        VersusFinished,
        GameFailed
    }
}

﻿using UnityEngine;

[CreateAssetMenu(menuName = "Game/Data/MeleeWeaponData")]
public class MeleeWeaponData : ScriptableObject
{
    [SerializeField, Tooltip("The damage per hit of the melee weapon")]
    [Range(0, 200)] private int damage;

    public int Damage { get { return damage; } }
}

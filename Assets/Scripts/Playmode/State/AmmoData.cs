﻿using System;
using UnityEngine;

namespace Achluophobia
{
    [CreateAssetMenu(menuName = "Game/Data/Ammo")]
    [Serializable]
    public class AmmoData : ScriptableObject
    {
        [Range(1, 500), Tooltip("Max ammo the player can carry")] 
        public int MaxAmmoOnPlayer = 1;

        [Range(0, 50), Tooltip("Max ammo in a magazine. Use 0 for a non-reloading weapon")] 
        public int MagasineSize;

        [Tooltip("Check this if you want unlimited ammo")] 
        public bool IsUnlimited = false;
    }
}
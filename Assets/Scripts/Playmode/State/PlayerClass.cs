﻿namespace Achluophobia
{
    public enum PlayerClass
    {
        Assault,
        Medic,
        Scout,
        Mastodonte,
        Incinerator
    }
}
﻿using System;
using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public delegate void HealthChangedEventHandler(GameObject topParent, float oldHealthPoints, float newHealthPoints);
    public delegate void DeathEventHandler(GameObject killer);
    public delegate void ExplosionDeathEventHandler();
    public delegate void HealthStationHealEventHandler(int ammountOfHealthGiven);
    public delegate void HitEventHandler(GameObject attacker, int damage, bool isExplosionHit);

    [AddComponentMenu("Game/State/Health")]
    public class Health : GameScript
    {
        [SerializeField, Tooltip("ScriptableObject containing the health data")]
        private HealthData health;
        private GameObject lastAttacker;
        private int healthPoints;
        private bool isExplosionKill;

        public event HealthChangedEventHandler OnHealthChanged;
        public event DeathEventHandler OnDeath;
        public event HealthStationHealEventHandler OnHealthStationHealing;
        public event ExplosionDeathEventHandler OnExplosionDeath;
        public event HitEventHandler OnHit;

        public void Awake()
        {
            if (health == null) return;

            health.InitialHealth = health.InitialHealth > health.MaxHealth
                ? health.MaxHealth
                : health.InitialHealth;
            healthPoints = health.InitialHealth;
            isExplosionKill = false;
        }

        public int HealthPoints
        {
            get { return healthPoints; }
            private set
            {
                int oldHealthPoints = healthPoints;
                healthPoints = value < 0 ? 0 : (value > health.MaxHealth ? health.MaxHealth : value);

                if (OnHealthChanged != null) OnHealthChanged(gameObject, oldHealthPoints, healthPoints);

                if (healthPoints <= 0 && OnDeath != null)
                {
                    if (isExplosionKill && OnExplosionDeath != null) OnExplosionDeath();
                    OnDeath(lastAttacker);
                }
                isExplosionKill = false;
            }
        }

        public GameObject LastAttacker
        {
            get { return lastAttacker; }
        }

        /// <summary>
        /// Unused, but here for test reasons (See IHealth)
        /// </summary>
        /// <returns>The current health points.</returns>
        public int GetHealth()
        {
            return healthPoints;
        }

        /// <summary>
        /// Unused, but here for test reasons (See IHealth)
        /// </summary>
        /// <returns>The initial health points</returns>
        public int GetInitialHealth()
        {
            return health.InitialHealth;
        }

        public int GetMaxHealth()
        {
            return health.MaxHealth;
        }

        public void Hit(GameObject attacker, int hitPoints, bool isExplosionHit)
        {
            if (OnHit != null) OnHit(attacker, hitPoints, isExplosionHit);

            lastAttacker = attacker;
            isExplosionKill = isExplosionHit;
            if (hitPoints >= 0) HealthPoints -= hitPoints;
            else throw new ArgumentException("hitPoints in Health.Hit() can't be negative");
        }

        public void Heal(int healPoints, bool isHealthStationHeal)
        {
            if (healPoints >= 0) HealthPoints += healPoints;
            else throw new ArgumentException("healPoints in Health.Heal() can't be negative");
            if (isHealthStationHeal && OnHealthStationHealing != null) OnHealthStationHealing(healPoints);
        }

        public void Reset()
        {
            HealthPoints = health.MaxHealth;
        }
    }
}
﻿using System;
using System.Collections;
using UnityEngine;
using Harmony;

namespace Achluophobia
{
    public delegate void AmmoChangedEventHandler(int ammoInWeapon, int ammoOnPlayer, bool isAmmoUnlimited);
    public delegate void AmmoDepletedEventHandler();


    public class Ammo : GameScript
    {
        public static bool IsTestMode = false;

        [SerializeField, Tooltip("ScriptableObject containing the ammo data")]
        private AmmoData ammo;

        [SerializeField, Tooltip("The Sound for reloading")]
        private AudioClip reloadSound;

        private int ammoLeftInWeapon;
        private int ammoLeftOnPlayer;
        private bool isReloading;
        private Coroutine reload;
        private SoundPlayer soundPlayer;

        public event AmmoChangedEventHandler OnAmmoChanged;
        public event AmmoDepletedEventHandler OnAmmoDepleted;

        public void Awake() //Mise public pour les tests unitaires
        {
            if (!IsTestMode) InjectDependencies("InjectAmmo");
            Initialize();
        }

        private void InjectAmmo([ApplicationScope] SoundPlayer soundPlayer)
        {
            this.soundPlayer = soundPlayer;
        }

        private void Initialize()
        {
            if (ammo == null) return;
            if (ammo.MagasineSize == 0)
            {
                ammoLeftInWeapon = ammo.MaxAmmoOnPlayer;
            }
            else
            {
                ammoLeftInWeapon = ammo.MagasineSize;
                ammoLeftOnPlayer = ammo.MaxAmmoOnPlayer;
            }
        }

        public int GetAmmosLeftInWeapon()
        {
            return ammoLeftInWeapon;
        }

        public int GetAmmosLeftOnPlayer()
        {
            return ammoLeftOnPlayer;
        }

        public int GetMaxAmmo()
        {
            return ammo.MaxAmmoOnPlayer;
        }

        /// <summary>
        /// Reload the weapon and reduces the ammo
        /// on the player by the same amount. It returns
        /// a boolean indicating if it reloaded.
        /// </summary>
        /// <param name="reloadTime"></param>
        /// <returns></returns>
        public void Reload(float reloadTime)
        {
            int ammoAvailable;

            if (CanReload(out ammoAvailable))
            {
                reload = StartCoroutine(Reloading(reloadTime, ammoAvailable));
            }
        }

        /// <summary>
        /// Reload the weapon and reduces the ammo
        /// on the player by the same amount. It returns
        /// a boolean indicating if it reloaded for testing.
        /// </summary>
        public void ReloadMock(float reloadTime)
        {
            int ammoAvailable;

            if (CanReload(out ammoAvailable))
            {
                ReloadingMock(reloadTime, ammoAvailable);
            }
        }

        /// <summary>
        /// Reduces the ammo if necessary and returns a boolean indicating
        /// if there is ammo available.
        /// </summary>
        /// <returns></returns>
        public bool Deplete(float reloadTime)
        {
            if (isReloading) return false;

            if (NeedToReload())
            {
                if (IsTestMode) ReloadMock(reloadTime);
                Reload(reloadTime);
            }
            else if (ammoLeftInWeapon == 0)
            {
                return false;
            }
            else
            {
                --ammoLeftInWeapon;
                if (OnAmmoDepleted != null) OnAmmoDepleted();
                NotifyAmmoChanged();
                return true;
            }
            return false;
        }

        public void EmptyGun()
        {
            Reload(0);
        }

        /// <summary>
        /// Reduce the ammo when the burner is placed on the ground and return the ammount of
        /// ammos taken. If the ammount of wanted is higher than the number of ammos left, it returns
        /// only the amount of ammos left.
        /// </summary>
        /// <param name="fuelToRemove">The number of ammos we want to take</param>
        /// <returns></returns>
        public int DepleteForBurner(int fuelToRemove)
        {
            if (ammoLeftInWeapon < fuelToRemove)
            {
                fuelToRemove = ammoLeftInWeapon;
            }
            ammoLeftInWeapon = ammoLeftInWeapon - fuelToRemove;

            if (!IsCurrentWeaponAPistol())
            {
                NotifyAmmoChanged();
            }
            return fuelToRemove;
        }

        public void NotifyAmmoChanged()
        {
            if (OnAmmoChanged != null) OnAmmoChanged(ammoLeftInWeapon, ammoLeftOnPlayer, ammo.IsUnlimited);
        }

        public void StopReloading()
        {
            if (isReloading) StopCoroutine(reload);
            isReloading = false;
        }

        /// <summary>
        /// Add ammo to the ammo on the player.
        /// </summary>
        /// <param name="ammoQuantity"></param>
        public void PickUpAmmo(int ammoQuantity)
        {
            if (ammoQuantity <= 0)
            {
                throw new ArgumentException("Argument ammoQuantity must be 1 or greater");
            }

            if (ammo.MagasineSize == 0)
            {
                ammoLeftInWeapon = Math.Min(ammoLeftInWeapon + ammoQuantity, ammo.MaxAmmoOnPlayer);
            }
        
            else
            {
                ammoLeftOnPlayer = Math.Min(ammoLeftOnPlayer + ammoQuantity, ammo.MaxAmmoOnPlayer);
            }
            
            if (!IsCurrentWeaponAPistol())
            {
                NotifyAmmoChanged();
            }
        }
        
        /// <summary>
        /// Add ammo to the ammo on the player.
        /// </summary>
        /// <param name="ammoQuantity"></param>
        public void PickUpAmmoOnRespawn(int ammoQuantity, bool isPyroOnRespawn)
        {
            if (ammoQuantity <= 0)
            {
                throw new ArgumentException("Argument ammoQuantity must be 1 or greater");
            }

            if (isPyroOnRespawn)
            {
                ammoLeftInWeapon = ammoQuantity;
            }
            
            else
            {
                ammoLeftOnPlayer = ammoQuantity;
            }
            
            if (!IsCurrentWeaponAPistol())
            {
                NotifyAmmoChanged();
            }
        }

        private bool IsCurrentWeaponAPistol()
        {
            if (IsTestMode) return false;
            return GetComponentInParent<PlayerController>().GetCurrentWeapon().gameObject.CompareTag(R.S.Tag.Pistol);
        }
        /// <summary>
        /// Verify if it is a needed to reload.
        /// </summary>
        /// <returns>True if needed, false otherwise.</returns>
        private bool NeedToReload()
        {
            if (ammoLeftInWeapon > 0 || ammo.MagasineSize == 0)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Verify if it is possible to reload.
        /// </summary>
        /// <param name="ammoAvailable"></param>
        /// <returns> True if is reloadable, false otherwise.</returns>
        private bool CanReload(out int ammoAvailable)
        {
            if (!isReloading)
            {
                int neededAmmo = ammo.MagasineSize - ammoLeftInWeapon;
                ammoAvailable = Math.Min(neededAmmo, ammoLeftOnPlayer);

                return ammo.IsUnlimited || ammoAvailable > 0;
            }

            ammoAvailable = 0;
            return false;
        }

        private IEnumerator Reloading(float reloadTime, int neededAmmo)
        {
            if (reloadTime >= 0)
            {
                isReloading = true;

                soundPlayer.PlaySound(reloadSound);

                yield return new WaitForSeconds(reloadTime);

                if (!ammo.IsUnlimited)
                {
                    ammoLeftOnPlayer -= neededAmmo;
                }
                ammoLeftInWeapon += neededAmmo;

                isReloading = false;

                NotifyAmmoChanged();
            }
            else
            {
                throw new ArgumentException("Argument reloadTime must not be negative");
            }
        }

        private void ReloadingMock(float reloadTime, int neededAmmo)
        {
            if (reloadTime >= 0)
            {
                isReloading = true;

                if (!ammo.IsUnlimited)
                {
                    ammoLeftOnPlayer -= neededAmmo;
                }
                ammoLeftInWeapon += neededAmmo;

                isReloading = false;
            }
            else
            {
                throw new ArgumentException("Argument reloadTime must not be negative");
            }
        }
    }
}
﻿using System;
using UnityEngine;

[CreateAssetMenu(menuName = "Game/Data/ShootingWeapon")]
[Serializable]
public class ShootingWeaponData : ScriptableObject
{
    [Range(0.1f, 20f), Tooltip("Rate of fire in projectile/s")] 
    public float RateOfFire;
    
    [Range(0f, 10f), Tooltip("Reload time in seconds")] 
    public float ReloadTime;
    
    [Range(0, 40), Tooltip("Maximum effective range in meters")] 
    public float FiringRange;
    
    [Range(0, 100), Tooltip("Max damage the ShootingWeapon can do")] 
    public int MaxDamage;
}
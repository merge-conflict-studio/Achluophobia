﻿using System;
using UnityEngine;

[CreateAssetMenu(menuName = "Game/Data/Health")]
[Serializable]
public class HealthData : ScriptableObject
{
    [Range(0, 1000), Tooltip("Maximum health")] 
    public int MaxHealth;
    
    [Range(0, 1000), Tooltip("Starting health")] 
    public int InitialHealth;
}
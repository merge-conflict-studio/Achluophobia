﻿namespace Achluophobia
{
    public enum ObjectMaterial
    {
        Wood,
        Metal,
        Flesh
    }
}
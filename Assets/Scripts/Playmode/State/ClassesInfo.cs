﻿using Harmony;
using UnityEngine;

namespace Achluophobia
{
    public class ClassesInfo : Script
    {
        [SerializeField, Tooltip("ClassInfo of each classe in PlayerClass enum order")]
        private ClassInfo[] classes;
        
        public ClassInfo GetInfo(PlayerClass playerClass)
        {
            return classes[(int) playerClass];
        }
    }

}


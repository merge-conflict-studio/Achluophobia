﻿namespace Achluophobia
{
    public class AchievementName
    {
        public const string GameAchievement1 = "Lone Survivor";
        public const string GameAchievement2 = "Friendship Power";
        public const string GameAchievement3 = "I Swear I'm Not Competitive";
        public const string GameAchievement4 = "The Tortoise and the Hare";
        public const string GameAchievement5 = "I Swear They're not from Paper Mario";
        public const string GameAchievement6 = "Woops, Safety Was On";
        public const string AssaultAchievement1 = "Hunting the Hunter";
        public const string AssaultAchievement2 = "I've Got Your Back";
        public const string AssaultAchievement3 = "Terminator";
        public const string ScoutAchievement1 = "Who's the Fastest Now";
        public const string ScoutAchievement2 = "Follow the Light";
        public const string ScoutAchievement3 = "Forest Gump";
        public const string MastodonteAchievement1 = "Survival of the Fittest";
        public const string MastodonteAchievement2 = "The Final Solution";
        public const string MastodonteAchievement3 = "Tough Cookie";
        public const string MedicAchievement1 = "Never Made It To Hogwards";
        public const string MedicAchievement2 = "Heroes Never Die";
        public const string MedicAchievement3 = "It Doesn't Even Hurts";
        public const string IncineratorAchievement1 = "Fire in the Hole !";
        public const string IncineratorAchievement2 = "Not Afraid of the Dark";
    }
}

﻿using UnityEngine;

[CreateAssetMenu(menuName = "Game/Data/ProjectileData")]
public class ProjectileData : ScriptableObject
{
    [SerializeField, Tooltip("The damage per hit of the projectile")]
    [Range(0, 200)]
    private int damage;

    public int Damage { get { return damage; } }
}

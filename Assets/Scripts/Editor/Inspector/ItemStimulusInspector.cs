﻿using Harmony;
using UnityEditor;

namespace Achluophobia
{
    [CustomEditor(typeof(ItemStimulus), true)]
    public class ItemStimulusInspector : SensorInspector
    {
        protected override void OnDraw()
        {
            base.OnDraw();
            DrawSensorInspector(typeof(ItemStimulus), R.E.Layer.ItemSensor);
        }
    }
}
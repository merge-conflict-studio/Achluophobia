﻿using UnityEditor;

namespace Achluophobia
{
    [CustomEditor(typeof(GameScript), true)]
    public class GameScriptInspector : ScriptInspector
    {
    }
}
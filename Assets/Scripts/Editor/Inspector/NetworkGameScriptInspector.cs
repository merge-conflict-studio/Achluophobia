﻿using UnityEditor;

namespace Achluophobia
{
    [CustomEditor(typeof(NetworkGameScript), true)]
    public class NetworkGameScriptInspector : ScriptInspector
    {
    }
}
﻿using UnityEngine;

namespace Harmony
{
    /// <summary>
    /// Signature que tout méthode désirant être notifié du déclanchement d'un trigger 3D doit implémenter.
    /// </summary>
    public delegate void TriggerEventHandler(Collider other);

    /// <summary>
    /// Contient nombre de méthodes d'extensions pour les Renderers.
    /// </summary>
    public static class ColliderExtensions
    {
        public static ColliderEventsWrapper Events(this Collider collider)
        {
            ColliderEventsWrapper eventsWrapper = collider.gameObject.GetComponent<ColliderEventsWrapper>();
            if (eventsWrapper == null)
            {
                eventsWrapper = collider.gameObject.AddComponent<ColliderEventsWrapper>();
            }
            return eventsWrapper;
        }

        /// <summary>
        /// Wrapper événementiel pour les Collider.
        /// </summary>
        public class ColliderEventsWrapper : Script
        {
            /// <summary>
            /// Évènement se déclanchant lorsqu'un autre trigger entre en collision avec celui-ci.
            /// </summary>
            public event TriggerEventHandler OnEnterTrigger;

            /// <summary>
            /// Évènement se déclanchant lorsqu'un autre trigger n'est plus en collision avec celui-ci.
            /// </summary>
            public event TriggerEventHandler OnExitTrigger;

            private void OnTriggerEnter(Collider other)
            {
                if (OnEnterTrigger != null) OnEnterTrigger(other);
            }

            private void OnTriggerExit(Collider other)
            {
                if (OnExitTrigger != null) OnExitTrigger(other);
            }
        }
    }
}
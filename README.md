# Achluophobia

AUCUNE VENTE DE CE JEU N'EST PERMISE. CE JEU À ÉTÉ CRÉÉ À DES FINS ÉDUCATIVES. NOUS NE DÉTENONS PAS LES DROITS D'AUTEURS SUR LES IMAGES DES PERSONNAGES (MERCI UBISOFT)

[![Build Status](https://gitlab.com/merge-conflict-studio/Achluophobia/badges/master/pipeline.svg)](https://gitlab.com/merge-conflict-studio/Achluophobia/commits/master) [![Tests Report](https://img.shields.io/badge/test-report-brightgreen.svg)](https://merge-conflict-studio.io/Achluophobia/testreport/Index.html) [![Documentation](https://img.shields.io/badge/doc-report-brightgreen.svg)](https://merge-conflict-studio.gitlab.io/Achluophobia/doc/html/index.html)

Ce dépôt contient le projet Achluophobia. Pour y jouer, simplement lancer l'exécutable sur la machine de build ou se créer un build à partir de Unity.

## Contrôles

Une manette est nécessaire pour jouer à Achluophobia.

* Joystick gauche: Déplacer le joueur
* Joystick droit: Changer la direction du joueur
* Bouton 'A': Ramasser les items spéciaux
* Bouton 'B': Fermer et ouvrir la flashlight
* Bouton 'X': Recharger l'arme
* Bouton 'Y': Changer d'arme
* Bumper droit: Déposer un item spécial
* Gachette de droite: Tirer avec l'arme
* Gachette de gauche: Viser avec l'arme

## Technologies utilisées

* [Unity](https://unity3d.com/) - Le moteur de jeu utilisé.
* [NUnit](https://www.nunit.org/) - Framework de tests unitaires.
* [NSubstitute](http://nsubstitute.github.io/) - Framework de Mocks. La version incluse a été spécifiquement conçue pour Unity. Extraite des [UnityTestTools](https://www.assetstore.unity3d.com/en/#!/content/13802).
* [Inno Setup](http://www.jrsoftware.org/isinfo.php) - L'outil de création de fichier d'installation.
* Plusieurs nuanceurs personnalisés

## Auteurs

* Benjamin Lemelin - Programmation de l'outil d'injection de dépendances 'Harmony'
* Mathieu Carrier
* Philippe Deschenes
* Alexandre Lachance
* Jonathan Saindon
* Émile Turcotte

## License

Ce projet est autorisé sous licence MIT - voir le fichier [LICENSE.md] (LICENSE.md) pour plus de détails

## Remerciements

* [Jawnnypoo](https://github.com/Commit451/skyhook) - Pour son site [SkyHook](https://skyhook.glitch.me/), qui permet de convertir un WebHook de GitLab vers un WebHook pour Discord. C'est super pratique pour recevoir des messages sur le status actuel du projet.